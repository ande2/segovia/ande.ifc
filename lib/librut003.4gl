{
Programa : librut003.4gl 
Programo : Mynor Ramirez
Objetivo : Subruitinas para crear combobox 
}

DATABASE segovia 

DEFINE qrytext STRING 

{ Subrutina que carga el combobox de las empresas }

FUNCTION librut003_cbxempresas() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.codemp,a.nomemp ",
               " FROM glb_empresas a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codemp",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de las sucursales }

FUNCTION librut003_cbxsucursales(wcodemp) 
 DEFINE wcodemp LIKE glb_empresas.codemp 

 -- Llenando combobox 
 LET qrytext = "SELECT a.codsuc,a.nomsuc ",
               " FROM  glb_sucsxemp a ",
               " WHERE a.codemp = ",wcodemp,
               " ORDER BY 2 "

 CALL librut002_combobox("codsuc",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de las categorias }

FUNCTION librut003_cbxcategorias()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codcat,a.nomcat ",
               " FROM inv_categpro a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codcat",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de los colores }

FUNCTION librut003_cbxcolores()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codcol,a.nomcol ",
               " FROM inv_colorpro a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codcol",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de los colores disponibles x subcategoria de producto }

FUNCTION librut003_cbxcoloresxsubcat(wsubcat)
 DEFINE wsubcat LIKE inv_subcateg.subcat

 -- Llenando combobox
 LET qrytext = "SELECT UNIQUE a.codcol,a.nomcol ",
               " FROM inv_colorpro a,inv_products b ",
               " WHERE a.codcol = b.codcol ",
                 " AND b.subcat = ",wsubcat,
               " ORDER BY 2 "

 CALL librut002_combobox("codcol",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de los proveedores }

FUNCTION librut003_cbxproveedores()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codprv,a.nomprv ",
               " FROM inv_provedrs a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codprv",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de las undiades de medida }

FUNCTION librut003_cbxunidadesmedida(campoforma)
 DEFINE campoforma VARCHAR(15)

 -- Llenando combobox 
 LET qrytext = "SELECT a.unimed,a.nommed ",
               " FROM inv_unimedid a ",
               " ORDER BY 2 "

 CALL librut002_combobox(campoforma,qrytext)
END FUNCTION

{ Subrutina que carga el combobox de las medidas de producto }

FUNCTION librut003_cbxmedidas()
 -- Llenando combobox 
 LET qrytext = "SELECT a.codmed,a.codabr ",
               " FROM inv_medidpro a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codmed",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de las subcategorias }

FUNCTION librut003_cbxsubcategorias(wcodcat) 
 DEFINE wcodcat LIKE inv_subcateg.codcat 

 -- Llenando combobox 
 LET qrytext = "SELECT a.subcat,a.nomsub ",
               " FROM  inv_subcateg a ",
               " WHERE a.codcat = ",wcodcat,
               " ORDER BY 2 "

 CALL librut002_combobox("subcat",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de las subcategorias sin categoria }

FUNCTION librut003_cbxsubcateg() 
 DEFINE wcodcat LIKE inv_subcateg.codcat 

 -- Llenando combobox 
 LET qrytext = "SELECT a.subcat,a.nomsub ",
               " FROM  inv_subcateg a ",
               " WHERE a.lonmed = 1 ",
               " ORDER BY 2 "

 CALL librut002_combobox("subcat",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de los tipos de saldos }

FUNCTION librut003_cbxtipossaldo() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.tipsld,a.destip ",
               " FROM  inv_mtiposal a ",
               " WHERE haymov = 1 ",
               " ORDER BY 2 "

 CALL librut002_combobox("tipsld",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de productos }

FUNCTION librut003_cbxproductosabr() 
 -- Llenando combobox 
 LET qrytext = "SELECT a.cditem,a.codabr ",
               " FROM  inv_products a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codabr",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de los productos filtrados por categoria y subcategoria }

FUNCTION librut003_cbxproductosds(codcat,subcat)
DEFINE codcat , subcat INTEGER
 -- Llenando combobox
 LET qrytext = "SELECT a.cditem,a.dsitem ",
               " FROM  inv_products a ",
					" WHERE codcat = ",codcat,
					" AND   subcat = ",subcat,
               " ORDER BY 2 "

 CALL librut002_combobox("codabr",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de las bodegas x usuario }

FUNCTION librut003_cbxbodegasxusuario(wuserid) 
 DEFINE wuserid LIKE inv_permxbod.userid 

 -- Llenando combobox 
 LET qrytext = "SELECT a.codbod,a.nombod ",
               " FROM  inv_mbodegas a ",
               " WHERE EXISTS (SELECT b.userid FROM inv_permxbod b ",
                               "WHERE b.codbod = a.codbod ",
                                " AND b.userid = '",wuserid CLIPPED,"')",
               " ORDER BY 2 "

 CALL librut002_combobox("codbod",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de las bodegas }

FUNCTION librut003_cbxbodegas(whayfac)
 DEFINE whayfac SMALLINT,
        wstrfac STRING

 -- Verificando condicion de facturacion
 LET wstrfac = NULL
 CASE (whayfac)
  WHEN 0 LET wstrfac = "WHERE a.hayfac = 0"
  WHEN 1 LET wstrfac = "WHERE a.hayfac = 1"
  WHEN 2 LET wstrfac = NULL
 END CASE 

 -- Llenando combobox
 LET qrytext = "SELECT a.codbod,a.nombod ",
               " FROM  inv_mbodegas a ",
               wstrfac CLIPPED,
               " ORDER BY 2 "

 CALL librut002_combobox("codbod",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de los paises }

FUNCTION librut003_cbxpaises()
 -- Llenando combobox
 LET qrytext = "SELECT a.codpai,nompai ",
               " FROM  glb_mtpaises a ",
               " ORDER BY 2 "

 CALL librut002_combobox("codpai",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de los tipos de movimientos } 

FUNCTION librut003_cbxtipomovxusuario(wuserid) 
 DEFINE wuserid LIKE inv_permxtmv.userid 

 -- Llenando combobox 
 LET qrytext = "SELECT a.tipmov,a.nommov ",
               " FROM  inv_tipomovs a ",
               " WHERE EXISTS (SELECT b.userid FROM inv_permxtmv b ",
                               "WHERE b.tipmov = a.tipmov ",
                                " AND b.userid = '",wuserid CLIPPED,"')",
               " ORDER BY 2 "

 CALL librut002_combobox("tipmov",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de los empaques x producto }

FUNCTION librut003_cbxempaques(wcditem,filtro)
 DEFINE wcditem LIKE inv_epqsxpro.cditem,
        filtro  SMALLINT

 -- Llenando combobox
 IF filtro THEN 
    LET qrytext = "SELECT a.codepq,a.nomepq ",
                  " FROM  inv_epqsxpro a ",
                  " WHERE a.cditem = ",wcditem, 
                  " ORDER BY 2 "
 ELSE
    LET qrytext = "SELECT a.codepq,a.nomepq ",
                  " FROM  inv_epqsxpro a ",
                  " ORDER BY 2 "
 END IF 

 CALL librut002_combobox("codepq",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de los puntos de venta }

FUNCTION librut003_cbxpuntosventa()
 -- Llenando combobox 
 LET qrytext = "SELECT a.numpos,a.nompos ",
               " FROM fac_puntovta a ",
               " ORDER BY 2 "

 CALL librut002_combobox("numpos",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de las fases de una orden de trabajo }

FUNCTION librut003_cbxfasesot()
 -- Llenando combobox 
 LET qrytext = "SELECT a.tippar,a.valchr ",
               " FROM glb_paramtrs a ",
               " WHERE a.numpar = 100 ", 
               " ORDER BY 1 "

 -- Llenando combobox 
 CALL librut002_combobox("nofase",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de los cajeros, x puntos de venta, empresa y fecha de corte }

FUNCTION librut003_cbxcajeroscorte(wnumpos,wcodemp,ts)
 DEFINE wnumpos      LIKE fac_vicortes.numpos,
        wcodemp      LIKE fac_vicortes.codemp,
        numreg       SMALLINT,
        ts           SMALLINT,
        strfeccorte  STRING

 -- Condicion de corte
 IF ts THEN
    LET strfeccorte = " AND a.feccor IS NULL " 
 ELSE
    LET strfeccorte = " AND a.feccor IS NOT NULL " 
 END IF 

 -- Llenando combobox 
 LET qrytext = "SELECT UNIQUE a.userid,b.nomusr ",
               " FROM  fac_mtransac a,glb_usuarios b ",
               " WHERE a.numpos = ",wnumpos,
                 " AND a.codemp = ",wcodemp,
                 strfeccorte CLIPPED,
                 " AND b.userid = a.userid ",
               " ORDER BY 2 "

 LET numreg = librut002_cbxdin("cajero",qrytext)
 RETURN numreg
END FUNCTION

{ Subrutina que carga el combobox de los cajeros con corte de ventas }

FUNCTION librut003_cbxcajeros()
 DEFINE numreg SMALLINT 

 -- Llenando combobox
 LET qrytext = "SELECT UNIQUE a.userid,b.nomusr ",
               " FROM  fac_vicortes a,glb_usuarios b ",
               " WHERE b.userid = a.userid ",
               " ORDER BY 2 "

 LET numreg = librut002_cbxdin("cajero",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de los puntos de venta x cajero }

FUNCTION librut003_cbxpuntosventaxcajero(wuserid)
 DEFINE wuserid LIKE fac_usuaxpos.userid

 -- Llenando combobox 
 LET qrytext = "SELECT a.numpos,a.nompos ",
               " FROM fac_puntovta a,fac_usuaxpos b ",
               " WHERE a.numpos = b.numpos ",
                 " AND b.userid = '",wuserid CLIPPED,"'",
               " ORDER BY 2 "

 CALL librut002_combobox("numpos",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de los tipos de documento }

FUNCTION librut003_cbxtiposdocumento(wtipqry)
 DEFINE wtipqry SMALLINT,
        wwhere  STRING 
 
 -- Llenando combobox 
 LET qrytext = "SELECT a.tipdoc,a.nomdoc ",
               " FROM fac_tipodocs a ",
               " ORDER BY 2 "

 CALL librut002_combobox("tipdoc",qrytext)
END FUNCTION

{ Subrutina que carga el combobox de los tipos de documento por punto de venta }

FUNCTION librut003_cbxtiposdocxpos(wnumpos)
 DEFINE wnumpos LIKE fac_tdocxpos.numpos
 
 -- Llenando combobox 
 LET qrytext = "SELECT a.lnktdc,a.nomdoc ",
                "FROM  vis_tipdocxpos a ",
                "WHERE a.numpos = ",wnumpos,
                " ORDER BY 2"

 CALL librut002_combobox("lnktdc",qrytext)
END FUNCTION

{ Subrutina que llena el combobox de anios }

FUNCTION librut003_cbxanio(wannioc)
 DEFINE cba       ui.ComboBox,
        wannioc,i INT

 -- Asignando anio presente y el anterior
 LET cba = ui.ComboBox.forname("aniotr")
 CALL cba.clear()

 -- Verificando si anio tiene valor
 IF (wannioc=0) THEN
    LET wannioc = YEAR(CURRENT)-1
    FOR i=1 TO 2
     CALL cba.addItem(wannioc,wannioc)
     LET wannioc = (wannioc+1)
    END FOR
 ELSE
    CALL cba.addItem(wannioc,wannioc)
 END IF
END FUNCTION

{ Subrutina que carga el combobox de las fechas de inventario fisico }

FUNCTION librut003_cbxfecfisico(wcodemp,wcodsuc,wcodbod)
 DEFINE wcodemp LIKE inv_tofisico.codemp,
        wcodsuc LIKE inv_tofisico.codsuc,
        wcodbod LIKE inv_tofisico.codbod

 -- Llenando combobox
 LET qrytext = "SELECT UNIQUE a.fecinv,a.fecinv ",
               " FROM  inv_tofisico a ",
               " WHERE a.codemp = ",wcodemp,
                 " AND a.codsuc = ",wcodsuc,
                 " AND a.codbod = ",wcodbod,
               " ORDER BY 1 DESC"

 CALL librut002_combobox("fecinv",qrytext)
END FUNCTION

-- Subrutina para buscar los datos de un tipo de movimiento (inv_tipomovs) 

FUNCTION librut003_btipomovimiento(wtipmov)
 DEFINE w_tip_mov RECORD LIKE inv_tipomovs.*,
        wtipmov   LIKE inv_tipomovs.tipmov
	
 INITIALIZE w_tip_mov.* TO NULL 
 SELECT a.*
  INTO  w_tip_mov.*
  FROM  inv_tipomovs a
  WHERE (a.tipmov = wtipmov)
  IF (status=NOTFOUND) THEN
     RETURN w_tip_mov.*,FALSE
  ELSE
     RETURN w_tip_mov.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una empresa (glb_empresas) 

FUNCTION librut003_bempresa(wcodemp)
 DEFINE w_mae_emp RECORD LIKE glb_empresas.*,
        wcodemp   LIKE glb_empresas.codemp

 INITIALIZE w_mae_emp.* TO NULL 
 SELECT a.*
  INTO  w_mae_emp.*
  FROM  glb_empresas a
  WHERE (a.codemp = wcodemp)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_emp.*,FALSE
  ELSE
     RETURN w_mae_emp.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una sucursal (glb_sucsxemp) 

FUNCTION librut003_bsucursal(wcodsuc)
 DEFINE w_mae_suc RECORD LIKE glb_sucsxemp.*,
        wcodsuc   LIKE glb_sucsxemp.codsuc

 INITIALIZE w_mae_suc.* TO NULL 
 SELECT a.*
  INTO  w_mae_suc.*
  FROM  glb_sucsxemp a
  WHERE (a.codsuc = wcodsuc)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_suc.*,FALSE
  ELSE
     RETURN w_mae_suc.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una bodega (inv_mbodegas) 

FUNCTION librut003_bbodega(wcodbod)
 DEFINE w_mae_bod RECORD LIKE inv_mbodegas.*,
        wcodbod   LIKE inv_mbodegas.codbod

 INITIALIZE w_mae_bod.* TO NULL 
 SELECT a.*
  INTO  w_mae_bod.*
  FROM  inv_mbodegas a
  WHERE (a.codbod = wcodbod)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_bod.*,FALSE
  ELSE
     RETURN w_mae_bod.*,TRUE
  END IF

END FUNCTION

-- Subrutina para buscar los datos de un producto (inv_products) 

FUNCTION librut003_bproducto(wcditem)
 DEFINE w_inv_pro RECORD LIKE inv_products.*,
        wcditem   LIKE inv_products.cditem

 INITIALIZE w_inv_pro.* TO NULL 
 SELECT a.*
  INTO  w_inv_pro.*
  FROM  inv_products a
  WHERE (a.cditem = wcditem)
  IF (status=NOTFOUND) THEN
     RETURN w_inv_pro.*,FALSE
  ELSE
     RETURN w_inv_pro.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un producto x su codigo abreviado(inv_products) 

FUNCTION librut003_bordenabr(wdsitem)
 DEFINE w_inv_pro RECORD LIKE productos.*,
        wdsitem   LIKE productos.dsitem

 INITIALIZE w_inv_pro.* TO NULL 
 SELECT a.*
  INTO  w_inv_pro.*
  FROM  productos a
  WHERE (a.dsitem= wdsitem)
	 AND tipo > 0
  IF (status=NOTFOUND) THEN
     RETURN w_inv_pro.*,FALSE
  ELSE
     RETURN w_inv_pro.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un proveedor (inv_provedrs) 
FUNCTION librut003_bproductosabr(wcodabr)
 DEFINE w_inv_pro RECORD LIKE productos.*,
        wcodabr   LIKE inv_products.codabr
  
 INITIALIZE w_inv_pro.* TO NULL
 SELECT a.*
  INTO  w_inv_pro.*
  FROM  productos a
  WHERE (a.codabr= wcodabr)
    AND (a.tipo = 0)
  IF (status=NOTFOUND) THEN
     RETURN w_inv_pro.*,FALSE
  ELSE
     RETURN w_inv_pro.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un producto x su codigo abreviado(inv_products) 

FUNCTION librut003_bproductoabr(wcodabr)
 DEFINE w_inv_pro RECORD LIKE inv_products.*,
        wcodabr   LIKE inv_products.codabr

 INITIALIZE w_inv_pro.* TO NULL 
 SELECT a.*
  INTO  w_inv_pro.*
  FROM  inv_products a
  WHERE (a.codabr= wcodabr)
  IF (status=NOTFOUND) THEN
     RETURN w_inv_pro.*,FALSE
  ELSE
     RETURN w_inv_pro.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un proveedor (inv_provedrs) 

FUNCTION librut003_bproveedor(wcodprv)
 DEFINE w_mae_prv RECORD LIKE inv_provedrs.*,
        wcodprv   LIKE inv_provedrs.codprv

 INITIALIZE w_mae_prv.* TO NULL 
 SELECT a.*
  INTO  w_mae_prv.*
  FROM  inv_provedrs a
  WHERE (a.codprv= wcodprv)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_prv.*,FALSE
  ELSE
     RETURN w_mae_prv.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una unidad de medida (inv_unimedid) 

FUNCTION librut003_bumedida(wunimed)
 DEFINE w_mae_reg RECORD LIKE inv_unimedid.*,
        wunimed   LIKE inv_unimedid.unimed

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  inv_unimedid a
  WHERE (a.unimed= wunimed)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una medida de producto (inv_medidpro) 

FUNCTION librut003_bmedidapro(wcodmed)
 DEFINE w_mae_reg RECORD LIKE inv_medidpro.*,
        wcodmed   LIKE inv_medidpro.codmed

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  inv_medidpro a
  WHERE (a.codmed= wcodmed)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un cliente (fac_clientes) 

FUNCTION librut003_bcliente(wcodcli)
 DEFINE w_mae_cli RECORD LIKE fac_clientes.*,
        wcodcli   LIKE fac_clientes.codcli

 INITIALIZE w_mae_cli.* TO NULL 
 SELECT a.*
  INTO  w_mae_cli.*
  FROM  fac_clientes a
  WHERE (a.codcli= wcodcli)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_cli.*,FALSE
  ELSE
     RETURN w_mae_cli.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un movimiento (inv_mtransac) 

FUNCTION librut003_bmovimiento(wlnktra)
 DEFINE w_mae_tra RECORD LIKE inv_mtransac.*,
        wlnktra   LIKE inv_mtransac.lnktra

 INITIALIZE w_mae_tra.* TO NULL 
 SELECT a.*
  INTO  w_mae_tra.*
  FROM  inv_mtransac a
  WHERE (a.lnktra= wlnktra)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_tra.*,FALSE
  ELSE
     RETURN w_mae_tra.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un documento de facturacion (fac_mtransac)

FUNCTION librut003_bfacturacion(wlnktra)
 DEFINE w_mae_tra RECORD LIKE fac_mtransac.*,
        wlnktra   LIKE fac_mtransac.lnktra

 INITIALIZE w_mae_tra.* TO NULL
 SELECT a.*
  INTO  w_mae_tra.*
  FROM  fac_mtransac a
  WHERE (a.lnktra= wlnktra)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_tra.*,FALSE
  ELSE
     RETURN w_mae_tra.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un punto de venta (fac_puntovta) 

FUNCTION librut003_bpuntovta(wnumpos)
 DEFINE w_mae_reg RECORD LIKE fac_puntovta.*,
        wnumpos   LIKE fac_puntovta.numpos

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  fac_puntovta a
  WHERE (a.numpos= wnumpos)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un usuario (glb_usuarios) 

FUNCTION librut003_busuario(wuserid)
 DEFINE w_mae_reg RECORD LIKE glb_usuarios.*,
        wuserid   LIKE glb_usuarios.userid

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  glb_usuarios a
  WHERE (a.userid= wuserid)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un tipo de documento x punto de venta (fac_tdocxpos) 

FUNCTION librut003_btdocxpos(wlnktdc)
 DEFINE w_mae_reg RECORD LIKE fac_tdocxpos.*,
        wlnktdc   LIKE fac_tdocxpos.lnktdc

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  fac_tdocxpos a
  WHERE (a.lnktdc= wlnktdc)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un tipo de documento (fac_tipodocs) 

FUNCTION librut003_btipodoc(wtipdoc)
 DEFINE w_mae_reg RECORD LIKE fac_tipodocs.*,
        wtipdoc   LIKE fac_tipodocs.tipdoc

 INITIALIZE w_mae_reg.* TO NULL 
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  fac_tipodocs a
  WHERE (a.tipdoc= wtipdoc)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de una resolucion

FUNCTION librut003_bresolucion(wcodemp,wtipdoc,wnserie)
 DEFINE w_mae_reg RECORD LIKE fac_impresol.*,
        wcodemp   LIKE fac_impresol.codemp,
        wtipdoc   LIKE fac_impresol.tipdoc,
        wnserie   LIKE fac_impresol.nserie

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  fac_impresol a
  WHERE (a.codemp = wcodemp)
    AND (a.tipdoc = wtipdoc)
    AND (a.nserie = wnserie)
    AND (a.estado = 1)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para buscar los datos de un empaque x producto

FUNCTION librut003_bempaquexpro(wcditem,wcodepq)            
 DEFINE w_mae_reg RECORD LIKE inv_epqsxpro.*,
        wcditem   LIKE inv_epqsxpro.cditem,
        wcodepq   LIKE inv_epqsxpro.codepq

 INITIALIZE w_mae_reg.* TO NULL
 SELECT a.*
  INTO  w_mae_reg.*
  FROM  inv_epqsxpro a
  WHERE (a.cditem = wcditem)
    AND (a.codepq = wcodepq)
  IF (status=NOTFOUND) THEN
     RETURN w_mae_reg.*,FALSE
  ELSE
     RETURN w_mae_reg.*,TRUE
  END IF
END FUNCTION

-- Subrutina para obtener parametros del sistema

FUNCTION librut003_parametros(wnumpar,wtippar)
 DEFINE wnumpar LIKE glb_paramtrs.numpar,
        wtippar LIKE glb_paramtrs.tippar,
        wvalchr LIKE glb_paramtrs.valchr

 -- Seleccionando parametros
 INITIALIZE wvalchr TO NULL
 SELECT a.valchr
  INTO  wvalchr
  FROM  glb_paramtrs a
  WHERE (a.numpar = wnumpar)
    AND (a.tippar = wtippar)
  IF (status!=NOTFOUND) THEN
     RETURN TRUE,wvalchr
  ELSE
     RETURN FALSE,wvalchr
  END IF
END FUNCTION

-- Subrutina para obtener el correlativo maximo de un documento por empresa serie y tipo documento de venta 

FUNCTION librut003_correltipdoc(wr)
 DEFINE wr RECORD
         codemp LIKE fac_mtransac.codemp,
         tipdoc LIKE fac_mtransac.tipdoc, 
         nserie LIKE fac_mtransac.nserie,
         correl INTEGER, 
         conteo SMALLINT 
        END RECORD

 -- Capturando errores
 WHENEVER ERROR CONTINUE
 WHILE TRUE
  -- Seleccionando el numero maximo
  SELECT MAX(i.numdoc)
   INTO  wr.correl
   FROM  fac_mtransac i
   WHERE (i.codemp = wr.codemp)
     AND (i.tipdoc = wr.tipdoc)
     AND (i.nserie = wr.nserie)
     AND (i.numdoc IS NOT NULL)
   IF wr.correl IS NULL THEN
      LET wr.correl = 1
   ELSE
      LET wr.correl = (wr.correl+1)
   END IF

  -- Verificando que correlativo maximo no exista
  SELECT COUNT(*)
   INTO  wr.conteo
   FROM  fac_mtransac
   WHERE (i.codemp = wr.codemp)
     AND (i.tipdoc = wr.tipdoc)
     AND (i.nserie = wr.nserie)
     AND (i.numdoc = wr.correl)
   IF (wr.conteo>0) THEN
      CONTINUE WHILE
   END IF
   EXIT WHILE
 END WHILE

 -- Retornando captura de errores
 WHENEVER ERROR STOP

 -- Retornando correlativo
 RETURN wr.correl
END FUNCTION

-- Subrutina para obtener los saldos de un producto por empresa, sucursal, bodega, tipo de saldo, anio, mes y producto

FUNCTION librut003_saldosxproxmes(wcodemp,wcodsuc,wcodbod,wtipsld,waniotr,wmestra,wcditem)
 DEFINE wcodemp LIKE inv_saldopro.codemp, 
        wcodsuc LIKE inv_saldopro.codsuc, 
        wcodbod LIKE inv_saldopro.codbod, 
        wtipsld LIKE inv_saldopro.tipsld, 
        waniotr LIKE inv_saldopro.aniotr, 
        wmestra LIKE inv_saldopro.mestra, 
        wcditem LIKE inv_saldopro.cditem, 
        wsaluni LIKE inv_saldopro.canuni,
        wsalval LIKE inv_saldopro.totpro 

 -- Obteniendo saldos
 SELECT NVL(SUM(a.canuni),0),NVL(SUM(a.totpro),0)
  INTO  wsaluni,wsalval 
  FROM  inv_saldopro a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.tipsld = wtipsld 
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem 

 IF wsaluni IS NULL THEN LET wsaluni = 0 END IF 
 IF wsalval IS NULL THEN LET wsalval = 0 END IF 

 RETURN wsaluni,wsalval
END FUNCTION 

-- Subrutina para obtener los movimientos de un producto por empresa, sucursal, bodega, anio, mes y producto

FUNCTION librut003_saldosxmovxmes(wcodemp,wcodsuc,wcodbod,waniotr,wmestra,wcditem,wtipope)
 DEFINE wcodemp LIKE inv_dtransac.codemp, 
        wcodsuc LIKE inv_dtransac.codsuc, 
        wcodbod LIKE inv_dtransac.codbod, 
        waniotr LIKE inv_dtransac.aniotr, 
        wmestra LIKE inv_dtransac.mestra, 
        wcditem LIKE inv_dtransac.cditem, 
        wtipope LIKE inv_dtransac.tipope, 
        wsaluni LIKE inv_dtransac.canuni,
        wsalval LIKE inv_dtransac.totpro 

 -- Obteniendo saldos
 SELECT a.saluni,a.salval
  INTO  wsaluni,wsalval 
  FROM  vis_saldosxmovxmes a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem 
    AND a.tipope = wtipope

 IF wsaluni IS NULL THEN LET wsaluni = 0 END IF 
 IF wsalval IS NULL THEN LET wsalval = 0 END IF 

 RETURN wsaluni,wsalval
END FUNCTION 

-- Subrutina para obtener el saldo fisico de un producto

FUNCTION librut003_fisicoxproxmes(wcodemp,wcodsuc,wcodbod,waniotr,wmestra,wcditem)
 DEFINE wcodemp LIKE inv_tofisico.codemp, 
        wcodsuc LIKE inv_tofisico.codsuc, 
        wcodbod LIKE inv_tofisico.codbod, 
        waniotr LIKE inv_tofisico.aniotr, 
        wmestra LIKE inv_tofisico.mestra, 
        wcditem LIKE inv_tofisico.cditem, 
        wfisuni LIKE inv_tofisico.canuni,
        wfisval LIKE inv_tofisico.totpro,
        wfecinv DATE 
 
 -- Obteniendo saldos
 SELECT NVL(SUM(a.canuni),0),NVL(SUM(a.totpro),0)
  INTO  wfisuni,wfisval 
  FROM  inv_tofisico a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.aniotr = waniotr
    AND a.mestra = wmestra
    AND a.cditem = wcditem 
    AND a.finmes = 1

 IF wfisuni IS NULL THEN LET wfisuni = 0 END IF 
 IF wfisval IS NULL THEN LET wfisval = 0 END IF 

 RETURN wfisuni,wfisval
END FUNCTION 

-- Subrutina para chequear la existencia de un producto

FUNCTION librut003_chkexistencia(wcodemp,wcodsuc,wcodbod,wcditem)
 DEFINE wcodemp LIKE inv_proenbod.codemp, 
        wcodsuc LIKE inv_proenbod.codsuc, 
        wcodbod LIKE inv_proenbod.codbod, 
        wcditem LIKE inv_proenbod.cditem, 
        wexican LIKE inv_proenbod.exican 

 -- Obteniendo existencia
 SELECT NVL(a.exican,0)
  INTO  wexican
  FROM  inv_proenbod a
  WHERE a.codemp = wcodemp
    AND a.codsuc = wcodsuc
    AND a.codbod = wcodbod
    AND a.cditem = wcditem 

 IF (wexican IS NULL) THEN LET wexican = 0 END IF

 RETURN wexican 
END FUNCTION 

-- Subrutina para buscar el saldo anterior de un producto x empresa, sucursal, bodega y fecha

FUNCTION librut003_saldoantxpro(wcodemp,wcodsuc,wcodbod,wcditem,wfecfin)
 DEFINE wcodemp LIKE inv_proenbod.codemp, 
        wcodsuc LIKE inv_proenbod.codsuc, 
        wcodbod LIKE inv_proenbod.codbod, 
        wcditem LIKE inv_proenbod.cditem, 
        wsalcan LIKE inv_proenbod.exican,
        wsalval LIKE inv_proenbod.exival,
        winican LIKE inv_proenbod.exican,
        winival LIKE inv_proenbod.exival,
        wmovcan LIKE inv_proenbod.exican,
        wmovval LIKE inv_proenbod.exival,
        wtipsin SMALLINT,
        existe  SMALLINT,
        waniotr SMALLINT, 
        wmestra SMALLINT, 
        waniant SMALLINT, 
        wmesant SMALLINT, 
        wfecini DATE,
        wfecfin DATE 

 -- Obteniendo anio y mes
 LET waniotr = YEAR(wfecfin) 
 LET wmestra = MONTH(wfecfin) 
 LET wfecini = MDY(wmestra,1,waniotr) 

 -- Obteniendo el tipo de saldo INICIAL
 CALL librut003_parametros(3,2)
 RETURNING existe,wtipsin

 -- Obteniendo saldo inicial del mes
 CALL librut003_saldosxproxmes(wcodemp,
                               wcodsuc,
                               wcodbod,
                               wtipsin,
                               waniotr, 
                               wmestra, 
                               wcditem)
 RETURNING winican,winival

 -- Si no hay saldo inicial del mes verifica fisico del mes pasado
 IF ((winican+winival)=0) THEN
    -- Verificando mes
    IF (wmestra=1) THEN
       LET waniant = (waniotr-1)
       LET wmesant = 12
    ELSE
       LET waniant = waniotr
       LET wmesant = (wmestra-1)
    END IF

    -- Obteniendo fisico del mes pasado
    CALL librut003_fisicoxproxmes(wcodemp,
                                  wcodsuc,
                                  wcodbod,
                                  waniant,
                                  wmesant,
                                  wcditem)
    RETURNING winican,winival 
 END IF

 IF (wsalcan IS NULL) THEN LET wsalcan = 0 END IF
 IF (wsalval IS NULL) THEN LET wsalval = 0 END IF
 
 -- Calculando saldo de movimientos antes de la fecha inicial
 SELECT NVL(SUM(a.opeuni),0),
        NVL(SUM(a.opeval),0)
  INTO  wmovcan,
        wmovval
  FROM  inv_dtransac a
  WHERE a.codemp  = wcodemp
    AND a.codsuc  = wcodsuc
    AND a.codbod  = wcodbod
    AND a.cditem  = wcditem
    AND a.fecemi >= wfecini
    AND a.fecemi <= wfecfin
    AND a.estado  = "V";

 IF (wmovcan IS NULL) THEN LET wmovcan = 0 END IF
 IF (wmovval IS NULL) THEN LET wmovval = 0 END IF

 -- Calculando saldo
 LET wsalcan = (winican+wmovcan) 
 LET wsalval = (winival+wmovval) 

 RETURN wsalcan,wsalval 
END FUNCTION 

-- Subrutina para obtener la cantidad equivalente de un empaque

FUNCTION librut003_canempaque(wcditem,wcodepq)
 DEFINE wcanepq  LIKE inv_epqsxpro.cantid,
        wcditem  LIKE inv_epqsxpro.cditem,
        wcodepq  LIKE inv_epqsxpro.codepq

 -- Obteniendo cantidad de empaque
 SELECT NVL(a.cantid,1)
  INTO  wcanepq
  FROM  inv_epqsxpro a
  WHERE a.cditem = wcditem
    AND a.codepq = wcodepq
  IF wcanepq IS NULL THEN
     LET wcanepq = 1
  END IF

 RETURN wcanepq
END FUNCTION

FUNCTION librut003_print_cmd(nfile,nprin,npath)
DEFINE nfile   VARCHAR(20) ,
       nprin   VARCHAR(50) ,
       npath   VARCHAR(100),
		 runcm   STRING,
       ii      STRING,
       errfl   INTEGER


   --Convierte el archivo a formato DOS
   CALL FGL_SYSTEM("unix2dos "||nfile CLIPPED)
   --Traslada el archivo a la PC del usuario
   CALL FGL_PUTFILE(npath,"C:\\\\tmp\\"||nfile CLIPPED)
   --Arma el comando de impresi¢n
	DISPLAY "print ",nprin
	--SLEEP 10
   LET runcm ='cmd /C \"print /d:\\\\'||nprin CLIPPED||' C:\\tmp\\'||nfile CLIPPED||' '
   DISPLAY "cmd ",runcm 
   SLEEP 4
   --Manda a imprimir el reporte a la impresora compartida de
   --cualquier usuario de la red
   CALL ui.Interface.frontCall("standard","shellexec",[runcm],[ii])
   

	IF (errfl = -1 ) THEN
   	ERROR "NO HAY REGISTROS CON ESTOS DATOS!"
	END IF

END FUNCTION

FUNCTION librut003_print_cmd_w7(nfile,npc,nprin,npath)
DEFINE nfile   VARCHAR(20) ,
       npc     VARCHAR (50) ,
       nprin   VARCHAR(50) ,
       npath   VARCHAR(100),
		 runcm   STRING,
       ii      STRING,
       errfl   INTEGER


   --Convierte el archivo a formato DOS
   CALL FGL_SYSTEM("unix2dos "||nfile CLIPPED)
   --Traslada el archivo a la PC del usuario
   CALL FGL_PUTFILE(npath,"C:\\\\tmp\\"||nfile CLIPPED)
   --Arma el comando de impresi¢n
	DISPLAY "print ",nprin
	--SLEEP 10
   --LET runcm ='cmd /C \"lpr -S '||npc CLIPPED||" -P "||nprin CLIPPED||' C:\\tmp\\'||nfile CLIPPED||'"'
   LET runcm ='cmd /C lpr -S '||npc CLIPPED||" -P "||nprin CLIPPED||' C:\\tmp\\'||nfile CLIPPED --||'"'
   DISPLAY "runcm ",runcm 
   --lpr -S lonas-caja -P epson890 c:\tmp\facrpt001.txt
   SLEEP 4
   --Manda a imprimir el reporte a la impresora compartida de
   --cualquier usuario de la red
   CALL ui.Interface.frontCall("standard","shellexec",[runcm],[ii])
   

	IF (errfl = -1 ) THEN
   	ERROR "NO HAY REGISTROS CON ESTOS DATOS!"
	END IF

END FUNCTION


#########################################################################
## Function  : combo_din2
##
## Parameters: inputParameters
##
## Returnings: returnParameters
##
## Comments  : Funcion que genera un combo dinamico
#########################################################################
function combo_din2(vl_2_campo, vl_2_query)
define		vl_2_campo		  string
define		vl_2_query		  string
define		vl_2_lista		  char(500)
define		vl_2_lista2	  char(500)
define		vl_2_metodo	  string
define		vl_2_valor		  string
define		vl_2_valor2	  string
define		vl_2_combo      ui.ComboBox
define		vl_2_lista_nodb base.StringTokenizer

	let vl_2_campo = vl_2_campo.trim()
	let vl_2_query = vl_2_query.trim()

	#-- Obtiene el nodo del metodo Combo de la clase ui
	let vl_2_combo = ui.ComboBox.forName(vl_2_campo)
	if vl_2_combo is null
	then
		return
	end if

	#-- Limpia los registros anteriores del combo
	call vl_2_combo.clear()

	#-- Procesa segun el metodo convirtiendo a mayusculas para mejor tratamiento
	let vl_2_metodo = vl_2_query.toUpperCase()

	if vl_2_metodo matches "SELECT*"   then	
		#--Prepara el query
		prepare q_comboList2 from vl_2_query
		declare c_comboList2 cursor for q_comboList2

		#-- Obtiene la lista de valores de la tabla segun el query enviado
		foreach c_comboList2 into vl_2_lista, vl_2_lista2
			let vl_2_valor = vl_2_lista clipped
			let vl_2_valor2 = vl_2_lista2 clipped
			call vl_2_combo.addItem(vl_2_valor, vl_2_valor2)
		end foreach	
	else
	   #-- Obtiene la lista de valores que no son de base de datos
		let vl_2_lista_nodb = base.StringTokenizer.create(vl_2_query, "|")
		while vl_2_lista_nodb.hasMoreTokens()
         let vl_2_valor = vl_2_lista_nodb.nextToken()
         call vl_2_combo.addItem(vl_2_valor, vl_2_valor)
		end while
	end if
end function

