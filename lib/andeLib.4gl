

FUNCTION gral_reporte(tamanio, posicion, tipo, columnas, nom_reporte)
DEFINE tamanio     STRING
DEFINE posicion    STRING
DEFINE tipo        STRING
DEFINE ancho       STRING
DEFINE alto        STRING
DEFINE columnas    SMALLINT
DEFINE nom_reporte STRING
DEFINE myReporte   om.SaxDocumentHandler

        IF tamanio = "carta" OR tamanio = "CARTA" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "11in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "11in"
           END IF
        END IF

        IF tamanio = "oficio" OR tamanio = "OFICIO" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "13in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "13in"
           END IF       
        END IF

        {IF tamanio = "unapagina" THEN
           LET ancho = "10000"
           LET alto  = "10000"  
        END IF}

        IF NOT fgl_report_loadCurrentSettings(NULL) THEN
            RETURN
         END IF
         DISPLAY "ancho y alto ", ancho, alto
         CALL fgl_report_configurePageSize(ancho,alto)
         CALL fgl_report_configureCompatibilityOutput("124","Monospaced",false,null,"","")
         CALL fgl_report_setTitle ("TITULO DEL REPORTE")
         --CALL fgl_report_configureCompatibilityOutput(columnas,"Monospaced",false,nom_reporte,"","")
         --CALL fgl_report_configureMultipageOutput(0, 0, FALSE)
         CALL fgl_report_selectDevice(tipo)
         IF tamanio = "unapagina" THEN
           CALL fgl_report_configureXLSDevice(1,1,1,1,1,1,0)  
        END IF 
         LET myReporte = fgl_report_commitCurrentSettings()
RETURN myReporte
END FUNCTION

#########################################################################
## Function  : getPeso()
##
## Parameters: tabValida    << Tabla donde va a validar
##             colValida    << Columna que va a ir a validar
##             valKey       << Variable local que tiene la llave que va a validar
##
## Returnings: True / False
##
## Comments  : Recibe como parámetro el nombre del parametro global y
##             retorna el valor del parámetro
##             El nombre del parametro se ingresa en el catalogo general
##             de parámetros glbm0003.42r
#########################################################################

FUNCTION gral_reporte_all(tamanio, posicion, tipo, columnas, nom_reporte)
DEFINE tamanio     STRING
DEFINE posicion    STRING
DEFINE tipo        STRING
DEFINE ancho       STRING
DEFINE alto        STRING
DEFINE columnas    SMALLINT
DEFINE nom_reporte STRING
DEFINE myReporte   om.SaxDocumentHandler

        IF tamanio = "carta" OR tamanio = "CARTA" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "11in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "11in"
           END IF
        END IF

        IF tamanio = "oficio" OR tamanio = "OFICIO" THEN
           IF posicion = "horizontal" OR posicion = "HORIZONTAL" THEN
              LET ancho = "13in"
              LET alto  = "8.5in"
           ELSE
              LET ancho = "8.5in"
              LET alto  = "13in"
           END IF       
        END IF
        
        IF NOT fgl_report_loadCurrentSettings(NULL) THEN
            RETURN
         END IF
         --CALL fgl_report_configurePageSize(ancho,alto)
         CALL fgl_report_configureCompatibilityOutput(columnas,"Monospaced",false,nom_reporte,"","")
         --CALL fgl_report_configureMultipageOutput(0, 0, FALSE)
         CALL fgl_report_selectDevice(tipo)
         CALL fgl_report_configureXLSDevice(NULL,NULL,NULL,NULL,TRUE ,NULL,TRUE)
         LET myReporte = fgl_report_commitCurrentSettings()
RETURN myReporte
END FUNCTION

FUNCTION round(dval,dpos)

DEFINE 
  dval DECIMAL ,
  dpos SMALLINT ,
  ival DECIMAL (16,0)
  
LET ival = dval * (10 ** dpos)
LET dval = ival / (10 ** dpos)
RETURN dval

END FUNCTION 

#------------------------------------------------------
# Funcion:  numsem
# Recibe una fecha y devuelve el numero de la semana
#
# Let lNumSem = numSem(lfecha)
#
#------------------------------------------------------
FUNCTION numsem(fec1)
  DEFINE fec1, fec2 DATE 

  DEFINE numday SMALLINT 
  DEFINE numwday TINYINT 
  DEFINE numsem SMALLINT 

  LET fec2 = MDY(1,1,YEAR(TODAY))
  
  LET numday = (fec1 - fec2) + 1

  LET numwday = WEEKDAY (TODAY)

  LET numsem = (numday - numwday + 10) / 7

  RETURN numsem
  
END FUNCTION