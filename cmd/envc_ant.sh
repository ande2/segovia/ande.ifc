. /opt/informix/ifmx.sh
. /opt/4js/g230/dev/envcomp
. /app/scapp.dir/ifc/cmd/enva_sc.sh

NUM_PRO=`ps -c | grep bash`
export NUM_PRO

REMOTEHOST=`who -m | cut -d '(' -f2 | cut -d ')' -f1 | cut -d ':' -f1`
export REMOTEHOST

REMOTEPC=`cat /etc/hosts | grep $REMOTEHOST | cut -d ' ' -f6`
export REMOTEPC

FGLSERVER=$REMOTEHOST
export FGLSERVER
