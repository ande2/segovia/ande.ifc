# @(#) enva.sh 
# @(#) Mynor Ramirez
# @(#) Diciembre 2010 
# @(#) Shell utilizado para cargar las variables de aplicacion 

# Asignando valores
export TERM=vt100
export BASEDIR=/sistemas/apl/ifc
export FGLDBPATH=$BASEDIR/sch 
export DBPATH=$BASEDIR/lib
export FGLLDPATH=$BASEDIR/lib
export SPOOLDIR=$HOME/spl
export PATH=$BASEDIR/cmd:$PATH
