{
Fecha    : Diciembre 2010  
Programo : Mynor Ramirez
Objetivo : Reporte comparativo de inventario 
}

DATABASE segovia

-- Definicion de variables globales }

DEFINE w_mae_emp    RECORD LIKE glb_empresas.*,
       w_mae_suc    RECORD LIKE glb_sucsxemp.*,
       w_mae_bod    RECORD LIKE inv_mbodegas.*,
       w_datos      RECORD 
        codbod      LIKE inv_mtransac.codbod,
        codemp      LIKE inv_mtransac.codemp,
        codsuc      LIKE inv_mtransac.codbod,
        aniotr      LIKE inv_mtransac.aniotr, 
        mestra      LIKE inv_mtransac.mestra, 
        codcat      LIKE inv_categpro.codcat,
        subcat      LIKE inv_subcateg.subcat
       END RECORD, 
       v_inventario DYNAMIC ARRAY OF RECORD
        cditem      LIKE inv_products.cditem,  
        codabr      LIKE inv_products.codabr,  
        dsitem      LIKE inv_products.dsitem,
        iniuni      DEC(16,2),
        entuni      DEC(16,2),
        saluni      DEC(16,2),
        slduni      DEC(16,2),
        inival      DEC(16,2),
        entval      DEC(16,2),
        salval      DEC(16,2),
        sldval      DEC(16,2),
        cospro      DEC(14,6),
        fisuni      DEC(16,2),
        fisval      DEC(16,2),
        difuni      DEC(16,2),
        difval      DEC(16,2)
       END RECORD,
       w_total      RECORD
        tiniuni     DEC(16,2),
        tentuni     DEC(16,2),
        tsaluni     DEC(16,2),
        tslduni     DEC(16,2),
        tfisuni     DEC(16,2),
        tdifuni     DEC(16,2),
        tinival     DEC(16,2),
        tentval     DEC(16,2),
        tsalval     DEC(16,2),
        tsldval     DEC(16,2),
        tfisval     DEC(16,2),
        tdifval     DEC(16,2)
       END RECORD, 
       existe       SMALLINT,
       cntprod      INTEGER  

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar4")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invrep001.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep001_comparativo()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep001_comparativo()
 DEFINE qrytxt    CHAR(80),
        wpais     VARCHAR(255),
        loop      SMALLINT

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep001a AT 5,2  
  WITH FORM "invrep001a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invrep001",wpais,1)

  -- Llenando combobox
  -- Anios
  CALL librut003_cbxanio(0)

  LET loop = TRUE
  WHILE loop
   -- Inicializando datos
   CALL invrep001_inival()

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codbod,
                 w_datos.aniotr,
                 w_datos.mestra, 
                 w_datos.codcat,
                 w_datos.subcat 
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)
 
    ON ACTION salir
     -- Salida
     CALL invrep001_inival()  
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Limpiando datos  
     CALL v_inventario.clear()

     -- Desplegnado datos
     CALL invrep001_visualizar()
     NEXT FIELD codbod

    ON CHANGE codbod 
     -- Obteniendo datos de la bodega 
     CALL librut003_bbodega(w_datos.codbod) 
     RETURNING w_mae_bod.*,existe 

     -- Asignando datos de empresa y sucursal de la bodega
     LET w_datos.codemp = w_mae_bod.codemp 
     LET w_datos.codsuc = w_mae_bod.codsuc 

     -- Obteniendo datos de la empresa 
     CALL librut003_bempresa(w_mae_bod.codemp)
     RETURNING w_mae_emp.*,existe 
     -- Obteniendo datos de la sucursal 
     CALL librut003_bsucursal(w_mae_bod.codsuc)
     RETURNING w_mae_suc.*,existe 
   
     -- Desplegando datos de la empresa y sucursal 
     DISPLAY BY NAME w_datos.codemp,w_datos.codsuc,w_mae_emp.nomemp,w_mae_suc.nomsuc

     -- Limpiando combos de categorias y subcategorias
     LET w_datos.codcat = NULL
     LET w_datos.subcat = NULL
     CLEAR codcat,subcat

     -- Limpiando datos  
     CALL v_inventario.clear()
     CALL invrep001_limpiadetalle(50)

    ON CHANGE aniotr
     -- Limpiando datos  
     CALL v_inventario.clear()
     CALL invrep001_limpiadetalle(50)
 
    ON CHANGE mestra
     -- Limpiando datos
     CALL v_inventario.clear()
     CALL invrep001_limpiadetalle(50)

    ON CHANGE codcat
     -- Limpiando combos           
     LET w_datos.subcat = NULL
     CLEAR subcat

     -- Llenando combox de subcategorias
     CALL librut003_cbxsubcategorias(w_datos.codcat)   

     -- Limpiando datos                       
     CALL v_inventario.clear()
     CALL invrep001_limpiadetalle(50)

    ON CHANGE subcat 
     -- Limpiando datos
     CALL v_inventario.clear()
     CALL invrep001_limpiadetalle(50)

    AFTER FIELD codbod 
     IF w_datos.codbod IS NULL THEN
        ERROR "Error: debe de seleccionarse la bodega a listar." 
        NEXT FIELD codbod
     END IF

    AFTER FIELD aniotr 
     IF w_datos.aniotr IS NULL THEN
        ERROR "Error: debe de seleccionarse el anio a listar." 
        NEXT FIELD aniotr
     END IF

    AFTER FIELD mestra 
     IF w_datos.mestra IS NULL THEN
        ERROR "Error: debe de seleccionarse el mes a listar." 
        NEXT FIELD mestra 
     END IF

   END INPUT
   IF NOT loop THEN
     EXIT WHILE
   END IF
  END WHILE

 CLOSE WINDOW wrep001a 
END FUNCTION

-- Subrutina para visualizar el reporte comparativo 

FUNCTION invrep001_visualizar()
 DEFINE w_mae_prg  RECORD LIKE glb_programs.*

 -- Generando el reporte
 CALL invrep001_datos()

 -- Verificando si hay datos
 IF (cntprod=0) THEN
    CALL fgl_winmessage(
    "Atencion",
    "No existen datos con los filtros seleccionados.",
    "information")
    RETURN
 END IF

 -- Desplegando datos
 DISPLAY ARRAY v_inventario TO l_inventario.*
  ATTRIBUTES (COUNT=cntprod,ACCEPT=FALSE,CANCEL=FALSE)

  ON ACTION filtros
   EXIT DISPLAY

 END DISPLAY
END FUNCTION

-- Subrutina para generar los datos del reporte comparativo

FUNCTION invrep001_datos()
 DEFINE wtipsin          SMALLINT, 
        wcodcat          SMALLINT,
        wsubcat          SMALLINT,
        mesant           SMALLINT,
        aniant           SMALLINT,
        wnomcat          CHAR(50),
        wnomsub          CHAR(50),
        strcodcat        STRING,
        strsubcat        STRING,
        strqry           STRING,
        wfecini,wfecfin  DATE

 -- Verificando nulos
 IF (w_datos.codbod IS NULL) OR   
    (w_datos.aniotr IS NULL) OR
    (w_datos.mestra IS NULL) THEN 
    RETURN
 END IF

 -- Inicio de contador
 LET cntprod = 1

 -- Verificando condicion de categoria
 LET strcodcat = NULL
 IF w_datos.codcat IS NOT NULL THEN
    LET strcodcat = "AND c.codcat = ",w_datos.codcat
 END IF

 -- Verificando condicion de subcategoria
 LET strsubcat = NULL
 IF w_datos.subcat IS NOT NULL THEN
   LET strsubcat = "AND d.subcat = ",w_datos.subcat
 END IF

 -- Obteniendo el tipo de saldo INICIAL
 CALL librut003_parametros(3,2)
 RETURNING existe,wtipsin 

 -- Creando selecccion 
 LET strqry = "SELECT  b.cditem,b.codabr,b.dsitem,b.codcat,c.nomcat,b.subcat,d.nomsub ",
               " FROM  inv_proenbod x,inv_products b, inv_categpro c, inv_subcateg d",
               " WHERE x.codemp = ",w_datos.codemp,
                 " AND x.codsuc = ",w_datos.codsuc,
                 " AND x.codbod = ",w_datos.codbod,
                 " AND x.cditem = b.cditem ",
                 " AND c.codcat = b.codcat ",
                 " AND d.codcat = b.codcat ",
                 " AND d.subcat = b.subcat ",
                 strcodcat CLIPPED,
                 strsubcat CLIPPED, 
                 " ORDER BY 1"

 -- Seleccionando datos
 PREPARE c_prep FROM strqry 
 DECLARE c_prd  CURSOR FOR c_prep

 -- Inicializando totales
 LET w_total.tiniuni = 0
 LET w_total.tentuni = 0
 LET w_total.tsaluni = 0
 LET w_total.tslduni = 0
 LET w_total.tinival = 0
 LET w_total.tentval = 0
 LET w_total.tsalval = 0
 LET w_total.tsldval = 0
 LET w_total.tfisuni = 0 
 LET w_total.tfisval = 0 
 LET w_total.tdifuni = 0 
 LET w_total.tdifval = 0 

 FOREACH c_prd INTO v_inventario[cntprod].cditem,v_inventario[cntprod].codabr,
                    v_inventario[cntprod].dsitem,wcodcat,wnomcat,wsubcat,wnomsub

  -- Obteniendo saldo inicial
  CALL librut003_saldosxproxmes(w_datos.codemp,
                                w_datos.codsuc,
                                w_datos.codbod,
                                wtipsin,
                                w_datos.aniotr,
                                w_datos.mestra,
                                v_inventario[cntprod].cditem)
  RETURNING v_inventario[cntprod].iniuni,v_inventario[cntprod].inival 

  -- Verificando si hay saldo inicial del mes, sino toma el fisico del mes pasado
  IF ((v_inventario[cntprod].iniuni+v_inventario[cntprod].inival)=0) THEN
      LET v_inventario[cntprod].inival = 0

      -- Verificando mes
      IF (w_datos.mestra=1) THEN
         LET aniant = (w_datos.aniotr-1)
         LET mesant = 12
      ELSE
         LET aniant = w_datos.aniotr    
         LET mesant = (w_datos.mestra-1)
      END IF

      -- Obteniendo fisico del mes pasado 
      CALL librut003_fisicoxproxmes(w_datos.codemp,
                                    w_datos.codsuc,
                                    w_datos.codbod,
                                    aniant,
                                    mesant,
                                    v_inventario[cntprod].cditem)
      RETURNING v_inventario[cntprod].iniuni,v_inventario[cntprod].inival
  END IF

  -- Obteniendo entradas  
  CALL librut003_saldosxmovxmes(w_datos.codemp,
                                w_datos.codsuc,
                                w_datos.codbod,
                                w_datos.aniotr,
                                w_datos.mestra,
                                v_inventario[cntprod].cditem,
                                1)
  RETURNING v_inventario[cntprod].entuni,v_inventario[cntprod].entval 

  -- Obteniendo salidas 
  CALL librut003_saldosxmovxmes(w_datos.codemp,
                                w_datos.codsuc,
                                w_datos.codbod,
                                w_datos.aniotr,
                                w_datos.mestra,
                                v_inventario[cntprod].cditem,
                                0)
  RETURNING v_inventario[cntprod].saluni,v_inventario[cntprod].salval 

  -- Calculando saldo actual en unidades y valores
  LET v_inventario[cntprod].slduni = (v_inventario[cntprod].iniuni+v_inventario[cntprod].entuni-v_inventario[cntprod].saluni)
  LET v_inventario[cntprod].sldval = (v_inventario[cntprod].inival+v_inventario[cntprod].entval-v_inventario[cntprod].salval)

  -- Obteniendo fisico  
  CALL librut003_fisicoxproxmes(w_datos.codemp,
                                w_datos.codsuc,
                                w_datos.codbod,
                                w_datos.aniotr,
                                w_datos.mestra,
                                v_inventario[cntprod].cditem)
  RETURNING v_inventario[cntprod].fisuni,v_inventario[cntprod].fisval 

  -- Calculando costo promedio
  LET v_inventario[cntprod].cospro = 0

  -- Calculando diferencias
  LET v_inventario[cntprod].difuni = (v_inventario[cntprod].fisuni-v_inventario[cntprod].slduni)
  LET v_inventario[cntprod].difval = (v_inventario[cntprod].fisval-v_inventario[cntprod].sldval)

  -- Excluyenduyendo productos que no tengan saldo ni movimientos
  IF (v_inventario[cntprod].sldval=0) THEN
     IF ((v_inventario[cntprod].iniuni+v_inventario[cntprod].inival+
          v_inventario[cntprod].entuni+v_inventario[cntprod].entval+
          v_inventario[cntprod].saluni+v_inventario[cntprod].salval)=0) THEN
          CONTINUE FOREACH
     END IF
  END IF

  -- Totalizando 
  LET w_total.tiniuni = (w_total.tiniuni+v_inventario[cntprod].iniuni) 
  LET w_total.tentuni = (w_total.tentuni+v_inventario[cntprod].entuni)
  LET w_total.tsaluni = (w_total.tsaluni+v_inventario[cntprod].saluni)
  LET w_total.tslduni = (w_total.tslduni+v_inventario[cntprod].slduni)
  LET w_total.tfisuni = (w_total.tfisuni+v_inventario[cntprod].fisuni)
  LET w_total.tdifuni = (w_total.tdifuni+v_inventario[cntprod].difuni)
  LET w_total.tinival = (w_total.tinival+v_inventario[cntprod].inival)
  LET w_total.tentval = (w_total.tentval+v_inventario[cntprod].entval)
  LET w_total.tsalval = (w_total.tsalval+v_inventario[cntprod].salval)
  LET w_total.tsldval = (w_total.tsldval+v_inventario[cntprod].sldval)
  LET w_total.tfisval = (w_total.tfisval+v_inventario[cntprod].fisval)
  LET w_total.tdifval = (w_total.tdifval+v_inventario[cntprod].difval)

  -- Incrementando contador
  LET cntprod = (cntprod+1)
 END FOREACH
 CLOSE c_prd
 FREE  c_prd
 LET cntprod = (cntprod-1)

 -- Desplegando totales
 DISPLAY BY NAME w_total.tiniuni, w_total.tentuni, w_total.tsaluni,
                 w_total.tslduni, w_total.tfisuni, w_total.tdifuni ATTRIBUTE(REVERSE)
 DISPLAY BY NAME w_total.tinival, w_total.tentval, w_total.tsalval,
                 w_total.tsldval, w_total.tfisval, w_total.tdifval ATTRIBUTE(REVERSE,YELLOW)
END FUNCTION

-- Subrutina para inicializar las variables de trabajo }

FUNCTION invrep001_inival()
 -- Inicializando datos
 INITIALIZE w_datos.* TO NULL
 LET w_total.tiniuni = 0
 LET w_total.tentuni = 0
 LET w_total.tsaluni = 0
 LET w_total.tslduni = 0
 LET w_total.tinival = 0
 LET w_total.tentval = 0
 LET w_total.tsalval = 0
 LET w_total.tsldval = 0
 LET w_total.tfisuni = 0 
 LET w_total.tfisval = 0 
 LET w_total.tdifuni = 0 
 LET w_total.tdifval = 0 

 -- Limpiando detalle de productos
 CALL v_inventario.clear()
 CALL invrep001_limpiadetalle(50)

 -- Lllenando  combobox de bodegas x usuario
 CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME"))
 -- Llenando combox de categorias
 CALL librut003_cbxcategorias()   

 -- Desplegando datos 
 CLEAR FORM
 DISPLAY BY NAME w_total.tiniuni, w_total.tentuni, w_total.tsaluni,
                 w_total.tslduni, w_total.tfisuni, w_total.tdifuni ATTRIBUTE(REVERSE)
 DISPLAY BY NAME w_total.tinival, w_total.tentval, w_total.tsalval,
                 w_total.tsldval, w_total.tfisval, w_total.tdifval ATTRIBUTE(REVERSE,YELLOW)
END FUNCTION 

-- Subrutina para limpiar el detalle de productos

FUNCTION invrep001_limpiadetalle(cont)
 DEFINE i,cont INT  

 -- Limpiando 
 FOR i = 1 TO cont
  DISPLAY v_inventario[i].* TO l_inventario[i].*
 END FOR
END FUNCTION
