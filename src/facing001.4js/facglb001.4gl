{ 
Fecha    : diciembre 2006 
Programo : Mynor Ramirez 
Objetivo : Programa de variables globales facturacion 
}

DATABASE segovia 

{ Definicion de variables globale }

GLOBALS
DEFINE w_mae_pro   RECORD LIKE inv_products.*,
       w_mae_emp   RECORD LIKE glb_empresas.*,
       w_mae_suc   RECORD LIKE glb_sucsxemp.*,
       w_mae_tra   RECORD LIKE fac_mtransac.*,
       w_mae_act   RECORD LIKE inv_mtransac.*,
       w_tip_doc   RECORD LIKE fac_tipodocs.*,
       w_mae_tdc   RECORD LIKE fac_tdocxpos.*,
       w_mae_res   RECORD LIKE fac_impresol.*,
       w_mae_cli   RECORD LIKE fac_clientes.*,
       w_mae_pos   RECORD LIKE fac_puntovta.*,
       w_uni_med   RECORD LIKE inv_unimedid.*,
       w_med_pro   RECORD LIKE inv_medidpro.*,
       w_mae_ord   RECORD LIKE inv_morden.*,
       w_det_ord   RECORD LIKE inv_dorden.*,
       v_products  DYNAMIC ARRAY OF RECORD
        codpro     LIKE inv_products.cditem,
        cditem     LIKE inv_products.codabr, 
        dsitem     CHAR(50), 
        unimed     SMALLINT,
        nomuni     CHAR(30), 
        canori     LIKE fac_dtransac.canori, 
        unimto     LIKE fac_dtransac.unimto,
        canuni     LIKE fac_dtransac.cantid,
        preuni     LIKE fac_dtransac.preuni,
        totpro     LIKE fac_dtransac.totpro, 
        factor     LIKE fac_dtransac.factor,
		  nuitemf	 LIKE inv_dtransac.nuitem
       END RECORD, 
		v_ordenes    DYNAMIC ARRAY OF RECORD
		  lnkord1 LIKE inv_dorden.lnkord,
		  nuitem1 LIKE inv_dorden.nuitem,
		  subcat1 LIKE inv_dorden.subcat,
		  codcol1 LIKE inv_dorden.codcol,
		  cantid1 LIKE inv_dorden.cantid,
		  xlargo1 LIKE inv_dorden.xlargo,
		  yancho1 LIKE inv_dorden.yancho,
		  descrp1 LIKE inv_dorden.descrp,
		  observ1 LIKE inv_dorden.observ,
		  premed1 LIKE inv_dorden.premed,
		  precio1 LIKE inv_dorden.precio,
		  compre1 LIKE inv_dorden.compre,
		  desman1 LIKE inv_dorden.desman,
		  sinord1 LIKE inv_dorden.sinord,
		  ordext1 LIKE inv_dorden.ordext,
		  medida1 LIKE inv_dorden.medida,
		  tipord1 LIKE inv_dorden.tipord,
		  nofase1 LIKE inv_dorden.nofase
		 END RECORD,
		v_existencia DYNAMIC ARRAY OF RECORD
        bodega     INTEGER,
        nombod     VARCHAR(30,0),
        exiact     DECIMAL(4,2),
        salcan     DECIMAL(4,2)
		 END RECORD,
       xnumpos     LIKE fac_mtransac.numpos,
       totuni      DEC(14,2),
       totval      DEC(14,2),
       reimpresion SMALLINT,
       totlin      SMALLINT,
       nomori      STRING,   
       nomest      STRING,   
       nomdes      STRING, 
       b           ui.ComboBox,
       cba         ui.ComboBox,
       w_lnktra    INT,
       w           ui.Window,
       f           ui.Form,
       flag_prec   SMALLINT,
       pre_med     DECIMAL(14,2),
		 flag_especial SMALLINT,
		 flag_tmp    SMALLINT,
		 sin_orden   SMALLINT,
		 tot     LIKE inv_dorden.precio,
     username    LIKE glb_permxusr.userid
       
END GLOBALS
