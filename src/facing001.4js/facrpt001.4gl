{
Fecha    : Febrero 2011   
Programo : Mynor Ramirez 
Objetivo : Impresion de facturas.
}

-- Definicion de variables globales  

GLOBALS "facglb001.4gl" 
DEFINE w_mae_usr    RECORD LIKE glb_usuarios.*,
       fnt          RECORD
        cmp         CHAR(12),
        nrm         CHAR(12),
        tbl         CHAR(12),
        fbl,t88     CHAR(12),
        t66,p12     CHAR(12),
        p10,srp     CHAR(12),
        twd         CHAR(12),
        fwd         CHAR(12),
        tda,fda     CHAR(12),
        ini         CHAR(12)
       END RECorD,
       existe       SMALLINT, 
       filename     CHAR(90),
       pipeline     CHAR(90),
       nfile   VARCHAR(20),
       npc     VARCHAR (50),
       nprin   VARCHAR(50),
       npath   VARCHAR(100),
       i            INT

-- Subrutina para imprimir un movimiento de producto

FUNCTION facrpt001_facturacion(pct)
 DEFINE pct DEC(9,6),
	opc SMALLINT 
   DEFINE diractual STRING 

  -- CALL fgl_winmessage(" Atencion","Presione [ENTER] para imprimir el documento.","information")
  LET opc = box_pregunta("Imprimir?") 

  CASE (opc)
   WHEN 1 -- SI
    -- Definiendo archivo de impresion
    LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/facrpt001.txt"

    -- Asignando valores
    LET pipeline = "local"   

    -- Seleccionando fonts para impresora epson
    CALL librut001_fontsprn(pipeline,"epson") 
    RETURNING fnt.* 

    LET diractual = FGL_GETENV("PWD")

    -- Iniciando reporte
    START REPORT facrpt001_printfac TO filename 
     OUTPUT TO REPORT facrpt001_printfac(1,pct)
    FINISH REPORT facrpt001_printfac  
      
    --DISPLAY "num pos ", w_mae_tra.numpos
    -- Imprimiendo el reporte
        CALL librut001_enviareporte(filename, pipeline, "")
      
   WHEN 2 -- NO 
     CALL fgl_winmessage(" Atencion","No Se Imprimira . . .","information")
	
  END CASE

END FUNCTION 

-- Subrutinar para generar la impresion de la facturacion 

REPORT facrpt001_printfac(numero,pct) 
 DEFINE wcanletras  STRING,
        pct         DEC(9,6),
        wnommes     CHAR(10), 
        exis,i,j    SMALLINT, 
        totdet      SMALLINT, 
        numero,nl   SMALLINT,
        tarjeta     SMALLINT,
        wnomcol     CHAR(30), 
        wnomcla     CHAR(30), 
        lh,lv,si    CHAR(1),
        sd,ii,id    CHAR(1),
        x           CHAR(1),
        medida      VARCHAR(30),
        aniodos     CHAR(10),
        anio        CHAR(2),
		  atras       VARCHAR(20)

        OUTPUT LEFT   MARGIN 1
         PAGE   LENGTH 51
         TOP    MARGIN 0
         BOTTOM MARGIN 3

 FORMAT 
   BEFORE GROUP OF numero 
      -- Imprimiendo encabezado
      LET wcanletras = librut001_numtolet(w_mae_tra.totdoc) 
      LET wnommes    = librut001_nombremeses(MONTH(w_mae_tra.fecemi),1)
      LET aniodos    =w_mae_tra.fecemi
      LET anio       = aniodos[9,10]
      LET atras  = ASCII 27,ASCII 25,ASCII 66 --"B"

      -- Inicializando impresora
      --IF w_mae_tra.numpos <> 1 THEN
         --CS PRINT fnt.ini CLIPPED,atras CLIPPED,fnt.t66 CLIPPED,fnt.nrm CLIPPED,fnt.cmp CLIPPED,fnt.srp CLIPPED
      --ELSE 
         --SKIP 2 LINES    
      --END IF    
      --Carlos Santizo en Campero
	  
	  --Giovanni tambien esta en campero
      PRINT fnt.ini CLIPPED, fnt.t66 CLIPPED, fnt.nrm CLIPPED --, fnt.srp CLIPPED 
      SKIP 1 LINES
      -- Verificando formato 
      -- Verificando formato 
      CASE (w_tip_doc.numfor)
        WHEN 1 OR 3 -- Facturas
           --nuevo sistema de impresion lpd/lpr
           SKIP 4 LINES 
           PRINT COLUMN  10,w_mae_tra.lnktra   USING "<<<<<<<<<"
           SKIP 1 LINES 
           --IF w_mae_tra.numpos <> 1 THEN SKIP 1 LINE END IF 
           PRINT COLUMN 33, DAY(w_mae_tra.fecemi), 
                 COLUMN 44, wnommes CLIPPED,
                 COLUMN 53, YEAR(w_mae_tra.fecemi) 
           SKIP 1 LINE                
           PRINT COLUMN  15,w_mae_tra.nomcli[1,50]
           PRINT COLUMN  15,w_mae_tra.nomcli[51,100]
           PRINT COLUMN  48,w_mae_tra.numnit CLIPPED
           IF LENGTH(w_mae_tra.dircli) > 50 THEN
              PRINT COLUMN 15, w_mae_tra.dircli CLIPPED WORDWRAP RIGHT MARGIN 65
           ELSE
              PRINT COLUMN 15, w_mae_tra.dircli CLIPPED
              SKIP 1 LINES
           END IF
           PRINT COLUMN 20, w_mae_tra.telcli,
                 COLUMN 50, w_mae_tra.ordcmp CLIPPED
           SKIP 2 LINES  

          WHEN 2 -- Tickets
             SKIP 1 LINES
             PRINT COLUMN  90,w_mae_tra.lnktra   USING "<<<<<<<<<"
             SKIP 1 LINES 
             PRINT COLUMN   6,w_mae_tra.nomcli[1,60],
                   COLUMN  70,"FECHA: ",w_mae_tra.fecemi CLIPPED
             PRINT COLUMN   6,w_mae_tra.dircli CLIPPED, " (T.", w_mae_tra.telcli CLIPPED, ")"
             IF NOT w_mae_tra.hayord THEN
                PRINT COLUMN   6, "Cantidad",
                      COLUMN  20, "Descripcion",
                      COLUMN  58, "Precio U.",
                      COLUMN  70, "Precio Total"
             ELSE
                SKIP 1 LINES
            END IF

      END CASE 

  ON EVERY ROW
   -- Imprimiendo detalle 
   -- Verificando formato 
   
   CASE (w_tip_doc.numfor)
     WHEN 1 -- Facturas
       IF NOT w_mae_tra.hayord THEN 
          LET totdet = 15 
          FOR i = 1 TO 7
             IF v_products[i].cditem IS NULL THEN
                CONTINUE FOR
             END IF 
             IF v_products[i].canuni <=1 THEN
                IF v_products[i].nomuni MATCHES "*S" THEN
                   LET v_products[i].nomuni=v_products[i].nomuni[1,LENGTH(v_products[i].nomuni)-1]
                END IF
             END IF
             LET totdet = (totdet-1) 
            
             IF v_products[i].nomuni != "UNIDAD" THEN
                --Factura yardas
                PRINT COLUMN  5, fnt.cmp CLIPPED, " ",
                                 v_products[i].canuni USING "##,##&.&&" ,
                      COLUMN 24, v_products[i].nomuni CLIPPED, " ",
                                 v_products[i].dsitem[1,40] CLIPPED,
                      COLUMN 81, fnt.nrm CLIPPED, (v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
             ELSE
                --Factura por tapacarga
                PRINT COLUMN  5, fnt.cmp CLIPPED,  " ",  
                                 v_products[i].canuni USING "##,##&.&&" ,
                      COLUMN 24, v_products[i].dsitem[1,40] CLIPPED ,
                      COLUMN 81, fnt.nrm CLIPPED, (v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
             END IF
             IF v_products[i].nomuni MATCHES "*S" THEN
                LET v_products[i].nomuni=v_products[i].nomuni[1,LENGTH(v_products[i].nomuni)-1]
             END IF
             LET totdet = (totdet-1)
             PRINT COLUMN  16, fnt.cmp CLIPPED,
                               "PRECIO ",
                               (v_products[i].preuni*(1+(pct/100))) USING "Q<<,<<&.&&"," C/",v_products[i].nomuni,
                               fnt.nrm CLIPPED
         END FOR
         LET totdet = (totdet-1)
         PRINT COLUMN 15,"-------------- ULTIMA LINEA --------------" 

         -- Saltando lineas extra
         IF (totdet>0) THEN 
            SKIP totdet LINES 
                DISPLAY "linea 209ccc"
         END IF 
      ELSE --Cuando hay orden
         LET nl = 14
         FOR i = 1 to 6 
            IF v_ordenes[i].nuitem1 IS NULL OR
               v_ordenes[i].subcat1 IS NULL THEN
               CONTINUE FOR
            END IF
            IF NOT v_ordenes[i].medida1 THEN
               LET medida = "MTS"
            ELSE
               LET medida = "PIES"
            END IF
            -- Obteniendo clase de producto
            LET wnomcla = NULL
            SELECT a.nomsub
            INTO  wnomcla 
            FROM  inv_subcateg a
            WHERE a.subcat = v_ordenes[i].subcat1 
      
            -- Obteniendo color de producto
            LET wnomcol = NULL
            SELECT a.nomcol
            INTO  wnomcol 
            FROM  inv_colorpro a
            WHERE a.codcol = v_ordenes[i].codcol1
      
            IF NOT v_ordenes[i].desman1 THEN
               -- Diferenciando saran de las lonas en otro material
               IF wnomcla <> "SARAN" CLIPPED THEN
                  PRINT COLUMN  6, fnt.nrm CLIPPED, v_ordenes[i].cantid1 USING "##&.&&",
                        COLUMN 15, fnt.cmp CLIPPED, "LONAS ",wnomcla CLIPPED ," ",wnomcol CLIPPED --, fnt.nrm CLIPPED
                  PRINT COLUMN 25, v_ordenes[i].xlargo1 USING "<<&.&&", " X ", v_ordenes[i].yancho1 USING "<<&.&&",
                                   " ", medida, " ", "PRECIO U. ", v_ordenes[i].precio1 USING "Q<<,<<&.&&",
                        COLUMN 87, fnt.nrm CLIPPED, (v_ordenes[i].precio1 * v_ordenes[i].cantid1) USING "##,##&.&&"  --CS , fnt.cmp CLIPPED 
               ELSE
                  PRINT COLUMN  6, fnt.nrm CLIPPED, v_ordenes[i].cantid1 USING "##&.&&",
                        COLUMN 19,fnt.cmp CLIPPED,wnomcla CLIPPED," ",wnomcol CLIPPED,fnt.nrm CLIPPED
                  PRINT COLUMN 26,fnt.cmp CLIPPED,v_ordenes[i].xlargo1  USING "<<&.&&", " X ",v_ordenes[i].yancho1    USING "<<&.&&",
                                 " ",medida," ","PRECIO U. ",v_ordenes[i].precio1 USING "Q<<,<<&.&&",fnt.nrm CLIPPED,
                        COLUMN 76,(v_ordenes[i].precio1*v_ordenes[i].cantid1)USING "###,###,##&.&&"
               END IF
               LET nl = nl - 2
            ELSE
               PRINT COLUMN  6, fnt.nrm CLIPPED, v_ordenes[i].cantid1 USING "#&.&&",
                     COLUMN 19, "DESCRIPCION: "     
               IF LENGTH(v_ordenes[i].descrp1[01,20]) AND v_ordenes[i].descrp1 IS NOT NULL THEN 
                  LET nl = nl-1
                  PRINT COLUMN 16,fnt.cmp CLIPPED,v_ordenes[i].descrp1 WORDWRAP RIGHT MARGIN 60,
                                  fnt.nrm CLIPPED
               END IF
            END IF
         END FOR
         PRINT COLUMN 15, fnt.cmp CLIPPED, "ORDEN TRABAJO No. ",w_mae_ord.lnkord USING "<<<,<<<", fnt.nrm CLIPPED 
         PRINT COLUMN 15,"-------------- ULTIMA LINEA --------------" 
         --LET nl = 3 
         IF (nl>0) THEN 
            SKIP nl LINES 
         END IF 
      END IF 

    WHEN 2 -- Tickets 
     IF NOT w_mae_tra.hayord THEN
        LET totdet =  8
        FOR i = 1 TO 8  
           IF v_products[i].cditem IS NULL THEN
              CONTINUE FOR
           END IF
           IF v_products[i].canuni <=1 THEN
              IF v_products[i].nomuni MATCHES "*S" THEN
                LET v_products[i].nomuni=v_products[i].nomuni[1,LENGTH(v_products[i].nomuni)-1]
              END IF
          END IF
          LET totdet = (totdet-1)

          IF v_products[i].nomuni != "UNIDAD" THEN
             LET v_products[i].dsitem=v_products[i].nomuni CLIPPED," ",v_products[i].dsitem CLIPPED
          END IF
          PRINT COLUMN   2,v_products[i].canuni       USING "##,##&.&&" ,
                COLUMN  16,fnt.cmp CLIPPED, v_products[i].dsitem[1,40],
                           fnt.nrm CLIPPED,
                COLUMN  72,(v_products[i].preuni*(1+(pct/100))) USING "###,##&.&&",
                COLUMN  90,(v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
          LET totdet = (totdet-1)
      END FOR
      LET totdet = (totdet-1)
      PRINT COLUMN 15,"------------------ ULTIMA LINEA -------------------"
      -- Saltando lineas extra
      IF (totdet>0) THEN
         SKIP totdet LINES
DISPLAY "linea 369"         
      END IF
	ELSE
		  LET totdet = 8
		  FOR i = 1 TO 8
			 IF v_ordenes[i].cantid1 IS NULL AND
			  	 v_ordenes[i].subcat1 IS NULL AND
				 v_ordenes[i].codcol1 IS NULL THEN
				 LET totdet=totdet-1
				 CONTINUE FOR
			 END IF 
			 IF NOT v_ordenes[i].medida1 THEN
   		 	 LET medida = "METROS"
			 ELSE
   		 	 LET medida = "PIES"
			 END IF
			 -- Obteniendo clase de producto
			 LET wnomcla = NULL
			 SELECT a.nomsub
 			 INTO  wnomcla
 			 FROM  inv_subcateg a
 			 WHERE a.subcat = v_ordenes[i].subcat1
			
			 -- Obteniendo color de producto
			 LET wnomcol = NULL
			 SELECT a.nomcol
 			 INTO  wnomcol
 			 FROM  inv_colorpro a
 			 WHERE a.codcol = v_ordenes[i].codcol1

          IF NOT w_det_ord.desman THEN
             IF wnomcla <> "SARAN" CLIPPED THEN
                PRINT COLUMN   2,v_ordenes[i].cantid1         USING "##,###&.&&",
                      COLUMN  16,fnt.cmp CLIPPED,"LONAS ",wnomcla CLIPPED ," ",wnomcol CLIPPED," ",
                                v_ordenes[i].xlargo1  USING "<<&.&&", " X ",v_ordenes[i].yancho1    USING "<<&.&&",
                                " ",medida,fnt.nrm CLIPPED,
                      COLUMN 72,v_ordenes[i].precio1 USING                       "###,##&.&&",
                      COLUMN 90,(v_ordenes[i].precio1*v_ordenes[i].cantid1)USING "###,##&.&&"
					 LET totdet=totdet-1
             ELSE
                PRINT COLUMN   2,v_ordenes[i].cantid1         USING "##.###&.&&",
                      COLUMN  16,fnt.cmp CLIPPED,wnomcla CLIPPED," ",wnomcol CLIPPED," ",
                                v_ordenes[i].xlargo1  USING "<<&.&&"," X ",v_ordenes[i].yancho1    USING "<<&.&&",
                                " ",medida,fnt.nrm CLIPPED,
					 	    COLUMN 72,v_ordenes[i].precio1 USING "###,##&.&&",
                      COLUMN 90,(v_ordenes[i].precio1*v_ordenes[i].cantid1)USING "###,##&.&&"
					 LET totdet=totdet-1
            END IF
         ELSE
            PRINT COLUMN   2,v_ordenes[i].cantid1         USING "##,###&.&&",
                  COLUMN  16,fnt.cmp CLIPPED,"DESCRIPCION: ", v_ordenes[i].descrp1[1,60],fnt.nrm CLIPPED,
						COLUMN  72,v_ordenes[i].precio1                      USING "###,##&.&&",
                  COLUMN 90,(v_ordenes[i].precio1*v_ordenes[i].cantid1)USING "###,##&.&&"
				LET totdet=totdet-1
            END IF
		END FOR
      --LET nl = 3
      PRINT COLUMN 16,fnt.cmp CLIPPED, "ORDEN TRABAJO No. ",w_mae_ord.lnkord USING "<<<,<<<",fnt.nrm CLIPPED
      IF (totdet>0) THEN
         SKIP totdet LINES
      END IF
      SKIP 1 LINES
     END IF

   END CASE 

  ON LAST ROW
   -- Verificando formato 
   CASE (w_tip_doc.numfor)
      WHEN 1 -- Facturas
        -- Imprimiendo final
        SKIP 4 LINES
            PRINT COLUMN  47,w_mae_tra.totdoc  USING "###,###,##&.&&"
        SKIP 1 LINES
        PRINT COLUMN  14,"SUJETO A PAGOS TRIMESTRALES"
        SKIP 1 LINES 
        PRINT COLUMN  14,wcanletras CLIPPED
        SKIP 1 LINES 
        PRINT COLUMN  16,w_mae_tra.nserie CLIPPED," ",w_mae_tra.numdoc  USING "<<<<<<<<<<"," ",
                         w_mae_tra.usrope CLIPPED," ",TIME 

    WHEN 2 -- Ticket 
      -- SKIP 6 LINES 
	   PRINT COLUMN  65,"ABONO: ",w_mae_ord.totabo            USING "###,##&.&&"
	   PRINT COLUMN  65,"SALDO: ",w_mae_ord.salord            USING "###,##&.&&"
      PRINT COLUMN  65,"TOTAL: ",w_mae_tra.totdoc            USING "###,##&.&&"
      
   END CASE 
END REPORT 
