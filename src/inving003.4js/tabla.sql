DROP TABLE "sistemas".inv_masignac 
;
create table "sistemas".inv_masignac 
  (
    lnkasi serial not null ,
    lnkord integer not null ,
    userid char(15) not null ,
    fecsis date not null ,
    horsis datetime hour to second not null ,
    primary key (lnkasi)  constraint "sistemas".pkinvordenasi
  );
revoke all on "sistemas".inv_ordentra from "public" as "sistemas";




alter table "sistemas".inv_masignac add constraint (foreign key 
    (lnkord) references "sistemas".inv_ordentra);

drop table inv_dasignac
;
create table "sistemas".inv_dasignac
  (
    lnkasi integer not null ,
    correl smallint not null ,
    codemp smallint not null ,
    codsuc smallint not null ,
    codbod smallint not null ,
    cditem integer not null ,
    codabr char(20) not null ,
    unimed smallint not null ,
    canori decimal(14,2) not null ,
    unimto smallint not null ,
    cantid decimal(14,2) not null ,
    preuni decimal(12,2) not null ,
    totpro decimal(14,2) not null ,
    factor decimal(14,6) not null ,

    check (correl > 0 ) constraint "sistemas".ckfacdasignac1,

    check (totpro >= 0. ) constraint "sistemas".ckfacdasignac4
  );
revoke all on "sistemas".inv_dasignac from "public" as "sistemas";

alter table "sistemas".inv_dasignac add constraint (foreign key
    (lnkasi) references "sistemas".inv_masignac  on delete cascade
    constraint "sistemas".fkinvasignac1);

