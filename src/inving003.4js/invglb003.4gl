{ 
Fecha    : diciembre 2006 
Programo : Mynor Ramirez 
Objetivo : Programa de variables globales facturacion 
}

DATABASE segovia 

{ Definicion de variables globale }

GLOBALS
DEFINE w_mae_pro   RECORD LIKE inv_products.*,
       w_mae_emp   RECORD LIKE glb_empresas.*,
       w_mae_suc   RECORD LIKE glb_sucsxemp.*,
       w_mae_tra   RECORD LIKE inv_mtransac.*,
       w_mae_act   RECORD LIKE inv_mtransac.*,
       w_tip_doc   RECORD LIKE fac_tipodocs.*,
       w_mae_tdc   RECORD LIKE fac_tdocxpos.*,
       w_mae_res   RECORD LIKE fac_impresol.*,
       w_mae_cli   RECORD LIKE fac_clientes.*,
       w_mae_pos   RECORD LIKE fac_puntovta.*,
       w_uni_med   RECORD LIKE inv_unimedid.*,
       w_med_pro   RECORD LIKE inv_medidpro.*,
       w_mae_ord   RECORD LIKE inv_ordentra.*,
       v_products  DYNAMIC ARRAY OF RECORD
        codpro     LIKE inv_products.cditem,
        cditem     LIKE inv_products.codabr, 
        dsitem     CHAR(50), 
        unimed     SMALLINT,
        nomuni     CHAR(30), 
        canori     LIKE fac_dtransac.canori, 
        unimto     LIKE fac_dtransac.unimto,
        canuni     LIKE fac_dtransac.cantid,
        preuni     LIKE fac_dtransac.preuni,
        totpro     LIKE fac_dtransac.totpro, 
        factor     LIKE fac_dtransac.factor 
       END RECORD, 
		v_existencia DYNAMIC ARRAY OF RECORD
        bodega     INTEGER,
        nombod     VARCHAR(30,0),
        exiact     DECIMAL(4,2),
        salcan     DECIMAL(4,2)
		 END RECORD,
       {ord     DYNAMIC ARRAY OF RECORD
         orden    INTEGER,
         material SMALLINT,
         color    SMALLINT,
         tamano   VARCHAR(30),
         nomcli   VARCHAR(30)
      END RECORD,
}
       xnumpos     LIKE fac_mtransac.numpos,
       totuni      DEC(14,2),
       totval      DEC(14,2),
       reimpresion SMALLINT,
       totlin      SMALLINT,
       nomori      STRING,   
       nomest      STRING,   
       nomdes      STRING, 
       b           ui.ComboBox,
       cba         ui.ComboBox,
       w_lnktra    INT,
       w           ui.Window,
       f           ui.Form,
       flag_prec   SMALLINT,
       pre_med     DECIMAL(14,2),
		 flag_especial SMALLINT,
		 flag_tmp    SMALLINT,
		 flag_rim    SMALLINT
END GLOBALS
