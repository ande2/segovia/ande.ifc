{
Fecha    : Febrero 2011   
Programo : Mynor Ramirez 
Objetivo : Impresion de facturas.
}

-- Definicion de variables globales  

GLOBALS "invglb003.4gl" 
DEFINE w_mae_usr    RECORD LIKE glb_usuarios.*,
       fnt          RECORD
        cmp         CHAR(12),
        nrm         CHAR(12),
        tbl         CHAR(12),
        fbl,t88     CHAR(12),
        t66,p12     CHAR(12),
        p10,srp     CHAR(12),
        twd         CHAR(12),
        fwd         CHAR(12),
        tda,fda     CHAR(12),
        ini         CHAR(12)
       END RECorD,
       existe       SMALLINT, 
       filename     CHAR(90),
       pipeline     CHAR(90),
       i            INT

-- Subrutina para imprimir un movimiento de producto

FUNCTION facrpt001_facturacion(pct)
 DEFINE pct DEC(9,6),
	opc SMALLINT 

-- CALL fgl_winmessage(" Atencion","Presione [ENTER] para imprimir el documento.","information")
	IF w_mae_ord.ordext IS NULL OR w_mae_ord.ordext = 0 THEN
LET opc = librut001_menugraba("Confirmacion",
                                        "Desea Imprimir?",
                                        "Si",
                                        "No",
                                        "Cancelar",
                                        "")

  CASE (opc)
   WHEN 0 -- cancelar
     CALL fgl_winmessage(" Atencion","No Se Imprimira . . .","information")

   WHEN 1 -- Grabando
 -- Definiendo archivo de impresion
 LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/facrpt001.spl"
	DISPLAY filename

 -- Asignando valores
 LET pipeline = "local"   

 -- Seleccionando fonts para impresora epson
 CALL librut001_fontsprn(pipeline,"epson") 
 RETURNING fnt.* 

 -- Iniciando reporte
 START REPORT facrpt001_printfac TO filename 
  OUTPUT TO REPORT facrpt001_printfac(1,pct)
 FINISH REPORT facrpt001_printfac 

 -- Imprimiendo el reporte
 CALL librut001_enviareporte(filename,
                             pipeline,
                             "")
   WHEN 2 -- Modificando
     CALL fgl_winmessage(" Atencion","No Se Imprimira . . .","information")
	
  END CASE
END IF

END FUNCTION 

-- Subrutinar para generar la impresion de la facturacion 

REPORT facrpt001_printfac(numero,pct) 
 DEFINE wcanletras  STRING,
        pct         DEC(9,6),
        wnommes     CHAR(10), 
        exis,i      SMALLINT, 
        totdet      SMALLINT, 
        numero,nl   SMALLINT,
        tarjeta     SMALLINT,
        wnomcol     CHAR(30), 
        wnomcla     CHAR(30), 
        lh,lv,si    CHAR(1),
        sd,ii,id    CHAR(1),
        x           CHAR(1),
        medida      VARCHAR(30),
        aniodos     CHAR(10),
        anio        CHAR(2)

        OUTPUT LEFT   MARGIN 1
         PAGE   LENGTH 51
         TOP    MARGIN 0
         BOTTOM MARGIN 3

 FORMAT 
  BEFORE GROUP OF numero 
   -- Imprimiendo encabezado
   LET wcanletras = librut001_numtolet(w_mae_tra.totdoc) 
   LET wnommes    = librut001_nombremeses(MONTH(w_mae_tra.fecemi),1)
   LET aniodos    =w_mae_tra.fecemi
	LET anio       = aniodos[9,10]
   DISPLAY "1 ",wcanletras 
   DISPLAY "2 ",wnommes    
   DISPLAY "3 ",aniodos    
	DISPLAY "4 ",anio       


   -- Inicializando impresora
   PRINT fnt.ini CLIPPED,fnt.t66 CLIPPED,fnt.nrm CLIPPED,fnt.p12 CLIPPED,fnt.srp 

   -- Verificando formato 
   -- Verificando formato 
   CASE (w_tip_doc.numfor)
    WHEN 1 -- Facturas
     SKIP 5 LINES 
     PRINT COLUMN  10,w_mae_tra.lnktra   USING "<<<<<<<<<"
     SKIP 1 LINES 
     PRINT COLUMN  35,DAY(w_mae_tra.fecemi)    , 3 SPACES,
                      wnommes                  , 1 SPACES,
                      YEAR(w_mae_tra.fecemi) 
     SKIP 1 LINES
     PRINT COLUMN  12,w_mae_tra.nomcli
     SKIP 1 LINES 
     PRINT COLUMN  46,w_mae_tra.numnit CLIPPED
     IF LENGTH(w_mae_tra.dircli) > 50 THEN
        PRINT COLUMN  17,w_mae_tra.dircli CLIPPED WORDWRAP RIGHT MARGIN 65
     ELSE
        PRINT COLUMN  17,w_mae_tra.dircli CLIPPED
        SKIP 1 LINES
     END IF
     PRINT COLUMN  17,w_mae_tra.telcli,
           COLUMN  50,w_mae_tra.ordcmp CLIPPED
     SKIP 2 LINES 
    WHEN 2 -- Tickets
	  SKIP 2 LINES
     PRINT COLUMN  90,w_mae_tra.lnktra   USING "<<<<<<<<<"
     SKIP 1 LINES 
     PRINT COLUMN   6,w_mae_tra.nomcli[1,60],
           COLUMN  70,"FECHA: ",w_mae_tra.fecemi CLIPPED
     PRINT COLUMN   6,w_mae_tra.dircli
--     SKIP 1 LINES 
	  IF NOT w_mae_tra.hayord THEN
     		PRINT COLUMN   6, "Cantidad",
               COLUMN  20, "Descripcion",
               COLUMN  54, "Precio U.",
               COLUMN  74, "Precio Total"
	 	ELSE
			SKIP 1 LINES
		END IF
    WHEN 3 -- Facturas zona 11
   -- Inicializando impresora
     PRINT fnt.ini CLIPPED,fnt.t88 CLIPPED,fnt.nrm CLIPPED,fnt.p12 CLIPPED,fnt.srp 
     SKIP 7 LINES
     PRINT COLUMN  12,w_mae_tra.lnktra   USING "<<<<<<<<<"
     SKIP 5 LINES
     PRINT COLUMN  33,DAY(w_mae_tra.fecemi)    , 3 SPACES,
                      wnommes                  ,11 SPACES,
                      fnt.cmp CLIPPED,anio,fnt.nrm CLIPPED,fnt.p12 CLIPPED,fnt.srp
     SKIP 1 LINES
     PRINT COLUMN  12,w_mae_tra.nomcli [1,45],
           COLUMN  52,w_mae_tra.numnit CLIPPED
     SKIP 1 LINES
     PRINT COLUMN  13,fnt.cmp CLIPPED,w_mae_tra.dircli       CLIPPED,fnt.nrm CLIPPED,fnt.p12 CLIPPED,fnt.srp
   --  SKIP 1 LINES
     SKIP 1 LINES
     IF (NOT w_mae_tra.credit) THEN
		  IF w_mae_tra.telcli IS NULL THEN
	        PRINT COLUMN  13,w_mae_tra.usrope ,
   	           COLUMN  33, "X"
		  ELSE
	        PRINT COLUMN  13,w_mae_tra.usrope ,
   	           COLUMN  33, "X" , 
      	        COLUMN  45,"Tel.: ",w_mae_tra.telcli CLIPPED
		  END IF
     ELSE
		  IF w_mae_tra.telcli IS NULL THEN
   	     PRINT COLUMN  13, w_mae_tra.usrope ,
	              COLUMN  39, "X"
		  ELSE
       	  PRINT COLUMN  13, w_mae_tra.usrope ,
          	     COLUMN  39, "X" , 
      	        COLUMN  45,"Tel.: ",w_mae_tra.telcli CLIPPED
		  END IF
     END IF
     --    COLUMN  50,w_mae_tra.ordcmp CLIPPED
     SKIP 2 LINES
   END CASE 

  ON EVERY ROW
   -- Imprimiendo detalle 
   -- Verificando formato 
   CASE (w_tip_doc.numfor)
    WHEN 1 -- Facturas
     IF NOT w_mae_tra.hayord THEN 
      LET totdet = 15 
      FOR i = 1 TO 7
       IF v_products[i].cditem IS NULL THEN
          CONTINUE FOR
       END IF 
		 IF v_products[i].canuni <=1 THEN
			 IF v_products[i].nomuni MATCHES "*S" THEN
				LET v_products[i].nomuni=v_products[i].nomuni[1,LENGTH(v_products[i].nomuni)-1]
			 END IF
		 END IF
       LET totdet = (totdet-1) 
      
       IF v_products[i].nomuni != "UNIDAD" THEN
          PRINT COLUMN   0,v_products[i].canuni       USING "##,###,##&.&&" ,
                COLUMN  16,fnt.cmp CLIPPED,
                           v_products[i].nomuni CLIPPED, " ",
                           v_products[i].dsitem[1,40],
                           fnt.nrm CLIPPED,
                COLUMN  62,(v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
       ELSE
          PRINT COLUMN   0,v_products[i].canuni       USING "##,###,##&.&&" ,
                COLUMN  16,fnt.cmp CLIPPED, v_products[i].dsitem[1,40],
                           fnt.nrm CLIPPED,
                COLUMN  62,(v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
       END IF
		 IF v_products[i].nomuni MATCHES "*S" THEN
			LET v_products[i].nomuni=v_products[i].nomuni[1,LENGTH(v_products[i].nomuni)-1]
		 END IF
       LET totdet = (totdet-1)
       PRINT COLUMN  16,fnt.cmp CLIPPED,
                        "PRECIO ",
                        (v_products[i].preuni*(1+(pct/100))) USING "Q<<,<<&.&&"," C/",v_products[i].nomuni,
                        fnt.nrm CLIPPED
      END FOR
      LET totdet = (totdet-1)
      PRINT COLUMN 15,"-------------- ULTIMA LINEA --------------" 

      -- Saltando lineas extra
      IF (totdet>0) THEN 
         SKIP totdet LINES 
      END IF 
     ELSE
		IF NOT w_mae_ord.medida THEN
			LET medida = "METROS"
		ELSE
			LET medida = "PIES"
		END IF
      -- Obteniendo clase de producto
      LET wnomcla = NULL
      SELECT a.nomsub
       INTO  wnomcla 
       FROM  inv_subcateg a
       WHERE a.subcat = w_mae_ord.subcat 

      -- Obteniendo color de producto
      LET wnomcol = NULL
      SELECT a.nomcol
       INTO  wnomcol 
       FROM  inv_colorpro a
       WHERE a.codcol = w_mae_ord.codcol 

		IF NOT w_mae_ord.desman THEN
			-- Diferenciando saran de las lonas en otro material
			IF wnomcla <> "SARAN" CLIPPED THEN
				IF w_mae_ord.cantid = 1 THEN
			      PRINT COLUMN   6,w_mae_ord.cantid             USING "##&.&&",
			            COLUMN  16,"LONA ",wnomcla
		   	   PRINT COLUMN  16,"COLOR ",wnomcol 
			      PRINT COLUMN  16,"DE " ,w_mae_ord.xlargo  USING "<<&.&&",
			                      " X ",w_mae_ord.yancho    USING "<<&.&&",
   	                         " ",medida
				ELSE
			      PRINT COLUMN   6,w_mae_ord.cantid             USING "##&.&&",
			            COLUMN  16,"LONAS ",wnomcla
	   		   PRINT COLUMN  16,"COLOR ",wnomcol 
			      PRINT COLUMN  16,"DE " ,w_mae_ord.xlargo  USING "<<&.&&",
			                      " X ",w_mae_ord.yancho    USING "<<&.&&",
      	                      " ",medida
				END IF
			ELSE
	   	   PRINT COLUMN   6,w_mae_ord.cantid             USING "##&.&&",
	      	              16,wnomcla
		      PRINT COLUMN  16,"COLOR ",wnomcol 
		      PRINT COLUMN  16,"DE " ,w_mae_ord.xlargo  USING "<<&.&&",
	   	                   " X ",w_mae_ord.yancho    USING "<<&.&&",
									 " ",medida
			END IF
		ELSE
			PRINT COLUMN   5,w_mae_ord.cantid             USING "#&.&&"
			PRINT COLUMN  16,"DESCRIPCION: "     
			IF LENGTH(w_mae_ord.descrp[01,20]) THEN 
				LET nl = nl-1
				PRINT COLUMN 16,w_mae_ord.descrp WORDWRAP RIGHT MARGIN 45
			END IF
		END IF
      LET nl = 3 
      PRINT COLUMN 16,"ORDEN TRABAJO No. ",w_mae_ord.lnkord USING "<<<" 
      PRINT COLUMN 16,"PRECIO UNITARIO ",w_mae_ord.precio USING "Q<<,<<&.&&",
            COLUMN 48,w_mae_tra.totdoc            USING "###,###,##&.&&"
      IF (nl>0) THEN 
         SKIP nl LINES 
      END IF 
      SKIP 7 LINES 
     END IF 

    WHEN 2 -- Tickets 
     IF NOT w_mae_tra.hayord THEN
      LET totdet = 10
      FOR i = 1 TO 10
       IF v_products[i].cditem IS NULL THEN
          CONTINUE FOR
       END IF
       IF v_products[i].canuni <=1 THEN
          IF v_products[i].nomuni MATCHES "*S" THEN
            LET v_products[i].nomuni=v_products[i].nomuni[1,LENGTH(v_products[i].nomuni)-1]
          END IF
       END IF
       LET totdet = (totdet-1)

       IF v_products[i].nomuni != "UNIDAD" THEN
          PRINT COLUMN   2,v_products[i].canuni       USING "##,##&.&&" ,
                COLUMN  16,fnt.cmp CLIPPED,
                           v_products[i].nomuni CLIPPED, " ",
                           v_products[i].dsitem[1,36],
                           fnt.nrm CLIPPED,
                COLUMN  72,(v_products[i].preuni*(1+(pct/100))) USING "###,##&.&&",
                COLUMN  90,(v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
       ELSE
          PRINT COLUMN   2,v_products[i].canuni       USING "##,##&.&&" ,
                COLUMN  16,fnt.cmp CLIPPED, v_products[i].dsitem[1,40],
                           fnt.nrm CLIPPED,
                COLUMN  72,(v_products[i].preuni*(1+(pct/100))) USING "###,##&.&&",
                COLUMN  90,(v_products[i].totpro*(1+(pct/100))) USING "###,##&.&&"
       END IF
       LET totdet = (totdet-1)
      END FOR
      LET totdet = (totdet-1)
      PRINT COLUMN 15,"------------------ ULTIMA LINEA -------------------"
      -- Saltando lineas extra
      IF (totdet>0) THEN
         SKIP totdet LINES
      END IF
	  ELSE
      IF NOT w_mae_ord.medida THEN
         LET medida = "METROS"
      ELSE
         LET medida = "PIES"
      END IF
      -- Obteniendo clase de producto
      LET wnomcla = NULL
      SELECT a.nomsub
       INTO  wnomcla
       FROM  inv_subcateg a
       WHERE a.subcat = w_mae_ord.subcat

      -- Obteniendo color de producto
      LET wnomcol = NULL
      SELECT a.nomcol
       INTO  wnomcol
       FROM  inv_colorpro a
       WHERE a.codcol = w_mae_ord.codcol

      IF NOT w_mae_ord.desman THEN
         -- Diferenciando saran de las lonas en otro material
         IF wnomcla <> "SARAN" CLIPPED THEN
            IF w_mae_ord.cantid = 1 THEN
               PRINT COLUMN   8,w_mae_ord.cantid             USING "#&.&&",
                     COLUMN  16,"LONA ",wnomcla
               PRINT COLUMN  16,"COLOR ",wnomcol
               PRINT COLUMN  16,"DE " ,w_mae_ord.xlargo  USING "<<&.&&",
                               " X ",w_mae_ord.yancho    USING "<<&.&&",
                               " ",medida
            ELSE
               PRINT COLUMN   5,w_mae_ord.cantid             USING "#&.&&",
                     COLUMN  16,"LONAS ",wnomcla
               PRINT COLUMN  16,"COLOR ",wnomcol
               PRINT COLUMN  16,"DE " ,w_mae_ord.xlargo  USING "<<&.&&",
                               " X ",w_mae_ord.yancho    USING "<<&.&&",
                               " ",medida
            END IF
         ELSE
            PRINT COLUMN   8,w_mae_ord.cantid             USING "#&.&&",
                          16,wnomcla
            PRINT COLUMN  16,"COLOR ",wnomcol
            PRINT COLUMN  16,"DE " ,w_mae_ord.xlargo  USING "<<&.&&",
                            " X ",w_mae_ord.yancho    USING "<<&.&&",
                            " ",medida
         END IF
         #PRINT COLUMN   5,w_mae_ord.cantid             USING "#&.&&"
         IF LENGTH(w_mae_ord.descrp[01,20]) THEN
            LET nl = nl-1
            PRINT COLUMN 16,w_mae_ord.descrp WORDWRAP RIGHT MARGIN 45
         END IF
      END IF
      LET nl = 3
      PRINT COLUMN 16,"ORDEN TRABAJO No. ",w_mae_ord.lnkord USING "<<<"
      PRINT COLUMN 16,"PRECIO UNITARIO ",w_mae_ord.precio USING "Q<<,<<&.&&",
            COLUMN 60,w_mae_tra.totdoc            USING "Q###,###,##&.&&"
      IF (nl>0) THEN
         SKIP nl LINES
      END IF
      SKIP 1 LINES
     END IF

    WHEN 3 -- Facturas Z. 11

     IF NOT w_mae_tra.hayord THEN
      LET totdet = 15
      FOR i = 1 TO 13
       IF v_products[i].cditem IS NULL THEN
          CONTINUE FOR
       END IF
       LET totdet = (totdet-1)

       PRINT COLUMN   1,v_products[i].canuni       USING "##,###,##&.&&" ,
             COLUMN  16,fnt.cmp CLIPPED, v_products[i].nomuni[1,6]," ",
             COLUMN  22,fnt.cmp CLIPPED,
                        v_products[i].dsitem[1,40],
                        fnt.nrm CLIPPED,
             COLUMN  62,(v_products[i].preuni*(1+(pct/100))) USING "##,##&.&&",
                        fnt.nrm CLIPPED,
             COLUMN  70,(v_products[i].totpro*(1+(pct/100))) USING "##,###,##&.&&"
      END FOR
      LET totdet = (totdet-1)
      PRINT COLUMN 16,"-------------- ULTIMA LINEA --------------"

      -- Saltando lineas extra
      IF (totdet>0) THEN
         SKIP totdet LINES
      END IF
     ELSE
		IF NOT w_mae_ord.medida THEN
			LET medida = "METROS"
		ELSE
			LET medida = "PIES"
		END IF
      -- Obteniendo clase de producto
      LET wnomcla = NULL
      SELECT a.nomsub
       INTO  wnomcla 
       FROM  inv_subcateg a
       WHERE a.subcat = w_mae_ord.subcat 

      -- Obteniendo color de producto
      LET wnomcol = NULL
      SELECT a.nomcol
       INTO  wnomcol 
       FROM  inv_colorpro a
       WHERE a.codcol = w_mae_ord.codcol 

		IF NOT w_mae_ord.desman THEN
			-- Diferenciando saran de las lonas en otro material
			IF wnomcla <> "SARAN" CLIPPED THEN
				IF w_mae_ord.cantid = 1 THEN
               SKIP 1 LINES
			      PRINT COLUMN   5,w_mae_ord.cantid             USING "#&.&&",
			            COLUMN  16,"LONA ",wnomcla
		   	   PRINT COLUMN  16,"COLOR ",wnomcol 
			      PRINT COLUMN  16,"DE " ,w_mae_ord.xlargo  USING "<<&.&&",
			                      " X ",w_mae_ord.yancho    USING "<<&.&&",
   	                         " ",medida
				ELSE
               SKIP 1 LINES
			      PRINT COLUMN   5,w_mae_ord.cantid             USING "#&.&&",
			            COLUMN  16,"LONAS ",wnomcla
	   		   PRINT COLUMN  16,"COLOR ",wnomcol 
			      PRINT COLUMN  16,"DE " ,w_mae_ord.xlargo  USING "<<&.&&",
			                      " X ",w_mae_ord.yancho    USING "<<&.&&",
      	                      " ",medida
				END IF
			ELSE
            SKIP 1 LINES
	   	   PRINT COLUMN   5,w_mae_ord.cantid             USING "#&.&&",
	      	      COLUMN  16,wnomcla
		      PRINT COLUMN  16,"COLOR ",wnomcol 
		      PRINT COLUMN  16,"DE " ,w_mae_ord.xlargo  USING "<<&.&&",
	   	                   " X ",w_mae_ord.yancho    USING "<<&.&&",
									 " ",medida
			END IF
		ELSE
			PRINT COLUMN   5,w_mae_ord.cantid             USING "#&.&&"
			PRINT COLUMN  16,"DESCRIPCION: "     
			IF LENGTH(w_mae_ord.descrp[01,20]) THEN 
				LET nl = nl-1
				PRINT COLUMN 16,w_mae_ord.descrp WORDWRAP RIGHT MARGIN 45
			END IF
		END IF
      LET nl = 4 
      PRINT COLUMN 16,"ORDEN TRABAJO No. ",w_mae_ord.lnkord USING "<<<" 
      PRINT COLUMN 16,"PRECIO UNITARIO ",w_mae_ord.precio USING "Q##,##&.&&",
            COLUMN 73,w_mae_tra.totdoc            USING "##,###,##&.&&"
      IF (nl>0) THEN 
         SKIP nl LINES 
      END IF 
      SKIP 5 LINES 
     END IF

   END CASE 

  ON LAST ROW
   -- Verificando formato 
   CASE (w_tip_doc.numfor)
    WHEN 1 -- Facturas
     -- Imprimiendo final
     SKIP 4 LINES
     PRINT COLUMN  48,w_mae_tra.totdoc            USING "###,###,##&.&&"
     SKIP 1 LINES
     PRINT COLUMN  14,"SUJETO A PAGOS TRIMESTRALES"
     SKIP 1 LINES 
     PRINT COLUMN  14,wcanletras CLIPPED
     SKIP 1 LINES 
     PRINT COLUMN  16,w_mae_tra.nserie CLIPPED," ",w_mae_tra.numdoc  USING "<<<<<<<<<<"," ",
                      w_mae_tra.usrope CLIPPED," ",TIME 

    WHEN 2 -- Ticket 
    -- SKIP 6 LINES 
     PRINT COLUMN  65,"TOTAL: ",w_mae_tra.totdoc            USING "###,##&.&&"
    WHEN 3 -- Facturas Z. 11
     -- Imprimiendo final
     SKIP 1 LINES
     PRINT COLUMN  54,w_mae_tra.totdoc            USING "###,###,##&.&&"
	     SKIP 3 LINES
   	  PRINT COLUMN  09,fnt.cmp CLIPPED,wcanletras CLIPPED,fnt.nrm CLIPPED,fnt.p12 CLIPPED,fnt.srp
     SKIP 2 LINES
     PRINT COLUMN  16,w_mae_tra.nserie CLIPPED,"-",w_mae_tra.numdoc  USING "<<<<<<<<<<","  Hora:",TIME
     PRINT fnt.ini CLIPPED,fnt.t66 CLIPPED,fnt.nrm CLIPPED,fnt.p12 CLIPPED,fnt.srp
   END CASE 
END REPORT 
