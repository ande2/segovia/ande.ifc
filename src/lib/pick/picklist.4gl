DATABASE segovia
 
MAIN
 DEFINE retorna      RECORD  
         codigo      VARCHAR(20),
         nombre      VARCHAR(100)
	END RECORD, 
        regreso      SMALLINT 

 CALL ui.Interface.loadStyles("../../std/styles")
 CALL librut002_formlistcli("Seleccion de Empaques del Producto 1","Codigo","Descripcion","codepq","nomepq","inv_epqsxpro",
                            "cditem=1",1)
 RETURNING retorna.*,regreso 
END MAIN 

-- Subrutina para seleccionar datos de una lista 

FUNCTION librut002_selectlist(Title0,Title1,Title2,Field1,Field2,Tabname,Condicion,OrderNum)
 DEFINE Title0       STRING,
	Title1,  
	Title2       STRING,  
	Field1, 
	Field2,
	Condicion    STRING,
	Tabname      STRING,
	OrderNum     STRING, 
        retorna      RECORD  
         codigo      VARCHAR(20),
         nombre      VARCHAR(100)
	END RECORD,
	la_busqueda  DYNAMIC ARRAY OF RECORD 	
         codigo      VARCHAR(20),
         nombre      VARCHAR(100)
        END RECORD,
        i            SMALLINT,
        w            ui.Window,
        f            ui.Form,
        sqlstmnt     STRING,
        refe         VARCHAR(100),
        buscar       VARCHAR(105),
        scr_cnt      SMALLINT

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/formlist")

 -- Definiendo tecla de aceptar
 OPTIONS ACCEPT KEY escape

 LET int_flag = 0  

 -- Abriendo la ventana                                                                
 OPEN WINDOW find_ayuda 
  WITH FORM "formlist3"

  -- Obteniendo datos de la ventana
  LET w = ui.Window.getcurrent()
  LET f = w.getForm()

  -- Desplegandp titulso
  CALL fgl_settitle(title0)
  CALL f.setElementText("formonly.codigo",title1)
  CALL f.setElementText("formonly.nombre",title2)

  -- Inicializando datos
  CALL la_busqueda.clear()
  LET int_flag = FALSE  
  LET i = 1

  -- Creando busqueda
  LET sqlstmnt = " SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED, 
                  " FROM ",tabname CLIPPED,
                  " WHERE ",condicion CLIPPED, 
                  " ORDER BY ",OrderNum

  -- Preparando busqueda
  PREPARE ex2_sqlst FROM sqlstmnt

  -- Seleccionando datos
  DECLARE westos0 CURSOR FOR ex2_sqlst
  CALL la_busqueda.CLEAR()
  LET i =1
  FOREACH westos0 INTO la_busqueda[i].*
   LET i=i+1
  END FOREACH
  FREE westos0
  FREE ex2_sqlst

  -- Definiendo tecla de aceptacion
  OPTIONS ACCEPT KEY RETURN

  -- Desplegnado datos
  MESSAGE "Presione [ENTER] para seleccionar"

  DISPLAY ARRAY la_busqueda TO sa_busqueda.* 
   ATTRIBUTE(ACCEPT=FALSE) 
    BEFORE DISPLAY
     -- Si no se ecnontraron datos
     IF i=1 THEN
        MESSAGE "Atencion: no existen datos registrados." 
     END IF
  
    BEFORE ROW
     LET i       = ARR_CURR()
     LET scr_cnt = SCR_LINE()
     LET retorna.* = la_busqueda[i].*
     IF LENGTH(la_busqueda[i].codigo)=0 THEN
        CALL fgl_dialog_setcurrline(scr_cnt,i-1)
     END IF

    ON ACTION seleccion
     EXIT DISPLAY

    ON ACTION cancel
     EXIT DISPLAY 

    ON KEY(RETURN)
     EXIT DISPLAY

    AFTER DISPLAY
     CONTINUE DISPLAY
  END DISPLAY
  MESSAGE "" 
 CLOSE WINDOW find_ayuda

 -- Definiendo tecla de aceptacion
 OPTIONS ACCEPT KEY ESCAPE

 -- Inicializando datos
 CALL la_busqueda.clear()

 -- Cargando acciones default
 CALL ui.Interface.loadActionDefaults("../std/actiondefaults")

 RETURN retorna.*,int_flag 
END FUNCTION
