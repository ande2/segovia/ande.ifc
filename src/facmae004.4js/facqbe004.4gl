{
facqbe004.4gl 
Mynor Ramirez
Mantenimiento de resoluciones 
}

{ Definicion de variables globales }

GLOBALS "facglo004.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION facqbe004_resoluciones(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        westado             LIKE fac_impresol.estado, 
        qrytext,qrypart     CHAR(1000),    
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qryres,qry          STRING,   
        msg                 STRING,
        rotulo              CHAR(12), 
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando operacion
  LET rotulo = NULL
  IF (operacion=3) THEN 
     LET rotulo = "Borrar" 
  END IF 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL facmae004_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codemp,a.tipdoc,a.nserie,a.nresol,a.fecres,a.numini,
                                a.numfin,a.estado,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel
     -- Salida
     CALL facmae004_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = "SELECT UNIQUE a.lnkres,c.nomemp,d.nomdoc,a.nserie,a.nresol,a.estado ",
                  "FROM  fac_impresol a,glb_empresas c,fac_tipodocs d ",
                  "WHERE c.codemp = a.codemp ",
                    "AND d.tipdoc = a.tipdoc ",
                    "AND ",qrytext CLIPPED,
                  " ORDER BY 1,2,3,4"

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_datos1 SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_resols.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_datos1 INTO v_resols[totlin].tlnkres THRU v_resols[totlin].tnresol,westado
    -- Verficando estado
    IF (westado=0) THEN
       LET v_resols[totlin].testado = "mars_cancelar.png"
    ELSE
       LET v_resols[totlin].testado = "mars_cheque.png"
    END IF 

    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_datos1
   FREE  c_datos1
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_resols TO s_resols.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL facmae004_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL facmae004_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF facmae004_resoluciones(2) THEN
         EXIT DISPLAY 
      ELSE 
         -- Desplegando datos
         CALL facqbe004_datos(v_resols[ARR_CURR()].tlnkres)
      END IF 

     ON KEY (CONTROL-M) 
      IF (operacion=2) THEN 
       -- Modificando 
       IF facmae004_resoluciones(2) THEN
          EXIT DISPLAY 
       ELSE 
         -- Desplegando datos
          CALL facqbe004_datos(v_resols[ARR_CURR()].tlnkres)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF facqbe004_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Esta resolucion ya tiene movimientos, por favor VERIFICAR ...",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de "||rotulo CLIPPED||" esta resolucion ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                   msg,
                                   "Si",
                                   "No",
                                   NULL,
                                   NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
            --  Eliminando
            CALL facmae004_grabar(3)
            EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte 
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Empresa"
      LET arrcols[2] = "Tipo de Documento"
      LET arrcols[3] = "Numero Serie"
      LET arrcols[4] = "Numero Resolucion"
      LET arrcols[5] = "Fecha Resolucion"
      LET arrcols[6] = "Correlativo Inicial"
      LET arrcols[7] = "Correlativo Final"
      LET arrcols[8] = "Estado"
      LET arrcols[9] = "Usuario Registro"
      LET arrcols[10]= "Fecha Registro"
      LET arrcols[11]= "Hora Registro" 

      -- Se aplica el mismo query de la seleccion para enviar al reporte 
      LET qry        = "SELECT c.nomemp,d.nomdoc,a.nserie,a.nresol,a.fecres,a.numini,a.numfin,",
                              "CASE (a.estado) WHEN 1 THEN 'Alta' WHEN 0 THEN 'Baja' END,",
                              "a.userid,a.fecsis,a.horsis ",
                       " FROM fac_impresol a,glb_empresas c,fac_tipodocs d ",
                       " WHERE c.codemp = a.codemp ",
                          "AND d.tipdoc = a.tipdoc ",
                          "AND ",qrytext CLIPPED,
                       " ORDER BY 1,2,3,4 "

      -- Ejecutando el reporte
      LET res = librut002_excelreport("Resoluciones",qry,11,1,arrcols)

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL facqbe004_datos(v_resols[1].tlnkres)
      ELSE 
         CALL facqbe004_datos(v_resols[ARR_CURR()].tlnkres)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen resoluciones con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION facqbe004_datos(wlnkres)
 DEFINE wlnkres LIKE fac_impresol.lnkres,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  fac_impresol a "||
              "WHERE a.lnkres = "||wlnkres||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_datos1t SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_datos1t INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.codemp THRU w_mae_pro.estado
  DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
 END FOREACH
 CLOSE c_datos1t
 FREE  c_datos1t

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.codemp THRU w_mae_pro.estado
 DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION 

-- Subrutina para verificar si la resolucion ya tiene movimientos 

FUNCTION facqbe004_integridad()
 DEFINE conteo SMALLINT

 -- Verificando 
 SELECT COUNT(*)
  INTO  conteo
  FROM  fac_mtransac a 
  WHERE (a.codemp = w_mae_pro.codemp) 
    AND (a.tipdoc = w_mae_pro.tipdoc) 
    AND (a.nserie = w_mae_pro.nserie) 
    AND (a.nserie = w_mae_pro.nserie) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF
END FUNCTION 
