{ 
facglo004.4gl
Mynor Ramirez
Mantenimiento de Resoluciones
}

DATABASE segovia 

{ Definicion de variables globale }

GLOBALS
DEFINE w_mae_pro   RECORD LIKE fac_impresol.*,
       v_resols  DYNAMIC ARRAY OF RECORD
        tlnkres    LIKE fac_impresol.lnkres,
        tnomemp    CHAR(30),
        tnomdoc    CHAR(30),
        tnserie    LIKE fac_impresol.nserie,
        tnresol    LIKE fac_impresol.nresol, 
        testado    CHAR(20)
       END RECORD
END GLOBALS
