{
invmae004.4gl 
Mynor Ramirez
Mantenimiento de tipos de movimiento
}

-- Definicion de variables globales 

GLOBALS "invglo004.4gl"
DEFINE existe,wcorrmov SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar3")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invmae003.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL invmae004_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae004_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae004a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header("invmae004",wpais,1) 
  CALL librut001_dpelement("labela","Accesos a Usuarios") 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Tipos de Movimiento"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Deshabilitar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de tipos de movimiento."
    CALL invqbe004_tipomovs(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo tupo de movimiento."
    LET savedata = invmae004_tipomovs(1) 
   COMMAND "Modificar"
    " Modificacion de un tipo de movimiento existente."
    CALL invqbe004_tipomovs(2) 
   COMMAND "Borrar"
    " Eliminacion de un tipo de movimiento existente."
    CALL invqbe004_tipomovs(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae004_tipomovs(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Obteniendo parametro de correlativo de tipos de movimiento
 CALL librut003_parametros(6,1)
 RETURNING existe,wcorrmov 
 IF NOT existe THEN
    CALL fgl_winmessage(
    " Atencion:",
    " No existe definido el correlativo para tipos de movimiento. \n"||
    " Definir primero correlativo antes de registrar tipos de movimiento.",
    "information")
    RETURN FALSE 
 END IF 

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae004_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nommov, 
                w_mae_pro.nomabr,
                w_mae_pro.tipope,
                w_mae_pro.hayval 
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE INPUT
    -- Verificando integridad
    -- Si tipo de movimiento ya tiene movimientos no pueden modificarse los campos de nombre, tipo de operacion
    IF (operacion=2) THEN 
      -- Verificando integridad
      IF invqbe004_integridad() THEN
         -- Deshabilitando campos
         CALL Dialog.SetFieldActive("nommov",FALSE) 
         CALL Dialog.SetFieldActive("tipope",FALSE) 
      ELSE 
         -- Habilitando campos
         CALL Dialog.SetFieldActive("nommov",TRUE) 
         CALL Dialog.SetFieldActive("tipope",TRUE) 
      END IF
    END IF 

   AFTER FIELD nommov  
    --Verificando nombre del tipo de movimiento 
    IF (LENGTH(w_mae_pro.nommov)=0) THEN
       ERROR "Error: nombre del tipo de movimiento invalido, VERIFICA"
       LET w_mae_pro.nommov = NULL
       NEXT FIELD nommov  
    END IF

    -- Verificando que no exista otro tipo de movimiento con el mismo nombre
    SELECT UNIQUE (a.tipmov)
     FROM  inv_tipomovs a
     WHERE (a.tipmov != w_mae_pro.tipmov) 
       AND (a.nommov  = w_mae_pro.nommov) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro tipo de movimiento con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nommov
     END IF 

   AFTER FIELD nomabr  
    --Verificando nombre abreviado
    IF (LENGTH(w_mae_pro.nomabr)=0) THEN
       ERROR "Error: nombre abreviado del tipo de movimiento invalido, VERIFICA"
       LET w_mae_pro.nomabr = NULL
       NEXT FIELD nomabr  
    END IF

   AFTER FIELD tipope  
    --Verificando tipo de operacion
    IF (w_mae_pro.tipope IS NULL) THEN
       NEXT FIELD tipope  
    END IF

   AFTER FIELD hayval 
    --Verificando tipo de operacion
    IF (w_mae_pro.hayval IS NULL) THEN
       NEXT FIELD hayval  
    END IF

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.nommov IS NULL THEN 
       NEXT FIELD nommov
    END IF
    IF w_mae_pro.nomabr IS NULL THEN 
       NEXT FIELD nomabr
    END IF
    IF w_mae_pro.tipope IS NULL THEN 
       NEXT FIELD tipope
    END IF
    IF w_mae_pro.hayval IS NULL THEN 
       NEXT FIELD hayval
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae004_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando tipo de movimiento
    CALL invmae004_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL invmae004_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un tipo de movimiento

FUNCTION invmae004_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando tipo de movimiento ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.tipmov),0)
    INTO  w_mae_pro.tipmov
    FROM  inv_tipomovs a
    IF (w_mae_pro.tipmov=0) OR
       (w_mae_pro.tipmov IS NULL) THEN
       LET w_mae_pro.tipmov = wcorrmov 
    ELSE 
       LET w_mae_pro.tipmov = (w_mae_pro.tipmov+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO inv_tipomovs   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.tipmov 

   --Asignando el mensaje 
   LET msg = "Tipo de Movimiento (",w_mae_pro.tipmov USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE inv_tipomovs
   SET    inv_tipomovs.*      = w_mae_pro.*
   WHERE  inv_tipomovs.tipmov = w_mae_pro.tipmov 

   --Asignando el mensaje 
   LET msg = "Tipo de Movimiento (",w_mae_pro.tipmov USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando
   DELETE FROM inv_tipomovs 
   WHERE (inv_tipomovs.tipmov = w_mae_pro.tipmov)

   --Asignando el mensaje 
   LET msg = "Tipo de Movimiento (",w_mae_pro.tipmov USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae004_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae004_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL 
   LET w_mae_pro.tipmov = 0 
   LET w_mae_pro.hayval = 0 
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME") 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.tipmov THRU w_mae_pro.hayval 
 DISPLAY BY NAME w_mae_pro.tipmov,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION
