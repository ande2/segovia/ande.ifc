{ 
invglo004.4gl
Mynor Ramirez
Mantenimiento de tipos de movimiento
}

DATABASE segovia 

{ Definicion de variables globale }

GLOBALS

CONSTANT linpan = 50

DEFINE w_mae_pro   RECORD LIKE inv_tipomovs.*,
       v_tipomovs  DYNAMIC ARRAY OF RECORD
        ttipmov    LIKE inv_tipomovs.tipmov,
        tnommov    LIKE inv_tipomovs.nommov, 
        ttipope    CHAR(20)
       END RECORD, 
       v_permxusr  DYNAMIC ARRAY OF RECORD
        tcheckb    SMALLINT,
        tuserid    LIKE inv_permxtmv.userid,
        tnomusr    VARCHAR(50), 
        tusuaid    LIKE inv_permxtmv.usuaid,
        tfecsis    LIKE inv_permxtmv.fecsis,
        thorsis    LIKE inv_permxtmv.horsis
       END RECORD
END GLOBALS
