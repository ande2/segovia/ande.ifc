{
facmae003.4gl 
Mynor Ramirez
Mantenimiento de tipos de documento x punto de venta
}

-- Definicion de variables globales 

GLOBALS "facglo003.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("facmae003.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL facmae003_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION facmae003_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "facmae003a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais

  CALL librut001_header("facmae003",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Tipos de Documento x Punto de Venta"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Eliminar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de tipos de documento x punto de venta."
    CALL facqbe003_tipos(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo tipo de documento."
    LET savedata = facmae003_tipos(1) 
   COMMAND "Modificar"
    " Modificacion de un tipo de dcoumento x punto de venta existente."
    CALL facqbe003_tipos(2) 
   COMMAND "Borrar"
    " Eliminacion de un tipo de documento x punto de venta existente."
    CALL facqbe003_tipos(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION facmae003_tipos(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL facmae003_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.numpos,
                w_mae_pro.codemp,
                w_mae_pro.tipdoc,
                w_mae_pro.nserie,
                w_mae_pro.numcor, 
                w_mae_pro.estado
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE INPUT
    -- Verificando integridad
    -- Si tipo de docunmento ya tiene movimientos no se puede modificarse los campos siguientes 
    IF (operacion=2) THEN -- Si es modificacion
     IF facqbe003_integridad() THEN
       CALL Dialog.SetFieldActive("numpos",FALSE)
       CALL Dialog.SetFieldActive("codemp",FALSE)
       CALL Dialog.SetFieldActive("tipdoc",FALSE)
       CALL Dialog.SetFieldActive("nserie",FALSE)
       CALL Dialog.SetFieldActive("numcor",FALSE)
     ELSE
       CALL Dialog.SetFieldActive("numpos",TRUE)
       CALL Dialog.SetFieldActive("codemp",TRUE)
       CALL Dialog.SetFieldActive("tipdoc",TRUE)
       CALL Dialog.SetFieldActive("nserie",TRUE)
       CALL Dialog.SetFieldActive("numcor",TRUE)
     END IF
    END IF 

   AFTER FIELD numpos 
    --Verificando punto de venta
    IF w_mae_pro.numpos IS NULL THEN
       ERROR "Error: punto de venta invalido, VERIFICA ..."
       NEXT FIELD numpos
    END IF

   AFTER FIELD codemp
    --Verificando empresa
    IF w_mae_pro.codemp IS NULL THEN
       ERROR "Error: empresa invalida, VERIFICA ..."
       NEXT FIELD codemp
    END IF

   AFTER FIELD tipdoc 
    --Verificando tipo de documento
    IF w_mae_pro.tipdoc IS NULL THEN
       ERROR "Error: tipo de documento invalido, VERIFICA ..."
       NEXT FIELD tipdoc
    END IF

   AFTER FIELD nserie
    -- Verificando numero de serie
    IF (LENGTH(w_mae_pro.nserie)<=0) THEN 
       ERROR "Error: numero de serie invalido, VERIFICA ..."
       NEXT FIELD nserie
    END IF 

   AFTER FIELD numcor
    --Verificando correlativo
    IF w_mae_pro.numcor IS NULL THEN
       ERROR "Error: correlativo inicial invalido, VERIFICA ..."
       NEXT FIELD numcor 
    END IF

   AFTER FIELD estado  
    --Verificando estado
    IF w_mae_pro.estado IS NULL THEN
       ERROR "Error: estado invalido, VERIFICA ..."
       NEXT FIELD estado
    END IF

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.numpos IS NULL THEN 
       NEXT FIELD numpos
    END IF
    IF w_mae_pro.codemp IS NULL THEN 
       NEXT FIELD codemp
    END IF
    IF w_mae_pro.tipdoc IS NULL THEN 
       NEXT FIELD tipdoc
    END IF
    IF w_mae_pro.nserie IS NULL THEN 
       NEXT FIELD nserie
    END IF
    IF w_mae_pro.numcor IS NULL THEN 
       NEXT FIELD numcor 
    END IF
    IF w_mae_pro.estado IS NULL THEN 
       NEXT FIELD estado
    END IF

    -- Verificando si tipo de documento por punto de venta,empresa y serie ya existe
    SELECT UNIQUE a.lnktdc
     FROM  fac_tdocxpos a
     WHERE a.lnktdc != w_mae_pro.lnktdc 
       AND a.numpos  = w_mae_pro.numpos 
       AND a.codemp  = w_mae_pro.codemp  
       AND a.tipdoc  = w_mae_pro.tipdoc 
       AND a.nserie  = w_mae_pro.nserie
     IF (status!=NOTFOUND) THEN
       -- Desplegando mensaje
       CALL fgl_winmessage(
       " Atencion", 
       " Ya existe registrado un tipo de documento con dicha informacion. \n"||
       " VERIFICAR punto de venta, empresa, tipo documento y serie.",
       "information")
       CONTINUE INPUT 
     END IF 

    -- Verificando si tipo de documento por empresa y serie ya existe
    SELECT UNIQUE a.lnktdc
     FROM  fac_tdocxpos a
     WHERE a.lnktdc != w_mae_pro.lnktdc 
       AND a.codemp  = w_mae_pro.codemp  
       AND a.tipdoc  = w_mae_pro.tipdoc 
       AND a.nserie  = w_mae_pro.nserie
     IF (status!=NOTFOUND) THEN
       -- Desplegando mensaje
       CALL fgl_winmessage(
       " Atencion", 
       " Ya existe registrado un tipo de documento con dicha informacion. \n"||
       " VERIFICAR  empresa, tipo documento y serie.",
       "information")
       CONTINUE INPUT 
     END IF 
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL facmae003_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando 
    CALL facmae003_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL facmae003_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un tipo de documento

FUNCTION facmae003_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando tipo de documento ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   LET w_mae_pro.lnktdc = 0

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO fac_tdocxpos   
   VALUES (w_mae_pro.*)
   LET w_mae_pro.lnktdc = SQLCA.SQLERRD[2]

   --Asignando el mensaje 
   LET msg = "Tipo de documento (",w_mae_pro.lnktdc USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE fac_tdocxpos
   SET    fac_tdocxpos.*      = w_mae_pro.*
   WHERE  fac_tdocxpos.lnktdc = w_mae_pro.lnktdc 

   --Asignando el mensaje 
   LET msg = "Tipo de Documento (",w_mae_pro.lnktdc USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando 
   DELETE FROM fac_tdocxpos 
   WHERE (fac_tdocxpos.lnktdc = w_mae_pro.lnktdc)

   --Asignando el mensaje 
   LET msg = "Tipo de Documento (",w_mae_pro.lnktdc USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL facmae003_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION facmae003_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.lnktdc = 0 
   LET w_mae_pro.estado = "A"
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME")
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Cargando combobox
 CALL librut003_cbxempresas() 
 CALL librut003_cbxpuntosventa()
 CALL librut003_cbxtiposdocumento(0) 

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.numpos THRU w_mae_pro.estado
 DISPLAY BY NAME w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION
