CONSTANT BOD_TRASLADO = 22
CONSTANT MOV_TRASLADO = 109

DATABASE segovia
MAIN
DEFINE sqlstatus INTEGER
DEFINE errMsg STRING  
DEFINE r RECORD 
   numrf1 LIKE inv_mtransac.numrf1,
   lnktra LIKE inv_mtransac.lnktra
END RECORD 
    
   CALL startlog("cleanTraslado.log")
   BEGIN WORK 
   TRY
      DECLARE cur_movnc CURSOR FOR 
      SELECT m.numrf1, m.lnktra
      FROM inv_mtransac m
      WHERE m.tipmov = 111
      AND m.numrf1 = "TEMPORAL:        " --No tiene referencia, esta incompleto

      DECLARE cur_movdup CURSOR FOR 
      SELECT m1.numrf1, MIN (m1.lnktra) lnktra --Los mas antiguos, traslados fantasmas
      FROM inv_mtransac m1
      WHERE m1.numrf1 IN (
      SELECT m.numrf1 
      FROM inv_mtransac m
      WHERE m.tipmov = 111
      AND m.estado = 'V'
      AND m.numrf1 <> "TEMPORAL:        "
      GROUP BY 1
      HAVING COUNT (m.numrf1) > 1
      )
      GROUP BY 1

      LET errMsg = "Descargar tabla de traslados incompletos"
      UNLOAD TO  'inv_mtransacnc.unl'
      SELECT m.lnktra
      FROM inv_mtransac m
      WHERE m.tipmov = 111
      AND m.numrf1 = "TEMPORAL:        "
      --
      LET errMsg = "Descargar tabla de traslados duplicados"
      UNLOAD TO  'inv_mtransacdup.unl'
      SELECT m1.numrf1, MIN (m1.lnktra) lnktra --Los mas antiguos, traslados fantasmas
      FROM inv_mtransac m1
      WHERE m1.numrf1 IN (
      SELECT m.numrf1 
      FROM inv_mtransac m
      WHERE m.tipmov = 111
      AND m.estado = 'V'
      AND m.numrf1 <> "TEMPORAL:        "
      GROUP BY 1
      HAVING COUNT (m.numrf1) > 1
      )
      GROUP BY 1
      --
      LET errMsg = "Anular Traslados Incompletos"
      FOREACH cur_movnc INTO r.*
         UPDATE inv_mtransac SET
            estado = "A",
            motanl = "ENVIOS INCOMPLETOS", 
            usranl = USER,
            fecanl = CURRENT,
            horanl = CURRENT
         WHERE lnktra = r.lnktra 
      END FOREACH 
      --
      LET errMsg = "Anular Traslados Duplicados"
      FOREACH cur_movdup INTO r.*
         UPDATE inv_mtransac SET
            estado = "A",
            motanl = "ENVIO DUPLICADO", 
            usranl = USER,
            fecanl = CURRENT,
            horanl = CURRENT
         WHERE lnktra = r.lnktra 
      END FOREACH 

   CATCH
      LET sqlstatus = sqlca.sqlcode
      ROLLBACK WORK
      LET errMsg = "Error al tratar de ",errMsg," \n"
      DISPLAY errMsg
      CALL errorlog(errMsg||sqlstatus USING "-<<<<"||" - "||err_get(sqlstatus) CLIPPED)
      EXIT PROGRAM 
   END TRY
   COMMIT WORK
   DISPLAY "Proceso de traslado ejecutado satisfactoriamente"
END MAIN 