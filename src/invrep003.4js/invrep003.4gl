{ 
Fecha    : Enero 2011 
Programo : invrep003.4gl 
Objetivo : Reporte de kardex de producto 
}

DATABASE segovia 

{ Definicion de variables globales }

DEFINE w_mae_bod RECORD LIKE inv_mbodegas.*,
       w_mae_emp RECORD LIKE glb_empresas.*,
       w_mae_suc RECORD LIKE glb_sucsxemp.*,
       w_datos   RECORD
        codbod   LIKE inv_mtransac.codbod,
        codemp   LIKE inv_mtransac.codemp,
        codsuc   LIKE inv_mtransac.codbod,
        codcat   LIKE inv_categpro.codcat,
        subcat   LIKE inv_subcateg.subcat,
        codabr   LIKE inv_dtransac.cditem,
        fecini   DATE,
        fecfin   DATE 
       END RECORD,
       fnt          RECORD
        cmp         CHAR(12),
        nrm         CHAR(12),
        tbl         CHAR(12),
        fbl,t88     CHAR(12),
        t66,p12     CHAR(12),
        p10,srp     CHAR(12),
        twd         CHAR(12),
        fwd         CHAR(12),
        tda,fda     CHAR(12),
        ini         CHAR(12)
       END RECORD,
       existe    SMALLINT,
       filename  CHAR(100),
       fcodcat   STRING, 
       fsubcat   STRING, 
       fcditem   STRING 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar7")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invrep003.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep003_kardex()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep003_kardex()
 DEFINE w_pro_bod         RECORD LIKE inv_proenbod.*,
        w_mae_pro         RECORD LIKE inv_products.*,
        imp1              RECORD
         codemp           LIKE inv_proenbod.codemp, 
         codsuc           LIKE inv_proenbod.codsuc, 
         codbod           LIKE inv_proenbod.codbod,
         cditem           LIKE inv_proenbod.cditem, 
         codabr           CHAR(20),
         dsitem           CHAR(60),
         nommed           CHAR(30),
         eximin           LIKE inv_proenbod.eximin, 
         eximax           LIKE inv_proenbod.eximax,
         usureg           LIKE inv_products.userid,
         fecreg           LIKE inv_products.fecsis,
         lnktra           LIKE inv_mtransac.lnktra,
         numrf1           CHAR(20),
         numrf2           CHAR(20), 
         observ           CHAR(200), 
         userid           LIKE inv_mtransac.userid,
         fecemi           LIKE inv_mtransac.fecemi,
         tipmov           LIKE inv_dtransac.tipmov,
         abrmov           LIKE inv_tipomovs.nomabr,
         tipope           LIKE inv_dtransac.tipope,
         canuni           LIKE inv_dtransac.canuni,
         horsis           LIKE inv_dtransac.horsis,
         salanu           DEC(14,2),
         salanv           DEC(14,2)
        END RECORD,
        wpais             VARCHAR(255),
        pipeline          VARCHAR(100), 
        qrytxt            STRING,
        strcodcat         STRING,
        strsubcat         STRING,
        strcditem         STRING,
        loop,haymov       SMALLINT

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep003a AT 5,2
  WITH FORM "invrep003a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invrep003",wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/invrep003.txt"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando  combobox de bodegas x usuario
  CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME"))
  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()
  -- Llenando combox de productos
  CALL librut003_cbxproductosabr()

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   CLEAR FORM

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codbod,
                 w_datos.codcat,
                 w_datos.subcat,
                 w_datos.codabr,
                 w_datos.fecini,
                 w_datos.fecfin
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "local" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)
     LET fcditem = GET_FLDBUF(w_datos.codabr)

     -- Verificando datos 
     IF w_datos.codbod IS NULL OR
        w_datos.codcat IS NULL OR
        w_datos.subcat IS NULL OR
        w_datos.codabr IS NULL OR
        w_datos.fecini IS NULL OR
        w_datos.fecfin IS NULL THEN
        ERROR "Error: deben completarse los filtos de seleccion." 
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

    ON CHANGE codbod
     -- Obteniendo datos de la bodega
     CALL librut003_bbodega(w_datos.codbod)
     RETURNING w_mae_bod.*,existe

     -- Asignando datos de empresa y sucursal de la bodega
     LET w_datos.codemp = w_mae_bod.codemp
     LET w_datos.codsuc = w_mae_bod.codsuc

     -- Obteniendo datos de la empresa
     CALL librut003_bempresa(w_mae_bod.codemp)
     RETURNING w_mae_emp.*,existe
     -- Obteniendo datos de la sucursal
     CALL librut003_bsucursal(w_mae_bod.codsuc)
     RETURNING w_mae_suc.*,existe

     -- Desplegando datos de la empresa y sucursal
     DISPLAY BY NAME w_datos.codemp,w_datos.codsuc,w_mae_emp.nomemp,w_mae_suc.nomsuc

     -- Limpiando combos de categorias y subcategorias
     LET w_datos.codcat = NULL
     LET w_datos.subcat = NULL
     CLEAR codcat,subcat

    ON CHANGE codcat
     -- Limpiando combos
     LET w_datos.subcat = NULL
     LET w_datos.codabr = NULL
     CLEAR subcat,codabr

     -- Llenando combox de subcategorias
     IF w_datos.codcat IS NOT NULL THEN 
        CALL librut003_cbxsubcategorias(w_datos.codcat)
     END IF 

	ON CHANGE subcat
		LET w_datos.codabr = NULL
		CLEAR codabr
		IF w_datos.codcat IS NOT NULL AND w_datos.subcat IS NOT NULL THEN
			CALL librut003_cbxproductosds(w_datos.codcat,w_datos.subcat)
		END IF

    AFTER FIELD codbod
     -- Verificando bodega 
     IF w_datos.codbod IS NULL THEN
        ERROR "Error: debe de seleccionarse la bodega a listar."
        NEXT FIELD codbod
     END IF

    AFTER FIELD fecini
     -- Verificando fecha inicial
     IF w_datos.fecini IS NULL THEN
        ERROR "Error: debe ingresarse la fecha inicial a listar."
        NEXT FIELD fecini 
     END IF

    AFTER FIELD fecfin
     -- Verificando fecha final
     IF w_datos.fecfin IS NULL THEN
        ERROR "Error: debe ingresarse la fecha final a listar."
        NEXT FIELD fecfin 
     END IF

    AFTER INPUT
     -- Verificando datos
     IF w_datos.codbod IS NULL OR
        w_datos.codcat IS NULL OR
        w_datos.subcat IS NULL OR
        w_datos.codabr IS NULL OR
        w_datos.fecini IS NULL OR
        w_datos.fecfin IS NULL OR
        pipeline IS NULL THEN
        NEXT FIELD codbod
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Verificando seleccion de categoria
   LET strcodcat = NULL
   IF w_datos.codcat IS NOT NULL THEN
      LET strcodcat = "AND d.codcat = ",w_datos.codcat
   END IF

   -- Verificando condicion de subcategoria
   LET strsubcat = NULL
   IF w_datos.subcat IS NOT NULL THEN
      LET strsubcat = "AND e.subcat = ",w_datos.subcat
   END IF

   -- Verificando condicion de producto
   LET strcditem = NULL
   IF w_datos.codabr IS NOT NULL THEN
      LET strcditem = "AND a.cditem = ",w_datos.codabr 
   END IF

   -- Construyendo seleccion 
   LET qrytxt = "SELECT a.codemp,a.codsuc,a.codbod,a.cditem,b.codabr,b.dsitem,c.nommed,", 
                       "a.eximin,a.eximax,b.userid,b.fecsis ",
                  "FROM  inv_proenbod a,inv_products b,inv_unimedid c,inv_categpro d,inv_subcateg e ",
                  "WHERE a.codemp = ",w_datos.codemp,
                   " AND a.codsuc = ",w_datos.codsuc,
                   " AND a.codbod = ",w_datos.codbod,
                   " AND b.cditem = a.cditem ",
                   " AND c.unimed = b.unimed ",
                   " AND d.codcat = b.codcat ",
                   " AND e.codcat = b.codcat ",
                   " AND e.subcat = b.subcat ",
                   strcodcat CLIPPED," ",
                   strsubcat CLIPPED," ",
                   strcditem CLIPPED,
                   " ORDER BY 1,2,3,4"

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep003 FROM qrytxt 
   DECLARE c_crep003 CURSOR FOR c_rep003
   LET existe = FALSE
   FOREACH c_crep003 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       -- Seleccionando fonts para impresora epson
       CALL librut001_fontsprn(pipeline,"epson")
       RETURNING fnt.*

       LET existe = TRUE
       START REPORT invrep003_invkrxpro TO filename
    END IF 

    -- Calculando saldo anterior del producto
    --CALL librut003_saldoantxpro(imp1.codemp,imp1.codsuc,imp1.codbod,imp1.cditem,(w_datos.fecini-1))
    --RETURNING imp1.salanu,imp1.salanv 
		CALL invrep003_saldoant(imp1.codemp,imp1.codsuc,imp1.codbod,imp1.cditem,(w_datos.fecini-1))
		RETURNING imp1.salanu

    -- Seleccionando movimientos del producto 
    DECLARE cmovto CURSOR FOR
    SELECT a.lnktra,a.numrf1,a.numrf2,a.observ,a.userid,d.fecemi,d.tipmov,t.nomabr,d.tipope,d.canuni,d.horsis
     FROM  inv_mtransac a,inv_dtransac d,inv_tipomovs t
     WHERE (a.lnktra  = d.lnktra)
       AND (a.estado  = "V") 
       AND (d.codemp  = imp1.codemp) 
       AND (d.codsuc  = imp1.codsuc) 
       AND (d.codbod  = imp1.codbod) 
       AND (d.cditem  = imp1.cditem) 
       AND (d.fecemi >= w_datos.fecini)     
       AND (d.fecemi <= w_datos.fecfin)       
       AND (t.tipmov  = d.tipmov)
		 --AND (d.codepq = 0 ) 
     ORDER BY d.fecemi,d.horsis,d.lnktra   

     LET haymov = FALSE
     FOREACH cmovto INTO imp1.lnktra THRU imp1.horsis
      IF NOT haymov THEN
         LET haymov = TRUE
      END IF 

      -- Llenando reporte
      OUTPUT TO REPORT invrep003_invkrxpro(imp1.*,haymov)
     END FOREACH

     -- Si producto no tiene movimiento imprime una linea indicandolo
     IF NOT haymov THEN
        -- Llenando reporte
        OUTPUT TO REPORT invrep003_invkrxpro(imp1.*,haymov)
     END IF 
   END FOREACH
   CLOSE c_crep003 
   FREE  c_crep003 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT invrep003_invkrxpro 

		-- Enviando a imprimir PDF
		CALL librut001_rep_pdf("Kardex",filename,9,"L",4)
      -- Enviando reporte al destino seleccionado
      --CALL librut001_enviareporte(filename,pipeline,"Kardex Por Producto")
      --ERROR "" 
      --CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE
 CLOSE WINDOW wrep003a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT invrep003_invkrxpro(imp1,haymov)
 DEFINE imp1              RECORD
         codemp           LIKE inv_proenbod.codemp, 
         codsuc           LIKE inv_proenbod.codsuc, 
         codbod           LIKE inv_proenbod.codbod,
         cditem           LIKE inv_proenbod.cditem, 
         codabr           CHAR(20),
         dsitem           CHAR(60),
         nommed           CHAR(30),
         eximin           LIKE inv_proenbod.eximin, 
         eximax           LIKE inv_proenbod.eximax,
         usureg           LIKE inv_products.userid,
         fecreg           LIKE inv_products.fecsis,
         lnktra           LIKE inv_mtransac.lnktra,
         numrf1           CHAR(20),
         numrf2           CHAR(20), 
         observ           CHAR(200), 
         userid           LIKE inv_mtransac.userid,
         fecemi           LIKE inv_mtransac.fecemi,
         tipmov           LIKE inv_dtransac.tipmov,
         abrmov           LIKE inv_tipomovs.nomabr,
         tipope           LIKE inv_dtransac.tipope,
         canuni           LIKE inv_dtransac.canuni,
         horsis           LIKE inv_dtransac.horsis,
         salanu           DEC(14,2),
         salanv           DEC(14,2)
        END RECORD,
        w_cargou,w_saluni DEC(14,2),
        w_cartov,w_cartou DEC(14,2),
        w_abonou,w_abotou DEC(14,2),
        haymov            SMALLINT, 
        linea             CHAR(154),
        wdocto            CHAR(22),
        wobs              CHAR(50) 

  OUTPUT PAGE   LENGTH 40
         LEFT   MARGIN 0
         TOP    MARGIN 0
         BOTTOM MARGIN 0

  FORMAT
   FIRST PAGE HEADER
    LET linea = "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________"--,
                --"____"

    -- Configurando tipos de letra 
    --PRINT ASCII 27 -- COLOR NEGRO 

    -- Imprimiendo encabezado 
    PRINT COLUMN   1,ASCII 27 CLIPPED,"Inventarios",
          COLUMN 130,PAGENO USING "Pagina: <<<<"
    PRINT COLUMN   1,"Invrep003",
          COLUMN  58,"K A R D E X  P O R  P R O D U C T O",
          COLUMN 130,"Fecha : ",TODAY USING "dd/mmm/yyyy"
    PRINT COLUMN   1,"(",FGL_GETENV("LOGNAME") CLIPPED,")", 
          COLUMN  56,"Periodo Del (",w_datos.fecini USING "dd/mmm/yyyy",
           	     ") Al (",       w_datos.fecfin USING "dd/mmm/yyyy",")", 
          COLUMN 130,"Hora  : ",TIME
    PRINT linea	       
    -- Inicializando datos
    LET w_cartou = 0
    LET w_abotou = 0
    LET w_saluni = imp1.salanu
    PRINT w_mae_emp.nomemp         CLIPPED
          
    PRINT "SUCURSAL (",imp1.codsuc USING "<<<<",") ",
          w_mae_suc.nomsuc         CLIPPED
    PRINT "BODEGA (",imp1.codbod   USING "<<<<",") ",
          w_mae_bod.nombod         CLIPPED
    SKIP 1 LINES 
    PRINT COLUMN   1,"Producto           : ",imp1.codabr  CLIPPED,2 SPACES,
                                             imp1.dsitem  CLIPPED,
          COLUMN 110,"Existencia Minima  :  ",imp1.eximin  USING "<<<,<<&.&&"
    PRINT COLUMN   1,"Unidad de Medida   : ",imp1.nommed  CLIPPED, 
          COLUMN 110,"Existencia Maxima  :  ",imp1.eximax  USING "<<<,<<&.&&"
    PRINT COLUMN   1,"Usuario Registro   : ",imp1.usureg 
    PRINT COLUMN   1,"Fecha de Registro  : ",imp1.fecreg  USING "dd/mmm/yyyy",
          COLUMN 103,"Saldo Anterior --> ",
          COLUMN 123,imp1.salanu  USING "--,---,--&.&&"
    PRINT linea 
    PRINT "Fecha de     Tipo    Numero de              Descripcion del Docume",
          "nto                              |-",
          "-----------   Unidades   ------------|  Usuario"
    PRINT "Emision      Movto   Documento                                    ",
          "                                   ",
          "  Cargos        Abonos         Saldo    Opero"
    PRINT linea

  --BEFORE GROUP OF imp1.cditem
   --SKIP TO TOP OF PAGE


   -- Imprimiendo datos del producto
  ON EVERY ROW
   -- Imprimiendo movimientos  
   -- Verificando si hay movimiento        
   IF haymov THEN
      -- Inicializado datos 
      LET w_cargou = 0 
      LET w_abonou = 0
      -- Calculando saldos

      CASE (imp1.tipope)        
       WHEN 1 -- Cargos
        LET w_cargou  = imp1.canuni   
        LET w_cartou  = (w_cartou+imp1.canuni)
        LET w_saluni  = (w_saluni+imp1.canuni)
       WHEN 0 -- Abonos
        LET w_abonou  = imp1.canuni 
        LET w_abotou  = (w_abotou+imp1.canuni)
        LET w_saluni  = (w_saluni-imp1.canuni)
       WHEN 2 -- Traslados
        LET w_abonou  = imp1.canuni 
        LET w_abotou  = (w_abotou+imp1.canuni)
        LET w_saluni  = (w_saluni-imp1.canuni)
      END CASE
      IF UPSHIFT(imp1.numrf1) MATCHES "TRASLADO:*" THEN
			LET imp1.numrf1 = "T:",imp1.numrf1[11,20] CLIPPED
		END IF
			
      -- Imprimiendo movimientos  
      LET wdocto = imp1.lnktra USING "<<<<<<<<<<","/",imp1.numrf1 CLIPPED,"/",imp1.numrf2 CLIPPED  
      PRINT imp1.fecemi               USING "dd/mmm/yyyy"  ,2 SPACES,
            imp1.abrmov                                    ,2 SPACES,
            wdocto                                         ,1 SPACES,
            imp1.observ[1,50]                              ,2 SPACES,
            w_cargou                  USING "##,###,##&.&&",1 SPACES,
            w_abonou                  USING "##,###,##&.&&",1 SPACES,
            w_saluni                  USING "--,---,--&.&&",4 SPACES,
            imp1.userid 

      -- Verifica si tiene descripcion
      {IF (LENGTH(imp1.observ)>51) THEN
         PRINT COLUMN 43,imp1.observ[51,100] 
      END IF
      IF (LENGTH(imp1.observ)>100) THEN
         PRINT COLUMN 43,imp1.observ[101,150]
      END IF}
   END IF

  AFTER GROUP OF imp1.cditem
   -- Verificando si hubieron movimiento 
   IF haymov THEN
    PRINT linea 

    -- Totalizando
    PRINT COLUMN   1,"Existencia del Producto ...",
          COLUMN  95,w_cartou                  USING "##,###,##&.&&",1 SPACES,
                     w_abotou                  USING "##,###,##&.&&",1 SPACES,
                     w_saluni                  USING "--,---,--&.&&"
   ELSE
      PRINT COLUMN 1, "*** N O  E X I S T E N  M O V I M I E N T O S  R E G I S T R A D O S ***"
      PRINT linea 
   END IF 

{  ON LAST ROW
   -- Imprimiendo filtros
   SKIP 1 LINES
   PRINT "FILTROS"
   IF (LENGTH(fcodcat)>0) THEN
      PRINT "Categoria    : ",fcodcat
   END IF
   IF (LENGTH(fsubcat)>0) THEN
      PRINT "Subcategoria : ",fsubcat
   END IF
   IF (LENGTH(fcditem)>0) THEN
      PRINT "Producto     : ",fcditem
   END IF
}
END REPORT
FUNCTION invrep003_saldoant(codemp,codsuc,codbod,cditem,fecfin)
DEFINE codemp INT          ,
		 codsuc INT          ,
		 codbod INT          ,
		 cditem INT          ,
		 fecfin DATE         ,
		 saldo  DECIMAL(14,2)
	--DISPLAY codemp," - ",codsuc," - ",codbod," - ",cditem," - ",fecfin
		LET saldo=0
		SELECT SUM(opeuni)
		  INTO saldo
		  FROM inv_dtransac a
		 WHERE a.codemp = codemp
			AND a.codsuc = codsuc
			AND a.codbod = codbod
			AND a.cditem = cditem
			AND a.fecemi <= fecfin
			AND a.estado = "V"
			--AND a.codepq = 0

	IF saldo IS NULL THEN
		LET saldo = 0
	END IF

RETURN saldo
END FUNCTION
