{ 
Fecha    : Diciembre 2011        
Programo : Mynor Ramirez
Objetivo : Programa de criterios de seleccion para:
           Consulta/Anulacion/Impresion de movimientos de inventario
}

-- Definicion de variables globales 
GLOBALS "invglb001.4gl" 

-- Subrutina para consultar/anular/imprimir movimientos 

FUNCTION invqbx001_movimientos(operacion)
 DEFINE qrytext,qrypart     STRING,--CHAR(500),
        loop         SMALLINT,
        operacion           SMALLINT,
        runcmd       STRING,
        titmenu             CHAR(15)
        --msg   CHAR(80)

 -- Verificando operacion
 CASE (operacion)
  WHEN 1 LET titmenu  = " Consultar"
  WHEN 2 LET titmenu  = " Anular" 
 END CASE 

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Mostrando campo de motivo de anulacion 
 CALL f.setFieldHidden("formonly.motanl",0)
 CALL f.setElementHidden("labelb",0)

 -- Llenando combobox de tipo de movimiento
 CALL librut003_cbxtipomovxusuario(FGL_GETENV("LOGNAME"))

 -- Buscando datos 
 LET loop = TRUE 
 WHILE loop 
  -- Inicializando las variables 
  INITIALIZE qrytext,qrypart TO NULL
  LET int_flag = 0 
  CLEAR FORM

  -- Construyendo busqueda 
  CONSTRUCT BY NAME qrytext 
                 ON a.lnktra,
                    a.codbod,
                    a.tipmov,
                    a.fecemi,
                    a.codori,
                    a.coddes,
                    a.numrf1,
                    a.numrf2,
                    a.observ,
                    a.motanl, 
                    a.fecsis,
                    a.horsis,
                    a.userid,
                    a.estado,
                    a.aniotr,
                    a.mestra 
   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT CONSTRUCT
  END CONSTRUCT
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Preparando la busqueda 
  ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

  -- Creando la busqueda 
  LET qrypart = " SELECT a.lnktra,a.codemp,a.codsuc,a.codbod,a.fecemi ",
                " FROM  inv_mtransac a ",
                " WHERE EXISTS (SELECT b.userid FROM inv_permxbod b WHERE b.userid = 'sistemas'  AND b.codbod = a.codbod) ",
                " AND ", qrytext CLIPPED,
                " ORDER BY a.lnktra DESC ,a.fecemi DESC" 

  -- Declarando el cursor 
  PREPARE estqbe001 FROM qrypart
  DECLARE c_movtos SCROLL CURSOR WITH HOLD FOR estqbe001  
  OPEN c_movtos 
  FETCH FIRST c_movtos INTO w_mae_tra.lnktra
  IF (status = NOTFOUND) THEN
     INITIALIZE w_mae_tra.* TO NULL
     ERROR ""
     CALL fgl_winmessage(
     "Atencion",
     "No existen movimientos con el criterio seleccionado.",
     "stop") 
  ELSE
     ERROR "" 

     -- Desplegando datos 
     CALL invqbx001_datos()

     -- Fetchando movimientos 
     MENU titmenu 
      BEFORE MENU 
       -- Verificando tipo de operacion 
       CASE (operacion)
        WHEN 1 HIDE OPTION "Anular"
        WHEN 2 HIDE OPTION "Imprimir" 
       END CASE 
	    IF w_mae_tra.haytra = 0 AND (w_mae_tra.tipope = 2 OR 
          w_mae_tra.tipope = 3) AND w_mae_tra.estado <> "A" THEN
          SHOW OPTION "confirmar"
	    ELSE
          HIDE OPTION "confirmar"
	    END IF

      COMMAND "Siguiente"
       "Visualiza el siguiente movimiento en la lista."
       FETCH NEXT c_movtos INTO w_mae_tra.lnktra 
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas movimientos siguientes en lista.", 
           "information")
           FETCH LAST c_movtos INTO w_mae_tra.lnktra
       END IF 

       -- Desplegando datos 
       CALL invqbx001_datos()
	    IF w_mae_tra.haytra = 0 AND (w_mae_tra.tipope = 2 OR 
          w_mae_tra.tipope = 3) AND w_mae_tra.estado <> "A" THEN
          SHOW OPTION "confirmar"
	    ELSE
          HIDE OPTION "confirmar"
	    END IF

		ON ACTION confirmar
			CALL invqbx001_out_bodtra()
			INITIALIZE w_tra.* TO NULL
			CALL invqbx001_datos()
	    IF w_mae_tra.haytra = 0 AND (w_mae_tra.tipope = 2 OR 
          w_mae_tra.tipope = 3) AND w_mae_tra.estado <> "A" THEN
          SHOW OPTION "confirmar"
	    ELSE
          HIDE OPTION "confirmar"
	    END IF

      COMMAND "Anterior"
       "Visualiza el movimientos anterior en la lista."
       FETCH PREVIOUS c_movtos INTO w_mae_tra.lnktra
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas movimientos anteriores en lista.", 
           "information")
           FETCH FIRST c_movtos INTO w_mae_tra.lnktra
        END IF

       -- Desplegando datos 
       CALL invqbx001_datos()
	    IF w_mae_tra.haytra = 0 AND (w_mae_tra.tipope = 2 OR 
          w_mae_tra.tipope = 3) AND w_mae_tra.estado <> "A" THEN
          SHOW OPTION "confirmar"
	    ELSE
          HIDE OPTION "confirmar"
	    END IF

      COMMAND "Primero" 
       "Visualiza el primer movimiento en la lista."
       FETCH FIRST c_movtos INTO w_mae_tra.lnktra
        -- Desplegando datos 
        CALL invqbx001_datos()
	    IF w_mae_tra.haytra = 0 AND (w_mae_tra.tipope = 2 OR 
          w_mae_tra.tipope = 3) AND w_mae_tra.estado <> "A" THEN
          SHOW OPTION "confirmar"
	    ELSE
          HIDE OPTION "confirmar"
	    END IF

      COMMAND "Ultimo" 
       "Visualiza el ultimo movimiento en la lista."
       FETCH LAST c_movtos INTO w_mae_tra.lnktra
        -- Desplegando datos
        CALL invqbx001_datos()
	    IF w_mae_tra.haytra = 0 AND (w_mae_tra.tipope = 2 OR 
          w_mae_tra.tipope = 3) AND w_mae_tra.estado <> "A" THEN
          SHOW OPTION "confirmar"
	    ELSE
          HIDE OPTION "confirmar"
	    END IF

      COMMAND "Anular"
       "Permite anular el movimiento actual en pantalla."
       -- Verificando si movimiento ya fue anulado
       IF (w_mae_tra.estado="A") THEN
          CALL fgl_winmessage(
          "Atencion",
          "Movimiento ya fue anulado.",
          "stop")
          CONTINUE MENU 
		 ELSE
	--		 IF w_mae_tra.tipope = 2 AND w_mae_tra.haytra=1 THEN
	--		 	CALL fgl_winmessage( "Atencion", "Traslado ya confirmado, no puede anularlo.", "stop")
	--			CONTINUE MENU
	--		 END IF  
       END IF 

       -- Anulando movimiento
       IF invqbx001_anular() THEN
          CONTINUE MENU 
       END IF 

       -- Desplegando datos
       CALL invqbx001_datos()
	    IF w_mae_tra.haytra = 0 AND (w_mae_tra.tipope = 2 OR 
          w_mae_tra.tipope = 3) AND w_mae_tra.estado <> "A" THEN
          SHOW OPTION "confirmar"
	    ELSE
          HIDE OPTION "confirmar"
	    END IF

      COMMAND "Productos"
       "Permite visualizar el detalle completo de productos del movimiento."
       CALL inving001_verproductos(1,2)

       --Bitacora
      COMMAND "Bitacora"
      LET runcmd  =  'fglrun bitacora.42r ', 'segovia ', 13, ' ',
                     w_mae_tra.lnktra,' "',w_mae_tra.observ,'"'
      RUN runcmd

      COMMAND "Imprimir" 
       "Permite imprimir el movimiento actual en pantalla." 
       -- Verificando si movimiento ya fue anulado
       IF (w_mae_tra.estado="A") THEN
          CALL fgl_winmessage(
          "Atencion",
          "Movimiento ya fue anulado.",
          "stop")
          CONTINUE MENU
       END IF

       -- Imprimiendo reporte
       ERROR "Imprimiendo reporte ... por favor espere ..."
       LET reimpresion = TRUE 
       CALL invrpt001_reportemov()
       ERROR "" 

      COMMAND "Modificar"
       "Permitie modificar el moviiento actual en pantalla."
       -- Modificando mivimiento 
		 IF w_mae_tra.tipope = 2 AND w_mae_tra.haytra = 1 THEN
			 CALL box_valdato("Traslado confirmado. No se puede modificar")
		 ELSE
		 	 SELECT * INTO w_mae_tip.* FROM inv_tipomovs WHERE tipmov = w_mae_tra.tipmov
       	 CALL inving001_movimientos(2)
		 END IF

       -- Desplegando datos
       CALL invqbx001_datos()

      COMMAND "Consultar" 
       "Regresa a la pantalla de seleccion."
       EXIT MENU

      ON ACTION cancel 
       LET loop = FALSE 
       EXIT MENU 

      COMMAND KEY(F4,CONTROL-E)
       LET loop = FALSE 
       EXIT MENU
     END MENU
   END IF     
  CLOSE c_movtos
 END WHILE

 -- Inicializando datos 
 CALL inving001_inival(1)
END FUNCTION 

{ Subrutina para desplegar los datos del registro }

FUNCTION invqbx001_datos()
 DEFINE existe SMALLINT,
         runcmd       STRING,
        lbl0 VARCHAR(20)

 -- Desplegando datos 
 CLEAR FORM

 -- Obteniendo datos del movimiento
 CALL librut003_bmovimiento(w_mae_tra.lnktra)
 RETURNING w_mae_tra.*,existe

 -- Obteniendo datos de la empresa
 CALL librut003_bempresa(w_mae_tra.codemp)
 RETURNING w_mae_emp.*,existe

 -- Obteniendo datos de la sucursal
 CALL librut003_bsucursal(w_mae_tra.codsuc)
 RETURNING w_mae_suc.*,existe

 -- Obtiendo nombre del estado
 LET nomest = librut001_valores(w_mae_tra.estado)

 -- Obteniendo datos del origen
 CALL librut003_bproveedor(w_mae_tra.codori)
 RETURNING w_mae_prv.*,existe
 LET nomori = w_mae_prv.nomprv

 -- Obteniendo datos del origen
 CALL librut003_bcliente(w_mae_tra.coddes)
 RETURNING w_mae_cli.*,existe
 LET nomdes = w_mae_cli.nomcli

 -- Desplegando datos del movimiento 
 CLEAR FORM 
 DISPLAY BY NAME w_mae_tra.codbod,w_mae_tra.tipmov, 
                 w_mae_tra.numrf1,w_mae_tra.numrf2, 
                 w_mae_tra.aniotr,w_mae_tra.mestra,
                 w_mae_tra.observ,w_mae_tra.estado,
                 w_mae_tra.fecemi,w_mae_tra.fecsis,
                 w_mae_tra.horsis,w_mae_tra.userid,
                 w_mae_emp.nomemp,w_mae_suc.nomsuc,
                 nomest          ,w_mae_tra.motanl,
                 w_mae_tra.lnktra--,w_mae_tra.numord 
IF w_mae_tra.tipope > 1 THEN
	IF w_mae_tra.estado ="A" THEN
		LET lbl0 ="       ANULADO    "
      DISPLAY BY NAME lbl0 ATTRIBUTES (RED)
	ELSE
   	IF w_mae_tra.haytra = 0 THEN
      	LET lbl0 = "    SIN CONFIRMAR"
      	DISPLAY BY NAME lbl0 ATTRIBUTES (RED)
   	ELSE
      	LET lbl0 = "     CONFIRMADO"
      	DISPLAY BY NAME lbl0 ATTRIBUTES(RED)
   	END IF
	END IF
ELSE
      LET lbl0 = NULL
      DISPLAY BY NAME lbl0
END IF
   -- Escondiendo campos de acuerdo al tipo de movimiento 
 CASE (w_mae_tra.tipope) 
  WHEN 1 DISPLAY BY NAME w_mae_tra.codori,nomori 
  WHEN 0 DISPLAY BY NAME w_mae_tra.coddes,nomdes 
  WHEN 2 DISPLAY BY NAME w_mae_tra.coddes,nomdes 
  WHEN 3 DISPLAY BY NAME w_mae_tra.coddes,nomdes 
 END CASE 

 -- Seleccionando detalle del movimiento 
 CALL invqbx001_detalle()
END FUNCTION 

{ Subrutina para seleccionar datos del detalle }

FUNCTION invqbx001_detalle()
 DEFINE existe  SMALLINT

 -- Inicializando vector de productos
 CALL inving001_inivec()
 
 -- Seleccionando detalle del movimiento 
 DECLARE cdet CURSOR FOR
 SELECT x.codabr,
        y.dsitem,
        x.codepq,
        z.nomepq,
        x.canepq,
        x.canuni,
        x.canmtc,
        x.preuni,
        x.totpro,
        x.lnkord,
        x.nuitem
 --SELECT x.codabr,
        --y.dsitem,
        --x.codepq,
        --z.nomepq,
        --x.canepq, 
        --x.canuni,
        --x.canmtc,
        --x.preuni,
        --x.totpro,
        --x.correl,
        --y.cditem
  FROM  inv_dtransac x, inv_products y, inv_epqsxpro z
  WHERE (x.lnktra = w_mae_tra.lnktra)
    AND (y.cditem = x.cditem)
    AND (z.cditem = x.cditem)
    AND (z.codepq = x.codepq) 
  ORDER BY x.correl 

  LET totlin = 1 
  FOREACH cdet INTO v_products[totlin].*
	IF v_products[totlin].dsitem MATCHES "*MEDIDA*" AND
      v_products[totlin].lnkord IS NOT NULL AND
      v_products[totlin].nuitem IS NOT NULL THEN
		SELECT dsitem INTO v_products[totlin].dsitem
		  FROM productos
		 WHERE productos.cditem = v_products[totlin].lnkord
			AND productos.tipo   = v_products[totlin].nuitem
	END IF
   -- Acumulando contador
   LET totlin = (totlin+1) 
  END FOREACH
  CLOSE cdet
  FREE  cdet
  LET totlin = (totlin-1)  

  -- Despelgando datos del detalle
  DISPLAY ARRAY v_products TO l_products.* ATTRIBUTE(BLUE)
   BEFORE DISPLAY
    EXIT DISPLAY
  END DISPLAY 

  -- Desplegando totales
  CALL inving001_totdet() 
END FUNCTION 

-- Subrutina para anular el movimiento 

FUNCTION invqbx001_anular()
 DEFINE loop,regreso SMALLINT,
        msg          STRING ,
		  numref       VARCHAR(40)

 -- Ingresando motivo de la anulacion
 LET loop = TRUE
 WHILE loop
  LET regreso = FALSE
  INPUT BY NAME w_mae_tra.motanl  
   ATTRIBUTES (UNBUFFERED) 
   ON ACTION cancel
    -- Salida
    LET regreso = TRUE
    LET loop    = FALSE
    LET w_mae_tra.motanl = NULL
    CLEAR motanl 
    EXIT INPUT 
   AFTER FIELD motanl
    -- Verificando motivo
    IF (LENGTH(w_mae_tra.motanl)<=0) THEN
       ERROR "Error: debe ingresarse el motivo de la anulacion, VERIFICA ..."
       NEXT FIELD motanl
    END IF 
  END INPUT 
  IF regreso THEN     
     EXIT WHILE
  END IF 

  -- Verificando anulacion
  IF NOT librut001_yesornot("Confirmacion",
                            "Desea Anular el Movimiento ?",
                            "Si",
                            "No",
                            "question") THEN
     LET regreso = TRUE 
     EXIT WHILE
  END IF 

  -- Anulando movimiento 
  LET loop = FALSE 
  ERROR " Anulando Movimiento ..." ATTRIBUTE(CYAN)

  -- Iniciando Transaccion
  BEGIN WORK
   TRY --NAP
   -- Marcando maestro del movimiento como anulado 
   SET LOCK MODE TO WAIT 
   DECLARE c_anul CURSOR FOR
   SELECT a.*
    FROM  inv_mtransac a
    WHERE (a.lnktra = w_mae_tra.lnktra) 
    FOR UPDATE 
    FOREACH c_anul 
     UPDATE inv_mtransac
     SET    inv_mtransac.estado = "A",
            inv_mtransac.motanl = w_mae_tra.motanl, 
            inv_mtransac.usranl = USER,
            inv_mtransac.fecanl = CURRENT,
            inv_mtransac.horanl = CURRENT
     WHERE CURRENT OF c_anul
    END FOREACH
    CLOSE c_anul
    FREE  c_anul

	IF w_mae_tra.tipope =2 AND w_mae_tra.haytra = 1 THEN
	  --LET numref = "*:*",w_mae_tra.lnktra using "<<<","*"
     LET numref = "*:*",w_mae_tra.lnktra using "<<<<<<<","*"
     UPDATE inv_mtransac
     SET    inv_mtransac.estado = "A",
            inv_mtransac.motanl = "Anulo Traslado; "|| w_mae_tra.motanl,
				inv_mtransac.numrf1 = w_mae_tra.numrf1||" - ANULADO",
				inv_mtransac.numrf2 = w_mae_tra.numrf2||" - ANULADO",
            inv_mtransac.usranl = USER,
            inv_mtransac.fecanl = CURRENT,
            inv_mtransac.horanl = CURRENT
     WHERE UPPER(numrf1) MATCHES numref
       AND numrf2        = w_mae_tra.numrf1
	END IF
   CATCH --NAP
      CALL errorlog(sqlca.sqlcode||"-"||err_get(sqlca.sqlcode))
      ROLLBACK WORK 
      CALL fgl_winmessage(
          "Atencion",
          "Hubo un error al intentar anular movimiento, consulte a sistemas.",
          "stop")
      RETURN FALSE 
   END TRY --NAP
   -- Finalizando Transaccion
   COMMIT WORK
   CALL fgl_winmessage(
          "Atencion",
          "Movimiento anulado exitosamente!",
          "information")
   ERROR ""
 END WHILE 

 RETURN regreso 
END FUNCTION 

FUNCTION invqbx001_traslado(operacion)
DEFINE   w_tra  RECORD LIKE inv_mtransac.*,
			w_bod  RECORD LIKE inv_mbodegas.*,
			w_bod1 RECORD LIKE inv_mbodegas.*,
			w_ord  RECORD LIKE inv_ordentra.*,
			bodnom VARCHAR(50),
			nomori VARCHAR(50),
			codori INTEGER, -- Codigo de origen  (PROVEEDOR)
			coddes INTEGER, -- Codigo de destino (CLIENTE)
			opc    SMALLINT,
			operacion SMALLINT

	INITIALIZE coddes,codori,nomori,w_tra.*,w_bod.*,w_bod1.*,w_ord.* TO NULL 
   LET w_tra.* = w_mae_tra.*
	CASE operacion
		WHEN 1
   		SELECT * INTO w_bod1.* FROM inv_mbodegas WHERE codbod = w_mae_tra.codbod
			LET bodnom= "*BODEGA*TRASLADO*"
			LET nomori= "*",w_bod1.nombod CLIPPED,"*"
		
			SELECT inv_mbodegas.*
	  		INTO w_bod.*
	  		FROM inv_mbodegas
	 		WHERE nombod MATCHES bodnom

			SELECT inv_provedrs.codprv
	  		INTO codori 
	  		FROM inv_provedrs
	 		WHERE nomprv MATCHES nomori
		
			SELECT *
	  		INTO w_mae_tip.*
	  		FROM inv_tipomovs
	 		WHERE nommov ="A BODEGA TRASLADO"

			SELECT nombod
	  		INTO bodnom
	  		FROM inv_mbodegas
	 		WHERE codbod = w_tra.codbod

			LET w_mae_tra.numrf1 = "TEMPORAL: ",w_tra.lnktra USING "<<<<<<<" 
			LET flag_sa = FALSE
		WHEN 2 
			LET bodnom= "*BODEGA*TRASLADO*"
			LET nomori= "*",w_mae_cli.nomcli CLIPPED,"*"
		
			SELECT inv_mbodegas.*
	  		INTO w_bod.*
	  		FROM inv_mbodegas
	 		WHERE nombod MATCHES bodnom
		
			SELECT fac_clientes.codcli
	  		INTO coddes 
	  		FROM fac_clientes
	 		WHERE nomcli MATCHES nomori
		
			SELECT *
	  		INTO w_mae_tip.*
	  		FROM inv_tipomovs
	 		WHERE nommov ="SALIDA BODEGA TRASLADO"

			SELECT nombod
	  		INTO bodnom
	  		FROM inv_mbodegas
	 		WHERE codbod = w_tra.codbod

			LET w_mae_tra.numrf1 = "TRASLADO: ",w_tra.lnktra USING "<<<<<<<" 
			LET flag_sa = FALSE
		WHEN 3
   		SELECT * INTO w_bod1.* FROM inv_mbodegas WHERE codbod = w_mae_tra.codbod
			LET bodnom= "*",w_mae_cli.nomcli,"*"
			LET nomori= "*",w_bod1.nombod CLIPPED,"*"
			CALL box_valdato("Ingresando traslado a "||w_mae_cli.nomcli CLIPPED)
		
			SELECT inv_mbodegas.*
	  		INTO w_bod.*
	  		FROM inv_mbodegas
	 		WHERE nombod MATCHES bodnom

			SELECT inv_provedrs.codprv
	  		INTO codori 
	  		FROM inv_provedrs
	 		WHERE nomprv MATCHES nomori
		
			SELECT *
	  		INTO w_mae_tip.*
	  		FROM inv_tipomovs
	 		WHERE nommov ="INGRESO POR TRASLADO"

			SELECT nombod
	  		INTO bodnom
	  		FROM inv_mbodegas
	 		WHERE codbod = w_tra.codbod
			
			LET w_mae_tra.numrf1 = "TRASLADO: ",w_tra.lnktra USING "<<<<<<<" 
			LET flag_sa = TRUE
	END CASE
		{
		lnktra  7
		codemp  1
		codsuc  5
		codbod  1
		tipmov  100
		haytra  0
		codori  1
		coddes  0
		numrf1  1
		numrf2
		numord
		aniotr  2011
		mestra  2
		fecemi  02/02/2011
		tipope  1
		observ  INVENTARIO INICIAL
		estado  V
		motanl
		fecanl
		horanl
		usranl
		impres  N
		userid  ximena
		fecsis  26/01/2011
		horsis  10:17:18
		}
	LET w_mae_tra.codemp = w_bod.codemp
	LET w_mae_tra.codsuc = w_bod.codsuc
	LET w_mae_tra.codbod = w_bod.codbod
	LET w_mae_tra.tipmov = w_mae_tip.tipmov
	LET w_mae_tra.haytra = 0
	LET w_mae_tra.codori = codori
	LET w_mae_tra.coddes = coddes
	LET w_mae_tra.numrf2 = w_tra.numrf1
	LET w_mae_tra.numord = w_tra.numord
	LET w_mae_tra.aniotr = YEAR(TODAY)
	LET w_mae_tra.mestra = MONTH(TODAY)
	LET w_mae_tra.fecemi = CURRENT
	LET w_mae_tra.tipope = w_mae_tip.tipope
	LET w_mae_tra.observ = "TRASLADO ENTRE BODEGAS, DESDE: ",bodnom CLIPPED 
	LET w_mae_tra.estado = "V"
	LET w_mae_tra.motanl = NULL
	LET w_mae_tra.fecanl = NULL
	LET w_mae_tra.horanl = NULL
	LET w_mae_tra.usranl = NULL
	LET w_mae_tra.impres = "N"
	LET w_mae_tra.userid = FGL_GETENV("LOGNAME")
	LET w_mae_tra.fecsis = CURRENT
	LET w_mae_tra.horsis = CURRENT HOUR TO SECOND

  IF operacion = 3 THEN
     DISPLAY BY NAME w_mae_tra.codbod,w_mae_tra.tipmov,w_mae_tra.codori,w_mae_tra.numrf1,w_mae_tra.fecemi,w_mae_tra.observ,w_mae_tra.estado
  END IF

  IF operacion = 2 THEN
     LET opc = librut001_menugraba("Confirmacion",
                                   "Que desea hacer con el traslado?",
                                   "Confirmarlo?",
                                   "Pendiente?",
                                   "Cancelarlo?",
                                   "")
    CASE opc
      WHEN 0
		   DISPLAY BY NAME w_tra.codbod,w_tra.tipmov,w_tra.codori,w_tra.numrf1,w_tra.fecemi,w_tra.observ,w_tra.estado
         CALL fgl_winmessage(" Atencion","El Traslado Se Cancelara.","stop")
			 LET flag_sa = TRUE
          IF invqbx001_anular() THEN
             EXIT CASE 
          END IF
      WHEN 1
         CALL fgl_winmessage(" Atencion","Se Confirmo El Traslado.","information")
   
	      CALL inving001_grabar(1)
		   CALL invqbx001_update(w_tra.lnktra)
	   WHEN 2
			LET flag_sa = FALSE
		   DISPLAY BY NAME w_tra.codbod,w_tra.tipmov,w_tra.codori,w_tra.numrf1,w_tra.fecemi,w_tra.observ,w_tra.estado
         CALL fgl_winmessage(" Atencion","El Traslado Quedara Pendiente.","information")
    END CASE
  ELSE
   	LET flag_sa = FALSE
		CALL inving001_grabar(1)
  END IF

END FUNCTION

FUNCTION invqbx001_in_bodtra()
DEFINE   w_tra  RECORD LIKE inv_mtransac.*,
			w_bod  RECORD LIKE inv_mbodegas.*,
			w_bod1 RECORD LIKE inv_mbodegas.*,
			w_ord  RECORD LIKE inv_ordentra.*,
			bodnom VARCHAR(50),
			nomori VARCHAR(50),
			codori INTEGER, -- Codigo de origen  (PROVEEDOR)
			coddes INTEGER, -- Codigo de destino (CLIENTE)
			opc    SMALLINT,
			operacion SMALLINT

	     INITIALIZE coddes,codori,nomori,w_tra.*,w_bod.*,w_bod1.*,w_ord.* TO NULL 
        LET w_tra.* = w_mae_tra.*
   
	 		SELECT * INTO w_bod1.* FROM inv_mbodegas WHERE codbod = w_tra.codbod
			LET bodnom= "*BODEGA*TRASLADO*"
			LET nomori= "*",w_bod1.nombod CLIPPED,"*"
		
			SELECT inv_mbodegas.*
	  		INTO w_bod.*
	  		FROM inv_mbodegas
	 		WHERE nombod MATCHES bodnom
         IF STATUS <> 0 THEN
            CALL box_valdato("Error #1 en traslado, contacte soporte.")
            DISPLAY SQLERRMESSAGE
            SLEEP 2
            EXIT PROGRAM(0)
         END IF

			SELECT inv_provedrs.codprv
	  		INTO codori 
	  		FROM inv_provedrs
	 		WHERE nomprv MATCHES nomori
         IF SQLCA.SQLCODE  <> 0 THEN
            CALL box_valdato("Error #2 en traslado, contacte soporte.")
            DISPLAY SQLERRMESSAGE
            SLEEP 2
            EXIT PROGRAM(0)
         END IF
		
			SELECT *
	  		INTO w_mae_tip.*
	  		FROM inv_tipomovs
	 		WHERE nommov ="A BODEGA TRASLADO"
         AND   tipope =1
         IF SQLCA.SQLCODE <> 0 THEN
            CALL box_valdato("Error #3 en traslado, contacte soporte.")
            DISPLAY SQLERRMESSAGE
            SLEEP 2
            EXIT PROGRAM(0)
         END IF

			SELECT nombod
	  		INTO bodnom
	  		FROM inv_mbodegas
	 		WHERE codbod = w_tra.codbod
         IF SQLCA.SQLCODE <> 0 THEN
            CALL box_valdato("Error #4 en traslado, contacte soporte.")
            DISPLAY SQLERRMESSAGE
            SLEEP 2
            EXIT PROGRAM(0)
         END IF

			LET w_mae_tra.numrf1 = "TEMPORAL: ",w_tra.lnktra USING "<<<<<<<" 

	LET w_mae_tra.codemp = w_bod.codemp
	LET w_mae_tra.codsuc = w_bod.codsuc
	LET w_mae_tra.codbod = w_bod.codbod
	LET w_mae_tra.tipmov = w_mae_tip.tipmov
	LET w_mae_tra.haytra = 0
	LET w_mae_tra.codori = codori
	LET w_mae_tra.coddes = coddes
	LET w_mae_tra.numrf2 = w_tra.numrf1
	LET w_mae_tra.numord = w_tra.numord
	LET w_mae_tra.aniotr = YEAR(TODAY)
	LET w_mae_tra.mestra = MONTH(TODAY)
	LET w_mae_tra.fecemi = CURRENT
	LET w_mae_tra.tipope = w_mae_tip.tipope
	LET w_mae_tra.observ = "TRASLADO ENTRE BODEGAS, DESDE: ",bodnom CLIPPED 
	LET w_mae_tra.estado = "V"
	LET w_mae_tra.motanl = NULL
	LET w_mae_tra.fecanl = NULL
	LET w_mae_tra.horanl = NULL
	LET w_mae_tra.usranl = NULL
	LET w_mae_tra.impres = "N"
	LET w_mae_tra.userid = FGL_GETENV("LOGNAME")
	LET w_mae_tra.fecsis = CURRENT
	LET w_mae_tra.horsis = CURRENT HOUR TO SECOND

   LET confirma = 2
   CALL inving001_grabar(1)

END FUNCTION
FUNCTION invqbx001_out_bodtra()
DEFINE   w_tra  RECORD LIKE inv_mtransac.*,
			w_bod  RECORD LIKE inv_mbodegas.*,
			w_bod1 RECORD LIKE inv_mbodegas.*,
			w_ord  RECORD LIKE inv_ordentra.*,
			bodnom VARCHAR(50),
			nomori VARCHAR(50),
			codori INTEGER, -- Codigo de origen  (PROVEEDOR)
			coddes INTEGER, -- Codigo de destino (CLIENTE)
         opc    SMALLINT

         LET w_tra.* = w_mae_tra.*
         
			LET bodnom= "*BODEGA*TRASLADO*"
			LET nomori= "*",w_mae_cli.nomcli CLIPPED,"*"
		
			SELECT inv_mbodegas.*
	  		INTO w_bod.*
	  		FROM inv_mbodegas
	 		WHERE nombod MATCHES bodnom
         IF SQLCA.SQLCODE <> 0 THEN
            CALL box_valdato("Error #4 en traslado, contacte soporte.")
            DISPLAY SQLERRMESSAGE
            SLEEP 2
            EXIT PROGRAM(0)
         END IF
		
			SELECT fac_clientes.codcli
	  		INTO coddes 
	  		FROM fac_clientes
	 		WHERE nomcli MATCHES nomori
         IF SQLCA.SQLCODE <> 0 THEN
            CALL box_valdato("Error #5 en traslado, contacte soporte.")
            DISPLAY SQLERRMESSAGE
            SLEEP 2
            EXIT PROGRAM(0)
         END IF
		
			SELECT *
	  		INTO w_mae_tip.*
	  		FROM inv_tipomovs
	 		WHERE nommov ="SALIDA BODEGA TRASLADO"
         IF SQLCA.SQLCODE <> 0 THEN
            CALL box_valdato("Error #6 en traslado, contacte soporte.")
            DISPLAY SQLERRMESSAGE
            SLEEP 2
            EXIT PROGRAM(0)
         END IF

			SELECT nombod
	  		INTO bodnom
	  		FROM inv_mbodegas
	 		WHERE codbod = w_tra.codbod
         IF SQLCA.SQLCODE <> 0 THEN
            CALL box_valdato("Error #7 en traslado, contacte soporte.")
            DISPLAY SQLERRMESSAGE
            SLEEP 2
            EXIT PROGRAM(0)
         END IF


         LET w_mae_tra.codemp = w_bod.codemp
         LET w_mae_tra.codsuc = w_bod.codsuc
         LET w_mae_tra.codbod = w_bod.codbod
         LET w_mae_tra.tipmov = w_mae_tip.tipmov
         LET w_mae_tra.haytra = 0
         LET w_mae_tra.codori = codori
         LET w_mae_tra.coddes = coddes
			LET w_mae_tra.numrf1 = "TRASLADO: ",w_tra.lnktra USING "<<<<<<<" 
         LET w_mae_tra.numrf2 = w_tra.numrf1
         LET w_mae_tra.numord = w_tra.numord
         LET w_mae_tra.aniotr = YEAR(TODAY)
         LET w_mae_tra.mestra = MONTH(TODAY)
         LET w_mae_tra.fecemi = CURRENT
         LET w_mae_tra.tipope = w_mae_tip.tipope
         LET w_mae_tra.observ = "TRASLADO ENTRE BODEGAS, DESDE: ",bodnom CLIPPED 
         LET w_mae_tra.estado = "V"
         LET w_mae_tra.motanl = NULL
         LET w_mae_tra.fecanl = NULL
         LET w_mae_tra.horanl = NULL
         LET w_mae_tra.usranl = NULL
         LET w_mae_tra.impres = "N"
         LET w_mae_tra.userid = FGL_GETENV("LOGNAME")
         LET w_mae_tra.fecsis = CURRENT
         LET w_mae_tra.horsis = CURRENT HOUR TO SECOND

         
   LET opc = librut001_menugraba("Confirmacion",
                                 "Que desea hacer con el traslado?",
                                 "Confirmarlo?",
                                 "Pendiente?",
                                 "Cancelarlo?",
                                 "")
    CASE opc
      WHEN 0
		   DISPLAY BY NAME w_tra.codbod,w_tra.tipmov,w_tra.codori,w_tra.numrf1,w_tra.fecemi,w_tra.observ,w_tra.estado
         CALL fgl_winmessage(" Atencion","El Traslado Se Cancelara.","stop")
         LET w_mae_tra.* = w_tra.*
          IF invqbx001_anular() THEN
             EXIT CASE 
          END IF
      WHEN 1
         LET confirma = 1
         LET flag_tras = 1
	      CALL inving001_grabar(1)
         LET w_mae_tra.* = w_tra.*
         CALL invqbx001_in_boddes()
		   CALL invqbx001_update(w_tra.lnktra)
         CALL fgl_winmessage(" Atencion","Se Confirmo El Traslado.","information")
	   WHEN 2
			LET flag_sa = FALSE
		   DISPLAY BY NAME w_tra.codbod,w_tra.tipmov,w_tra.codori,w_tra.numrf1,w_tra.fecemi,w_tra.observ,w_tra.estado
         CALL fgl_winmessage(" Atencion","El Traslado Quedara Pendiente.","information")
    END CASE         
         
END FUNCTION
FUNCTION invqbx001_in_boddes()
DEFINE   w_tra  RECORD LIKE inv_mtransac.*,
			w_bod  RECORD LIKE inv_mbodegas.*,
			w_bod1 RECORD LIKE inv_mbodegas.*,
			w_ord  RECORD LIKE inv_ordentra.*,
			bodnom VARCHAR(50),
			nomori VARCHAR(50),
			codori INTEGER, -- Codigo de origen  (PROVEEDOR)
			coddes INTEGER, -- Codigo de destino (CLIENTE)
         opc    SMALLINT

         LET w_tra.* = w_mae_tra.*
WHENEVER ERROR CONTINUE
         SELECT * INTO w_bod1.* FROM inv_mbodegas WHERE codbod = w_mae_tra.codbod
			LET bodnom= "*",w_mae_cli.nomcli CLIPPED,"*"
			LET nomori= "*",w_bod1.nombod CLIPPED,"*"
			CALL box_valdato("Ingresando traslado "||w_mae_cli.nomcli CLIPPED)
         SLEEP 2
		
			SELECT inv_mbodegas.*
	  		INTO w_bod.*
	  		FROM inv_mbodegas
	 		WHERE nombod MATCHES bodnom
         IF SQLCA.SQLCODE <> 0 THEN
            CALL box_valdato("Error #8 en traslado, contacte a soporte")
            DISPLAY SQLERRMESSAGE
            SLEEP 2
            EXIT PROGRAM (0)
         END IF

			SELECT inv_provedrs.codprv
	  		INTO codori 
	  		FROM inv_provedrs
	 		WHERE nomprv MATCHES nomori
         IF SQLCA.SQLCODE <> 0 THEN
            CALL box_valdato("Error #9 en traslado, contacte a soporte")
            DISPLAY SQLERRMESSAGE
            SLEEP 2
            EXIT PROGRAM (0)
         END IF
		
			SELECT *
	  		INTO w_mae_tip.*
	  		FROM inv_tipomovs
	 		WHERE nommov ="INGRESO POR TRASLADO"
         IF SQLCA.SQLCODE <> 0 THEN
            CALL box_valdato("Error #10 en traslado, contacte a soporte")
            DISPLAY SQLERRMESSAGE, " TIPO"
            SLEEP 2
            EXIT PROGRAM (0)
         END IF

			SELECT nombod
	  		INTO bodnom
	  		FROM inv_mbodegas
	 		WHERE codbod = w_tra.codbod
         IF SQLCA.SQLCODE <> 0 THEN
            CALL box_valdato("Error #11 en traslado, contacte a soporte")
            SLEEP 2
            EXIT PROGRAM (0)
         END IF
         
WHENEVER ERROR STOP
			LET w_mae_tra.numrf1 = "TRASLADO: ",w_tra.lnktra USING "<<<<<<<" 
         LET w_mae_tra.codemp = w_bod.codemp
         LET w_mae_tra.codsuc = w_bod.codsuc
         LET w_mae_tra.codbod = w_bod.codbod
         LET w_mae_tra.tipmov = w_mae_tip.tipmov
         LET w_mae_tra.haytra = 0
         LET w_mae_tra.codori = codori
         LET w_mae_tra.coddes = coddes
			LET w_mae_tra.numrf1 = "TRASLADO: ",w_tra.lnktra USING "<<<<<<<" 
         LET w_mae_tra.numrf2 = w_tra.numrf1
         LET w_mae_tra.numord = w_tra.numord
         LET w_mae_tra.aniotr = YEAR(TODAY)
         LET w_mae_tra.mestra = MONTH(TODAY)
         LET w_mae_tra.fecemi = CURRENT
         LET w_mae_tra.tipope = w_mae_tip.tipope
         LET w_mae_tra.observ = "TRASLADO ENTRE BODEGAS, DESDE: ",bodnom CLIPPED 
         LET w_mae_tra.estado = "V"
         LET w_mae_tra.motanl = NULL
         LET w_mae_tra.fecanl = NULL
         LET w_mae_tra.horanl = NULL
         LET w_mae_tra.usranl = NULL
         LET w_mae_tra.impres = "N"
         LET w_mae_tra.userid = FGL_GETENV("LOGNAME")
         LET w_mae_tra.fecsis = CURRENT
         LET w_mae_tra.horsis = CURRENT HOUR TO SECOND

         LET confirma = 2 
         CALL inving001_grabar(1)
         
END FUNCTION
FUNCTION invqbx001_update(lnktra)
DEFINE lnktra   LIKE inv_mtransac.lnktra

	UPDATE inv_mtransac SET haytra=1
	 WHERE inv_mtransac.lnktra = lnktra

END FUNCTION

FUNCTION invqbx001_cambia_mtc()
DEFINE --w_art RECORD LIKE inv_products.*,
		 i     SMALLINT--,
--		 flag  SMALLINT,
--		 canmtc DECIMAL(14,2)

	FOR i = 1 TO v_products.getLength()
		IF v_products[i].cditem is null then
			CALL v_products.deleteElement(i)
		ELSE
			LET v_products[i].canuni = 0
			LET v_products[i].canepq = 0 
		END IF
	END FOR


	DISPLAY ARRAY v_products TO l_products.*
		BEFORE ROW
			EXIT DISPLAY
	END DISPLAY 
END FUNCTION
