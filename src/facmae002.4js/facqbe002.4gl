{
facqbe002.4gl 
Mynor Ramirez
Mantenimiento de tipos de documento 
}

{ Definicion de variables globales }

GLOBALS "facglo002.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION facqbe002_tipos(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),    
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        qryres,qry          STRING,   
        msg                 STRING,
        rotulo              CHAR(12), 
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando operacion
  LET rotulo = NULL
  IF (operacion=3) THEN 
     LET rotulo = "Borrar" 
  END IF 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL facmae002_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.tipdoc,a.nomdoc,a.nomabr,a.exeimp,a.hayimp,a.numfor,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel
     -- Salida
     CALL facmae002_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.tipdoc,a.nomdoc ",
                 " FROM fac_tipodocs a ",
                 " WHERE ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_datos1 SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_tipodocs.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_datos1 INTO v_tipodocs[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_datos1
   FREE  c_datos1
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_tipodocs TO s_tipodocs.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL facmae002_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscando
      CALL facmae002_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF facmae002_tipos(2) THEN
         EXIT DISPLAY 
      ELSE 
         -- Desplegando datos
         CALL facqbe002_datos(v_tipodocs[ARR_CURR()].ttipdoc)
      END IF 

     ON KEY (CONTROL-M) 
      IF (operacion=2) THEN 
       -- Modificando 
       IF facmae002_tipos(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL facqbe002_datos(v_tipodocs[ARR_CURR()].ttipdoc)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF facqbe002_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este tipo de documento ya tiene movimientos, por favor VERIFICAR ...",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de "||rotulo CLIPPED||" este tipo de documento ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                   msg,
                                   "Si",
                                   "No",
                                   NULL,
                                   NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
            --  Eliminando
            CALL facmae002_grabar(3)
            EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte 
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Tipo" 
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Abreviado"
      LET arrcols[4] = "Exento Impuesto"
      LET arrcols[5] = "Requiere Impresion"
      LET arrcols[6] = "Formato Impresion"
      LET arrcols[7] = "Usuario Registro"
      LET arrcols[8] = "Fecha Registro"
      LET arrcols[9] = "Hora Registro" 

      -- Se aplica el mismo query de la seleccion para enviar al reporte 
      LET qry        = "SELECT a.tipdoc,a.nomdoc,a.nomabr,",
                              "CASE (a.exeimp) WHEN 1 THEN 'Si' WHEN 0 THEN 'No' END,",
                              "CASE (a.hayimp) WHEN 1 THEN 'Si' WHEN 0 THEN 'No' END,",
                              "a.numfor,a.userid,a.fecsis,a.horsis ",
                       " FROM fac_tipodocs a ",
                       " WHERE ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res = librut002_excelreport("Tipos de Documento",qry,10,1,arrcols)

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL facqbe002_datos(v_tipodocs[1].ttipdoc)
      ELSE 
         CALL facqbe002_datos(v_tipodocs[ARR_CURR()].ttipdoc)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen tipos de documento con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

-- Subrutina para desplegar los datos del mantenimiento 

FUNCTION facqbe002_datos(wtipdoc)
 DEFINE wtipdoc LIKE fac_tipodocs.tipdoc,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  fac_tipodocs a "||
              "WHERE a.tipdoc = "||wtipdoc||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_datos1t SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_datos1t INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.nomdoc THRU w_mae_pro.numfor 
  DISPLAY BY NAME w_mae_pro.tipdoc,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
 END FOREACH
 CLOSE c_datos1t
 FREE  c_datos1t

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomdoc THRU w_mae_pro.numfor 
 DISPLAY BY NAME w_mae_pro.tipdoc,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION 

-- Subrutina para verificar si el tipo de documento ya tiene movimientos 

FUNCTION facqbe002_integridad()
 DEFINE conteo SMALLINT

-- Verificando ventas
 SELECT COUNT(*)
  INTO  conteo
  FROM  fac_mtransac a
  WHERE (a.tipdoc = w_mae_pro.tipdoc)
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     -- Reoluciones 
     SELECT COUNT(*)
      INTO  conteo
      FROM  fac_impresol a
      WHERE (a.tipdoc = w_mae_pro.tipdoc)
      IF (conteo>0) THEN
         RETURN TRUE
      ELSE
         -- Tipos de documento x punto de venta
         SELECT COUNT(*)
          INTO  conteo
          FROM  fac_tdocxpos a
          WHERE (a.tipdoc = w_mae_pro.tipdoc)
          IF (conteo>0) THEN
             RETURN TRUE
          ELSE
             RETURN FALSE
          END IF 
      END IF
  END IF
END FUNCTION 
