{
facmae002.4gl 
Mynor Ramirez
Mantenimiento de tipos de documento 
}

-- Definicion de variables globales 

GLOBALS "facglo002.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("facmae002.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL facmae002_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION facmae002_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "facmae002a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais

  CALL librut001_header("facmae002",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Tipos de Documento"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Eliminar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de tipos de documento."
    CALL facqbe002_tipos(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo tipo de documento."
    LET savedata = facmae002_tipos(1) 
   COMMAND "Modificar"
    " Modificacion de un tipo de dcoumento existente."
    CALL facqbe002_tipos(2) 
   COMMAND "Borrar"
    " Eliminacion de un tipo de documento existente."
    CALL facqbe002_tipos(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION facmae002_tipos(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL facmae002_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nomdoc,
                w_mae_pro.nomabr,
                w_mae_pro.exeimp,
                w_mae_pro.hayimp,
                w_mae_pro.numfor
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE INPUT
    -- Verificando integridad
    -- Si tipo de docunmento ya tiene movimientos no se puede modificar nombre 
    IF (operacion=2) THEN -- Si es modificacion
     IF facqbe002_integridad() THEN
       CALL Dialog.SetFieldActive("nomdoc",FALSE)
     ELSE
        CALL Dialog.SetFieldActive("nomdoc",TRUE)
     END IF
    END IF 

   AFTER FIELD nomdoc  
    --Verificando nombre del tipo de documento
    IF (LENGTH(w_mae_pro.nomdoc)=0) THEN
       ERROR "Error: nombre invalido, VERIFICA"
       LET w_mae_pro.nomdoc = NULL
       NEXT FIELD nomdoc  
    END IF

    -- Verificando que no exista otro tipo de documento con el mismo nombre
    SELECT UNIQUE (a.tipdoc)
     FROM  fac_tipodocs a
     WHERE (a.tipdoc != w_mae_pro.tipdoc) 
       AND (a.nomdoc  = w_mae_pro.nomdoc) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro tipo de documento con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nomdoc
     END IF 

   AFTER FIELD nomabr 
    --Verificando nombre abreviado del tipo de documento
    IF (LENGTH(w_mae_pro.nomabr)=0) THEN
       ERROR "Error: nombre abreviado invalido, VERIFICA"
       LET w_mae_pro.nomabr = NULL
       NEXT FIELD nomabr  
    END IF

   AFTER FIELD exeimp 
    --Verificando exento de impuesto
    IF w_mae_pro.exeimp IS NULL THEN
       ERROR "Error: exento de impuesto invalido, VERIFICA ..."
       NEXT FIELD exeimp
    END IF

   AFTER FIELD hayimp 
    --Verificando impresion
    IF w_mae_pro.hayimp IS NULL THEN
       ERROR "Error: requiere impresion invalida, VERIFICA ..."
       NEXT FIELD hayimp
    END IF

   AFTER FIELD numfor 
    --Verificando formato 
    IF w_mae_pro.numfor IS NULL THEN
       ERROR "Error: formato invalido, VERIFICA ..."
       NEXT FIELD numfor 
    END IF

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.nomdoc IS NULL THEN 
       NEXT FIELD nomdoc
    END IF
    IF w_mae_pro.nomabr IS NULL THEN 
       NEXT FIELD nomabr
    END IF
    IF w_mae_pro.exeimp IS NULL THEN 
       NEXT FIELD exeimp
    END IF
    IF w_mae_pro.hayimp IS NULL THEN 
       NEXT FIELD hayimp
    END IF
    IF w_mae_pro.numfor IS NULL THEN 
       NEXT FIELD numfor
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL facmae002_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando 
    CALL facmae002_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL facmae002_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un tipo de documento

FUNCTION facmae002_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando tipo de documento ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.tipdoc),0)
    INTO  w_mae_pro.tipdoc
    FROM  fac_tipodocs a
    IF (w_mae_pro.tipdoc IS NULL) THEN
       LET w_mae_pro.tipdoc = 1
    ELSE 
       LET w_mae_pro.tipdoc = (w_mae_pro.tipdoc+1)
    END IF 

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO fac_tipodocs   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.tipdoc 

   --Asignando el mensaje 
   LET msg = "Tipo de Documento (",w_mae_pro.tipdoc USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE fac_tipodocs
   SET    fac_tipodocs.*        = w_mae_pro.*
   WHERE  fac_tipodocs.tipdoc = w_mae_pro.tipdoc 

   --Asignando el mensaje 
   LET msg = "Tipo de Documento (",w_mae_pro.tipdoc USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando tipos
   DELETE FROM fac_tipodocs 
   WHERE (fac_tipodocs.tipdoc = w_mae_pro.tipdoc)

   --Asignando el mensaje 
   LET msg = "Tipo de Documento (",w_mae_pro.tipdoc USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL facmae002_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION facmae002_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.tipdoc = 0 
   LET w_mae_pro.exeimp = 0
   LET w_mae_pro.hayimp = 1
   LET w_mae_pro.numfor = 1
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME")
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.tipdoc THRU w_mae_pro.numfor 
 DISPLAY BY NAME w_mae_pro.tipdoc,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION
