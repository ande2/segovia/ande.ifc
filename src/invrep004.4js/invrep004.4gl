{ 
Fecha    : Enero 2011 
Programo : invrep004.4gl 
Objetivo : Reporte de toma fisico de inventario
}

DATABASE segovia 

{ Definicion de variables globales }

DEFINE w_mae_bod RECORD LIKE inv_mbodegas.*,
       w_mae_emp RECORD LIKE glb_empresas.*,
       w_mae_suc RECORD LIKE glb_sucsxemp.*,
       w_datos   RECORD
        codbod   LIKE inv_mtransac.codbod,
        codemp   LIKE inv_mtransac.codemp,
        codsuc   LIKE inv_mtransac.codbod,
        codcat   LIKE inv_categpro.codcat,
        subcat   LIKE inv_subcateg.subcat
       END RECORD,
       pipeline          VARCHAR(100), 
       fnt       RECORD
        cmp      CHAR(12),
        nrm      CHAR(12),
        tbl      CHAR(12),
        fbl,t88  CHAR(12),
        t66,p12  CHAR(12),
        p10,srp  CHAR(12),
        twd      CHAR(12),
        fwd      CHAR(12),
        tda,fda  CHAR(12),
        ini      CHAR(12)
       END RECORD,
       existe    SMALLINT,
       filename  CHAR(100),
       fcodcat   STRING, 
       fsubcat   STRING 

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar13")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invrep004.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep004_tomafisica()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep004_tomafisica()
 DEFINE w_pro_bod         RECORD LIKE inv_proenbod.*,
        w_mae_pro         RECORD LIKE inv_products.*,
        imp1              RECORD
         codemp           LIKE inv_proenbod.codemp, 
         codsuc           LIKE inv_proenbod.codsuc, 
         codbod           LIKE inv_proenbod.codbod,
         cditem           LIKE inv_proenbod.cditem, 
         codabr           CHAR(20),
         dsitem           CHAR(50),
         nommed           CHAR(25),
         nomepq           CHAR(25)
        END RECORD,
		  w_extras 			  RECORD 
         codcat           INTEGER,
         subcat           INTEGER,
			codabr			  LIKE inv_medidpro.codabr
		  END RECORD,
        wpais             VARCHAR(255),
        qrytxt            STRING,
        strcodcat         STRING,
        strsubcat         STRING,
        strcditem         STRING,
        loop              SMALLINT

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep004a AT 5,2
  WITH FORM "invrep004a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invrep004",wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/invrep004.txt"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando  combobox de bodegas x usuario
  CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME"))
  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   LET pipeline = "" 
   CLEAR FORM

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codbod,
                 w_datos.codcat,
                 w_datos.subcat
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "pdf" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)

     -- Verificando datos 
     IF w_datos.codbod IS NULL THEN
        ERROR "Error: debe seleccionarse la bodega ..." 
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

	 ON ACTION imprimir_d
     -- Asignando dispositivo 
	  LET pipeline = "local"

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)

     -- Verificando datos 
     IF w_datos.codbod IS NULL THEN
        ERROR "Error: debe seleccionarse la bodega ..." 
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

    ON CHANGE codbod
     -- Obteniendo datos de la bodega
     CALL librut003_bbodega(w_datos.codbod)
     RETURNING w_mae_bod.*,existe

     -- Asignando datos de empresa y sucursal de la bodega
     LET w_datos.codemp = w_mae_bod.codemp
     LET w_datos.codsuc = w_mae_bod.codsuc

     -- Obteniendo datos de la empresa
     CALL librut003_bempresa(w_mae_bod.codemp)
     RETURNING w_mae_emp.*,existe
     -- Obteniendo datos de la sucursal
     CALL librut003_bsucursal(w_mae_bod.codsuc)
     RETURNING w_mae_suc.*,existe

     -- Desplegando datos de la empresa y sucursal
     DISPLAY BY NAME w_datos.codemp,w_datos.codsuc,w_mae_emp.nomemp,w_mae_suc.nomsuc

     -- Limpiando combos de categorias y subcategorias
     LET w_datos.codcat = NULL
     LET w_datos.subcat = NULL
     CLEAR codcat,subcat

    ON CHANGE codcat
     -- Limpiando combos
     LET w_datos.subcat = NULL
     CLEAR subcat

     -- Llenando combox de subcategorias
     IF w_datos.codcat IS NOT NULL THEN 
        CALL librut003_cbxsubcategorias(w_datos.codcat)
     END IF 

    AFTER FIELD codbod
     -- Verificando bodega 
     IF w_datos.codbod IS NULL THEN
        ERROR "Error: debe de seleccionarse la bodega a listar."
        NEXT FIELD codbod
     END IF

    AFTER INPUT
     -- Verificando datos
     IF w_datos.codbod IS NULL OR
        pipeline IS NULL THEN
        NEXT FIELD codbod
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Verificando seleccion de categoria
   LET strcodcat = NULL
   IF w_datos.codcat IS NOT NULL THEN
      LET strcodcat = "AND d.codcat = ",w_datos.codcat
   END IF

   -- Verificando condicion de subcategoria
   LET strsubcat = NULL
   IF w_datos.subcat IS NOT NULL THEN
      LET strsubcat = "AND e.subcat = ",w_datos.subcat
   END IF

   -- Construyendo seleccion 
   LET qrytxt = " SELECT a.codemp,a.codsuc,a.codbod,a.cditem,b.codabr,b.dsitem,c.nommed,'' ,d.codcat,e.subcat,g.codcol,f.codabr ", 
                  " FROM  inv_proenbod a,inv_products b,inv_unimedid c,inv_categpro d,inv_subcateg e, inv_medidpro f , inv_colorpro g ",
                  " WHERE a.codemp = ",w_datos.codemp,
                   " AND a.codsuc = ",w_datos.codsuc,
                   " AND a.codbod = ",w_datos.codbod,
                   " AND b.cditem = a.cditem ",
                   " AND b.codmed = f.codmed ",
                   " AND b.codcol = g.codcol ",
                   " AND c.unimed = b.unimed ",
                   " AND d.codcat = b.codcat ",
                   " AND e.codcat = b.codcat ",
                   " AND e.subcat = b.subcat ",
                   strcodcat CLIPPED," ",
                   strsubcat CLIPPED," ",
                   " ORDER BY 1,2,3,9,10,11,12 "

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep004 FROM qrytxt 
   DECLARE c_crep004 CURSOR FOR c_rep004
   LET existe = FALSE
   FOREACH c_crep004 INTO imp1.* , w_extras.*
    -- Iniciando reporte
    IF NOT existe THEN
       -- Seleccionando fonts para impresora epson
       CALL librut001_fontsprn(pipeline,"epson")
       RETURNING fnt.*

       LET existe = TRUE
       START REPORT invrep004_invtomafisica TO filename
    END IF 

    -- Obteniendo primer empaque 
    INITIALIZE imp1.nomepq TO NULL
    SELECT a.nomepq
     INTO  imp1.nomepq
     FROM  inv_epqsxpro a
     WHERE a.lnkepq = (SELECT MIN(b.lnkepq)
                        FROM  inv_epqsxpro b 
                        WHERE b.cditem = imp1.cditem
                          AND b.codepq >0)
      AND  a.cditem = imp1.cditem 

    -- Llenando reporte
    OUTPUT TO REPORT invrep004_invtomafisica(imp1.*)
   END FOREACH
   CLOSE c_crep004 
   FREE  c_crep004 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT invrep004_invtomafisica 

      -- Enviando reporte al destino seleccionado

		CASE pipeline
			WHEN "pdf"
				CALL librut001_rep_pdf("Toma de Inventario Fisico",filename,9,"P",4)
			WHEN "local"
      		CALL librut001_enviareporte(filename,pipeline,"Toma de Inventario Fisico")
      		ERROR "" 
      		CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
		END CASE
   ELSE
      ERROR "" 
      CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE
 CLOSE WINDOW wrep004a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT invrep004_invtomafisica(imp1)
 DEFINE imp1              RECORD
         codemp           LIKE inv_proenbod.codemp, 
         codsuc           LIKE inv_proenbod.codsuc, 
         codbod           LIKE inv_proenbod.codbod,
         cditem           LIKE inv_proenbod.cditem, 
         codabr           CHAR(20),
         dsitem           CHAR(45),
         nommed           CHAR(15),
         nomepq           CHAR(25)
        END RECORD,
        linea            VARCHAR(152)
  OUTPUT PAGE   LENGTH 68
         LEFT   MARGIN 0
         BOTTOM MARGIN 0
         TOP    MARGIN 0
         RIGHT  MARGIN 0

  FORMAT 
   PAGE HEADER
	 IF pipeline <> "local" THEN
      LET linea = "__________________________________________________",
                  "________________________________________________________"
	 ELSE
      LET linea = "__________________________________________________",
                  "______________________________________________"
	 END IF
                --"__________________________________________________"
                --"__________________________________________________"
                --"____________________________________"
   
    -- Configurando tipos de letra
    PRINT ASCII 27 -- color negro

    -- Imprimiendo Encabezado
    PRINT COLUMN   1,"Inventarios",
	  		 COLUMN  85,PAGENO USING "Pagina: <<<<"
	 IF pipeline = "pdf" THEN
       PRINT COLUMN   1,"Invrep004",
             COLUMN  30,"T O M A  D E  I N V E N T A R I O  F I S I C O",
             COLUMN  85,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
       PRINT COLUMN   1,"(",FGL_GETENV("LOGNAME") CLIPPED,")",
             COLUMN  85,"Hora  : ",TIME 
	 ELSE
       PRINT COLUMN   1,"Invrep004",
             COLUMN  35,"TOMA DE INVENTARIO FISICO",
             COLUMN  75,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
       PRINT COLUMN   1,"(",FGL_GETENV("LOGNAME") CLIPPED,")",
             COLUMN  75,"Hora  : ",TIME 
	 END IF 
    SKIP 1 LINES 
    PRINT COLUMN   1,fnt.cmp CLIPPED,"INVENTARIO REALIZADO EL ______________         TICKET __________ FACTURA __________ MOVIMIENTO __________" ,fnt.nrm CLIPPED
    PRINT linea  CLIPPED
	 IF pipeline <> "local" THEN
       PRINT "Codigo                Descripcion del Producto                   Unidad",
             " de Medida     E X I S T E N C I A"
       PRINT "Producto                                                               ",
             "               F I S I C A" 
	 ELSE
       PRINT "Codigo                Descripcion del Producto                   Unidad",
             " de Medida     EXISTENCIA"
       PRINT "Producto                                                               ",
             "               FISICA" 
	 END IF
    PRINT linea CLIPPED

   BEFORE GROUP OF imp1.codbod
    -- Imprimiendo datos de la bodega 
    PRINT "EMPRESA  (",imp1.codemp USING "<<<",") ",w_mae_emp.nomemp CLIPPED
    PRINT "SUCURSAL (",imp1.codsuc USING "<<<",") ",w_mae_suc.nomsuc CLIPPED
    PRINT "BODEGA   (",imp1.codbod USING "<<<",") ",w_mae_bod.nombod CLIPPED
    SKIP 1 LINES

   ON EVERY ROW
    -- Imprimiendo datos
    PRINT COLUMN   1,imp1.codabr                                 ,
          COLUMN  23,imp1.dsitem                                 ,2  SPACES,
                     imp1.nommed                          --       ,2  SPACES,
                     --"____________________"
    PRINT linea CLIPPED
    --SKIP 1 LINES 

   AFTER GROUP OF imp1.codbod
    -- Totalizando por bodega
    PRINT COLUMN   1,
                     "Total ",GROUP COUNT(*)    USING "<<<,<<#",
                     " Producto(s) en Bodega"
    SKIP 1 LINES 

END REPORT 
