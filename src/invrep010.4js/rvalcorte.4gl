{ 
Fecha    : Enero 2011
Programa : invrep007.4gl 
Objetivo : Reporte de existencias en bodega. 
}

GLOBALS "rvalcorte_glob.4gl"

{ Definicion de variables globales }
DEFINE handler om.SaxDocumentHandler

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar7")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("rvalcorte.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep007_existencias()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep007_existencias()
 DEFINE w_pro_bod         RECORD LIKE inv_proenbod.*,
        w_mae_pro         RECORD LIKE inv_products.*,
        formato VARCHAR(10)
 DEFINE imp1      RECORD
         codemp   LIKE inv_proenbod.codemp, 
         cditem   LIKE inv_proenbod.cditem, 
         codabr   CHAR(20),
         dsitem   VARCHAR(100),
         nommed   CHAR(20),
			totbod1  LIKE inv_proenbod.exican,
			totbod8  LIKE inv_proenbod.exican,
			totbod12 LIKE inv_proenbod.exican,
			totbod16 LIKE inv_proenbod.exican,
			totbod20 LIKE inv_proenbod.exican,
			totbod21 LIKE inv_proenbod.exican,
			totbod22 LIKE inv_proenbod.exican,
         exican   LIKE inv_proenbod.exican,
         pulcom   LIKE inv_proenbod.cospro
        END RECORD,
		  categ,subcat,codcol INTEGER,
		  codabr_med       LIKE inv_medidpro.codabr,
        wpais             VARCHAR(255),
        pipeline,qrytxt   STRING,
        cmbtext           STRING,
        streximin         STRING,
        streximax         STRING,
        strexiact         STRING,
        loop              SMALLINT,
        strcodbod2        STRING,
        strcodabr2        STRING,
        strcodcat2        STRING,
        strsubcat2        STRING,
        strfecemi2        STRING,
        strhorsis2        STRING,
        streximin2        STRING,
        streximax2        STRING,
        strexiact2        STRING, qry_cnt varchar(1000),contador integer

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep007a AT 5,2 
 WITH FORM "rvalcortea" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invrep007",wpais,1)
  CALL fgl_settitle("Inventario Valorizado a EXCEL")

  -- Definiendo archivo de impresion

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando  combobox de bodegas x usuario
  --CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME"))
  LET cmbtext = "SELECT a.codbod,a.nombod ",
                " FROM  inv_mbodegas a ",
                " WHERE EXISTS (SELECT b.userid FROM inv_permxbod b ",
                " WHERE b.codbod = a.codbod ",
                " AND b.userid = '",FGL_GETENV("LOGNAME") CLIPPED,"')",
 					 " AND bodcon = 0 ",
                " ORDER BY 2 "
  CALL librut002_combobox("codbod",cmbtext)

  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   LET w_datos.exiact = 3 
   LET w_datos.eximin = 3 
   LET w_datos.eximax = 3 
   LET w_datos.detrep = 2 
   CLEAR FORM

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codbod,
                 w_datos.codcat,
                 w_datos.subcat,
                 w_datos.codabr,
                 w_datos.fecemi
                 --w_datos.detrep
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "local" 
    IF w_datos.codbod IS NULL OR
        pipeline IS NULL THEN
		  ERROR "ERROR: Debe ingresar una bodega... VERIFIQUE"
        NEXT FIELD codbod
     END IF


     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)

     EXIT INPUT 

    ON CHANGE codbod
      --Obteniendo datos de la bodega
     CALL librut003_bbodega(w_datos.codbod)
     RETURNING w_mae_bod.*,existe

    AFTER INPUT 
     -- Verificando datos
	DISPLAY "w_datos. ",w_datos.codbod
     IF w_datos.codbod IS NULL OR
        pipeline IS NULL THEN
        NEXT FIELD codbod
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Verificando seleccion de bodega   
   LET strcodbod = NULL
   LET strcodbod2= NULL
   IF w_datos.codbod IS NOT NULL THEN
      LET strcodbod  = " AND a.codbod = ",w_datos.codbod
      LET strcodbod2 = " AND x.codbod = ",w_datos.codbod
   END IF

   -- Verificando seleccion de producto 
   LET strcodabr = NULL
   LET strcodabr2= NULL
   IF w_datos.codabr IS NOT NULL THEN
      LET strcodabr = " AND b.cditem = ",w_datos.codabr
      LET strcodabr2= " AND z.cditem = ",w_datos.codabr
   END IF

   -- Verificando seleccion de categoria
   LET strcodcat = NULL
   LET strcodcat2= NULL
   IF w_datos.codcat IS NOT NULL THEN
      LET strcodcat = " AND b.codcat = ",w_datos.codcat
      LET strcodcat2= " AND z.codcat = ",w_datos.codcat
   END IF

   -- Verificando condicion de subcategoria
   LET strsubcat = NULL
   LET strsubcat2= NULL
   IF w_datos.subcat IS NOT NULL THEN
      LET strsubcat = " AND b.subcat = ",w_datos.subcat
      LET strsubcat2= " AND z.subcat = ",w_datos.subcat
   END IF

   -- Verificando condicion de fecha
	IF w_datos.fecemi IS NOT NULL OR w_datos.horsis IS NOT NULL THEN
	LET strfecemi = NULL
		IF w_datos.fecemi IS NOT NULL AND (w_datos.horsis IS NULL OR w_datos.horsis = "00:00:00" ) THEN
      	LET strfecemi = " AND a.fecemi <= '",w_datos.fecemi CLIPPED,"'"
		ELSE	
			LET strfecemi = " AND LPAD(YEAR(a.fecemi),4,0)||LPAD(MONTH(a.fecemi),2,0)||LPAD(DAY(a.fecemi),2,0)||a.horsis <= '2011123123:00:00'"
   	END IF
	END IF
	
   LET strfecemi = NULL
   IF w_datos.fecemi IS NOT NULL THEN
      LET strfecemi = "AND a.fecemi <= '",w_datos.fecemi CLIPPED,"'"
   END IF

   -- Verificando condicion de hora
	LET strhorsis = NULL
   IF w_datos.horsis IS NOT NULL AND w_datos.horsis <> "00:00:00" THEN
      LET strhorsis = "AND a.horsis <= '",w_datos.horsis CLIPPED,"'"
   END IF
   
   LET existe = FALSE
	INITIALIZE imp1.* TO NULL

   LET qry_cnt =
        " SELECT   ",
            " count(a.codabr)   ",
        " FROM   ",
            " inv_dtransac AS a,  ",
            " inv_products AS b,  ",
            " inv_unimedid AS c   ",
        " WHERE   ",
            " a.cditem = b.cditem AND   ",
            " c.unimed = b.unimed AND   ",
            --" b.codmed = d.codmed  ",
         " a.estado ='V' ",
         " AND   a.codbod <> 21 ",
         strfecemi CLIPPED," ",
         strcodbod CLIPPED," ",
         strcodcat CLIPPED," ",
         strsubcat CLIPPED," ",
         strcodabr CLIPPED," "

   PREPARE cnt FROM qry_cnt
   EXECUTE cnt INTO contador

   IF contador=0 THEN
      CALL box_valdato("No hay ningun producto con existencia mayor a 0")
      DISPLAY "paso"
      CONTINUE WHILE
   END IF

   LET formato = rvalcorte_formato()

   IF formato IS NULL THEN
      CONTINUE WHILE
   END IF
   IF fgl_report_loadCurrentSettings("rvalcorte.4rp") THEN
            CALL fgl_report_selectDevice(formato)
            CALL fgl_report_selectPreview(1)
            LET handler = fgl_report_commitCurrentSettings()
   END IF

   IF HANDLER IS NOT NULL THEN
      CALL run_rvalcorte_to_handler(handler)
   END IF

  END WHILE
 CLOSE WINDOW wrep007a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT invrep007_impinvbod(imp1)
 DEFINE imp1      RECORD
         codemp   LIKE inv_proenbod.codemp, 
         cditem   LIKE inv_proenbod.cditem, 
         codabr   CHAR(20),
         dsitem   VARCHAR(100),
         nommed   CHAR(20),
			totbod1  LIKE inv_proenbod.exican,
			totbod8  LIKE inv_proenbod.exican,
			totbod12 LIKE inv_proenbod.exican,
			totbod16 LIKE inv_proenbod.exican,
			totbod20 LIKE inv_proenbod.exican,
			totbod21 LIKE inv_proenbod.exican,
			totbod22 LIKE inv_proenbod.exican,
         exican   LIKE inv_proenbod.exican,
         pulcom   LIKE inv_proenbod.cospro
        END RECORD,
        linea     VARCHAR(180),
        exis               ,
        lLines    SMALLINT ,
		  flag_lLocal SMALLINT
  OUTPUT LEFT   MARGIN  0
			RIGHT  MARGIN  0
         TOP    MARGIN  0 
         BOTTOM MARGIN  0 
			   PAGE LENGTH 41

  FORMAT 
   FIRST PAGE HEADER
	 LET lLines = 49
	 LET flag_lLocal = FALSE
    LET linea  = "__________________________________________________",
                 "__________________________________________________",
                 "__________________________________________________",
                 "__________________"--"________________________________"
                 --"____________________________________________"
	IF w_datos.detrep  = 2 THEN
	 LET lLines = 41
    LET linea  = "__________________________________________________",
                 "__________________________________________________",
                 "________________________________" 
	END IF

    -- Configurando tipos de letra
    --PRINT ASCII 27
    --PRINT fnt.ini CLIPPED

    -- Imprimiendo Encabezado
	 IF w_mae_bod.codbod IS NOT NULL AND w_mae_bod.codbod > 0 THEN
       PRINT COLUMN   1,ASCII 27,"Inventarios"--,
            -- COLUMN 137,PAGENO USING "Pagina: <<<<"
       PRINT COLUMN   1,"Invrep007",
             COLUMN  55,"EXISTENCIAS EN ",w_mae_bod.nombod,
             COLUMN 112,"Fecha : ",TODAY USING "dd/mmm/yyyy"
		 IF w_datos.fecemi IS NULL THEN
          PRINT COLUMN  83,"** AL DIA **",
                COLUMN 112,"Hora  : ",TIME
		 ELSE
          PRINT COLUMN   1,"Al: ",w_datos.fecemi USING "dd/mmm/yyyy",' - ',w_datos.horsis,
                COLUMN 112,"Hora  : ",TIME
		 END IF
	 ELSE
       PRINT COLUMN   1,ASCII 27,"Inventarios"--,
	     --COLUMN 137,PAGENO USING "Pagina: <<<<"
		IF w_datos.detrep = 1 THEN
       PRINT COLUMN   1,"Invrep007",
             COLUMN  80,"EXISTENCIAS GENERALES",
             COLUMN 147,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
       PRINT COLUMN  83,"** AL DIA **",
             COLUMN 147,"Hora  : ",TIME 
		ELSE
       PRINT COLUMN   1,"Invrep007",
             COLUMN  60,"EXISTENCIAS GENERALES",
             COLUMN 117,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
       PRINT COLUMN  63,"** AL DIA **",
             COLUMN 117,"Hora  : ",TIME 
		END IF
	 END IF
    PRINT linea 
	 IF w_datos.detrep = 2 THEN
    	PRINT "Codigo Prod.       Descripcion del Producto                       Unidad de ",
				"             Total          Ultimo     "
    	PRINT "                                                                   Medida   ",
				"            General         Precio     "
    	--PRINT "Codigo Prod.       Descripcion del Producto                                 Unidad de ",
				--"                         Total"
    	--PRINT "                                                                             Medida  ",
				--"                        General"
	 ELSE
    	PRINT "Codigo Prod.       Descripcion del Producto                 Unidad de ",
				"      Total       Total       Total        Total       Total      Total       Total        Total "
    	PRINT "                                                              Medida  ",
				"     Zona 17     Zona 09     2da  C.      Zona 11      Cenma     Conf 17     Traslado     General"
	 END IF
    PRINT linea
	 LET lLines = lLines - 7

	PAGE HEADER
	 LET lLines = 41
	   IF w_datos.detrep = 2 THEN
		   LET lLines = 40
    	   PRINT "Codigo Prod.       Descripcion del Producto                       Unidad de ",
				   "             Total          Ultimo     "
    	   PRINT "                                                                   Medida   ",
				   "            General         Precio     "
	   ELSE
    	  PRINT "Codigo Prod.       Descripcion del Producto                 Unidad de ",
				  "      Total       Total       Total        Total       Total      Total       Total        Total "
    	  PRINT "                                                              Medida  ",
				  "     Zona 17     Zona 09     2da  C.      Zona 11      Cenma     Conf 17     Traslado     General"
	   END IF
      PRINT linea
		LET lLines = lLines - 3


   ON EVERY ROW
    -- Imprimiendo productos
	 IF w_datos.detrep = 2 THEN
		 --IF flag_lLocal = FALSE THEN
       	PRINT COLUMN   1,imp1.codabr[1,18]                       CLIPPED,
             	COLUMN  20,imp1.dsitem[1,50]                       CLIPPED,
             	COLUMN  68,imp1.nommed[1,10]                       CLIPPED,
             	COLUMN  85,imp1.exican                 USING  "---,--&.&&",
               COLUMN 105,"Q. ",imp1.pulcom                 USING  "---,--&.&&"
				 	--COLUMN 106,w_fact.ser_fact                         CLIPPED,1 SPACES,
                          --w_fact.num_fact             USING        "<<<<&",
             	--COLUMN 117,w_tickt.ser_tickt                       CLIPPED,1 SPACES,
                          --w_tickt.num_tickt           USING       "<<<<&",
				 	--COLUMN 129,w_mov.lnk_mov               USING       "<<<<&"
			--LET flag_lLocal=TRUE
		--ELSE
       --PRINT COLUMN   1,imp1.codabr[1,18]                         CLIPPED,
             --COLUMN  20,imp1.dsitem[1,50]                         CLIPPED,
             --COLUMN  78,imp1.nommed[1,10]                         CLIPPED,
             --COLUMN 105,imp1.exican                   USING  "---,--&.&&"
--
		--END IF
 	 ELSE
       PRINT COLUMN   1,imp1.codabr[1,18]                       CLIPPED,
             COLUMN  20,imp1.dsitem[1,40]                       CLIPPED,
             COLUMN  62,imp1.nommed[1,8]                        CLIPPED,
			    COLUMN  73,imp1.totbod1                USING  "---,--&.&&",
			    COLUMN  85,imp1.totbod8                USING  "---,--&.&&",
			    COLUMN  97,imp1.totbod12               USING  "---,--&.&&",
			    COLUMN 109,imp1.totbod16               USING  "---,--&.&&",
			    COLUMN 121,imp1.totbod20               USING  "---,--&.&&",
			    COLUMN 133,imp1.totbod21               USING  "---,--&.&&",
			    COLUMN 145,imp1.totbod22               USING  "---,--&.&&",
             COLUMN 158,imp1.exican                 USING  "---,--&.&&"
	 END IF
	  LET lLines = lLines -1
	  IF lLines <= 1 THEN
		  SKIP TO TOP OF PAGE
	  END IF
	  --IF w_datos.detrep = 1 OR 

   ON LAST ROW
    -- Imprimiendo filtros
	 IF lLines >= 3 THEN
       PRINT "FILTROS"
       IF (LENGTH(fcodcat)>0) THEN
          PRINT "Categoria         : ",fcodcat
       END IF
       IF (LENGTH(fsubcat)>0) THEN
          PRINT "Subcategoria      : ",fsubcat
       END IF
	 END IF
	 IF lLines >= 1 THEN
       PRINT    "Existencia Actual : ",fexiact 
	 END IF
END REPORT 
FUNCTION invrep007_temptab(opc)
DEFINE opc INTEGER

	IF opc = 1 THEN
		CREATE TEMP TABLE totbod
		(
		codemp   INTEGER ,
		cditem   INTEGER ,
		codabr   CHAR(20),
		dsitem   VARCHAR(100),
		nommed   CHAR(20),
		totbod1  decimal(14,2),
		totbod8  decimal(14,2),
		totbod12 decimal(14,2),
		totbod16 decimal(14,2),
		totbod20 decimal(14,2),
		totbod21 decimal(14,2),
		totbod22 decimal(14,2),
		exican   DECIMAL(14,2),
      pulcom   DECIMAL(14,2)
		)
	ELSE
		DROP TABLE totbod
	END IF
END FUNCTION

FUNCTION rvalcorte_formato()
DEFINE forma VARCHAR(10)

	OPEN WINDOW rep WITH FORM "grw_form"
		LET forma = rvalcorte_elige()
	CLOSE WINDOW rep

RETURN forma
END FUNCTION

FUNCTION rvalcorte_elige()
DEFINE salida VARCHAR(10)

   --OPEN WINDOW rep WITH FORM "grw_form"
   LET salida = "PDF"
   INPUT BY NAME salida WITHOUT DEFAULTS

      ON ACTION ACCEPT
         LET INT_FLAG = FALSE
         LET salida = GET_FLDBUF(salida)
			EXIT INPUT

      ON KEY (ESCAPE)
         LET INT_FLAG = TRUE
         LET salida=""
			EXIT INPUT
      
      ON ACTION CANCEL
         LET INT_FLAG = TRUE
         LET salida=""
			EXIT INPUT
      
   END INPUT
   
--CLOSE FORM rep

RETURN salida
END FUNCTION
