{ 
Fecha    : Enero 2011
Programa : invrep007.4gl 
Objetivo : Reporte de existencias en bodega. 
}

GLOBALS "rvalcorte_glob.4gl"

{ Definicion de variables globales }
DEFINE handler om.SaxDocumentHandler

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar7")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("rvalcorte.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep007_existencias()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep007_existencias()
 DEFINE w_pro_bod         RECORD LIKE inv_proenbod.*,
        w_mae_pro         RECORD LIKE inv_products.*,
        formato VARCHAR(10)
 DEFINE imp1      RECORD
         codemp   LIKE inv_proenbod.codemp, 
         cditem   LIKE inv_proenbod.cditem, 
         codabr   CHAR(20),
         dsitem   VARCHAR(100),
         nommed   CHAR(20),
			totbod1  LIKE inv_proenbod.exican,
			totbod8  LIKE inv_proenbod.exican,
			totbod12 LIKE inv_proenbod.exican,
			totbod16 LIKE inv_proenbod.exican,
			totbod20 LIKE inv_proenbod.exican,
			totbod21 LIKE inv_proenbod.exican,
			totbod22 LIKE inv_proenbod.exican,
         exican   LIKE inv_proenbod.exican,
         pulcom   LIKE inv_proenbod.cospro
        END RECORD,
		  categ,subcat,codcol INTEGER,
		  codabr_med       LIKE inv_medidpro.codabr,
        wpais             VARCHAR(255),
        pipeline,qrytxt   STRING,
        cmbtext           STRING,
        streximin         STRING,
        streximax         STRING,
        strexiact         STRING,
        loop              SMALLINT,
        strcodbod2        STRING,
        strcodabr2        STRING,
        strcodcat2        STRING,
        strsubcat2        STRING,
        strfecemi2        STRING,
        strhorsis2        STRING,
        streximin2        STRING,
        streximax2        STRING,
        strexiact2        STRING

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep007a AT 5,2 
 WITH FORM "rvalcortea" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invrep007",wpais,1)
  CALL fgl_settitle("Inventario Valorizado a EXCEL")

  -- Definiendo archivo de impresion

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando  combobox de bodegas x usuario
  --CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME"))
  LET cmbtext = "SELECT a.codbod,a.nombod ",
                " FROM  inv_mbodegas a ",
                " WHERE EXISTS (SELECT b.userid FROM inv_permxbod b ",
                " WHERE b.codbod = a.codbod ",
                " AND b.userid = '",FGL_GETENV("LOGNAME") CLIPPED,"')",
 					 " AND bodcon = 0 ",
                " ORDER BY 2 "
  CALL librut002_combobox("codbod",cmbtext)

  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   LET w_datos.exiact = 3 
   LET w_datos.eximin = 3 
   LET w_datos.eximax = 3 
   LET w_datos.detrep = 2 
   CLEAR FORM

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codbod,
                 w_datos.codcat,
                 w_datos.subcat,
                 w_datos.codabr,
                 w_datos.fecemi
                 --w_datos.detrep
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "local" 
    IF w_datos.codbod IS NULL OR
        pipeline IS NULL THEN
		  ERROR "ERROR: Debe ingresar una bodega... VERIFIQUE"
        NEXT FIELD codbod
     END IF


     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)

     EXIT INPUT 

    ON CHANGE codbod
      --Obteniendo datos de la bodega
     CALL librut003_bbodega(w_datos.codbod)
     RETURNING w_mae_bod.*,existe

    AFTER INPUT 
     -- Verificando datos
	DISPLAY "w_datos. ",w_datos.codbod
     IF w_datos.codbod IS NULL OR
        pipeline IS NULL THEN
        NEXT FIELD codbod
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Verificando seleccion de bodega   
   LET strcodbod = NULL
   LET strcodbod2= NULL
   IF w_datos.codbod IS NOT NULL THEN
      LET strcodbod  = " AND a.codbod = ",w_datos.codbod
      LET strcodbod2 = " AND x.codbod = ",w_datos.codbod
   END IF

   -- Verificando seleccion de producto 
   LET strcodabr = NULL
   LET strcodabr2= NULL
   IF w_datos.codabr IS NOT NULL THEN
      LET strcodabr = " AND b.cditem = ",w_datos.codabr
      LET strcodabr2= " AND z.cditem = ",w_datos.codabr
   END IF

   -- Verificando seleccion de categoria
   LET strcodcat = NULL
   LET strcodcat2= NULL
   IF w_datos.codcat IS NOT NULL THEN
      LET strcodcat = " AND b.codcat = ",w_datos.codcat
      LET strcodcat2= " AND z.codcat = ",w_datos.codcat
   END IF

   -- Verificando condicion de subcategoria
   LET strsubcat = NULL
   LET strsubcat2= NULL
   IF w_datos.subcat IS NOT NULL THEN
      LET strsubcat = " AND b.subcat = ",w_datos.subcat
      LET strsubcat2= " AND z.subcat = ",w_datos.subcat
   END IF

   -- Verificando condicion de fecha
	IF w_datos.fecemi IS NOT NULL OR w_datos.horsis IS NOT NULL THEN
	LET strfecemi = NULL
		IF w_datos.fecemi IS NOT NULL AND (w_datos.horsis IS NULL OR w_datos.horsis = "00:00:00" ) THEN
      	LET strfecemi = " AND a.fecemi <= '",w_datos.fecemi CLIPPED,"'"
		ELSE	
			LET strfecemi = " AND LPAD(YEAR(a.fecemi),4,0)||LPAD(MONTH(a.fecemi),2,0)||LPAD(DAY(a.fecemi),2,0)||a.horsis <= '2011123123:00:00'"
{
      	LET strfecemi = " AND a.lnktra <= ( SELECT MAX(x.lnktra) FROM inv_dtransac x , inv_products z "||
								 " WHERE LPAD(YEAR(x.fecemi),4,0) ||LPAD(MONTH(x.fecemi),2,0) || LPAD(DAY(x.fecemi),2,0) || x.horsis  <= '",
                           YEAR(w_datos.fecemi) USING "&&&&", MONTH(w_datos.fecemi) USING "&&", DAY(w_datos.fecemi) USING "&&",
                           w_datos.horsis CLIPPED,"'",
								 " AND x.cditem = z.cditem ",
								 " AND a.cditem = x.cditem ",
								 " ",strcodbod2," ",strcodabr2," ",strcodcat2," ",strsubcat2," )"
}
   	END IF
	END IF
	
   LET strfecemi = NULL
   IF w_datos.fecemi IS NOT NULL THEN
      LET strfecemi = "AND a.fecemi <= '",w_datos.fecemi CLIPPED,"'"
   END IF

   -- Verificando condicion de hora
	LET strhorsis = NULL
   IF w_datos.horsis IS NOT NULL AND w_datos.horsis <> "00:00:00" THEN
      LET strhorsis = "AND a.horsis <= '",w_datos.horsis CLIPPED,"'"
   END IF
   
{
   -- Construyendo seleccion 
		LET qrytxt = " SELECT a.codemp , a.cditem, b.codabr , b.dsitem , c.nommed , ",
						 " b.codcat,b.subcat,b.codcol,d.codabr,nvl(sum(a.opeuni),0) ",
						 " FROM inv_dtransac a , inv_products b , inv_unimedid c , inv_medidpro d ",
						 " WHERE a.cditem = b.cditem ",
						 " AND   d.codmed = b.codmed ",
						 " AND   c.unimed = b.unimed ",
						 " AND   a.estado ='V' ",
						 " AND   a.codbod <> 21 ",
						 strfecemi CLIPPED," ",
						 strcodbod CLIPPED," ",
						 strcodcat CLIPPED," ",
						 strsubcat CLIPPED," ",
						 strcodabr CLIPPED," ",
						 " GROUP BY 1,2,3,4,5,6,7,8,9 ",
						 " ORDER BY 1,6,7,8,9 "

	IF FGL_GETENV("LOGNAME") = "sistemas" THEN
		DISPLAY qrytxt
	END IF

{
   	LET qrytxt = " SELECT a.codemp,a.cditem,b.codabr,b.dsitem,c.nommed, ", 
                   " SUM(a.exican) ",
                   " FROM  inv_proenbod a,inv_products b,inv_unimedid c,inv_categpro d,inv_subcateg e , inv_dtransac f",
                   " WHERE b.cditem = a.cditem ",
                   " AND c.unimed = b.unimed ",
                   " AND d.codcat = b.codcat ",
                   " AND e.codcat = b.codcat ",
                   " AND e.subcat = b.subcat ",
						 " AND f.cditem = b.cditem ",
                   " GROUP BY 1,2,3,4,5 ",
                   " ORDER BY 3 "
						 
	-- CREANDO TABLA TEMPORAL
	CALL invrep007_temptab(1)
	PREPARE c_rep007 FROM qrytxt
   DECLARE c_crep007 CURSOR FOR c_rep007
	FOREACH c_crep007 INTO imp1.codemp,imp1.cditem,imp1.codabr,imp1.dsitem,imp1.nommed,categ,subcat,codcol,codabr_med,imp1.exican
		IF imp1.dsitem MATCHES "*A LA MEDIDA*" THEN
			CONTINUE FOREACH
		END IF
		INITIALIZE imp1.totbod1  , imp1.totbod8  , imp1.totbod12 , imp1.totbod16 , 
                 imp1.totbod20 , imp1.totbod21 , imp1.totbod22 TO NULL
		IF w_datos.detrep = 1 AND
         w_datos.codbod IS NULL THEN
			SELECT NVL(exican,0)
			  INTO imp1.totbod1
			  FROM inv_proenbod
			 WHERE cditem=imp1.cditem
				AND codbod=1
			SELECT NVL(exican,0)
			  INTO imp1.totbod8
			  FROM inv_proenbod
			 WHERE cditem=imp1.cditem
				AND codbod=8
			SELECT NVL(exican,0)
			  INTO imp1.totbod12
			  FROM inv_proenbod
			 WHERE cditem=imp1.cditem
				AND codbod=12
			SELECT NVL(exican,0)
			  INTO imp1.totbod16
			  FROM inv_proenbod
			 WHERE cditem=imp1.cditem
				AND codbod=16
			SELECT NVL(exican,0)
			  INTO imp1.totbod20
			  FROM inv_proenbod
			 WHERE cditem=imp1.cditem
				AND codbod=20
			SELECT NVL(exican,0)
			  INTO imp1.totbod21
			  FROM inv_proenbod
			 WHERE cditem=imp1.cditem
				AND codbod=21
			SELECT NVL(exican,0)
			  INTO imp1.totbod22
			  FROM inv_proenbod
			 WHERE cditem=imp1.cditem
				AND codbod=22
			IF imp1.totbod1  IS NULL THEN LET imp1.totbod1 =0 END IF
			IF imp1.totbod8  IS NULL THEN LET imp1.totbod8 =0 END IF
			IF imp1.totbod12 IS NULL THEN LET imp1.totbod12=0 END IF
			IF imp1.totbod16 IS NULL THEN LET imp1.totbod16=0 END IF
			IF imp1.totbod20 IS NULL THEN LET imp1.totbod20=0 END IF
			IF imp1.totbod21 IS NULL THEN LET imp1.totbod21=0 END IF
			IF imp1.totbod22 IS NULL THEN LET imp1.totbod22=0 END IF
		ELSE
		END IF

		IF imp1.codemp IS NOT NULL AND
			imp1.cditem IS NOT NULL AND
			imp1.codabr IS NOT NULL AND
			imp1.dsitem IS NOT NULL AND
			imp1.nommed IS NOT NULL AND
			imp1.exican IS NOT NULL THEN
			INSERT INTO totbod VALUES (imp1.*)
		END IF


	END FOREACH
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
	LET hora = TIME
	LET hora2= hora
   LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/rvalcorte-",
                  hora2[1,2],"_",hora2[4,5],"_",hora2[7,8],".txt"
	DECLARE c_crep002 CURSOR FOR
		SELECT *
		  FROM totbod
		 --ORDER BY 3
}       
   LET existe = FALSE
	INITIALIZE imp1.* TO NULL

   LET formato = rvalcorte_formato()

   IF formato IS NULL THEN
      CONTINUE WHILE
   END IF
   IF fgl_report_loadCurrentSettings("rvalcorte.4rp") THEN
            CALL fgl_report_selectDevice(formato)
            CALL fgl_report_selectPreview(1)
            LET handler = fgl_report_commitCurrentSettings()
   END IF

   IF HANDLER IS NOT NULL THEN
      CALL run_rvalcorte_to_handler(handler)
   END IF
   {FOREACH c_crep002 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       -- Seleccionando fonts para impresora epson
       CALL librut001_fontsprn(pipeline,"epson")
       RETURNING fnt.*
       DISPLAY "pase"
       LET existe = TRUE
       START REPORT invrep007_impinvbod TO filename
    END IF 
      WHENEVER ERROR CONTINUE
      SELECT pulcom
        INTO imp1.pulcom
        FROM inv_products
       WHERE cditem = imp1.cditem
      WHENEVER ERROR STOP
      IF SQLCA.SQLCODE <> 0 THEN
         DISPLAY SQLCA.SQLCODE, " - ", SQLERRMESSAGE
         EXIT PROGRAM (1)
      END IF
         
    -- Llenando el reporte
      DISPLAY imp1.*
      OUTPUT TO REPORT invrep007_impinvbod(imp1.*)
   END FOREACH
   CLOSE c_crep002 
   FREE  c_crep002 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT invrep007_impinvbod 

      -- Enviando reporte al destino seleccionado
		IF w_datos.codbod IS NOT NULL THEN
			CALL librut001_rep_pdf("Inventario Por Bodega",filename,10,"L",4)
		ELSE
			IF w_datos.detrep = 1 THEN
				CALL librut001_rep_pdf("Inventario General",filename,8,"L",4)
			ELSE
				CALL librut001_rep_pdf("Inventario General",filename,10,"L",4)
			END IF
		END IF
   ELSE
      ERROR "" 
      CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
	-- ELIMINANDO TABLA TEMPORAL
	CALL invrep007_temptab(2)
}   
  END WHILE
 CLOSE WINDOW wrep007a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT invrep007_impinvbod(imp1)
 DEFINE imp1      RECORD
         codemp   LIKE inv_proenbod.codemp, 
         cditem   LIKE inv_proenbod.cditem, 
         codabr   CHAR(20),
         dsitem   VARCHAR(100),
         nommed   CHAR(20),
			totbod1  LIKE inv_proenbod.exican,
			totbod8  LIKE inv_proenbod.exican,
			totbod12 LIKE inv_proenbod.exican,
			totbod16 LIKE inv_proenbod.exican,
			totbod20 LIKE inv_proenbod.exican,
			totbod21 LIKE inv_proenbod.exican,
			totbod22 LIKE inv_proenbod.exican,
         exican   LIKE inv_proenbod.exican,
         pulcom   LIKE inv_proenbod.cospro
        END RECORD,
        linea     VARCHAR(180),
        exis               ,
        lLines    SMALLINT ,
		  flag_lLocal SMALLINT
  OUTPUT LEFT   MARGIN  0
			RIGHT  MARGIN  0
         TOP    MARGIN  0 
         BOTTOM MARGIN  0 
			   PAGE LENGTH 41

  FORMAT 
   FIRST PAGE HEADER
	 LET lLines = 49
	 LET flag_lLocal = FALSE
    LET linea  = "__________________________________________________",
                 "__________________________________________________",
                 "__________________________________________________",
                 "__________________"--"________________________________"
                 --"____________________________________________"
	IF w_datos.detrep  = 2 THEN
	 LET lLines = 41
    LET linea  = "__________________________________________________",
                 "__________________________________________________",
                 "________________________________" 
	END IF

    -- Configurando tipos de letra
    --PRINT ASCII 27
    --PRINT fnt.ini CLIPPED

    -- Imprimiendo Encabezado
	 IF w_mae_bod.codbod IS NOT NULL AND w_mae_bod.codbod > 0 THEN
       PRINT COLUMN   1,ASCII 27,"Inventarios"--,
            -- COLUMN 137,PAGENO USING "Pagina: <<<<"
       PRINT COLUMN   1,"Invrep007",
             COLUMN  55,"EXISTENCIAS EN ",w_mae_bod.nombod,
             COLUMN 112,"Fecha : ",TODAY USING "dd/mmm/yyyy"
		 IF w_datos.fecemi IS NULL THEN
          PRINT COLUMN  83,"** AL DIA **",
                COLUMN 112,"Hora  : ",TIME
		 ELSE
          PRINT COLUMN   1,"Al: ",w_datos.fecemi USING "dd/mmm/yyyy",' - ',w_datos.horsis,
                COLUMN 112,"Hora  : ",TIME
		 END IF
	 ELSE
       PRINT COLUMN   1,ASCII 27,"Inventarios"--,
	     --COLUMN 137,PAGENO USING "Pagina: <<<<"
		IF w_datos.detrep = 1 THEN
       PRINT COLUMN   1,"Invrep007",
             COLUMN  80,"EXISTENCIAS GENERALES",
             COLUMN 147,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
       PRINT COLUMN  83,"** AL DIA **",
             COLUMN 147,"Hora  : ",TIME 
		ELSE
       PRINT COLUMN   1,"Invrep007",
             COLUMN  60,"EXISTENCIAS GENERALES",
             COLUMN 117,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
       PRINT COLUMN  63,"** AL DIA **",
             COLUMN 117,"Hora  : ",TIME 
		END IF
	 END IF
    PRINT linea 
	 IF w_datos.detrep = 2 THEN
    	PRINT "Codigo Prod.       Descripcion del Producto                       Unidad de ",
				"             Total          Ultimo     "
    	PRINT "                                                                   Medida   ",
				"            General         Precio     "
    	--PRINT "Codigo Prod.       Descripcion del Producto                                 Unidad de ",
				--"                         Total"
    	--PRINT "                                                                             Medida  ",
				--"                        General"
	 ELSE
    	PRINT "Codigo Prod.       Descripcion del Producto                 Unidad de ",
				"      Total       Total       Total        Total       Total      Total       Total        Total "
    	PRINT "                                                              Medida  ",
				"     Zona 17     Zona 09     2da  C.      Zona 11      Cenma     Conf 17     Traslado     General"
	 END IF
    PRINT linea
	 LET lLines = lLines - 7

	PAGE HEADER
	 LET lLines = 41
	   IF w_datos.detrep = 2 THEN
		   LET lLines = 40
    	   PRINT "Codigo Prod.       Descripcion del Producto                       Unidad de ",
				   "             Total          Ultimo     "
    	   PRINT "                                                                   Medida   ",
				   "            General         Precio     "
	   ELSE
    	  PRINT "Codigo Prod.       Descripcion del Producto                 Unidad de ",
				  "      Total       Total       Total        Total       Total      Total       Total        Total "
    	  PRINT "                                                              Medida  ",
				  "     Zona 17     Zona 09     2da  C.      Zona 11      Cenma     Conf 17     Traslado     General"
	   END IF
      PRINT linea
		LET lLines = lLines - 3


   ON EVERY ROW
    -- Imprimiendo productos
	 IF w_datos.detrep = 2 THEN
		 --IF flag_lLocal = FALSE THEN
       	PRINT COLUMN   1,imp1.codabr[1,18]                       CLIPPED,
             	COLUMN  20,imp1.dsitem[1,50]                       CLIPPED,
             	COLUMN  68,imp1.nommed[1,10]                       CLIPPED,
             	COLUMN  85,imp1.exican                 USING  "---,--&.&&",
               COLUMN 105,"Q. ",imp1.pulcom                 USING  "---,--&.&&"
				 	--COLUMN 106,w_fact.ser_fact                         CLIPPED,1 SPACES,
                          --w_fact.num_fact             USING        "<<<<&",
             	--COLUMN 117,w_tickt.ser_tickt                       CLIPPED,1 SPACES,
                          --w_tickt.num_tickt           USING       "<<<<&",
				 	--COLUMN 129,w_mov.lnk_mov               USING       "<<<<&"
			--LET flag_lLocal=TRUE
		--ELSE
       --PRINT COLUMN   1,imp1.codabr[1,18]                         CLIPPED,
             --COLUMN  20,imp1.dsitem[1,50]                         CLIPPED,
             --COLUMN  78,imp1.nommed[1,10]                         CLIPPED,
             --COLUMN 105,imp1.exican                   USING  "---,--&.&&"
--
		--END IF
 	 ELSE
       PRINT COLUMN   1,imp1.codabr[1,18]                       CLIPPED,
             COLUMN  20,imp1.dsitem[1,40]                       CLIPPED,
             COLUMN  62,imp1.nommed[1,8]                        CLIPPED,
			    COLUMN  73,imp1.totbod1                USING  "---,--&.&&",
			    COLUMN  85,imp1.totbod8                USING  "---,--&.&&",
			    COLUMN  97,imp1.totbod12               USING  "---,--&.&&",
			    COLUMN 109,imp1.totbod16               USING  "---,--&.&&",
			    COLUMN 121,imp1.totbod20               USING  "---,--&.&&",
			    COLUMN 133,imp1.totbod21               USING  "---,--&.&&",
			    COLUMN 145,imp1.totbod22               USING  "---,--&.&&",
             COLUMN 158,imp1.exican                 USING  "---,--&.&&"
	 END IF
	  LET lLines = lLines -1
	  IF lLines <= 1 THEN
		  SKIP TO TOP OF PAGE
	  END IF
	  --IF w_datos.detrep = 1 OR 

   ON LAST ROW
    -- Imprimiendo filtros
	 IF lLines >= 3 THEN
       PRINT "FILTROS"
       IF (LENGTH(fcodcat)>0) THEN
          PRINT "Categoria         : ",fcodcat
       END IF
       IF (LENGTH(fsubcat)>0) THEN
          PRINT "Subcategoria      : ",fsubcat
       END IF
	 END IF
	 IF lLines >= 1 THEN
       PRINT    "Existencia Actual : ",fexiact 
	 END IF
END REPORT 
FUNCTION invrep007_temptab(opc)
DEFINE opc INTEGER

	IF opc = 1 THEN
		CREATE TEMP TABLE totbod
		(
		codemp   INTEGER ,
		cditem   INTEGER ,
		codabr   CHAR(20),
		dsitem   VARCHAR(100),
		nommed   CHAR(20),
		totbod1  decimal(14,2),
		totbod8  decimal(14,2),
		totbod12 decimal(14,2),
		totbod16 decimal(14,2),
		totbod20 decimal(14,2),
		totbod21 decimal(14,2),
		totbod22 decimal(14,2),
		exican   DECIMAL(14,2),
      pulcom   DECIMAL(14,2)
		)
	ELSE
		DROP TABLE totbod
	END IF
END FUNCTION

FUNCTION rvalcorte_formato()
DEFINE forma VARCHAR(10)

	OPEN WINDOW rep WITH FORM "grw_form"
		LET forma = rvalcorte_elige()
	CLOSE WINDOW rep

RETURN forma
END FUNCTION

FUNCTION rvalcorte_elige()
DEFINE salida VARCHAR(10)

   --OPEN WINDOW rep WITH FORM "grw_form"
   LET salida = "PDF"
   INPUT BY NAME salida WITHOUT DEFAULTS

      ON ACTION ACCEPT
         LET INT_FLAG = FALSE
         LET salida = GET_FLDBUF(salida)
			EXIT INPUT

      ON KEY (ESCAPE)
         LET INT_FLAG = TRUE
         LET salida=""
			EXIT INPUT
      
      ON ACTION CANCEL
         LET INT_FLAG = TRUE
         LET salida=""
			EXIT INPUT
      
   END INPUT
   
--CLOSE FORM rep

RETURN salida
END FUNCTION
