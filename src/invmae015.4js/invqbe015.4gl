{
invqbe015.4gl 
Mynor Ramirez
Mantenimiento de tipos de saldo
}

{ Definicion de variables globales }

GLOBALS "invglo015.4gl" 
DEFINE totlin,totusr INT,
       w             ui.Window,
       f             ui.Form

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe015_tiposald(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion           SMALLINT,
        titmenu             STRING,
        qryres,qry          STRING,
        msg                 STRING,
        rotulo              CHAR(12)

 -- Obteniendo datos de la window actual
 LET w = ui.Window.getCurrent()
 LET f = w.getForm()

  -- Verificando operacion
  CASE (operacion)
   WHEN 1 LET titmenu = " Buscar"
          LET qryres  = NULL
   WHEN 2 LET titmenu = " Modificar"
          LET qryres  = NULL  
          LET rotulo  = "Modify" 
   WHEN 3 LET titmenu = " Borrar"
          LET qryres  = NULL 
          LET rotulo  = "Borrar" 
  END CASE

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae015_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.tipsld,a.destip,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel       
     -- Salida
     CALL invmae015_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.tipsld,a.destip "||
                 " FROM inv_mtiposal a "||
                 " WHERE "||qrytext CLIPPED||
                 " ORDER BY 1 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_tiposald SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_tiposald.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_tiposald INTO v_tiposald[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_tiposald
   FREE  c_tiposald
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_tiposald TO s_tiposald.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel  
      -- Salida
      CALL invmae015_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar 
      -- Buscar 
      CALL invmae015_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF invmae015_tiposald(2) THEN
         EXIT DISPLAY 
      ELSE 
         -- Desplegando datos
         CALL invqbe015_datos(v_tiposald[ARR_CURR()].ttipsld)
      END IF 

     ON KEY (CONTROL-M)
      -- Modificando 
      IF (operacion=2) THEN 
       IF invmae015_tiposald(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL invqbe015_datos(v_tiposald[ARR_CURR()].ttipsld)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF invqbe015_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Este tipo de saldo ya tiene registros, por favor VERIFICAR ...",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de "||rotulo CLIPPED||" este tipo de saldo ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                    msg,
                                    "Si",
                                    "No",
                                    NULL,
                                    NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL invmae015_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Tipo Saldo"
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Usuario Registro"
      LET arrcols[4] = "Fecha Registro"
      LET arrcols[5] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = "SELECT a.* ",
                       " FROM inv_mtiposal a ",
                       " WHERE ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res        = librut002_excelreport("Tipos de Movimient",qry,5,1,arrcols)

     BEFORE ROW 
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL invqbe015_datos(v_tiposald[1].ttipsld)
      ELSE
         CALL invqbe015_datos(v_tiposald[ARR_CURR()].ttipsld)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen tipos de saldo con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

{ Subrutina para desplegar los datos del mantenimiento }

FUNCTION invqbe015_datos(wtipsld)
 DEFINE wtipsld LIKE inv_mtiposal.tipsld,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  inv_mtiposal a "||
              "WHERE a.tipsld = "||wtipsld||
              " ORDER BY 1 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_tiposaldt SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_tiposaldt INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.destip
  DISPLAY BY NAME w_mae_pro.tipsld,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
 END FOREACH
 CLOSE c_tiposaldt
 FREE  c_tiposaldt

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.destip
 DISPLAY BY NAME w_mae_pro.tipsld,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION 

-- Subrutina para verificar si el tipo de saldo ya tiene registros

FUNCTION invqbe015_integridad()
 DEFINE conteo SMALLINT

 -- Verificando 
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_dtiposal a
  WHERE (a.tipsld = w_mae_pro.tipsld) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     SELECT COUNT(*)
      INTO  conteo
      FROM  inv_saldopro a
      WHERE (a.tipsld = w_mae_pro.tipsld) 
      IF (conteo>0) THEN
         RETURN TRUE 
      ELSE
         RETURN FALSE
      END IF 
  END IF
END FUNCTION 
