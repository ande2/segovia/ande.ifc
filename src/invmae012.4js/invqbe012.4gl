{
invqbe012.4gl 
Mynor Ramirez
Mantenimiento de subcategorias 
}

{ Definicion de variables globales }

GLOBALS "invglo012.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe012_subcategorias(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion,scr,arr   SMALLINT,
        titmenu             STRING,
        qry                 STRING,
        msg                 STRING,
        rotulo              CHAR(12),
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando operacion
  LET rotulo = NULL
  IF (operacion=3) THEN 
     LET rotulo  = "Borrar" 
  END IF   

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae012_inival(1)
   CALL librut003_cbxcategorias() 
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codcat,a.subcat,a.nomsub,a.codabr,a.tipsub,a.lonmed,
                                a.premin,a.presug,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel  
     -- Salida
     CALL invmae012_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codcat,b.nomcat,a.subcat,a.nomsub,a.codabr ",
                 " FROM inv_subcateg a,inv_categpro b ",
                 " WHERE b.codcat = a.codcat AND ",qrytext CLIPPED,
                 " ORDER BY 2,3,4 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_subcategorias SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_sucsxemp.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_subcategorias INTO v_sucsxemp[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_subcategorias
   FREE  c_subcategorias
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_sucsxemp TO s_subcategorias.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel
      -- Salida
      CALL invmae012_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- buscar
      CALL invmae012_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF invmae012_subcategorias(2) THEN
         EXIT DISPLAY 
      ELSE 
         -- Desplegando datos
         CALL invqbe012_datos(v_sucsxemp[ARR_CURR()].tcodcat,
                              v_sucsxemp[ARR_CURR()].tsubcat)
      END IF 

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN
       -- Modificando 
       IF invmae012_subcategorias(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Desplegando datos
          CALL invqbe012_datos(v_sucsxemp[ARR_CURR()].tcodcat,
                               v_sucsxemp[ARR_CURR()].tsubcat)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF invqbe012_integridad() THEN
         LET msg = " Esta subcategoria ya existe en algun producto. \n Subcategoria no puede eliminarse."
         CALL fgl_winmessage(
         " Atencion",msg,"stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de "||rotulo CLIPPED||" esta subcategoria ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                  msg,
                                  "Si",
                                  "No",
                                  NULL,
                                  NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL invmae012_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Catagoria"
      LET arrcols[2] = "Subcategoria"
      LET arrcols[3] = "Nombre Subcategoria" 
      LET arrcols[4] = "Codigo Abreviado" 
      LET arrcols[5] = "Tipo Subcategoria" 
      LET arrcols[6] = "Lona a la Medida"
      LET arrcols[7] = "Precio Minimo" 
      LET arrcols[8] = "Precio Sugerido" 
      LET arrcols[9] = "Usuario Registro"
      LET arrcols[10]= "Fecha Registro"
      LET arrcols[11]= "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = "SELECT b.nomcat,a.subcat,a.nomsub,a.codabr,",
                              "CASE (a.tipsub) WHEN 1 THEN 'LOCAL' WHEN 0 THEN 'EXTERNA' END,",
                              "CASE (a.lonmed) WHEN 1 THEN 'SI' WHEN 0 THEN 'NO' END,",
                              "a.premin,a.presug,a.userid,a.fecsis,a.horsis ", 
                       " FROM inv_subcateg a,inv_categpro b ",
                       " WHERE b.codcat = a.codcat AND ",qrytext CLIPPED,
                       " ORDER BY 1,2 "

      -- Ejecutando el reporte
      LET res        = librut002_excelreport("Subcategoriaes",qry,11,1,arrcols)

     BEFORE ROW 
      -- Asignando contadores del vector
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()

      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL invqbe012_datos(v_sucsxemp[1].tcodcat,
                              v_sucsxemp[1].tsubcat)
      ELSE
         CALL invqbe012_datos(v_sucsxemp[ARR_CURR()].tcodcat,
                              v_sucsxemp[ARR_CURR()].tsubcat)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen subcategorias con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

{ Subrutina para desplegar los datos del mantenimiento }

FUNCTION invqbe012_datos(wcodcat,wsubcat)
 DEFINE wcodcat LIKE inv_subcateg.codcat,
        wsubcat LIKE inv_subcateg.subcat,
        existe  SMALLINT,
        qryres  STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  inv_subcateg a "||
              "WHERE a.subcat = "||wsubcat||
              " ORDER BY 1 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_subcategoriast SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_subcategoriast INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.nomsub,w_mae_pro.codabr,w_mae_pro.tipsub THRU w_mae_pro.presug
  DISPLAY BY NAME w_mae_pro.codcat,w_mae_pro.subcat,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
 END FOREACH
 CLOSE c_subcategoriast
 FREE  c_subcategoriast

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomsub,w_mae_pro.codabr,w_mae_pro.tipsub THRU w_mae_pro.presug
 DISPLAY BY NAME w_mae_pro.codcat,w_mae_pro.subcat,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION 

-- Subrutina para verificar si la subcategoria existe ya en algun producto

FUNCTION invqbe012_integridad()
 DEFINE conteo SMALLINT

 -- Verificando productos 
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_products a 
  WHERE (a.subcat = w_mae_pro.subcat)
  IF (conteo>0) THEN
     RETURN TRUE 
  ELSE
     RETURN FALSE 
  END IF
END FUNCTION 
