{
invqbe014.4gl 
Mynor Ramirez
Mantenimiento de empaques 

{ Definicion de variables globales }

GLOBALS "invglo014.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe014_empaques(operacion)

 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion,scr,arr   SMALLINT,
        qry                 STRING,
        msg                 STRING,
        rotulo              CHAR(12),
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando operacion
  LET rotulo = NULL
  IF (operacion=3) THEN
     LET rotulo = "Borrar" 
  END IF   

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Cargando combobox de unidade de medida
  CALL librut003_cbxunidadesmedida("unimed")

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae014_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codepq,a.nomepq,a.cantid,a.unimed,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL invmae014_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codepq,a.nomepq,a.cantid,b.nommed ",
                 " FROM inv_empaques a,inv_unimedid b ",
                 " WHERE b.unimed = a.unimed AND ",qrytext CLIPPED,
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_empaques SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_empaques.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_empaques INTO v_empaques[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_empaques
   FREE  c_empaques
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_empaques TO s_empaques.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL invmae014_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Buscar
      CALL invmae014_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Verificando integridad 
      LET res =  invqbe014_integridad()
      IF (res>0) THEN
         -- Evaluando mensaje
         CASE (res)
          WHEN 1 LET msg = " Este empaque ya fue asignado a algun producto. \n Empaque no puede modificarse."
          WHEN 2 LET msg = " Este empaque ya tiene movimientos registrados. \n Empaque no puede modificarse."
         END CASE 
      
         CALL fgl_winmessage(" Atencion",msg,"stop")
         CONTINUE DISPLAY 
      END IF 

      -- Modificando 
      IF invmae014_empaques(2) THEN
         EXIT DISPLAY 
      ELSE
         -- Desplegando datos
         CALL invqbe014_datos(v_empaques[ARR_CURR()].tcodepq)
      END IF 

     ON KEY (CONTROL-M)   
      -- Modificando 
      IF (operacion=2) THEN 
       -- Verificando integridad 
       LET res = invqbe014_integridad()
       IF (res>0) THEN
          -- Evaluando mensaje
          CASE (res)
           WHEN 1 LET msg = " Este empaque ya fue asignado a algun producto. \n Empaque no puede modificarse."
           WHEN 2 LET msg = " Este empaque ya tiene movimientos registrados. \n Empaque no puede modificarse."
          END CASE 
      
          CALL fgl_winmessage(" Atencion",msg,"stop")
          CONTINUE DISPLAY 
       END IF 

       -- Modificando
       IF invmae014_empaques(2) THEN
          EXIT DISPLAY 
       ELSE 
          -- Desplegando datos
          CALL invqbe014_datos(v_empaques[ARR_CURR()].tcodepq)
       END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      LET res =  invqbe014_integridad()
      IF (res>0) THEN
         -- Evaluando mensaje
         CASE (res)
          WHEN 1 LET msg = " Este empaque ya fue asignado a algun producto. \n Empaque no puede borrarse."
          WHEN 1 LET msg = " Este empaque ya tiene movimientos registrados. \n Empaque no puede borrarse."
         END CASE 
      
         CALL fgl_winmessage(" Atencion",msg,"stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Este SEGURO de "||rotulo CLIPPED||" este empaque ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                  msg,
                                  "Si",
                                  "No",
                                  NULL,
                                  NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL invmae014_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1] = "Empaque"
      LET arrcols[2] = "Nombre"
      LET arrcols[3] = "Cantidad"
      LET arrcols[4] = "Unidad Medida"
      LET arrcols[5] = "Nombre U/Medida"
      LET arrcols[6] = "Usuario Registro"
      LET arrcols[7] = "Fecha Registro"
      LET arrcols[8] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = "SELECT a.codepq,a.nomepq,a.cantid,a.unimed,b.nommed,a.userid,a.fecsis,a.horsis ",
                       " FROM inv_empaques a,inv_unimedid b ",
                       " WHERE b.unimed = a.unimed AND ",qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res        = librut002_excelreport("Empaques",qry,8,1,arrcols)

     BEFORE ROW 
      -- Asignando contadores del vector
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()

      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL invqbe014_datos(v_empaques[1].tcodepq)
      ELSE
         CALL invqbe014_datos(v_empaques[ARR_CURR()].tcodepq)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen empaques con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

{ Subrutina para desplegar los datos del mantenimiento }

FUNCTION invqbe014_datos(wcodepq)
 DEFINE wcodepq   LIKE inv_empaques.codepq,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  inv_empaques a "||
              "WHERE a.codepq = '"||wcodepq||"'"||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_empaquest SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_empaquest INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.nomepq THRU w_mae_pro.unimed
  DISPLAY BY NAME w_mae_pro.codepq,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
 END FOREACH
 CLOSE c_empaquest
 FREE  c_empaquest

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.nomepq THRU w_mae_pro.unimed
 DISPLAY BY NAME w_mae_pro.codepq,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION 

-- Subrutina para verificar si el empaque ya tiene movimientos  

FUNCTION invqbe014_integridad()
 DEFINE conteo SMALLINT

 -- Verificando productos
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_epqsxpro a
  WHERE (a.codepq = w_mae_pro.codepq) 
  IF (conteo>0) THEN
     RETURN 1
  ELSE
     -- Verificando transacciones
     SELECT COUNT(*)
      INTO  conteo
      FROM  inv_dtransac a
      WHERE (a.codepq = w_mae_pro.codepq) 
      IF (conteo>0) THEN
         RETURN 2
      ELSE
         RETURN 0 
      END IF 
  END IF 
END FUNCTION 
