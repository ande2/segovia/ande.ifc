drop view "sistemas".vis_ordentraint
;
create view "sistemas".vis_ordentraint (numpos,nombrepos,ordentrabajo,fechaorden,cliente,cantidad,clase,color,medidas,nombremedida,usuario,serie,documento,
                                        ordencompra,fechaofrecida,descripcion,hora) as
  select x0.numpos ,x5.nompos ,x0.lnkord ,x0.fecord ,x4.nomcli,
         x0.cantid ,x2.nomsub ,x3.nomcol,((x0.xlargo || ' X ' ) ||
         x0.yancho ) ,CASE WHEN (x0.medida = 1 )  THEN 'PIES'  WHEN
        (x0.medida = 0 )  THEN 'METROS'  END,  x4.usrope ,x4.nserie,
         x4.numdoc ,x4.ordcmp ,x0.fecofe ,x0.observ,x0.horsis
    from "sistemas".inv_ordentra x0 , "sistemas".fac_clientes x1 ,
         "sistemas".inv_subcateg x2 , "sistemas".inv_colorpro x3 ,
         "sistemas".fac_mtransac x4 , "sistemas".fac_puntovta x5 
   where ((((((x1.codcli = x0.codcli) and (x2.subcat = x0.subcat ))
     and (x3.codcol = x0.codcol))     and (x4.lnktra = x0.lnktra))
     and (x5.numpos = x0.numpos))     and (x0.tipord = 1)) ;
