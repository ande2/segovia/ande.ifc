{ 
invqbe010.4gl 
Mynor Ramirez
Mantenimiento de productos 
}

{ Definicion de variables globales }

GLOBALS "invglo010.4gl" 
DEFINE totlin,totepq,totexi INT,
       totpre               INT,
       w                    ui.Window,
       f                    ui.Form

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe010_productos(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        westado             LIKE inv_products.estado,
        wstatus             LIKE inv_products.status,
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion,scr,arr   SMALLINT,
        qry                 STRING,
        msg                 STRING,
        runcmd              STRING, 
        rotulo              CHAR(12)

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando si operacion es borrar
  LET rotulo = NULL
  IF (operacion=3) THEN
     LET rotulo = "Borrar" 
  END IF  

  -- Verificando si operacion es precios
  IF (operacion=4) THEN 
     -- Escondiendo y mostrando elementos de la ventana
     CALL f.setElementtext("labelf","Lista de Precios")
     CALL invqbe010_ventana(0,1)
  END IF 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae010_inival(1)
   LET int_flag = 0

   -- Cargando combobox de categorias
   CALL librut003_cbxcategorias()
   CALL librut003_cbxcolores()
   CALL librut003_cbxproveedores()
   CALL librut003_cbxunidadesmedida("unimed")
   CALL librut003_cbxmedidas()

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.cditem,a.codcat,a.subcat,a.codprv,a.codcol,a.codmed,a.dsitem,a.unimed,
                                a.codabr,a.premin,a.presug,a.estado,a.status,a.userid,a.fecsis,a.horsis   
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL invmae010_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

    AFTER FIELD codcat
     -- Cargando combobox de subcategorias
     IF (LENGTH(GET_FLDBUF(a.codcat))>0) THEN 
        CALL librut003_cbxsubcategorias(GET_FLDBUF(a.codcat)) 
     ELSE
        CLEAR subcat 
     END IF 
   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Verificando operacion
   IF (operacion<4) THEN 
    -- Creando la busqueda
    LET qrypart = " SELECT UNIQUE a.cditem,a.dsitem,a.codabr,a.estado,a.status ",
                  " FROM inv_products a ",
                  " WHERE ",qrytext CLIPPED,
                  " ORDER BY 2 "

    -- Declarando el cursor
    PREPARE cprod FROM qrypart
    DECLARE c_products SCROLL CURSOR WITH HOLD FOR cprod

    -- Inicializando vector de seleccion
    CALL v_products.clear() 
   
    --Inicializando contador
    LET totlin = 1

    -- Llenando vector de seleccion 
    FOREACH c_products INTO v_products[totlin].tcditem THRU v_products[totlin].tcodabr,westado,wstatus
     -- Verficando estado
     IF (westado=0) THEN
        LET v_products[totlin].testado = "mars_cancelar.png"
     ELSE
        LET v_products[totlin].testado = "mars_cheque.png"
     END IF

     -- Verficando status
     IF (wstatus=0) THEN
        LET v_products[totlin].tstatus = "mars_bloqueo.png"
     ELSE
        LET v_products[totlin].tstatus = "mars_cheque.png"
     END IF

     -- Incrementando contador
     LET totlin = (totlin+1) 
    END FOREACH 
    CLOSE c_products
    FREE  c_products
    LET totlin = (totlin-1) 
    ERROR "" 

    -- Verificando si hubieron datos   
    IF (totlin>0) THEN 
     -- Desplegando vector de seleccion 
     DISPLAY ARRAY v_products TO s_products.*
      ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)
 
      ON ACTION cancel 
       -- Salida
       CALL invmae010_inival(1)
       LET loop = FALSE
       EXIT DISPLAY 
 
      ON ACTION buscar
       -- Buscar
       CALL invmae010_inival(1)
       EXIT DISPLAY 

      ON ACTION calculator
       -- Cargando calculadora
       IF NOT winshellexec("calc") THEN
          ERROR "Atencion: calculadora no disponible."
       END IF

      ON ACTION modificar
       -- Modificando 
       IF invmae010_productos(2) THEN
          EXIT DISPLAY 
       ELSE
          -- Datos generales
          CALL invqbe010_datos(v_products[ARR_CURR()].tcditem)
          -- Empaques
          CALL invqbe010_epqsxpro(v_products[ARR_CURR()].tcditem,1)
          -- Existencias
          CALL invqbe010_existpro(v_products[ARR_CURR()].tcditem)
       END IF 
 
      ON KEY (CONTROL-M) 
       -- Modificando 
       IF (operacion=2) THEN 
        IF invmae010_productos(2) THEN
           EXIT DISPLAY 
        ELSE
           -- Datos generales
           CALL invqbe010_datos(v_products[ARR_CURR()].tcditem)
           -- Empaques
           CALL invqbe010_epqsxpro(v_products[ARR_CURR()].tcditem,1)
           -- Existencias
           CALL invqbe010_existpro(v_products[ARR_CURR()].tcditem)
        END IF 
       END IF 
 
      ON ACTION borrar
       -- Borrando
       -- Verificando integridad 
       IF invqbe010_integridad() THEN
          CALL fgl_winmessage(
          " Atencion",
          " Este producto ya tiene movimientos, no puede borrarse.",
          "stop")
          CONTINUE DISPLAY 
       END IF 
 
       -- Comfirmacion de la accion a ejecutar 
       LET msg = " Esta SEGURO de "||rotulo CLIPPED||" este producto ? "
       LET opc = librut001_menuopcs("Confirmacion",
                                    msg,
                                    "Si",
                                    "No",
                                    NULL,
                                    NULL)
 
       -- Verificando operacion
       CASE (opc) 
        WHEN 1
          IF (operacion=3) THEN
              --  Eliminando
              CALL invmae010_grabar(3)
             EXIT DISPLAY
          END IF 
        WHEN 2
          CONTINUE DISPLAY
       END CASE 

      ON ACTION empaques
       -- Verificando si existen empaques
       IF (totepq=0) THEN
          ERROR "Atencion: no existen empaques registrados."
       END IF
 
       -- Desplegando empaques x producto
       CALL invqbe010_detepqsxpro(v_products[ARR_CURR()].tcditem)
 
      ON ACTION existencia
       -- Verificando si hay existencias
       IF (totexi=0) THEN
          ERROR "Atencion: producto sin existencias en ninguna bodega."
          CONTINUE DISPLAY 
       END IF
   
       -- Despelgando detalle de existencias del producto
       CALL invqbe010_detexistencia()

      --Bitacora
      ON ACTION bitacora
         LET runcmd  =  'fglrun bitacora.42r ', 'segovia ', 11, ' ', v_products[ARR_CURR()].tcditem
         LET runcmd  =  runcmd, " '",v_products[ARR_CURR()].tdsitem,"'"
         RUN runcmd
       
      ON ACTION reporte
       -- Reporte de datos seleccionados a excel
       -- Asignando nombre de columnas
       LET arrcols[1] = "Producto"
       LET arrcols[2] = "Codigo Abreviado"
       LET arrcols[3] = "Nombre Producto"
       LET arrcols[4] = "Categoria"
       LET arrcols[5] = "Subcategoria"
       LET arrcols[6] = "Proveedor"
       LET arrcols[7] = "Medidas"
       LET arrcols[8] = "Color"
       LET arrcols[9] = "Unidad de Medida" 
       LET arrcols[10]= "Estado" 
       LET arrcols[11]= "Estatus" 
       LET arrcols[12]= "Usuario Registro"
       LET arrcols[13]= "Fecha Registro"
       LET arrcols[14]= "Hora Registro"
 
       -- Se aplica el mismo query de la seleccion para enviar al reporte
       LET qry        = "SELECT a.cditem,a.codabr,a.dsitem,c.nomcat,h.nomsub,d.nomprv,f.codabr,e.nomcol,b.nommed,",
                               "CASE (a.estado) WHEN 1 THEN 'Alta' WHEN 0 THEN 'Baja' END,",
                               "CASE (a.status) WHEN 1 THEN 'Liberado' WHEN 0 THEN 'Bloqueado' END,",
                               "a.userid,a.fecsis,a.horsis ",
                        " FROM inv_products a,inv_unimedid b,inv_categpro c,inv_provedrs d,inv_colorpro e,inv_medidpro f,",
                                "inv_subcateg h ", 
                        " WHERE c.codcat = a.codcat AND ",
                               "h.codcat = a.codcat AND ",
                               "h.subcat = a.subcat AND ",
                               "d.codprv = a.codprv AND ",
                               "e.codcol = a.codcol AND ",
                               "f.codmed = a.codmed AND ",
                               "b.unimed = a.unimed AND ",
                               qrytext CLIPPED,
                        " ORDER BY 2,3 "

       -- Ejecutando el reporte
       LET res        = librut002_excelreport("Productos",qry,14,1,arrcols)

      BEFORE ROW 
       -- Asignando contadores del vector
       LET arr = ARR_CURR()
       LET scr = SCR_LINE()
 
       -- Verificando control del total de lineas 
       IF (ARR_CURR()>totlin) THEN
          CALL FGL_SET_ARR_CURR(1)
 
          -- Datos generales
          CALL invqbe010_datos(v_products[1].tcditem)
          -- Empaques
          CALL invqbe010_epqsxpro(v_products[1].tcditem,1)
          -- Existencias 
          CALL invqbe010_existpro(v_products[1].tcditem)
       ELSE
          -- Datos generales
          CALL invqbe010_datos(v_products[ARR_CURR()].tcditem)
          -- Empaques 
          CALL invqbe010_epqsxpro(v_products[ARR_CURR()].tcditem,1)
          -- Existencias 
          CALL invqbe010_existpro(v_products[ARR_CURR()].tcditem)
       END IF 
 
      BEFORE DISPLAY
       -- Desabilitando y habilitando opciones segun operacion
       CASE (operacion) 
        WHEN 1 -- Consultar 
 	 CALL DIALOG.setActionActive("modificar",FALSE)
 	 CALL DIALOG.setActionActive("borrar",FALSE)
        WHEN 2 -- Modificar
 	 CALL DIALOG.setActionActive("modificar",TRUE)
 	 CALL DIALOG.setActionActive("borrar",FALSE)
 	 CALL DIALOG.setActionActive("empaques",FALSE)
 	 CALL DIALOG.setActionActive("existencia",FALSE)
 	 CALL DIALOG.setActionActive("reporte",FALSE)
        WHEN 3 -- Eliminar        
 	 CALL DIALOG.setActionActive("modificar",FALSE)
 	 CALL DIALOG.setActionActive("borrar",TRUE)
 	 CALL DIALOG.setActionActive("empaques",FALSE)
 	 CALL DIALOG.setActionActive("existencia",FALSE)
 	 CALL DIALOG.setActionActive("reporte",FALSE)
       END CASE
 
       -- Escondiendo campo de seleccion de empaques x producto
       CALL f.setFieldHidden("tcheckb",1)
 
     END DISPLAY 
    ELSE 
      CALL fgl_winmessage(
      " Atencion",
      " No existen productos con el criterio seleccionado.",
      "stop")
    END IF 
   ELSE 
      --Operacion = 4
    -- Actualizando precios 
    CALL invqbe010_precios(qrytext)
    CALL f.setElementtext("labelf","Lista de Precios")

   END IF 
  END WHILE

  -- Verificando si operacion es precios
  IF (operacion=4) THEN 
     -- Escondiendo y mostrando elementos de la ventana
     CALL invqbe010_ventana(1,0)
  END IF 
END FUNCTION

{ Subrutina para desplegar los datos del mantenimiento }

FUNCTION invqbe010_datos(wcditem)
 DEFINE wcditem   LIKE inv_products.cditem,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  inv_products a "||
              "WHERE a.cditem = "||wcditem||
              " ORDER BY a.dsitem "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_productst SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_productst INTO w_mae_pro.*
  -- Cargando combobox de subcategorias
  CALL librut003_cbxsubcategorias(w_mae_pro.codcat)

  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.codcat THRU w_mae_pro.status 
  DISPLAY BY NAME w_mae_pro.cditem,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
 END FOREACH
 CLOSE c_productst
 FREE  c_productst

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.codcat THRU w_mae_pro.status 
 DISPLAY BY NAME w_mae_pro.cditem,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION 

-- Subrutina para buscar los empaques x usuario 

FUNCTION invqbe010_epqsxpro(wcditem,opcion)
 DEFINE wcditem   LIKE inv_products.cditem,
        i,opcion  SMALLINT,
        qrytext   STRING 

 -- Inicializando vector de datos
 CALL v_epqsxpro.clear()
 FOR i = 1 TO 4
  DISPLAY v_epqsxpro[i].* TO s_epqsxpro[i].* 
 END FOR

 -- Seleccionando datos
 CASE (opcion)
  WHEN 1 -- Consulta de empaques                 
   LET qrytext = "SELECT 1,a.codepq,a.nomepq,a.cantid,a.lnkepq "||
                  "FROM  inv_epqsxpro a " ||
                  "WHERE a.cditem = "||wcditem||
                   " ORDER BY 5 DESC " 

  WHEN 2 -- Cargando empaques                             
   LET qrytext = "SELECT 0,a.codepq,a.nomepq,a.cantid "||
                  "FROM  inv_empaques a " ||
                  "WHERE NOT exists (SELECT b.codepq FROM inv_epqsxpro b "||
                                     "WHERE b.cditem = "||wcditem||
                                      " AND b.codepq = a.codepq) "||
                    "AND a.unimed = "||w_mae_pro.unimed|| 
                   " ORDER BY 3" 
 END CASE 

 -- Seleccionando datos
 PREPARE cp1 FROM qrytext 
 DECLARE cepqs CURSOR FOR cp1 
 LET totepq = 1
 FOREACH cepqs INTO v_epqsxpro[totepq].* 
  -- Desplegando datos
  IF (totepq<=4) THEN 
     DISPLAY v_epqsxpro[totepq].* TO s_epqsxpro[totepq].* 
  END IF 
  LET totepq = (totepq+1) 
 END FOREACH
 CLOSE cepqs
 FREE  cepqs
 LET totepq = (totepq-1) 
END FUNCTION 

-- Subrutina para visualizar el detalle de empaques x producto 

FUNCTION invqbe010_detepqsxpro(wcditem)
 DEFINE wcditem    LIKE inv_products.cditem,
        loop,opc,i SMALLINT

 -- Desplegando empaques 
 LET loop = TRUE
 WHILE loop
  DISPLAY ARRAY v_epqsxpro TO s_epqsxpro.*
   ATTRIBUTE(COUNT=totepq,ACCEPT=FALSE,CANCEL=FALSE)
   ON ACTION cancel  
    -- Salida
    LET loop = FALSE
    EXIT DISPLAY 

   ON ACTION agregarepqs  
    -- Agregando empaque
    CALL invqbe010_epqsxpro(wcditem,2) 
    IF (totepq=0) THEN
        CALL fgl_winmessage(
        " Atencion",
        " No existen mas empaques para agregar.\n Todos los empaques ya existen.", 
        "stop")

       -- Cargando empaques del producto 
       CALL invqbe010_epqsxpro(wcditem,1) 
       EXIT DISPLAY 
    ELSE  
      -- Mostrando campo de seleccion de empaques
      CALL f.setFieldHidden("tcheckb",0)

      -- Ingresando empaques
      INPUT ARRAY v_epqsxpro WITHOUT DEFAULTS FROM s_epqsxpro.*
       ATTRIBUTE(MAXCOUNT=totepq,INSERT ROW=FALSE,
                 APPEND ROW=FALSE,DELETE ROW=FALSE,
                 ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
       ON ACTION cancel
        -- Salida
        EXIT INPUT 

       ON ACTION accept
        -- Verificando si existen empaques que agregar
        IF NOT invqbe010_hayepqs(1) THEN
           ERROR "Atencion: no existen empaques marcados que agregar."
           CONTINUE INPUT
        END IF

        -- Grabando empaques seleccionados 
        LET opc = librut001_menugraba("Confirmacion de Empaques",
                                      "Que desea hacer ?",
                                      "Guardar",
                                      "Modificar",
                                      "Cancelar",
                                      NULL)

        CASE (opc)
         WHEN 0 -- Cancelando
          EXIT INPUT 
         WHEN 1 -- Grabando
          -- Iniciando la transaccion
          BEGIN WORK 

          -- Guardando accesos
          FOR i = 1 TO totepq
           IF (v_epqsxpro[i].tcheckb=0) OR
              (v_epqsxpro[i].tcheckb IS NULL) THEN 
              CONTINUE FOR
           END IF 

           -- Grabando  
           SET LOCK MODE TO WAIT
           INSERT INTO inv_epqsxpro 
           VALUES (0,
                   wcditem,
                   v_epqsxpro[i].tcodepq, 
                   v_epqsxpro[i].tnomepq, 
                   v_epqsxpro[i].tcantid, 
                   USER, 
                   CURRENT, 
                   CURRENT HOUR TO SECOND)
          END FOR

          -- Terminando la transaccion
          COMMIT WORK 

          EXIT INPUT 
         WHEN 2 -- Modificando
          CONTINUE INPUT 
        END CASE

       BEFORE ROW
        -- Verificando control del total de lineas 
        IF (ARR_CURR()>totepq) THEN
           CALL FGL_SET_ARR_CURR(1)
        END IF 
      END INPUT 

      -- Escondiendo campo de seleccion de empaques 
      CALL f.setFieldHidden("tcheckb",1)

      -- Cargando empaques del producto 
      CALL invqbe010_epqsxpro(wcditem,1) 
      EXIT DISPLAY 
    END IF 

   ON ACTION eliminarepqs  
    -- Eliminando empaques
    -- Mostrando campo de seleccion de empaques
    CALL f.setFieldHidden("tcheckb",0)

    -- Seleccionando empaques
    INPUT ARRAY v_epqsxpro WITHOUT DEFAULTS FROM s_epqsxpro.*
     ATTRIBUTE(MAXCOUNT=totepq,INSERT ROW=FALSE,
               APPEND ROW=FALSE,DELETE ROW=FALSE,
               ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
     ON ACTION cancel
      -- Salida
      EXIT INPUT 

     ON ACTION accept
      -- Verificando si existen empaques que eliminar
      IF NOT invqbe010_hayepqs(0) THEN
         ERROR "Atencion: no existen empaques desmarcados que borrar."
         CONTINUE INPUT 
      END IF

      -- Eliminando empaques seleccionados
      LET opc = librut001_menugraba("Confirmacion de Empaques",
                                    "Que desea hacer ?",
                                    "Borrar",
                                    "Modificar",
                                    "Cancelar",
                                    NULL)

      CASE (opc)
       WHEN 0 -- Cancelando
        EXIT INPUT 
       WHEN 1 -- Grabando
        -- Iniciando la transaccion
        BEGIN WORK 

        -- Guardando accesos
        FOR i = 1 TO totepq
         IF (v_epqsxpro[i].tcheckb=1) OR 
            (v_epqsxpro[i].tcheckb IS NULL) THEN 
            CONTINUE FOR
         END IF 

         -- Eliminando  
         SET LOCK MODE TO WAIT
         DELETE FROM inv_epqsxpro 
         WHERE inv_epqsxpro.cditem = wcditem 
           AND inv_epqsxpro.codepq = v_epqsxpro[i].tcodepq 
        END FOR

        -- Terminando la transaccion
        COMMIT WORK 

        EXIT INPUT 
       WHEN 2 -- Modificando
        CONTINUE INPUT 
      END CASE

     BEFORE ROW
      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totepq) THEN
         CALL FGL_SET_ARR_CURR(1)
      END IF 

     ON CHANGE tcheckb 
      -- Verificando eliminacion
      IF (v_epqsxpro[ARR_CURR()].tcodepq=0) THEN
         IF (v_epqsxpro[ARR_CURR()].tcheckb=0) THEN
            LET v_epqsxpro[ARR_CURR()].tcheckb = 1 
            DISPLAY v_epqsxpro[ARR_CURR()].tcheckb TO s_epqsxpro[SCR_LINE()].tcheckb 
            ERROR "Atencion: empaque default no puede eliminarse."
            NEXT FIELD tcheckb 
         END IF 
      END IF 
    END INPUT 

    -- Escondiendo campo de seleccion de empaques
    CALL f.setFieldHidden("tcheckb",1)

    -- Cargando empaques del producto
    CALL invqbe010_epqsxpro(wcditem,1) 
    EXIT DISPLAY 

   BEFORE ROW 
    -- Verificando control del total de lineas 
    IF (ARR_CURR()>totepq) THEN
       CALL FGL_SET_ARR_CURR(1)
    END IF 
  END DISPLAY 
 END WHILE 
END FUNCTION 

-- Subrutina para verificar si existen empaques seleccionados que agregar o que borrar

FUNCTION invqbe010_hayepqs(estado) 
 DEFINE i,estado,hayepq SMALLINT

 -- Chequeando estado
 LET hayepq = 0
 FOR i = 1 TO totepq
  -- Verificando estado
  IF (v_epqsxpro[i].tcheckb=estado) THEN
     LET hayepq = 1
     EXIT FOR
  END IF 
 END FOR
 
 RETURN hayepq
END FUNCTION 

-- Subrutina para buscar las existencias del producto

FUNCTION invqbe010_existpro(wcditem)
 DEFINE wcditem   LIKE inv_products.cditem,
        totalexi  DEC(14,2),
        i         SMALLINT,
        qrytext   STRING

 -- Inicializando vector de datos
 CALL v_existpro.clear()
 FOR i = 1 TO 4
  DISPLAY v_existpro[i].* TO s_existpro[i].*
 END FOR

 -- Seleccionando datos
 LET qrytext = "SELECT b.nomemp,c.nomsuc,d.nombod,NVL(a.exican,0),a.fulent,a.fulsal "||
                "FROM  inv_proenbod a,glb_empresas b,glb_sucsxemp c,inv_mbodegas d "||
                "WHERE a.cditem = "||wcditem||
                "  AND b.codemp = a.codemp "||
                "  AND c.codemp = a.codemp "||
                "  AND c.codsuc = a.codsuc "||
                "  AND d.codemp = a.codemp "||
                "  AND d.codsuc = a.codsuc "||
                "  AND d.codbod = a.codbod "||
                " ORDER BY 4 DESC" 

 -- Seleccionando datos
 PREPARE cp2 FROM qrytext
 DECLARE cexist CURSOR FOR cp2
 LET totexi = 1
 LET totalexi = 0 
 FOREACH cexist INTO v_existpro[totexi].*
  -- Totalizando existencia
  LET totalexi = (totalexi+v_existpro[totexi].tcantid)

  -- Desplegando datos
  IF (totexi<=4) THEN
     DISPLAY v_existpro[totexi].* TO s_existpro[totexi].*
  END IF
  LET totexi = (totexi+1)
 END FOREACH
 CLOSE cexist
 FREE  cexist
 LET totexi = (totexi-1)
 DISPLAY BY NAME totalexi 
END FUNCTION 

-- Subrutina para visualizar el detalle de existencias

FUNCTION invqbe010_detexistencia()
 -- Escondiendo tabla de empaques
 CALL f.setElementHidden("labelb",1)
 CALL f.setElementHidden("table2",1)
 CALL f.setElementHidden("agregarepqs",1)
 CALL f.setElementHidden("eliminarepqs",1)

 -- Desplegando existencias
 DISPLAY ARRAY v_existpro TO s_existpro.*
  ATTRIBUTE(COUNT=totexi,ACCEPT=FALSE,CANCEL=FALSE)
  ON ACTION cancel  
   -- Salida
   EXIT DISPLAY 

  BEFORE ROW 
   -- Verificando control del total de lineas 
   IF (ARR_CURR()>totexi) THEN
      CALL FGL_SET_ARR_CURR(1)
   END IF 
 END DISPLAY 

 -- Mostrando tabla de empaques
 CALL f.setElementHidden("labelb",0)
 CALL f.setElementHidden("table2",0)
 CALL f.setElementHidden("agregarepqs",0)
 CALL f.setElementHidden("eliminarepqs",0)
END FUNCTION 

-- Subrutina para actualizar los precios 

FUNCTION invqbe010_precios(qrytext)
 DEFINE loop,opc,i,linea  SMALLINT,
        qrypart           STRING,
        qrytext,msg       STRING 

 -- Cargando los productos seleccionados
 -- Preparando la busqueda
 ERROR "Cargando productos ..." ATTRIBUTE(CYAN)

 -- Creando la busqueda
 LET qrypart = " SELECT UNIQUE a.cditem,'',a.codabr,a.dsitem,b.nommed,a.pulcom,0,a.premin,0,a.presug ",
                 " FROM inv_products a,inv_unimedid b ",
                 " WHERE b.unimed = a.unimed AND ",qrytext CLIPPED,
                 " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprem FROM qrypart
 DECLARE c_precios CURSOR FOR cprem 

 -- Inicializando vector de seleccion
 CALL v_preciosm.clear() 
   
 --Inicializando contador
 LET totpre = 1
 -- Llenando vector de seleccion 
 FOREACH c_precios INTO v_preciosm[totpre].*         
  -- Obteniendo ultimo precio minimo
  SELECT NVL(a.preant,0)
   INTO  v_preciosm[totpre].ppreant
   FROM  his_preciosm a
   WHERE a.lnkpre = (SELECT MAX(b.lnkpre)
                      FROM  his_preciosm b
                      WHERE b.cditem = a.cditem)
     AND a.cditem = v_preciosm[totpre].pcditem
  IF v_preciosm[totpre].ppreant IS NULL THEN LET v_preciosm[totpre].ppreant = 0 END IF 

  -- Obteniendo ultimo precio sugerido
  SELECT NVL(a.preant,0)
   INTO  v_preciosm[totpre].ppresan 
   FROM  his_precioss a
   WHERE a.lnkpre = (SELECT MAX(b.lnkpre)
                      FROM  his_precioss b
                      WHERE b.cditem = a.cditem)
     AND a.cditem = v_preciosm[totpre].pcditem
  IF v_preciosm[totpre].ppresan IS NULL THEN LET v_preciosm[totpre].ppresan = 0 END IF 

  -- Advirtiendo precio menor al anterior
  IF (v_preciosm[totpre].pcompra=0) THEN
     LET v_preciosm[totpre].palerta = "mars_alerta.png" 
  ELSE
     LET v_preciosm[totpre].palerta = "" 
  END IF 

  -- Incrementando contador
  LET totpre = (totpre+1) 
 END FOREACH 
 CLOSE c_precios 
 FREE  c_precios 
 LET totpre = (totpre-1) 
 ERROR "" 

 -- Despelgando numero de productos 
 LET msg = "Lista de Precios - Productos ( ",totpre USING "<<<,<<<"," )"
 CALL f.setElementtext("labelf",msg) 

 -- Ingresando lista de precios
 LET loop = TRUE
 WHILE loop
  -- Ingresando precios
  INPUT ARRAY v_preciosm WITHOUT DEFAULTS FROM s_preciosm.*
   ATTRIBUTE(MAXCOUNT=totpre,INSERT ROW=FALSE,
             APPEND ROW=FALSE,DELETE ROW=FALSE,
             ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)

   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT INPUT 

   ON ACTION accept
    -- Verificando precios antes de grabar
    LET linea = invqbe010_verificaprecios()
    IF (linea>0) THEN 
       LET msg = "Precio minimo mayor a precio sugerido. Linea ",linea USING "<<<<"
       CALL fgl_winmessage(" Atencion:",msg,"warning")
       CALL FGL_SET_ARR_CURR(linea) 
       CONTINUE INPUT 
    END IF 

    -- Grabando precios
    LET opc = librut001_menugraba("Actualizacion de Precios",
                                  "Que desea hacer ?",
                                  "Grabar",
                                  "Modificar",
                                  "Cancelar",
                                  NULL)
    CASE (opc)
     WHEN 0 -- Cancelando
      LET loop = FALSE
      EXIT INPUT 
     WHEN 1 -- Grabando
      LET loop = FALSE

      -- Iniciando la transaccion
      BEGIN WORK 

       -- Guardando accesos
       FOR i = 1 TO totpre
        -- Verificando producto
        IF (v_preciosm[i].pcditem IS NULL) THEN 
           CONTINUE FOR
        END IF 

        -- Actualizando
        SET LOCK MODE TO WAIT
        UPDATE inv_products 
        SET    inv_products.premin = v_preciosm[i].pprecio,
               inv_products.presug = v_preciosm[i].ppresug,
               inv_products.pulcom = v_preciosm[i].pcompra --NAP (2012-12-26)
        WHERE  inv_products.cditem = v_preciosm[i].pcditem 
       END FOR

      -- Terminando la transaccion
      COMMIT WORK 

      EXIT INPUT 
     WHEN 2 -- Modificando
      CONTINUE INPUT 
    END CASE

   ON ACTION cambioprecios
    -- Cambios de precios masivos 
    IF invqbe010_cambioprecios() THEN
       EXIT INPUT
    END IF 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   BEFORE ROW
    -- Verificando control del total de lineas 
    IF (ARR_CURR()>totpre) THEN
       CALL FGL_SET_ARR_CURR(1)
    END IF 

   AFTER FIELD pprecio
    -- Verificando precio minimo 
    IF v_preciosm[ARR_CURR()].pprecio IS NULL OR 
       (v_preciosm[ARR_CURR()].pprecio <0) THEN
       LET v_preciosm[ARR_CURR()].pprecio = 0 
       DISPLAY v_preciosm[ARR_CURR()].pprecio TO s_preciosm[SCR_LINE()].pprecio 
    END IF 

   AFTER FIELD ppresug
    -- Verificando precio sugerido 
    IF v_preciosm[ARR_CURR()].ppresug IS NULL OR 
       (v_preciosm[ARR_CURR()].ppresug <0) THEN
       LET v_preciosm[ARR_CURR()].ppresug = 0 
       DISPLAY v_preciosm[ARR_CURR()].ppresug TO s_preciosm[SCR_LINE()].ppresug 
    END IF 
  END INPUT 
 END WHILE 
END FUNCTION 

-- Subrutina para verificar si el producto ya existe en algun producto

FUNCTION invqbe010_integridad()
 DEFINE conteo SMALLINT

 -- Verificando 
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_dtransac a 
  WHERE (a.cditem = w_mae_pro.cditem) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF

  RETURN FALSE
END FUNCTION 

-- Subrutina para habilitar y deshabilitar elementos en la ventana

FUNCTION invqbe010_ventana(x,y)
 DEFINE x,y SMALLINT

 -- Habilitando tabla de lista de precios
 CALL f.setElementHidden("labelf",x)
 CALL f.setElementHidden("table4",x)
 
 -- Escondiendo tabla de empaques y existencias 
 CALL f.setElementHidden("labelx",y)
 CALL f.setElementHidden("table1",y)
 CALL f.setElementHidden("labele",y)
 CALL f.setElementHidden("table3",y)
 CALL f.setElementHidden("labelb",y)
 CALL f.setElementHidden("table2",y)
 CALL f.setElementHidden("agregarepqs",y)
 CALL f.setElementHidden("eliminarepqs",y)
END FUNCTION 

-- Subrutina para hacer el cambio de precios masivo

-- Subrutina para el menu principal del mantenimiento

FUNCTION invqbe010_cambioprecios() 
 DEFINE wc       RECORD 
         tippre  SMALLINT,
         tipcam  SMALLINT,
         campor  SMALLINT,
         porcam  DEC(10,2)
        END RECORD,
        regreso  SMALLINT,
        i        SMALLINT 

 -- Abriendo la ventana de mantenimiento
 OPEN WINDOW wqbe010a AT 5,2
  WITH FORM "invmae010b" ATTRIBUTE(BORDER)

  -- Inicializando datos
  CALL librut001_dpelement("labela","Cantidad") 
  LET wc.tippre = 1 
  LET wc.tipcam = 1 
  LET wc.campor = 1 
  LET wc.porcam = 0 

  OPTIONS INPUT WRAP 

  -- Ingresando parametros 
  INPUT BY NAME wc.* WITHOUT DEFAULTS 
   ATTRIBUTE(UNBUFFERED) 
   ON ACTION cancel
    -- Salida
    LET regreso = TRUE
    EXIT INPUT 

   ON ACTION accept
    -- Verificando cantidad
    IF wc.porcam IS NULL OR 
       wc.porcam <=0 THEN 
       ERROR "Error: porcentaje o cantidad invalido." 
       NEXT FIELD porcam 
    END IF

    -- Procesando el cambio
    LET regreso = FALSE
    EXIT INPUT 

   AFTER FIELD tippre 
    -- Verificando tipo de precio
    IF wc.tipcam IS NULL THEN
       ERROR "Error: tipo de precio invalido."
       NEXT FIELD tippre 
    END IF 

   AFTER FIELD tipcam
    -- Verificando tipo de cambio
    IF wc.tipcam IS NULL THEN
       ERROR "Error: tipo de cambio invalido."
       NEXT FIELD tipcam
    END IF 

   ON CHANGE campor 
    -- Desplegando cambio por
    CASE (wc.campor)
     WHEN 0 CALL librut001_dpelement("labela","Porcentaje") 
     WHEN 1 CALL librut001_dpelement("labela","Cantidad") 
    END CASE 
   
   AFTER FIELD campor
    -- Verificando cambio pot 
    IF wc.campor IS NULL THEN
       ERROR "Error: cambio por invalido."
       NEXT FIELD campor 
    END IF 

   AFTER FIELD porcam
    -- Verificando porcentaje o cantidad
    IF wc.porcam IS NULL OR 
       wc.porcam <=0 THEN
       ERROR "Error: porcentaje o cantidad invalido."
       NEXT FIELD porcam 
    END IF 

   AFTER INPUT
    -- Verificando campos
    IF wc.tippre IS NULL THEN
       NEXT FIELD tippre
    END IF
    IF wc.tipcam IS NULL THEN
       NEXT FIELD tipcam
    END IF
    IF wc.campor IS NULL THEN
       NEXT FIELD campor 
    END IF 
    IF wc.porcam IS NULL OR 
       wc.porcam <=0 THEN
       NEXT FIELD porcam 
    END IF 
  END INPUT 

  OPTIONS INPUT NO WRAP 

  IF NOT regreso THEN

   -- Procesando cambio 
   FOR i = 1 TO totpre
    IF v_preciosm[i].pcditem IS NULL THEN
       CONTINUE FOR
    END IF 

    -- Aplicando cambio 
    CALL invqbe010_calculacambio(wc.*,i)

   END FOR 
  END IF 
 CLOSE WINDOW wqbe010a

 RETURN regreso  
END FUNCTION 

-- Subrutina para calcular las condiciones del cambio de precio 

FUNCTION invqbe010_calculacambio(wc,idx)
 DEFINE wc          RECORD 
         tippre     SMALLINT,
         tipcam     SMALLINT,
         campor     SMALLINT,
         porcan     DEC(10,2)
        END RECORD,
        w_cantid    LIKE inv_products.presug,
        idx         SMALLINT 

 -- Verificando tipo de precio 
 CASE (wc.tippre) 
  WHEN 1 -- Precio minimo

   -- Verificando tipo de cambio 
   CASE (wc.tipcam)
    WHEN 1 -- Subir

      -- Verificando si cambio es por porcentaje o cantidad
      CASE (wc.campor)
       WHEN 1 -- cantidad

         -- Calculando nuevo precio en base al ultimo precio de compra
         IF (v_preciosm[idx].pcompra>0) THEN 
            LET v_preciosm[idx].pprecio = (v_preciosm[idx].pcompra+wc.porcan)
         ELSE
            LET v_preciosm[idx].pprecio = 0 
         END IF 

       WHEN 0 -- porcentaje

         -- Calculando nuevo precio en base al ultimo precio de compra
         IF (v_preciosm[idx].pcompra>0) THEN 
            LET v_preciosm[idx].pprecio = (v_preciosm[idx].pcompra+((v_preciosm[idx].pcompra*wc.porcan)/100))
         ELSE
            LET v_preciosm[idx].pprecio = 0 
         END IF 

      END CASE 

    WHEN 0 -- Bajar 

      -- Verificando si cambio es por porcentaje o cantidad
      CASE (wc.campor)
       WHEN 1 -- cantidad

         -- Calculando nuevo precio en base al ultimo precio de compra
         IF (v_preciosm[idx].pcompra>0) THEN 
            LET w_cantid = (v_preciosm[idx].pcompra-wc.porcan)
            IF (w_cantid>0) THEN 
                LET v_preciosm[idx].pprecio = (v_preciosm[idx].pcompra-wc.porcan)
            END IF 
         ELSE
            LET v_preciosm[idx].pprecio = 0 
         END IF 

       WHEN 2 -- porcentaje

         -- Calculando nuevo precio en base al ultimo precio de compra
         IF (v_preciosm[idx].pcompra>0) THEN 
            LET w_cantid = (v_preciosm[idx].pcompra-((v_preciosm[idx].pcompra*wc.porcan)/100))
            IF (w_cantid>0) THEN 
               LET v_preciosm[idx].pprecio = (v_preciosm[idx].pcompra-((v_preciosm[idx].pcompra*wc.porcan)/100))
            END IF 
         ELSE
            LET v_preciosm[idx].pprecio = 0 
         END IF 

      END CASE 

   END CASE 

  WHEN 0 -- Precio sugerido

   -- Verificando tipo de cambio 
   CASE (wc.tipcam)
    WHEN 1 -- Subir

      -- Verificando si cambio es por porcentaje o cantidad
      CASE (wc.campor)
       WHEN 1 -- cantidad

         -- Calculando nuevo precio en base al ultimo precio de compra
         IF (v_preciosm[idx].pcompra>0) THEN 
            LET v_preciosm[idx].ppresug = (v_preciosm[idx].pcompra+wc.porcan)
         ELSE
            LET v_preciosm[idx].ppresug = 0 
         END IF 

       WHEN 0 -- porcentaje

         -- Calculando nuevo precio en base al ultimo precio de compra
         IF (v_preciosm[idx].pcompra>0) THEN 
            LET v_preciosm[idx].ppresug = (v_preciosm[idx].pcompra+((v_preciosm[idx].pcompra*wc.porcan)/100))
         ELSE
            LET v_preciosm[idx].ppresug = 0 
         END IF 

      END CASE 

    WHEN 0 -- Bajar 

      -- Verificando si cambio es por porcentaje o cantidad
      CASE (wc.campor)
       WHEN 1 -- cantidad

         -- Calculando nuevo precio en base al ultimo precio de compra
         IF (v_preciosm[idx].pcompra>0) THEN 
            LET w_cantid = (v_preciosm[idx].pcompra-wc.porcan)
            IF (w_cantid>0) THEN 
                LET v_preciosm[idx].ppresug = (v_preciosm[idx].pcompra-wc.porcan)
            END IF 
         ELSE
            LET v_preciosm[idx].ppresug = 0 
         END IF 

       WHEN 2 -- porcentaje

         -- Calculando nuevo precio en base al ultimo precio de compra
         IF (v_preciosm[idx].pcompra>0) THEN 
            LET w_cantid = (v_preciosm[idx].pcompra-((v_preciosm[idx].pcompra*wc.porcan)/100))
            IF (w_cantid>0) THEN 
               LET v_preciosm[idx].ppresug = (v_preciosm[idx].pcompra-((v_preciosm[idx].pcompra*wc.porcan)/100))
            END IF 
         ELSE
            LET v_preciosm[idx].ppresug = 0 
         END IF 

      END CASE 

   END CASE 

 END CASE 
END FUNCTION 

-- Subrutina para verificar el cambio de precios

FUNCTION invqbe010_verificaprecios()
 DEFINE i,x SMALLINT

 -- Verificando
 LET x = 0 
 FOR i = 1 TO totpre
  IF v_preciosm[i].pcditem IS NULL THEN
     CONTINUE FOR
  END IF

  -- Verificando cambios
  IF v_preciosm[i].ppresug<v_preciosm[i].pprecio THEN
     LET x = i 
     EXIT FOR
  END IF 
 END FOR
 RETURN x 
END FUNCTION 

-- Nestor Pineda 2012-12-26
-- Subrutina para ingresar la clave de confirmacion antes de grabar
FUNCTION invmae010_confirmacion(lprogid)
DEFINE lprogid  LIKE glb_permxusr.progid
DEFINE passwd  LIKE glb_permxusr.passwd,
     username  LIKE glb_permxusr.userid,
     confirm   SMALLINT

   -- Desplegando pantalla de seguridad
   OPEN WINDOW wsecurity AT 9,27
   WITH FORM "invmae010c"

   LET username = fgl_getenv("LOGNAME")
   -- Ingresando clave de confirmacion 
   LET confirm = TRUE  
   INPUT BY NAME passwd
   ATTRIBUTE(UNBUFFERED,ACCEPT=FALSE,CANCEL=FALSE)

   ON ACTION cancel
   LET confirm = FALSE 
   EXIT INPUT 

   AFTER FIELD passwd
      -- Verificanod password
      IF LENGTH(passwd)<=0 THEN
         ERROR "Error: password invalido ..."
         NEXT FIELD passwd
      END IF 

      -- Verificando 
      SELECT UNIQUE a.userid
      FROM  glb_permxusr a
      WHERE progid = lprogid
        AND userid = username
        AND a.passwd = passwd
      IF (status=NOTFOUND) THEN
         ERROR "Error: password incorrecto ..."
         NEXT FIELD passwd 
      END IF
   END INPUT 
   CLOSE WINDOW wsecurity 
   RETURN confirm
END FUNCTION 