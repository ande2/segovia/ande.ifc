{
invmae010.4gl 
Mynor Ramirez
Mantenimiento de productos
}

-- Definicion de variables globales 

GLOBALS "invglo010.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar5")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("errlog")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL invmae010_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae010_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae010a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invmae010",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Obtebiendo datos de la ventana actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Escondiendo tabla de lista de precios
  CALL f.setElementHidden("labelf",1)
  CALL f.setElementHidden("table4",1)

  -- Cargando combobox de unidades de medida 
  CALL librut003_cbxunidadesmedida("unimed")

  -- Menu de opciones
  MENU " Productos"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Borrar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
    -- Precios
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),5) THEN 
       HIDE OPTION "Precios"
    END IF
   COMMAND "Buscar"
    " Busqueda de productos."
    CALL invqbe010_productos(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo producto."
    LET savedata = invmae010_productos(1) 
   COMMAND "Modificar"
    " Modificacion de un producto existente."
    CALL invqbe010_productos(2) 
   COMMAND "Borrar"
    " Eliminacion de un producto existente."
    CALL invqbe010_productos(3) 
   COMMAND "Precios"
    " Actualizacion de lista de precios de productos."
      IF invmae010_confirmacion(10) THEN 
         CALL invqbe010_productos(4)
      END IF 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae010_productos(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae010_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.codcat,
                w_mae_pro.subcat,
                w_mae_pro.codprv,
                w_mae_pro.codcol,
                w_mae_pro.codmed, 
                w_mae_pro.dsitem,
                w_mae_pro.estado,
                w_mae_pro.status
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON CHANGE subcat,codprv,codcol,codmed 
    -- Creando codigo abreviado del producto
    CALL invmae010_codigoabr()
   
   ON CHANGE codcat
    -- Cargando combobox de subcategorias
    CALL librut003_cbxsubcategorias(w_mae_pro.codcat) 

    -- Creando codigo abreviado del producto
    CALL invmae010_codigoabr()

   BEFORE INPUT 
    -- Verificando integridad
    -- Si un producto ya tiene movimiento no se puede modificar varios campos
    IF (operacion=2) THEN -- Si es modificacion
     IF invqbe010_integridad() THEN
        CALL Dialog.SetFieldActive("codcat",FALSE)
        CALL Dialog.SetFieldActive("subcat",FALSE)
        CALL Dialog.SetFieldActive("codprv",FALSE)
        CALL Dialog.SetFieldActive("codcol",FALSE)
        CALL Dialog.SetFieldActive("codmed",FALSE)
     ELSE
        CALL Dialog.SetFieldActive("codcat",TRUE)
        CALL Dialog.SetFieldActive("subcat",TRUE)
        CALL Dialog.SetFieldActive("codprv",TRUE)
        CALL Dialog.SetFieldActive("codcol",TRUE)
        CALL Dialog.SetFieldActive("codmed",TRUE)
     END IF
    END IF

   BEFORE FIELD codcat
    -- Cargando combobox de categorias
    CALL librut003_cbxcategorias()

    -- Creando codigo abreviado del producto
    CALL invmae010_codigoabr()

   AFTER FIELD codcat
    -- Verificando categoria 
    IF w_mae_pro.codcat IS NULL THEN
       NEXT FIELD codcat
    END IF 

    -- Creando codigo abreviado del producto
    CALL invmae010_codigoabr()

   BEFORE FIELD subcat 
    -- Cargando combobox de subcategorias
    CALL librut003_cbxsubcategorias(w_mae_pro.codcat) 

    -- Creando codigo abreviado del producto
    CALL invmae010_codigoabr()

   AFTER FIELD subcat
    -- Verificando subcategoria 
    IF w_mae_pro.subcat IS NULL THEN
       NEXT FIELD codcat 
    END IF 

    -- Creando codigo abreviado del producto
    CALL invmae010_codigoabr()

   BEFORE FIELD codprv 
    -- Cargando combobox de proveedores
    CALL librut003_cbxproveedores() 

    -- Creando codigo abreviado del producto
    CALL invmae010_codigoabr()

   AFTER FIELD codprv 
    -- Verificando proveedor
    IF w_mae_pro.codprv IS NULL THEN
       NEXT FIELD codprv
    END IF 

    -- Creando codigo abreviado del producto
    CALL invmae010_codigoabr()

   BEFORE FIELD codcol 
    -- Cargando combobox de colores
    CALL librut003_cbxcolores()

    -- Creando codigo abreviado del producto
    CALL invmae010_codigoabr()

   AFTER FIELD codcol 
    -- Verificando color
    IF w_mae_pro.codcol IS NULL THEN
       NEXT FIELD codcol
    END IF 

    -- Creando codigo abreviado del producto
    CALL invmae010_codigoabr()

   BEFORE FIELD codmed 
    -- Cargando combobox de medidas 
    CALL librut003_cbxmedidas()

    -- Creando codigo abreviado del producto
    CALL invmae010_codigoabr()

   AFTER FIELD codmed 
    -- Verificando medidas 
    IF w_mae_pro.codmed IS NULL THEN
       NEXT FIELD codmed
    END IF 

    -- Creando codigo abreviado del producto
    CALL invmae010_codigoabr()

   AFTER FIELD dsitem  
    --Verificando nombre del producto
    IF (LENGTH(w_mae_pro.dsitem)=0) THEN
       ERROR "Error: nombre del producto invalida, VERIFICA"
       LET w_mae_pro.dsitem = NULL
       NEXT FIELD dsitem  
    END IF

    -- Verificando que no exista otro producto con el mismo nombre
    SELECT UNIQUE (a.cditem)
     FROM  inv_products a
     WHERE (a.cditem != w_mae_pro.cditem) 
       AND (a.dsitem  = w_mae_pro.dsitem) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro producto con el mismo nombre, VERIFICA ...",
        "stop")
        NEXT FIELD dsitem
     END IF 

   AFTER FIELD estado 
    -- Verificando estado
    IF w_mae_pro.estado IS NULL THEN
       ERROR "Error: estado del producto invalido, VERIFICA ..."
       NEXT FIELD estado 
    END IF 

   AFTER FIELD status 
    -- Verificando estatus 
    IF w_mae_pro.status IS NULL THEN
       ERROR "Error: estatus del producto invalido, VERIFICA ..."
       NEXT FIELD status 
    END IF 

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.codcat IS NULL THEN 
       NEXT FIELD codcat
    END IF
    IF w_mae_pro.subcat IS NULL THEN
       NEXT FIELD codcat 
    END IF 
    IF w_mae_pro.codprv IS NULL THEN 
       NEXT FIELD codprv
    END IF
    IF w_mae_pro.codcol IS NULL THEN 
       NEXT FIELD codcol
    END IF
    IF w_mae_pro.codmed IS NULL THEN 
       NEXT FIELD codmed 
    END IF 
    IF w_mae_pro.dsitem IS NULL THEN 
       NEXT FIELD dsitem
    END IF
    IF w_mae_pro.estado IS NULL THEN
       NEXT FIELD estado 
    END IF 
    IF w_mae_pro.status IS NULL THEN
       NEXT FIELD status 
    END IF 

    -- Creando codigo abreviado del producto
    CALL invmae010_codigoabr()

    -- Verificando que no exista otro producto con el mismo codigo abreviado
    SELECT UNIQUE (a.cditem)
     FROM  inv_products a
     WHERE (a.cditem != w_mae_pro.cditem) 
       AND (a.codabr  = w_mae_pro.codabr) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro producto con el mismo codigo abreviado, VERIFICA.",
        "stop")
        NEXT FIELD codcat  
     END IF 
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae010_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando producto
    CALL invmae010_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL invmae010_inival(1)
 END IF 

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un producto

FUNCTION invmae010_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando producto ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos 
   LET w_mae_pro.premin = 0
   LET w_mae_pro.pulcom = 0
   LET w_mae_pro.presug = 0
   LET w_mae_pro.estado = 1
   LET w_mae_pro.status = 1

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO inv_products   
   VALUES (w_mae_pro.*)
   LET w_mae_pro.cditem = SQLCA.SQLERRD[2] 
   DISPLAY BY NAME w_mae_pro.cditem 

   --Asignando el mensaje 
   LET msg = "Producto (",w_mae_pro.cditem USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Asignando datos
   LET w_mae_pro.fulcam = TODAY

   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE inv_products
   SET    inv_products.*        = w_mae_pro.*
   WHERE  inv_products.cditem = w_mae_pro.cditem 

   --Asignando el mensaje 
   LET msg = "Producto (",w_mae_pro.cditem USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando productos
   DELETE FROM inv_products 
   WHERE (inv_products.cditem = w_mae_pro.cditem)

   --Asignando el mensaje 
   LET msg = "Producto (",w_mae_pro.cditem USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae010_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae010_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.cditem = 0 
   LET w_mae_pro.estado = 1 
   LET w_mae_pro.status = 1 
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME") 
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codcat THRU w_mae_pro.status 
 DISPLAY BY NAME w_mae_pro.cditem,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE) 
END FUNCTION

{ Subrutina para armar el codigo abreviado del prodcuto }

FUNCTION invmae010_codigoabr()
 DEFINE wcodabrcat LIKE inv_categpro.codabr,
        wcodabrsub LIKE inv_subcateg.codabr, 
        wcodabrprv LIKE inv_provedrs.codabr, 
        wcodabrcol LIKE inv_colorpro.codabr,
        wcodabrmed LIKE inv_medidpro.codabr 

 INITIALIZE wcodabrcat,wcodabrsub,wcodabrprv,wcodabrcol,w_mae_pro.codabr TO NULL

 -- Obteniendo codigo de la categoria  
 SELECT NVL(a.codabr,"NA")
  INTO  wcodabrcat   
  FROM  inv_categpro a 
  WHERE (a.codcat = w_mae_pro.codcat)

 -- Obteniendo codigo de la subcategoria  
 SELECT NVL(a.codabr,"NA")
  INTO  wcodabrsub   
  FROM  inv_subcateg a 
  WHERE (a.codcat = w_mae_pro.codcat)
    AND (a.subcat = w_mae_pro.subcat)

 -- Obteniendo codigo del proveedor
 SELECT NVL(a.codabr,"NA")
  INTO  wcodabrprv   
  FROM  inv_provedrs a 
  WHERE (a.codprv = w_mae_pro.codprv)

 -- Obteniendo codigo del color          
 SELECT NVL(a.codabr,"NA")
  INTO  wcodabrcol   
  FROM  inv_colorpro a 
  WHERE (a.codcol = w_mae_pro.codcol)

 -- Obteniendo codigo y unidad de medida de la medida
 SELECT NVL(a.codabr,"NA"),a.unimed
  INTO  wcodabrmed,w_mae_pro.unimed 
  FROM  inv_medidpro a 
  WHERE (a.codmed = w_mae_pro.codmed)

 LET w_mae_pro.codabr = wcodabrcat CLIPPED,wcodabrsub[3,4],wcodabrprv CLIPPED,wcodabrcol CLIPPED,wcodabrmed CLIPPED
 DISPLAY BY NAME w_mae_pro.codabr,w_mae_pro.unimed
END FUNCTION 
