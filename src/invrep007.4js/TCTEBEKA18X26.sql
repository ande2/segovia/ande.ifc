SELECT a.codemp , a.cditem , b.codabr ,
       b.dsitem , c.nommed , b.codcat ,
       b.subcat , b.codcol , d.codabr ,
       sum(a.opeuni)
  FROM inv_dtransac a , inv_products b ,
       inv_unimedid c , inv_medidpro d
 WHERE a.cditem = b.cditem
 AND   d.codmed = b.codmed
 AND   c.unimed = b.unimed
 AND   a.estado ='V'
 AND   a.codbod <> 21
 AND a.fecemi <= '31/12/2011'
 AND a.codbod =      1
 AND b.codcat =      1
 AND b.subcat =      1
 AND b.cditem =     80
GROUP BY 1,2,3,4,5,6,7,8,9
ORDER BY 1,6,7,8,9
