SELECT a.codemp, a.cditem, b.codabr ,
       b.dsitem , c.nommed ,
       b.codcat,b.subcat,b.codcol,sum(a.opeuni)
FROM inv_dtransac a , inv_products b ,
     inv_unimedid c
WHERE a.cditem = b.cditem
  AND   c.unimed = b.unimed
  AND   a.estado ='V'
  AND   a.codemp <> 21
  AND b.codcat =      1
  AND b.subcat =      2
GROUP BY 1,2,3,4,5,6,7,8
ORDER BY 1,6,7,8
