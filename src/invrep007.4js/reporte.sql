SELECT a.codemp,a.cditem,b.codabr,
       b.dsitem,c.nommed,sum(a.exican)
  FROM inv_proenbod a,inv_products b,
       inv_unimedid c,inv_categpro d,
       inv_subcateg e
 WHERE b.cditem = a.cditem
   AND c.unimed = b.unimed
   AND d.codcat = b.codcat
   AND e.codcat = b.codcat
   AND e.subcat = b.subcat
   AND d.codcat =      1
 GROUP BY 1,2,3,4,5
 ORDER BY b.codabr
