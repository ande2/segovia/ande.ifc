{
invmae006.4gl 
Mynor Ramirez
Mantenimiento de clientes 
}

-- Definicion de variables globales 

GLOBALS "invglo006.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invmae006.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL invmae006_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae006_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae006a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header("invmae006",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Clientes"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Deshabilitar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de clientes."
    CALL invqbe006_clientes(1) 
   COMMAND "Nuevo"
    " Ingreso de un nuevo cliente."
    LET savedata = invmae006_clientes(1) 
   COMMAND "Modificar"
    " Modificacion de un cliente existente."
    CALL invqbe006_clientes(2) 
   COMMAND "Borrar"
    " Eliminacion de un cliente existente."
    CALL invqbe006_clientes(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae006_clientes(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae006_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.nomcli,
                w_mae_pro.numnit,
                w_mae_pro.numtel,
                w_mae_pro.numfax,
                w_mae_pro.dircli,
                w_mae_pro.nomcon,
                w_mae_pro.bemail,
                w_mae_pro.estado, 
                w_mae_pro.status, 
                w_mae_pro.hayfac,
                w_mae_pro.moncre,
                w_mae_pro.diacre,
                w_mae_pro.haydat 
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   AFTER FIELD nomcli  
    --Verificando nombre del cliente
    IF (LENGTH(w_mae_pro.nomcli)=0) THEN
       ERROR "Error: nombre del cliente invalido, VERIFICA"
       LET w_mae_pro.nomcli = NULL
       NEXT FIELD nomcli  
    END IF

    -- Verificando que no exista otro cliente con el mismo nombre
    SELECT UNIQUE (a.codcli)
     FROM  fac_clientes a
     WHERE (a.codcli != w_mae_pro.codcli) 
       AND (a.nomcli  = w_mae_pro.nomcli) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro cliente con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nomcli
     END IF 
   AFTER FIELD numnit  
    --Verificando numero de nit 
    IF (LENGTH(w_mae_pro.numnit)=0) THEN
       ERROR "Error: numero de NIT invalida, VERIFICA"
       LET w_mae_pro.numnit = NULL
       NEXT FIELD numnit  
    END IF

    -- Verificando que no exista otro NIT                      
    SELECT UNIQUE (a.numnit)
     FROM  fac_clientes a
     WHERE (a.codcli != w_mae_pro.codcli) 
       AND (a.numnit  = w_mae_pro.numnit) 
       AND (a.numnit != "C/F")
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otro cliente con el mismo numero de NIT, VERIFICA ...",
        "information")
        NEXT FIELD numnit
     END IF 

   AFTER FIELD dircli
    --Verificando direccion
    IF (LENGTH(w_mae_pro.dircli)=0) THEN
       ERROR "Error: direccion invalida, VERIFICA ..."
       LET w_mae_pro.dircli = NULL
       NEXT FIELD dircli  
    END IF

   AFTER FIELD estado
    --Verificando estado del cliente
    IF w_mae_pro.estado IS NULL THEN
       ERROR "Error: estado del cliente invalido, VERIFICA ..."
       NEXT FIELD estado 
    END IF

   AFTER FIELD status
    --Verificando estatus del cliente
    IF w_mae_pro.status IS NULL THEN
       ERROR "Error: estatus del cliente invalido, VERIFICA ..."
       NEXT FIELD status 
    END IF

   AFTER FIELD hayfac 
    --Verificando si cliente factura
    IF w_mae_pro.hayfac IS NULL THEN
       ERROR "Error: cliente factura invalido, VERIFICA ..."
       NEXT FIELD hayfac  
    END IF

   AFTER FIELD moncre
    --Verificando monto de credito
    IF (w_mae_pro.moncre<0) OR
       w_mae_pro.moncre IS NULL THEN
       ERROR "Error: monto de credito invalido, VERIFICA ..."
       LET w_mae_pro.moncre = NULL
       CLEAR moncre
       NEXT FIELD moncre  
    END IF

   AFTER FIELD diacre 
    --Verificando dia de credito
    IF (w_mae_pro.diacre<0) OR
       w_mae_pro.diacre IS NULL THEN
       ERROR "Error: dias de credito invalidos, VERIFICA ..."
       LET w_mae_pro.diacre = NULL
       CLEAR diacre
       NEXT FIELD diacre  
    END IF

   AFTER FIELD haydat 
    --Verificando si cliente ingresa datos al facturar
    IF w_mae_pro.haydat IS NULL THEN
       ERROR "Error: datos factura invalidos, VERIFICA ..."
       NEXT FIELD haydat  
    END IF

   AFTER INPUT   
    --Verificando ingreso de datos
    IF w_mae_pro.nomcli IS NULL THEN 
       NEXT FIELD nomcli
    END IF
    IF w_mae_pro.numnit IS NULL THEN 
       NEXT FIELD numnit
    END IF
    IF w_mae_pro.dircli IS NULL THEN 
       NEXT FIELD dircli
    END IF
    IF w_mae_pro.estado IS NULL THEN 
       NEXT FIELD estado
    END IF
    IF w_mae_pro.status IS NULL THEN 
       NEXT FIELD status 
    END IF
    IF w_mae_pro.hayfac IS NULL THEN 
       NEXT FIELD hayfac
    END IF
    IF w_mae_pro.moncre IS NULL THEN 
       NEXT FIELD moncre
    END IF
    IF w_mae_pro.diacre IS NULL THEN 
       NEXT FIELD diacre
    END IF
    IF w_mae_pro.haydat IS NULL THEN
       NEXT FIELD haydat  
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae006_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando cliente
    CALL invmae006_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso
 IF (operacion=1) THEN
    CALL invmae006_inival(1)
 END IF

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar un cliente

FUNCTION invmae006_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando cliente ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO fac_clientes   
   VALUES (w_mae_pro.*)
   LET w_mae_pro.codcli = SQLCA.SQLERRD[2] 
   DISPLAY BY NAME w_mae_pro.codcli 

   --Asignando el mensaje 
   LET msg = "Cliente (",w_mae_pro.codcli USING "<<<<<<",") registrado."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE fac_clientes
   SET    fac_clientes.*      = w_mae_pro.*
   WHERE  fac_clientes.codcli = w_mae_pro.codcli 

   --Asignando el mensaje 
   LET msg = "Cliente (",w_mae_pro.codcli USING "<<<<<<",") actualizado."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando clientes
   DELETE FROM fac_clientes 
   WHERE (fac_clientes.codcli = w_mae_pro.codcli)

   --Asignando el mensaje 
   LET msg = "Cliente (",w_mae_pro.codcli USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae006_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae006_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codcli = 0 
   LET w_mae_pro.estado = 1 
   LET w_mae_pro.status = 1 
   LET w_mae_pro.hayfac = 1 
   LET w_mae_pro.haydat = 0 
   LET w_mae_pro.moncre = 0
   LET w_mae_pro.diacre = 0
   LET w_mae_pro.salcre = 0
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME")
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codcli,w_mae_pro.nomcli THRU w_mae_pro.estado 
 DISPLAY BY NAME w_mae_pro.codcli,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION
