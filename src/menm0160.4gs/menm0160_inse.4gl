################################################################################
# Funcion     : %M%
# ccliente : Modulo para ingreso de catalogo 
# Funciones   : cat_inse()
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : eAlvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        ccliente de la modificacion
################################################################################

GLOBALS "menm0160_glob.4gl"

FUNCTION cat_inse( operacion )
DEFINE
   operacion 	SMALLINT,
   err_msg 		CHAR(80),
   w_cuenta		INTEGER, -- Cuenta los registros que ya existen en mcat
   condicion	CHAR(300),
   respuesta   CHAR(06),
   lint_flag   SMALLINT,
   flagerr     SMALLINT,
   lexiste     SMALLINT,
   x_condicion VARCHAR(100),
   x,
   j,
   scr,
   idx			SMALLINT,
   opc         LIKE mprog.prog_id

   LET int_flag = FALSE

	CLEAR FORM
   LET condicion = "SELECT usu_id,usu_nom FROM adm_usu WHERE gru_id = ",  gr_reg.gru_id
    CALL combo_din2("lusu_id",condicion)
	--CALL encabezado()

   LET gtit_enc="MODIFICAR PERMISOS"
   DISPLAY BY NAME gr_reg.*

   DISPLAY ARRAY tree_arr TO sr_tree.* 

      BEFORE DISPLAY
         --CALL DIALOG.setActionHidden( "EditOpciones", 1 )
			CALL fgl_dialog_setkeylabel( "Get","Opciones" )

      BEFORE ROW 
         LET idx = ARR_CURR()
         LET scr = SCR_LINE()
            
      ON ACTION Edit
         LET idx = ARR_CURR()
         LET scr = SCR_LINE()
         LET tree_arr[idx].estado = NOT tree_arr[idx].estado
         DISPLAY tree_arr[idx].estado TO sr_tree[scr].estado
            
         CALL ModificarHijos ( tree_arr[idx].id, tree_arr[idx].estado )
         LET x = idx + 1
         FOR j = scr + 1 TO tree_arr.getLength()
            IF tree_arr[x].parentid = tree_arr[idx].id THEN
               DISPLAY tree_arr[x].estado TO sr_tree[j].estado
            ELSE
               EXIT FOR
            END IF
            LET x = x + 1
         END FOR
         CALL ui.Interface.refresh()

       ON ACTION accept
          CALL box_gradato("Desea guardar la información.")
          RETURNING respuesta
          CASE
             WHEN respuesta = "Si"
                LET int_flag = FALSE
             WHEN respuesta = "No"
                LET int_flag = TRUE
          END CASE
          EXIT DISPLAY

       ON ACTION cancel
         LET INT_FLAG = TRUE
          EXIT DISPLAY

      END DISPLAY

	IF int_flag = TRUE THEN
		LET int_flag = FALSE
		LET gr_reg.* = nr_reg.*
      CALL Limpiar()
		RETURN FALSE
	END IF


	BEGIN WORK

   WHENEVER ERROR CONTINUE

   DELETE   
      FROM  dmenprog
      WHERE gru_id = gr_reg.gru_id

   IF SQLCA.SQLCODE = 0 THEN
      IF tree_arr.getLength() IS NOT NULL THEN
         FOR idx = 1 TO tree_arr.getLength ()
            WHENEVER ERROR CONTINUE
            IF	tree_arr[idx].id IS NOT NULL AND
               tree_arr[idx].estado THEN
               LET opc = tree_arr[idx].id 
               INSERT 
                  INTO     dmenprog ( gru_id, prog_id, userid, fectran )
                  VALUES	         ( gr_reg.gru_id, opc, usuario, CURRENT YEAR TO SECOND)
            END IF
            WHENEVER ERROR STOP
            IF SQLCA.SQLCODE < 0 THEN
               LET err_msg = err_get(SQLCA.SQLCODE)
               ROLLBACK WORK
               ERROR err_msg
               CALL errorlog(err_msg CLIPPED)
               CALL box_valdato("Error al grabar el detalle.")
            END IF
         END FOR
      END IF
   END IF

   IF SQLCA.SQLCODE < 0 THEN
      ROLLBACK WORK
      RETURN FALSE
   ELSE
      COMMIT WORK
      CALL cat_desp15()
      CALL box_valdato("Los permisos han sido modificados.")
      RETURN TRUE
   END IF
END FUNCTION