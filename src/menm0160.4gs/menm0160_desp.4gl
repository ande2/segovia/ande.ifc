################################################################################
# Funcion     : %M%
# Descripcion : Funcion para consulta de catalogo 
# Funciones   : cat_busca_init() 
#               busca_cat()
#               fetch_cat(fetch_flag) 
#               cat_desp()
#               limpiar() 
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Nestor Pineda
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador     Fecha                    Descripcion de la modificacion
#
################################################################################


GLOBALS "menm0160_glob.4gl"
DEFINE detFiltrado BOOLEAN 

FUNCTION busca_cat()
DEFINE
   idx         INTEGER,
   r_det       recDet

   --CALL encabezado()

   LET gtit_enc="BUSQUEDA REGISTRO"
   SELECT proc_desc INTO gr_reg.proc_desc
   FROM bproc
   WHERE proc_id = gr_reg.proc_id
   --
   SELECT  cam_des
   INTO  gr_reg.cam_llavedes
   FROM  bcam
   WHERE  cam_llave = 1
   AND  tab_id in (  
      SELECT  tab_id FROM  btab 
      WHERE  tab_fid IS  NULL 
      AND  proc_id = gr_reg.proc_id )
   --
   INITIALIZE gr_filtro.* TO NULL
   LET gr_filtro.fecha = TODAY
   LET detFiltrado = TRUE
   LET where_clause = " fecha='",gr_filtro.fecha,"'"
   CALL getDetalle()
   
   DIALOG ATTRIBUTE ( UNBUFFERED )
   INPUT BY NAME gr_reg.* ATTRIBUTES (WITHOUT DEFAULTS)

   END INPUT 
   
   DISPLAY ARRAY gr_det TO sDet.*
   BEFORE DISPLAY 
      DISPLAY BY NAME detFiltrado
      --ON ACTION PRINT
         --CALL PrintReport ( )
   END DISPLAY
   BEFORE DIALOG 
      CALL DIALOG.setActionHidden("close", 1)
   ON ACTION filtro
      CALL FiltroBit()
      DISPLAY BY NAME detFiltrado
   ON ACTION ACCEPT
      EXIT DIALOG
   ON ACTION CANCEL
      EXIT DIALOG
      --
   END DIALOG  
   RETURN idx
END FUNCTION

FUNCTION FiltroBit()
DEFINE ldetFiltrado BOOLEAN
DEFINE lcondi STRING  
   OPEN WINDOW formFiltro WITH FORM "menm0160_filter"
      --ATTRIBUTE (STYLE = "dialog")

   LET ur_filtro.* = gr_filtro.*
   LET ldetFiltrado = detFiltrado
   LET lcondi = "tab_habilitado=1 AND proc_id=",gr_reg.proc_id
   CALL librut002_combobox("tab_id","select tab_id,tab_des FROM btab WHERE "||lcondi)
   DIALOG ATTRIBUTES(UNBUFFERED)
   CONSTRUCT where_clause
   ON    fecha, tab_id, cam_exp, cam_id, cam_vanterior, cam_vactual, usu_nom
   FROM  fecha, tab_id, cam_exp, cam_id, cam_vanterior, cam_vactual, usu_nom

   BEFORE CONSTRUCT 
      DISPLAY BY NAME gr_filtro.*

   BEFORE FIELD tab_id
      INITIALIZE gr_filtro.cam_id TO NULL
      DISPLAY BY NAME gr_filtro.cam_id
      
   BEFORE FIELD cam_id
      CALL GET_FLDBUF(tab_id) RETURNING gr_filtro.tab_id
      IF  gr_filtro.tab_id IS NOT NULL THEN 
         LET lcondi = "cam_bitacora=1 AND tab_id = ",gr_filtro.tab_id
         CALL librut002_combobox("cam_id","select cam_id,cam_des FROM bcam WHERE "||lcondi)
      ELSE 
         LET lcondi ="cam_bitacora=1 AND tab_id IN (SELECT tab_id FROM btab WHERE ",
                     "tab_habilitado=1 AND proc_id=",gr_reg.proc_id,")"
         CALL librut002_combobox("cam_id","select cam_id,cam_des FROM bcam WHERE "||lcondi)
      END IF
   END CONSTRUCT 

   BEFORE DIALOG 
      CALL DIALOG.setActionHidden("close", 1)

   ON ACTION CLEAR
      INITIALIZE gr_filtro.* TO NULL
      DISPLAY BY NAME gr_filtro.*
      LET detFiltrado = FALSE 
      
   ON ACTION ACCEPT
      CALL GET_FLDBUF(fecha, tab_id, cam_exp, cam_id, cam_vanterior, cam_vactual, usu_nom) 
      RETURNING   gr_filtro.fecha, gr_filtro.tab_id, gr_filtro.cam_exp, gr_filtro.cam_id,
                  gr_filtro.cam_vanterior, gr_filtro.cam_vactual, gr_filtro.usu_nom
      IF gr_filtro.fecha IS NOT NULL OR 
         gr_filtro.tab_id IS NOT NULL OR
         gr_filtro.cam_exp IS NOT NULL OR 
         gr_filtro.cam_id IS NOT NULL OR 
         gr_filtro.cam_vanterior IS NOT NULL OR 
         gr_filtro.cam_vactual IS NOT NULL OR
         gr_filtro.usu_nom IS NOT NULL THEN
         LET detFiltrado = TRUE 
      END IF 
      LET INT_FLAG = FALSE 
      EXIT DIALOG 
   ON ACTION CANCEL
      LET gr_filtro.* = ur_filtro.*
      LET detFiltrado = ldetFiltrado
      LET INT_FLAG = TRUE 
      EXIT DIALOG
   END DIALOG

   CLOSE WINDOW formFiltro
   IF INT_FLAG THEN --Ingreso cancelado
      LET INT_FLAG = FALSE
   ELSE 
      CALL getDetalle()
   END IF 
END FUNCTION

FUNCTION getDetalle()
DEFINE query STRING 
DEFINE idx INTEGER
DEFINE r_det recDet

   LET query = " SELECT * FROM (",
               " SELECT mov_fecha, tab_id, tab_nom, cam_exp, cam_id, cam_des, cam_vanterior, cam_vactual, usu_nom, ",
               "        CAST (mov_fecha as date) fecha",
               " FROM   vbit ",
               " WHERE  proc_id = ",gr_reg.proc_id,
               "   AND (TRIM (cam_llaveval) = ",gr_reg.cam_llaveval,
               "   OR  cam_llaveval = 'Todos')) ",
               " WHERE ",where_clause,
               " ORDER BY 1 DESC, 2"
   PREPARE ex_stmt FROM query
   DECLARE cat_ptr CURSOR FOR ex_stmt
      
   CALL gr_det.clear()
   LET idx = 0
   FOREACH cat_ptr INTO r_det.* 
      LET idx = idx + 1
      LET gr_det[idx].* = r_det.*
   END FOREACH

   DISPLAY ARRAY gr_det TO sDet.*
      BEFORE ROW 
         EXIT DISPLAY 
   END DISPLAY
END FUNCTION 