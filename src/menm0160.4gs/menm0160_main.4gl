################################################################################
# Funcion     : %M%
# Descripcion : Modulo principal para cliente
# Funciones   : main
#					 cat_init()
#               cat_menu()
#               encabezado()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Nestor Pineda
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador     Fecha                    Descripcion de la modificacion
#
################################################################################

GLOBALS "menm0160_glob.4gl"

MAIN
DEFINE
   n_param SMALLINT

DEFER INTERRUPT

OPTIONS
   INPUT WRAP,
   HELP FILE "cat_help.ex",
   HELP KEY CONTROL-W,
   COMMENT 	LINE OFF,
   PROMPT 	LINE LAST - 2,
   MESSAGE 	LINE LAST - 1,
   ERROR 	LINE LAST
                                                                                
   LET n_param = num_args()
   IF n_param = 0 THEN
      RETURN
   ELSE
      LET dbname = arg_val(1)
		LET gr_reg.proc_id = arg_val(2)
      LET gr_reg.cam_llaveval = arg_val(3)
      LET gr_reg.llaveval_des = arg_val(4)
      DATABASE dbname
   END IF
   CALL ui.interface.loadStyles("styles.4st")
   CALL startlog("menm0160.log")
	CALL cat_init(dbname)
	CALL cat_menu()
END MAIN

FUNCTION cat_init(dbname)
DEFINE
   nombre_departamento, 
   cmd                        CHAR(40),
   ip_usu, dbname             CHAR(20),
   --vempr_log                  LIKE mempr.empr_log,
   fecha_actual               DATE,
   x_condicion,
   dir_destino                CHAR(100)
   LET usuario = fgl_getenv("LOGNAME")
   LET fecha_actual = today
   let hr =fecha_actual
	--SELECT UPPER(mempr.empr_nom), mempr.empr_log,mempr.empr_nomlog
	--INTO nombre_empresa, vempr_log,vempr_nomlog  
	--FROM mempr
	--WHERE mempr.empr_db = dbname
END FUNCTION

FUNCTION cat_menu()
DEFINE 
   cuantos		SMALLINT,
   usuario CHAR(8),
   vopciones CHAR(255), --cadena de ceros y unos para control de permisos
   ord_opc SMALLINT,    --orden de las opciones en el menu
   cnt     SMALLINT,    --contdor
   tol ARRAY[13] OF RECORD
      opcion   CHAR(15),
      acc      CHAR(15),
      desc_opc CHAR(60),
      des_cod  SMALLINT,
      imagen    CHAR(20),
      linkprop INTEGER
   END RECORD,
   cpo ARRAY[13] OF RECORD
      opcion   CHAR(15),
      desc_opc CHAR(60),
      des_cod  SMALLINT,
      imagen    CHAR(20),
      linkprop INTEGER
   END RECORD,

   tb om.DomNode,
   tbi om.DomNode,
   tbs om.DomNode,
   tm  om.DomNode,
   tmg om.DomNode,
   tmi om.DomNode,
   tms om.DomNode,
   f  om.DomNode

   LET cnt = 1
   LET ord_opc = NULL

   OPEN FORM cat_form FROM "menm0160_form"
   DISPLAY FORM cat_form
   CLEAR FORM
   CALL fgl_settitle("BITACORA")

   -- captura nombre de usuario
   LET usuario = fgl_getenv("LOGNAME")

   -- captura permisos del usuario sobre las opciones del menu
   --CALL men_opc(usuario,"menm0160") RETURNING vopciones
   LET vopciones ="111"
   -- carga opciones de menu
   --DECLARE opciones_menu CURSOR FOR
      --SELECT   b.prog_ord,c.des_desc_md,b.prop_desc,
               --c.des_cod,c.des_desc_ct
         --FROM  ande:mprog a,ande:dprogopc b, sd_des c
         --WHERE a.prog_nom = "menm0160"
         --AND   b.prog_id = a.prog_id
         --AND   c.des_tipo = 22
         --AND   c.des_cod = b.des_cod
         --ORDER BY b.prog_ord
         --
	--LET cnt = 1
   --
   --FOREACH opciones_menu INTO ord_opc,cpo[cnt].*
          --LET cnt = cnt + 1
   --END FOREACH
   --FREE opciones_menu
--
   --FOR cnt = 1 TO LENGTH(vopciones)
          --LET tol[cnt].acc     =DOWNSHIFT(cpo[cnt].opcion CLIPPED)
          --LET tol[cnt].opcion  =cpo[cnt].opcion CLIPPED
          --LET tol[cnt].desc_opc = cpo[cnt].desc_opc CLIPPED
          --LET tol[cnt].des_cod  = cpo[cnt].des_cod
          --LET tol[cnt].imagen   = DOWNSHIFT(cpo[cnt].imagen CLIPPED)
          --LET tol[cnt].linkprop = cpo[cnt].linkprop
   --END FOR
--
   --LET aui=ui.Interface.getRootNode()
   --LET tb =aui.createChild("ToolBar")
   --LET tbi=createToolBarItem(tb,tol[1].acc,tol[1].opcion,tol[1].desc_opc,tol[1].imagen) --Buscar
   --LET tbi=createToolBarItem(tb,tol[2].acc,tol[2].opcion,tol[2].desc_opc,tol[2].imagen) --Imprimir
   --LET tbi=createToolBarItem(tb,tol[3].acc,tol[3].opcion,tol[3].desc_opc,tol[3].imagen) --Salir

-- menu top
{
   LET f  = ui.Interface.getRootNode()
   LET tm = f.createChild("TopMenu")
   LET tmg = tm.createChild("TopMenuGroup")
   CALL tmg.setAttribute("text","Archivo")

   LET tbi =creaopcion(tmg,tol[4].acc,tol[4].opcion,tol[4].desc_opc,tol[4].imagen) --ingreso
   LET tbi =creaopcion(tmg,tol[5].acc,tol[5].opcion,tol[5].desc_opc,tol[5].imagen) --modificar
   LET tbi =creaopcion(tmg,tol[6].acc,tol[6].opcion,tol[6].desc_opc,tol[6].imagen) --anular
   LET tms =tmg.CreateChild("TopMenuSeparator")
   LET tmi =creaopcion(tmg,tol[7].acc,tol[7].opcion,tol[7].desc_opc,tol[7].imagen) --salir
   LET tmg = tm.createChild("TopMenuGroup")
   CALL tmg.setAttribute("text","Edici�n")
   LET tmi =creaopcion(tmg,tol[1].acc,tol[1].opcion,tol[1].desc_opc,tol[1].imagen) --buscar
   LET tms =tmg.CreateChild("TopMenuSeparator")
   LET tmi =creaopcion(tmg,tol[2].acc,tol[2].opcion,tol[2].desc_opc,tol[2].imagen) -- anterior
   LET tmi =creaopcion(tmg,tol[3].acc,tol[3].opcion,tol[3].desc_opc,tol[3].imagen) -- proximo
   LET tmg =tm.createChild("TopMenuGroup")
   CALL tmg.setAttribute("text","Herramientas")
}
   MENU ""
      BEFORE MENU

         LET cuantos = 0
      -- despliega opciones a las que tiene acceso el usuario
         --HIDE OPTION ALL
         --FOR cnt = 1 TO LENGTH(vopciones)
            --IF vopciones[cnt] = 1 AND
               --(tol[cnt].des_cod = 1 OR
               --tol[cnt].des_cod = 6 OR
               --tol[cnt].des_cod = 9) THEN
               --DISPLAY tol[cnt].opcion
               --SHOW OPTION tol[cnt].opcion
               --LET cuantos = cuantos + 1
            --END IF
         --END FOR

         --IF cuantos = 0 THEN
            --CALL box_error("Lo siento, no tiene acceso a este programa")
            --EXIT MENU
         --END IF
         CALL busca_cat() RETURNING cuantos
         IF cuantos > 0 THEN
            --SHOW OPTION tol[2].opcion
         END IF  

-- opcion busqueda
	--COMMAND KEY("B") tol[1].opcion tol[1].desc_opc
   COMMAND KEY("B") "Buscar" "Buscar"
		HELP 2
		CALL busca_cat() RETURNING cuantos
      IF cuantos > 0 THEN
         --SHOW OPTION tol[2].opcion
      END if 

      
 -- opcion imprimir
   --COMMAND KEY ("R") tol[2].opcion tol[2].desc_opc
   COMMAND KEY ("R") "Reporte" "Reporte"
		HELP 8
      IF cuantos > 0 THEN 
         --CALL PrintReport ( )
      ELSE
         --CALL box_error("No hay registros para imprimir")
      END if

      
-- opcion salir
   --COMMAND key("Q") tol[3].opcion tol[3].desc_opc
   COMMAND key("Q") "Salir" "Salir"
		HELP 8
		EXIT MENU
   END MENU
END FUNCTION
