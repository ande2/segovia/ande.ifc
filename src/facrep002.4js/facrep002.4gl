{ 
Fecha    : Abril 2011  
Programo : facrep002.4gl 
Objetivo : Reporte de ordenes de trabajo internas  para confeccion 
}

DATABASE segovia 

{ Definicion de variables globales }

DEFINE w_mae_pto RECORD LIKE fac_puntovta.*,
       w_datos   RECORD
        numpos   LIKE inv_ordentra.numpos, 
        fec_ini  DATE,
        fec_fin  DATE,
		  horario  SMALLINT
       END RECORD,
       fnt       RECORD
        cmp      CHAR(12),
        nrm      CHAR(12),
        tbl      CHAR(12),
        fbl,t88  CHAR(12),
        t66,p12  CHAR(12),
        p10,srp  CHAR(12),
        twd      CHAR(12),
        fwd      CHAR(12),
        tda,fda  CHAR(12),
        ini      CHAR(12)
       END RECORD,
       wpais     VARCHAR(100), 
       existe    SMALLINT,
       filename  STRING,
       fnumpos   STRING

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar7")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("facrep002.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL facrep002_ordentra()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION facrep002_ordentra()
 DEFINE w_pro_bod         RECORD LIKE inv_proenbod.*,
        imp1              RECORD LIKE vis_ordentraint.*, 
        pipeline,qrytxt   STRING,
        strnumpos         STRING,
        horario_string    STRING,
        lOrdentra         STRING,
        loop              SMALLINT

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep006a AT 5,2
  WITH FORM "facrep002a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("facrep002",wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/facrep002.txt"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando  combobox de puntos de venta
  CALL librut003_cbxpuntosventa() 

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
	LET w_datos.fec_ini = TODAY
	LET w_datos.fec_fin = TODAY
   CLEAR FORM

   -- Construyendo busqueda
   INPUT BY NAME w_datos.numpos,
                 w_datos.fec_ini,
					  w_datos.fec_fin,
					  w_datos.horario
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)


		BEFORE INPUT
			LET w_datos.horario = 1
    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

	ON ACTION visualizar 
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Obteniendo filtros
     LET fnumpos = GET_FLDBUF(w_datos.numpos)

     -- Verificando datos 
     IF w_datos.fec_ini IS NULL OR w_datos.fec_fin IS NULL THEN
        ERROR "Error: deben de completarse los filtros de seleccion ..." 
        NEXT FIELD numpos 
     END IF 
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "local" 

     -- Obteniendo filtros
     LET fnumpos = GET_FLDBUF(w_datos.numpos)

     -- Verificando datos 
     IF w_datos.fec_ini IS NULL OR w_datos.fec_fin IS NULL THEN
        ERROR "Error: deben de completarse los filtros de seleccion ..." 
        NEXT FIELD numpos 
     END IF 
     EXIT INPUT 

    AFTER FIELD fec_ini
     -- Verificando fecha de las ordenes
     IF w_datos.fec_ini IS NULL THEN
		  LET w_datos.fec_ini = TODAY
     END IF

    AFTER FIELD fec_fin
     -- Verificando fecha de las ordenes
     IF w_datos.fec_fin IS NULL THEN
        NEXT FIELD fec_fin
	  ELSE
		  IF w_datos.fec_fin < w_datos.fec_ini THEN
			  ERROR "Error: Fecha final no puede ser mayor"
			  LET w_datos.fec_fin = w_datos.fec_ini
			  NEXT FIELD fec_fin
		  END IF
     END IF

    AFTER INPUT 
     -- Verificando datos
     IF w_datos.fec_ini IS NULL OR w_datos.fec_fin IS NULL OR  
        pipeline IS NULL THEN
        NEXT FIELD numpos 
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 


  -- Verificando seleccion de puntos de venta
   LET strnumpos= NULL
   IF w_datos.numpos IS NOT NULL THEN
      LET strnumpos= " AND a.numpos = ",w_datos.numpos 
   END IF
	CASE w_datos.horario
		WHEN 1
			LET horario_string = " AND b.nofase = 1 AND a.ordentrabajo = b.lnkord  AND a.numlona = b.nuitem "
			LET w_datos.fec_ini=w_datos.fec_ini-3
			LET lOrdentra = " , inv_dorden   b "
		WHEN 2
			LET horario_string = NULL--" AND a.hora > '13:00:00' " 
			LET lOrdentra = NULL --" , inv_ordentra b ", 
	END CASE
		

   -- Construyendo seleccion 
   LET qrytxt = " SELECT a.* ",
                " FROM vis_ordentraint a ",lOrdentra CLIPPED,
                " WHERE a.fechaorden >= '",w_datos.fec_ini,"' ",
                " AND   a.fechaorden <= '",w_datos.fec_fin,"' ", horario_string CLIPPED," ",
                 strnumpos CLIPPED,
                " ORDER BY 3,4,5 "

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep006 FROM qrytxt 
   DECLARE c_crep006 CURSOR FOR c_rep006
   LET existe = FALSE
	WHENEVER ERROR CONTINUE
   FOREACH c_crep006 INTO imp1.* 
    IF NOT existe THEN
       -- Seleccionando fonts para impresora epson
       CALL librut001_fontsprn(pipeline,"epson")
       RETURNING fnt.*

       LET existe = TRUE
       LET existe = TRUE
       START REPORT facrep002_impordenint TO filename
    END IF 
	 CALL facrep002_actualiza(imp1.ordentrabajo,imp1.numlona)

	--DISPLAY "NUMERO ",imp1.numlona
    -- Llenando el reporte
    OUTPUT TO REPORT facrep002_impordenint(imp1.*)
   END FOREACH
	WHENEVER ERROR STOP
   CLOSE c_crep006 
   FREE  c_crep006 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT facrep002_impordenint 

		--CALL librut001_rep_pdf("Ordenes de Trabajo",filename,10,"L",4)
      -- Enviando reporte al destino seleccionado
      DISPLAY "filename ", filename
      DISPLAY "pipeline ", pipeline
      CALL librut001_rep_pdf("Ordenes de Trabajo Internas Para Confeccion",filename,9,"L",1) 
      --CALL librut001_enviareporte(filename,pipeline,"Ordenes de Trabajo Internas Para Confeccion") 
      ERROR "" 
      CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 

  END WHILE
 CLOSE WINDOW wrep006a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT facrep002_impordenint(imp1)
 DEFINE imp1              RECORD --LIKE vis_ordentraint.*,
         numpos           SMALLINT,
         nombrepos        CHAR(40),
         ordentrabajo     INTEGER,
         numlona          INTEGER,
         fechaorden       DATE,
         cliente          CHAR(30),
         cantidad         DEC(12,2),
         clase            CHAR(17),
         color            CHAR(17),
         medidas          CHAR(17),
         nombremedida     CHAR(11),
         usuario          CHAR(15), 
         serie            CHAR(3),
         documento        CHAR(10), 
         ordencompra      CHAR(10),
         fechaofrecida    DATE,
         descripcion      CHAR(110),
         fecha            DATETIME HOUR TO SECOND 
        END RECORD, 
        linea             CHAR(158),
        lLineas           INTEGER  

  OUTPUT
			LEFT   MARGIN 1
			RIGHT  MARGIN 1
         PAGE   LENGTH 62
         BOTTOM MARGIN 0 
         TOP    MARGIN 0 

  FORMAT 
   FIRST PAGE HEADER
	 LET lLineas= 62
    LET linea = "______________________________________________",
					 "______________________________________________",
					 "____________________________________________"

    -- Imprimiendo Encabezado
    PRINT COLUMN   1,ASCII 27,fnt.cmp CLIPPED,"Facturacion", 
	  		 COLUMN 119,PAGENO USING "Pagina: <<<<"
    PRINT COLUMN   1,"Facrep002",
          COLUMN  55,"ORDENES DE TRABAJO INTERNAS PARA CONFECCION",
          COLUMN 118,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
    PRINT COLUMN   1,"(",FGL_GETENV("LOGNAME") CLIPPED,")",
          COLUMN  55,"Fecha de Emision (",w_datos.fec_ini USING "dd/mmm/yyyy"," - ",w_datos.fec_fin USING "dd/mmm/yyyy",")", 
          COLUMN 118,"Hora  : ",TIME 

    PRINT linea 
    PRINT "Orden     Nombre del Cliente       Cantidad Clase          Color          M e d i d a s        Venta Por     ",
          "  Factura/Ticket    Fecha" 
    PRINT "Descripcion de la Confeccion                                                                                 ",
          " Serie   Num. Doc. " 
    PRINT linea
	 LET lLineas=lLineas-8

	PAGE HEADER 
	 LET lLineas=62
    PRINT "Orden     Nombre del Cliente       Cantidad Clase          Color          M e d i d a s        Venta Por     ",
          "  Factura/Ticket    Fecha" 
    PRINT "Descripcion de la Confeccion                                                                                 ",
          " Serie   Num. Doc. " 
--Orden     Nombre del Cliente       Cantidad CLASE            COLOR          M E D I D A S         Venta Por      Factura/Ticket    Fecha
--Descripcion de la Confeccion                                                                                    Serie   Num. Doc. 
    PRINT linea 
	 LET lLineas=lLineas-3
   BEFORE GROUP OF imp1.numpos    
    -- Imprimiendo datos del punto de venta
    PRINT "Punto de Venta: ",imp1.nombrepos CLIPPED
    PRINT linea 
	 LET lLineas=lLineas-2

   ON EVERY ROW
	 IF lLineas < 3 THEN
		SKIP TO TOP OF PAGE
	 END IF
    -- Imprimiendo ordenes 
    PRINT COLUMN   1,imp1.ordentrabajo          USING  "<<<<<<",
                     "/"                                       ,
                     imp1.numlona               USING   "<<<<<",
          COLUMN  09,imp1.cliente                       CLIPPED,
          COLUMN  36,imp1.cantidad              USING    "###&",
          COLUMN  44,imp1.clase[1,14]                   CLIPPED,
          COLUMN  58,imp1.color                         CLIPPED,
          COLUMN  75,imp1.medidas                       CLIPPED,
                     " "                                       ,
                     imp1.nombremedida                  CLIPPED,
          COLUMN  99,imp1.usuario                       CLIPPED,
          COLUMN 114,imp1.serie                         CLIPPED,
          COLUMN 119,imp1.documento             USING  "#####&",
          COLUMN 129,imp1.fechaofrecida         USING "dd/mm/yy"
    PRINT COLUMN   1,imp1.descripcion                   CLIPPED
    PRINT linea
	 LET lLineas=lLineas-3
 
	 AFTER GROUP OF imp1.numpos
		PRINT "Total: ",GROUP COUNT(*) USING "<<<,<<&"," Orden(es) de Trabajo"
		SKIP TO TOP OF PAGE
	
END REPORT 

FUNCTION facrep002_actualiza(orden,lona)
DEFINE orden INTEGER,
       lona  INTEGER
	SELECT nofase
	  FROM inv_dorden
	 WHERE lnkord = orden
		AND nuitem = lona
	   AND nofase = 1
	IF STATUS = 0 THEN
		UPDATE inv_dorden  SET nofase=2
		 WHERE lnkord = orden
			AND nuitem = lona
	END IF 

END FUNCTION
