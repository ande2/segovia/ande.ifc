################################################################################
# Funcion     : %M%
# Descripcion : Definicion de variables para mantenimiento de catalogo 
# Funciones   :
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z% %W%
# Autor       : Nestor Pineda
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# 
################################################################################

DATABASE segovia

GLOBALS
TYPE recDet RECORD
      tab_id         LIKE bcam.tab_id,
      cam_id         LIKE bcam.cam_id,
      colno          INTEGER,
      cam_nom        LIKE bcam.cam_nom,
      cam_des        LIKE bcam.cam_des,
      cam_llave      LIKE bcam.cam_llave,
      cam_identifica LIKE bcam.cam_identifica,
      cam_bitacora   LIKE bcam.cam_bitacora
     END RECORD
TYPE recTree RECORD
      nombre         STRING,     -- text to be displayed for the node
      descripcion    STRING,
      parentid       STRING,     -- id of the parent node
      id             STRING,     -- id of the current node
      expanded       BOOLEAN,    -- node expansion flag (TRUE/FALSE) (optional)
      isnode         BOOLEAN     -- children indicator flag (TRUE/FALSE) (optional)
   END RECORD
TYPE recTab    RECORD
      tab_fid     LIKE btab.tab_fid,
      tab_id      LIKE btab.tab_id,
      tab_nom     LIKE btab.tab_nom,
      tab_des     LIKE btab.tab_des
   END RECORD
   
DEFINE 
   tree_arr DYNAMIC ARRAY OF recTree,
   gr_reg, nr_reg, ur_reg RECORD
      proc_id        LIKE bproc.proc_id,
      proc_desc      LIKE bproc.proc_desc,
      est_id         LIKE bproc.est_id
   END RECORD,
   gr_tab RECORD
      tab_id      LIKE btab.tab_id,
      tab_nom     LIKE btab.tab_nom,
      tab_des     LIKE btab.tab_des
   END RECORD ,
	gr_det,nr_det,ur_det,gr_tdet DYNAMIC ARRAY OF recDet,
   dbname         CHAR(20),
	vempr_nomlog   CHAR(15),
	usuario        CHAR(10),
	hr             DATE, 
	gtit_enc, gtit_pie,nombre_depproamento VARCHAR(80),
    where_report varchar (1000),
   --codempre LIKE mempr.empr_id,
       aui om.DOMnode,
       nombre_empresa CHAR (1000)
END GLOBALS
