{
invmae009.4gl 
Mynor Ramirez
Mantenimiento de bodegas 
}

-- Definicion de variables globales 

GLOBALS "invglo009.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar3")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invmae009.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL invmae009_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae009_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255), 
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae009a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais 
  CALL librut001_header("invmae009",wpais,1) 
  CALL librut001_dpelement("labela","Accesos a Usuarios")

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Bodegas"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Deshabilitar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de bodegas."
    CALL invqbe009_bodegas(1) 
   COMMAND "Nuevo"
    " Ingreso de una nueva bodega."
    LET savedata = invmae009_bodegas(1) 
   COMMAND "Modificar"
    " Modificacion de una bodega existente."
    CALL invqbe009_bodegas(2) 
   COMMAND "Borrar"
    " Eliminacion de una bodega existente."
    CALL invqbe009_bodegas(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae009_bodegas(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae009_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.codemp,
                w_mae_pro.codsuc,
                w_mae_pro.nombod,
                w_mae_pro.nomabr, 
                w_mae_pro.chkexi,
					 w_mae_pro.bodcon,
                w_mae_pro.hayfac
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE INPUT
    -- Verificando integridad
    -- Si bodega ya tiene movimientos no se puede modificar la empresa, la sucursal y el nombre
    IF (operacion=2) THEN -- Si es modificacion
     IF invqbe009_integridad() THEN
        CALL Dialog.SetFieldActive("codemp",FALSE)
        CALL Dialog.SetFieldActive("codsuc",FALSE)
        CALL Dialog.SetFieldActive("nombod",FALSE)
     ELSE
        CALL Dialog.SetFieldActive("codemp",TRUE)
        CALL Dialog.SetFieldActive("codsuc",TRUE)
        CALL Dialog.SetFieldActive("nombod",TRUE)
     END IF
    END IF

   BEFORE FIELD codemp
    -- Cargando combobox de empresas
    CALL librut003_cbxempresas() 

   AFTER FIELD codemp 
    --Verificando empresa
    IF w_mae_pro.codemp IS NULL THEN
       NEXT FIELD codemp
    END IF

   BEFORE FIELD codsuc 
    -- Cargando combobox de sucursales
    CALL librut003_cbxsucursales(w_mae_pro.codemp) 

   AFTER FIELD codsuc 
    --Verificando sucursal 
    IF w_mae_pro.codsuc IS NULL THEN
       NEXT FIELD codsuc
    END IF

   AFTER FIELD nombod  
    --Verificando nombre del bodega
    IF (LENGTH(w_mae_pro.nombod)=0) THEN
       ERROR "Error: nombre de la bodega invalida, VERIFICA"
       LET w_mae_pro.nombod = NULL
       NEXT FIELD nombod  
    END IF

    -- Verificando que no exista otra bodega con el mismo nombre
    SELECT UNIQUE (a.codbod)
     FROM  inv_mbodegas a
     WHERE (a.codbod != w_mae_pro.codbod) 
       AND (a.nombod  = w_mae_pro.nombod) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otra bodega con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nombod
     END IF 

   AFTER FIELD chkexi 
    --Verificando chequeo de existencia
    IF w_mae_pro.chkexi IS NULL THEN 
       NEXT FIELD chkexi
    END IF

   AFTER FIELD hayfac 
    --Verificando si bodega factura
    IF w_mae_pro.hayfac IS NULL THEN 
       NEXT FIELD hayfac 
    END IF
   AFTER INPUT   
    --Verificando ingreso de datos
    --Verificando empresa
    IF w_mae_pro.codemp IS NULL THEN
       NEXT FIELD codemp 
    END IF
    IF w_mae_pro.codsuc IS NULL THEN
       NEXT FIELD codsuc 
    END IF
    IF w_mae_pro.nombod IS NULL THEN 
       NEXT FIELD nombod
    END IF
    IF w_mae_pro.nomabr IS NULL THEN 
       NEXT FIELD nomabr
    END IF
    IF w_mae_pro.chkexi IS NULL THEN 
       NEXT FIELD chkexi
    END IF
    IF w_mae_pro.hayfac IS NULL THEN 
       NEXT FIELD hayfac 
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae009_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando bodega
    CALL invmae009_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso 
 IF (operacion=1) THEN
    CALL invmae009_inival(1) 
 END IF 

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar una bodega

FUNCTION invmae009_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando bodega ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.codbod),0)
    INTO  w_mae_pro.codbod 
    FROM  inv_mbodegas a
    IF (w_mae_pro.codbod IS NULL) THEN
       LET w_mae_pro.codbod = 1
    ELSE
       LET w_mae_pro.codbod = w_mae_pro.codbod+1
    END IF

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO inv_mbodegas   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.codbod 

   --Asignando el mensaje 
   LET msg = "Bodega (",w_mae_pro.codbod USING "<<<<<<",") registrada."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE inv_mbodegas
   SET    inv_mbodegas.*      = w_mae_pro.*
   WHERE  inv_mbodegas.codbod = w_mae_pro.codbod 

   --Asignando el mensaje 
   LET msg = "Bodega (",w_mae_pro.codbod USING "<<<<<<",") actualizada."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando bodegas
   DELETE FROM inv_mbodegas 
   WHERE  inv_mbodegas.codbod = w_mae_pro.codbod 

   --Asignando el mensaje 
   LET msg = "Bodega (",w_mae_pro.codbod USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae009_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae009_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME")
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.codsuc,w_mae_pro.codbod,w_mae_pro.nombod THRU w_mae_pro.hayfac 
 DISPLAY BY NAME w_mae_pro.codbod,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION
