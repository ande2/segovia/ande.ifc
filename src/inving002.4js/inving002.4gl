{
Fecha    : Diciembre 2010 
Programo : Mynor Ramirez
Objetivo : Programa de ingreso del inventario fisico
}

-- Definicion de variables globales

GLOBALS "invglb002.4gl"
DEFINE regreso     SMALLINT,
       diamax      SMALLINT,
       diamin      SMALLINT,
       existe      SMALLINT,
       msg         STRING 

-- Subrutina principal

MAIN
 -- Atrapando interrupts 
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar8")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("inving002.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de opciones
 CALL inving002_menu()
END MAIN

-- Subutina para el menu de ingreso del inventario fisico

FUNCTION inving002_menu()
 DEFINE regreso    SMALLINT, 
        wpais      VARCHAR(255), 
        titulo     STRING 

 -- Abriendo la ventana del mantenimiento
 OPEN WINDOW wing002a AT 5,2  
  WITH FORM "inving002a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ 

  -- Desplegando datos del encabezado
  CALL librut003_parametros(1,0)
  RETURNING existe,wpais
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()
  CALL librut001_header("inving002",wpais,1)

  -- Inicializando datos 
  CALL inving002_inival(1)

  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()

  -- Menu de opciones
  MENU " Opciones" 
   BEFORE MENU
    -- Verificando accesos
    -- Consultar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN
       HIDE OPTION "Consultar"
    END IF
    --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),5) THEN
       HIDE OPTION "Imprimir"
    END IF
    --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN
       HIDE OPTION "Ingresar"
    END IF
   COMMAND "Consultar" 
    " Consulta de inventarios fisicos existentes."
    CALL invqbx002_fisicos()
   COMMAND "Imprimir"
    " Impresion de reporte comparativo de inventario."
    RUN "fglrun rinvfisico.42r" 
   COMMAND "Ingresar" 
    " Ingreso de nuevos inventarios fisicos."
    CALL inving002_fisicos() 
   COMMAND "Salir"
    " Abandona el menu." 
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU  
  END MENU
 CLOSE WINDOW wing002a
END FUNCTION

-- Subrutina para el ingreso de los datos generales del ingreso del inventario

FUNCTION inving002_fisicos()
 DEFINE loop,existe,retroceso SMALLINT

 -- Obteniendo dias anteriores para ingreso de fecha del inventario
 CALL librut003_parametros(5,1)
 RETURNING existe,diamin
 IF NOT existe THEN
    CALL fgl_winmessage(
    " Atencion",
    " Parametro de maximo de dias anteriores no existe registrado.\n Definir parametro antes de ingresar el inventario.",
    "stop")
    RETURN 
 END IF

 -- Obteniendo dias posteriores para ingreso de fecha del inventario
 CALL librut003_parametros(5,2)
 RETURNING existe,diamax 
 IF NOT existe THEN
    CALL fgl_winmessage(
    " Atencion",
    " Parametro de maximo de dias posteriores no existe registrado.\n Definir parametro antes de ingresar el inventario.",
    "stop")
    RETURN 
 END IF

 -- Inicio del loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop   

  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos 
     CALL inving002_inival(1) 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_fis.codbod,
                w_mae_fis.codcat,
                w_mae_fis.subcat,
                w_mae_fis.fecinv WITHOUT DEFAULTS
    ATTRIBUTES(UNBUFFERED) 

   ON ACTION cancel 
    -- Salida 
    IF INFIELD(codbod) THEN
       LET loop = FALSE
       CALL inving002_inival(1)
       EXIT INPUT
    ELSE
       CALL inving002_inival(1)
       NEXT FIELD codbod
    END IF 

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON CHANGE codbod 
    -- Obteniendo datos de la bodega 
    CALL librut003_bbodega(w_mae_fis.codbod) 
    RETURNING w_mae_bod.*,existe 

    -- Asignando datos de empresa y sucursal de la bodega
    LET w_mae_fis.codemp = w_mae_bod.codemp 
    LET w_mae_fis.codsuc = w_mae_bod.codsuc 

    -- Obteniendo datos de la empresa 
    CALL librut003_bempresa(w_mae_bod.codemp)
    RETURNING w_mae_emp.*,existe 
    -- Obteniendo datos de la sucursal 
    CALL librut003_bsucursal(w_mae_bod.codsuc)
    RETURNING w_mae_suc.*,existe 
   
    -- Desplegando datos de la empresa y sucursal 
    DISPLAY BY NAME w_mae_emp.nomemp,w_mae_suc.nomsuc

    -- Limpiando combos de categorias y subcategorias
    LET w_mae_fis.codcat = NULL
    LET w_mae_fis.subcat = NULL
    CLEAR codcat,subcat

   ON CHANGE codcat
    -- Limpiando combos
    LET w_mae_fis.subcat = NULL
    CLEAR subcat

    -- Llenando combox de subcategorias
    IF w_mae_fis.codcat IS NOT NULL THEN
       CALL librut003_cbxsubcategorias(w_mae_fis.codcat)
    END IF

   AFTER FIELD codbod
    -- Verificando bodega
    IF w_mae_fis.codbod IS NULL THEN
       ERROR "Error: bodega invalida, VERIFICA ..."
       NEXT FIELD codbod 
    END IF 

   AFTER FIELD fecinv
    -- Verificando fecha de emision
    IF w_mae_fis.fecinv IS NULL OR 
       (w_mae_fis.fecinv >(TODAY+diamax)) OR 
       (w_mae_fis.fecinv <(TODAY-diamax)) THEN 
       ERROR "Error: fecha del inventario invalida, VERIFICA ..."
       NEXT FIELD fecinv
    END IF

    -- Asignando datos de fecha y anio 
    LET w_mae_fis.aniotr = YEAR(w_mae_fis.fecinv) 
    LET w_mae_fis.mestra = MONTH(w_mae_fis.fecinv) 

   AFTER INPUT
    -- Verificando datos
    IF w_mae_fis.codbod IS NULL THEN
       NEXT FIELD codbod
    END IF
    IF w_mae_fis.fecinv IS NULL THEN
       NEXT FIELD fecinv 
    END IF

    -- Asignando datos de fecha y anio 
    LET w_mae_fis.aniotr = YEAR(w_mae_fis.fecinv) 
    LET w_mae_fis.mestra = MONTH(w_mae_fis.fecinv) 
  END INPUT 
  IF NOT loop THEN
     EXIT WHILE 
  END IF 

  -- Cargando productos de la bodega para la toma del fisico 
  CALL inving002_cargaproducto()
  IF (totlin>0) THEN
     -- Ingresando detalle de productos
     LET retroceso = inving002_detingreso()
  ELSE
     CALL fgl_winmessage(
     " Atencion",
     " No existen productos registrados en la bodega que inventariar. \n Verifica datos del inventario.",
     "stop")
  END IF 
 END WHILE

 -- Inicializando datos 
 CALL inving002_inival(1) 
END FUNCTION

-- Subutina para el ingreso del detalle de productos del inventario 

FUNCTION inving002_detingreso()
 DEFINE loop       SMALLINT,
        opc        SMALLINT, 
        retroceso  SMALLINT 

 -- Iniciando el loop
 LET retroceso = FALSE
 LET loop      = TRUE
 WHILE loop
  -- Ingresando productos
  INPUT ARRAY v_products WITHOUT DEFAULTS FROM s_products.*
       ATTRIBUTE(MAXCOUNT=totlin,INSERT ROW=FALSE,
                 APPEND ROW=FALSE,DELETE ROW=FALSE,
                 ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
   ON ACTION ACCEPT
    -- Aceptar
    EXIT INPUT

   ON ACTION cancel 
    -- Cancelar
    LET loop = FALSE
    EXIT INPUT

   ON ACTION calculator
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

   ON CHANGE codepq
    -- Calculando cantidad total en unidades totales
    CALL inving002_cantidadtotal(ARR_CURR(),SCR_LINE())

    -- Totalizando unidades
    CALL inving002_totdet()

   ON CHANGE canepq
    -- Calculando cantidad total en unidades totales
    CALL inving002_cantidadtotal(ARR_CURR(),SCR_LINE())

    -- Totalizando unidades
    CALL inving002_totdet()

   BEFORE INPUT 
    -- Totalizando unidades
    CALL inving002_totdet()

   BEFORE ROW
    -- Cargando combobox de empaques x producto
    IF v_products[ARR_CURR()].codpro IS NOT NULL THEN 
       CALL librut003_cbxempaques(v_products[ARR_CURR()].codpro,1)
    ELSE
       LET v_products[ARR_CURR()].codepq = 0 
    END IF 

   AFTER FIELD codepq
    -- Verificando empaque
    IF v_products[ARR_CURR()].codepq IS NULL THEN
       LET v_products[ARR_CURR()].codepq = 0 
       DISPLAY v_products[ARR_CURR()].codepq TO s_products[SCR_LINE()].codepq
       NEXT FIELD codepq
    END IF 

    -- Calculando cantidad total en unidades totales
    CALL inving002_cantidadtotal(ARR_CURR(),SCR_LINE())

    -- Totalizando unidades
    CALL inving002_totdet()

   AFTER FIELD canepq
    -- Verificando cantidad
    IF v_products[ARR_CURR()].canepq IS NULL OR
       (v_products[ARR_CURR()].canepq <0) THEN 
       ERROR "Error: cantidad invalida."
       NEXT FIELD canepq 
    END IF 

    -- Calculando cantidad total en unidades totales
    CALL inving002_cantidadtotal(ARR_CURR(),SCR_LINE())

    -- Totalizando unidades
    CALL inving002_totdet()

   AFTER ROW
    -- Totalizando unidades
    CALL inving002_totdet()

   AFTER INPUT 
    -- Totalizando unidades
    CALL inving002_totdet()
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Verificando que se ingrese al menos un producto
  IF (totuni<=0) THEN
     CALL fgl_winmessage(
     " Atencion",
     "Debe ingresarse la cantidad fisica de por lo menos un producto.",
     "stop")
     CONTINUE WHILE
  END IF

  -- Menu de opciones
  LET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  -- Verificando opcion 
  CASE (opc)
   WHEN 0 -- Cancelando
    LET loop      = FALSE
    LET retroceso = FALSE
   WHEN 1 -- Grabando
    LET loop      = FALSE
    LET retroceso = FALSE

    -- Grabando inventario 
    CALL inving002_grabar()

   WHEN 2 -- Modificando
    LET loop = TRUE 
  END CASE
 END WHILE

 RETURN retroceso
END FUNCTION

-- Subrutina para cargar el producto de la bodega seleccionada 

FUNCTION inving002_cargaproducto()  
 DEFINE strcodcat  STRING,
        strsubcat  STRING,
        qrytext    STRING

 -- Verificando seleccion de categoria
 LET strcodcat = NULL
 IF w_mae_fis.codcat IS NOT NULL THEN
    LET strcodcat = "AND d.codcat = ",w_mae_fis.codcat
 END IF

 -- Verificando condicion de subcategoria
 LET strsubcat = NULL
 IF w_mae_fis.subcat IS NOT NULL THEN
    LET strsubcat = "AND e.subcat = ",w_mae_fis.subcat
 END IF

 -- Preparando seleccion de productos
 LET qrytext = "SELECT x.cditem,y.codabr,y.dsitem,x.exican ",
                "FROM  inv_proenbod x,inv_products y,inv_categpro d,inv_subcateg e ",
                "WHERE x.codemp = ",w_mae_fis.codemp, 
                 " AND x.codsuc = ",w_mae_fis.codsuc, 
                 " AND x.codbod = ",w_mae_fis.codbod, 
                 " AND x.cditem = y.cditem ",
                 " AND y.estado = 1",
                 " AND d.codcat = y.codcat ",
                 " AND e.codcat = y.codcat ",
                 " AND e.subcat = y.subcat ",
                 strcodcat CLIPPED," ",
                 strsubcat CLIPPED," ",
                 "ORDER BY 1"  

 -- Seleccionando productos
 PREPARE cpinv FROM qrytext 
 DECLARE cprod CURSOR FOR cpinv 
 LET totlin = 1
 FOREACH cprod INTO v_products[totlin].codpro,
                    v_products[totlin].cditem,
                    v_products[totlin].dsitem, 
                    v_products[totlin].canexi 

  -- Cargando inventario si ya existe 
  SELECT NVL(a.codepq,0),
         NVL(a.canepq,0),
         NVL(a.canuni,0),
         NVL(a.canexi,0) 
   INTO  v_products[totlin].codepq,
         v_products[totlin].canepq,
         v_products[totlin].canuni,
         v_products[totlin].canexi
   FROM  inv_tofisico a
   WHERE a.codemp = w_mae_fis.codemp
     AND a.codsuc = w_mae_fis.codsuc
     AND a.codbod = w_mae_fis.codbod
     AND a.aniotr = w_mae_fis.aniotr 
     AND a.mestra = w_mae_fis.mestra
     AND a.fecinv = w_mae_fis.fecinv
     AND a.cditem = v_products[totlin].codpro 

  IF v_products[totlin].codepq IS NULL THEN LET v_products[totlin].codepq = 0 END IF 
  IF v_products[totlin].canepq IS NULL THEN LET v_products[totlin].canepq = 0 END IF 
  IF v_products[totlin].canuni IS NULL THEN LET v_products[totlin].canuni = 0 END IF 
  IF v_products[totlin].canexi IS NULL THEN LET v_products[totlin].canexi = 0 END IF 

  -- Caculando diferencia
  LET v_products[totlin].candif = (v_products[totlin].canuni-v_products[totlin].canexi)

  -- Incrementando contador 
  LET totlin = (totlin+1)
 END FOREACH
 CLOSE cprod
 FREE  cprod
 LET totlin = (totlin-1)

 -- Desplegando totales
 LET msg = "Total Productos (",totlin USING "<<<,<<<",")"
 CALL f.setElementText("labelt",msg) 
END FUNCTION 

-- Subrutina para calcular las undiades totales (unidades ingresdas * cantidad empaque)

FUNCTION inving002_cantidadtotal(arr,scr) 
 DEFINE wequival LIKE inv_epqsxpro.cantid,  
        arr,scr  SMALLINT

 -- Calculando unidades totales
 LET wequival = librut003_canempaque(v_products[arr].codpro,v_products[arr].codepq)
 LET v_products[arr].canuni = (v_products[arr].canepq*wequival)

 -- Caculando diferencia
 LET v_products[arr].candif = (v_products[arr].canuni-v_products[arr].canexi)

 -- Desplegando datos
 DISPLAY v_products[arr].canuni TO s_products[scr].canuni 
 DISPLAY v_products[arr].candif TO s_products[scr].candif 
END FUNCTION 

-- Subrutina para totalizar unidades contadas

FUNCTION inving002_totdet()
 DEFINE i SMALLINT

 -- Totalizando
 LET totuni = 0
 FOR i = 1 TO totlin
  IF v_products[i].codpro IS NULL THEN
     CONTINUE FOR
  END IF 

  -- Totalizando
  LET totuni = (totuni+v_products[i].canuni)
 END FOR

 -- Desplegando total de unidades del inventario 
 DISPLAY BY NAME totuni ATTRIBUTE(REVERSE,YELLOW) 
END FUNCTION 

-- Subrutina para grabar el movimiento 

FUNCTION inving002_grabar()
 DEFINE i,finmes SMALLINT

 -- Grabando transaccion
 ERROR " Registrando Inventario Fisico ..." ATTRIBUTE(CYAN)

 -- Verificando si fecha es de ultimo dia del mes
 IF (librut001_diasmes(w_mae_fis.mestra,w_mae_fis.aniotr)=DAY(w_mae_fis.fecinv)) THEN 
    LET finmes = 1
 ELSE
    LET finmes = 0
 END IF 

 -- Iniciando la transaccion
 BEGIN WORK

   -- 1. Grabando inventario fisico
   FOR i = 1 TO totlin 
    IF v_products[i].cditem IS NULL OR 
       v_products[i].canuni IS NULL THEN 
       CONTINUE FOR 
    END IF 

    -- Eliminando registro antes de insertar
    SET LOCK MODE TO WAIT
    DELETE FROM inv_tofisico 
    WHERE inv_tofisico.codemp = w_mae_fis.codemp
      AND inv_tofisico.codsuc = w_mae_fis.codsuc
      AND inv_tofisico.codbod = w_mae_fis.codbod
      AND inv_tofisico.aniotr = w_mae_fis.aniotr 
      AND inv_tofisico.mestra = w_mae_fis.mestra
      AND inv_tofisico.fecinv = w_mae_fis.fecinv
      AND inv_tofisico.cditem = v_products[i].codpro 

    -- Grabando
    IF (v_products[i].canepq>0) THEN 
     SET LOCK MODE TO WAIT
     INSERT INTO inv_tofisico 
     VALUES (w_mae_fis.codemp       , -- empresa 
             w_mae_fis.codsuc       , -- sucursal 
             w_mae_fis.codbod       , -- bodega   
             w_mae_fis.aniotr       , -- anio del inventario
             w_mae_fis.mestra       , -- mes del inventario
             w_mae_fis.fecinv       , -- fecha del inventario 
             v_products[i].codpro   , -- codigo del producto 
             v_products[i].cditem   , -- codigo del producto abreviado
             v_products[i].codepq   , -- codigo del empaque
             v_products[i].canepq   , -- cantidad fisica del empaque
             v_products[i].canuni   , -- cantidad fisica del inventario
             0                      , -- total en valores del inventario 
             v_products[i].canexi   , -- existencia teorica del producto al ingreso 
             finmes                 , -- Inventario es fin de mes
             USER                   , -- Usuario que registro el inventario 
             CURRENT                , -- Fecha de registro del inventario 
             CURRENT HOUR TO SECOND)  -- Hora de registro del inventario 
    END IF 
   END FOR 

   -- Desplegando mensaje
   CALL fgl_winmessage(" Atencion"," Inventario fisico registrado.","information")

 -- Finalizando la transaccion
 COMMIT WORK

 ERROR "" 
END FUNCTION 

-- Subrutina para inicializar las variables de trabajo 

FUNCTION inving002_inival(i)
 DEFINE i      SMALLINT
 
 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1  
   INITIALIZE w_mae_fis.* TO NULL 
   CLEAR FORM 
 END CASE 

 -- Inicializando datos
 LET totlin = 0 
 LET totuni = 0

 -- Lllenando combo de bodegas
 CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME")) 

 -- Inicializando vectores de datos
 CALL inving002_inivec() 

 -- Desplegando total de unidades del inventario 
 DISPLAY BY NAME totuni,diamin,diamax ATTRIBUTE(REVERSE,YELLOW) 
 CALL f.setElementText("labelt","Total Productos (0)")
END FUNCTION

-- Subrutina para inicializar y limpiar vector de trabajo 

FUNCTION inving002_inivec()
 DEFINE i SMALLINT

 -- Inicializando vectores
 CALL v_products.clear()

 LET totlin = 0 
 FOR i = 1 TO 8
  -- Limpiando vector       
  CLEAR s_products[i].*
 END FOR 
END FUNCTION 
