{ 
Fecha    : Diciiembre 2011        
Programo : Mynor Ramirez
Objetivo : Programa de criterios de seleccion para:
           Consulta de ingreso de inventarios fisicos
}

-- Definicion de variables globales 
GLOBALS "invglb002.4gl" 

-- Subrutina para consultar ingresos de inventario fisico

FUNCTION invqbx002_fisicos()
 DEFINE qrytext,qrypart     CHAR(500),
        loop,existe         SMALLINT,
        qryres,msg,titqry   CHAR(80)

 -- Definiendo nivel de aislamiento 
 SET ISOLATION TO DIRTY READ 

 -- Buscando datos 
 LET loop = TRUE 
 WHILE loop 
  -- Inicializando las variables 
  INITIALIZE qrytext,qrypart TO NULL
  LET int_flag = 0 
  CALL inving002_inival(1)

  -- Construyendo busqueda 
  CONSTRUCT BY NAME qrytext 
                 ON a.codbod, 
                    a.fecinv 

   ON ACTION cancel
    -- Salida
    LET loop = FALSE
    EXIT CONSTRUCT

   ON ACTION calculator 
    -- Cargando calculadora
    IF NOT winshellexec("calc") THEN
       ERROR "Atencion: calculadora no disponible."
    END IF

  END CONSTRUCT
  IF NOT loop THEN
     EXIT WHILE
  END IF 

  -- Preparando la busqueda 
  ERROR " Seleccionando datos ... por favor espere ..." ATTRIBUTE(CYAN) 

  -- Creando la busqueda 
  LET qrypart = "SELECT UNIQUE a.codemp,a.codsuc,a.codbod,a.fecinv ",
                 "FROM  inv_tofisico a ",
                 "WHERE EXISTS (SELECT b.userid FROM inv_permxbod b WHERE b.userid = USER AND b.codbod = a.codbod) ",
                  " AND ", qrytext CLIPPED,
                  " ORDER BY a.codemp,a.codsuc,a.codbod,a.fecinv DESC" 

  -- Declarando el cursor 
  PREPARE estqbe002 FROM qrypart
  DECLARE c_fisico SCROLL CURSOR WITH HOLD FOR estqbe002  
  OPEN c_fisico 
  FETCH FIRST c_fisico INTO w_mae_fis.codemp,w_mae_fis.codsuc,w_mae_fis.codbod,w_mae_fis.fecinv
  IF (status = NOTFOUND) THEN
     INITIALIZE w_mae_fis.* TO NULL
     ERROR ""
     CALL fgl_winmessage(
     "Atencion",
     "No existen inventarios fisicos con el criterio seleccionado.",
     "stop") 
  ELSE
     ERROR "" 

     -- Desplegando datos 
     CALL invqbx002_datos()

     -- Fetchando inventarios fisicos
     MENU "Consulta" 
      COMMAND "Siguiente"
       "Visualiza el siguiente inventario fisico en la lista."
       FETCH NEXT c_fisico INTO w_mae_fis.codemp,w_mae_fis.codsuc,w_mae_fis.codbod,w_mae_fis.fecinv
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas inventarios fisicos siguientes en lista.", 
           "information")
           FETCH LAST c_fisico INTO w_mae_fis.codemp,w_mae_fis.codsuc,w_mae_fis.codbod,w_mae_fis.fecinv
       END IF 

       -- Desplegando datos 
       CALL invqbx002_datos()

      COMMAND "Anterior"
       "Visualiza el inventario fisico anterior en la lista."
       FETCH PREVIOUS c_fisico INTO w_mae_fis.codemp,w_mae_fis.codsuc,w_mae_fis.codbod,w_mae_fis.fecinv
        IF (status = NOTFOUND) THEN
           CALL fgl_winmessage(
           "Atencion",
           "No existen mas inventarios fisicos anteriores en lista.", 
           "information")
           FETCH FIRST c_fisico INTO w_mae_fis.codemp,w_mae_fis.codsuc,w_mae_fis.codbod,w_mae_fis.fecinv
        END IF

        -- Desplegando datos 
        CALL invqbx002_datos()

      COMMAND "Primero" 
       "Visualiza el primer inventario fisico en la lista."
       FETCH FIRST c_fisico INTO w_mae_fis.codemp,w_mae_fis.codsuc,w_mae_fis.codbod,w_mae_fis.fecinv
        -- Desplegando datos 
        CALL invqbx002_datos()

      COMMAND "Ultimo" 
       "Visualiza el ultimo movimiento en la lista."
       FETCH LAST c_fisico INTO w_mae_fis.codemp,w_mae_fis.codsuc,w_mae_fis.codbod,w_mae_fis.fecinv
        -- Desplegando datos
        CALL invqbx002_datos()

      COMMAND "Productos"
       "Permite visualizar el detalle completo de productos del inventario fisico."
       CALL invqbx002_verproductos()

      COMMAND "Consultar" 
       "Regresa a la pantalla de seleccion."
       EXIT MENU

      COMMAND KEY(F4,CONTROL-E)
       EXIT MENU
     END MENU
   END IF     
  CLOSE c_fisico
 END WHILE

 -- Inicializando datos 
 CALL inving002_inival(1)
END FUNCTION 

-- Subrutina para desplegar los datos del registro 

FUNCTION invqbx002_datos()
 DEFINE existe SMALLINT

 -- Desplegando datos 
 CLEAR FORM

 -- Obteniendo datos de la empresa
 CALL librut003_bempresa(w_mae_fis.codemp)
 RETURNING w_mae_emp.*,existe

 -- Obteniendo datos de la sucursal
 CALL librut003_bsucursal(w_mae_fis.codsuc)
 RETURNING w_mae_suc.*,existe

 -- Desplegando datos del inventario 
 CLEAR FORM 
 DISPLAY BY NAME w_mae_fis.codbod,w_mae_fis.fecinv, 
                 w_mae_emp.nomemp,w_mae_suc.nomsuc

 -- Seleccionando detalle del inventario 
 CALL invqbx002_detalle()
END FUNCTION 

-- Subrutina para seleccionar datos del detalle del inventario

FUNCTION invqbx002_detalle()
 DEFINE existe  SMALLINT,
        msg     STRING 

 -- Asignando datos de fecha y anio
 LET w_mae_fis.aniotr = YEAR(w_mae_fis.fecinv)
 LET w_mae_fis.mestra = MONTH(w_mae_fis.fecinv)

 -- Inicializando vector de productos
 CALL inving002_inivec()
 
 -- Seleccionando detalle del inventario 
 DECLARE cdet CURSOR FOR
 SELECT x.cditem, 
        x.codabr,
        y.dsitem,
        x.codepq,
        x.canepq,
        x.canuni,
        x.canexi,
        (x.canuni-x.canexi) 
  FROM  inv_tofisico x, inv_products y
  WHERE x.codemp = w_mae_fis.codemp
    AND x.codsuc = w_mae_fis.codsuc
    AND x.codbod = w_mae_fis.codbod
    AND x.aniotr = w_mae_fis.aniotr
    AND x.mestra = w_mae_fis.mestra
    AND x.fecinv = w_mae_fis.fecinv
    AND y.cditem = x.cditem 
  ORDER BY 2 

  LET totlin = 1 
  FOREACH cdet INTO v_products[totlin].*
   -- Desplegando datos en vector pantalla
   IF (totlin<=12) THEN
      DISPLAY v_products[totlin].* TO s_products[totlin].* ATTRIBUTE(BLUE) 
   END IF 

   -- Incrementando contador
   LET totlin = (totlin+1) 
  END FOREACH
  CLOSE cdet
  FREE  cdet
  LET totlin = (totlin-1)  

 -- Totalizando detalle
 CALL inving002_totdet()

 -- Desplegando totales
 LET msg = "Total Productos (",totlin USING "<<<,<<<",")"
 CALL f.setElementText("labelt",msg)
END FUNCTION 

-- Subrutina para ver el detalle de productos

FUNCTION invqbx002_verproductos()
 -- Desplegando productos
 DISPLAY ARRAY v_products TO s_products.*
  ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE,BLUE)

  ON ACTION cancel
   EXIT DISPLAY

  ON ACTION calculator
   -- Cargando calculadora
   IF NOT winshellexec("calc") THEN
      ERROR "Atencion: calculadora no disponible."
   END IF

  BEFORE ROW
   -- Verificando control del total de lineas
   IF (ARR_CURR()>totlin) THEN
      CALL FGL_SET_ARR_CURR(1)
   END IF
 END DISPLAY
END FUNCTION 
