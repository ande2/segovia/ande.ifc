{ 
invglo017.4gl
Mynor Ramirez
Mantenimiento de puntos de venta
}

DATABASE segovia 

{ Definicion de variables globales }

GLOBALS

CONSTANT linpan = 50

DEFINE w_mae_pro   RECORD LIKE fac_puntovta.*,
       v_puntos    DYNAMIC ARRAY OF RECORD
        tnumpos    LIKE fac_puntovta.numpos,
        tnompos    LIKE fac_puntovta.nompos 
       END RECORD,
       v_cajeros  DYNAMIC ARRAY OF RECORD
        tcheckb    SMALLINT,
        tuserid    LIKE inv_permxbod.userid,
        tnomusr    VARCHAR(50),
        tusuaid    LIKE inv_permxbod.usuaid,
        tfecsis    LIKE inv_permxbod.fecsis,
        thorsis    LIKE inv_permxbod.horsis
       END RECORD,
       w_mae_suc  RECORD LIKE glb_sucsxemp.*,
       w_mae_emp  RECORD LIKE glb_empresas.*, 
       w_mae_bod  RECORD LIKE inv_mbodegas.*
END GLOBALS
