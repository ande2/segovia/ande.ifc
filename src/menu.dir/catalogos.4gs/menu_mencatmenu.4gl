#########################################################################
## Function  : catMenu()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Opciones de Programa Menu de Catalogos
#########################################################################
SCHEMA segovia

GLOBALS "menuGlobals.4gl"
FUNCTION catMenu()
  DEFINE recbac  RECORD LIKE mprog.*
  DEFINE rec  RECORD LIKE mprog.*
  DEFINE reg_dprogopc RECORD LIKE dprogopc.*
  DEFINE qry  STRING
  DEFINE cmb  STRING
  DEFINE cmb1  STRING
  DEFINE cmb2  STRING
  DEFINE resp SMALLINT
  DEFINE message STRING
  DEFINE tit	  STRING
  DEFINE where_clause STRING

  LET cmb = "SELECT a.prog_id, a.prog_nom FROM mprog a WHERE a.prog_tip IN('M') AND est_id = 13"
  LET cmb1 = "SELECT a.est_id, a.est_desc FROM mest a WHERE a.est_id IN(13,14,20)"

  OPEN WINDOW menCatMenu AT 1,1 WITH FORM "menCatMenu" 
      LET tit = vempr_nom CLIPPED, " - [C�talogos de Menus]"
  CALL fgl_settitle(tit)
      MENU ""
          BEFORE MENU
              HIDE OPTION ALL
              SHOW OPTION "buscar","agregar"
              INITIALIZE rec.* TO NULL
              INITIALIZE reg_dprogopc.* TO NULL
              LET recbac.* = rec.*
              LET usuario = FGL_GETENV("LOGNAME")

          ON ACTION buscar
              SHOW OPTION ALL
              CALL combo_din2("prog_padre", cmb)
              CALL combo_din2("est_id", cmb1)

              LET recbac.* = rec.*

              INITIALIZE rec.* TO NULL	

              DISPLAY BY NAME rec.prog_id, rec.prog_nom, rec.prog_tip, 
                             rec.prog_dirpro, rec.prog_padre, rec.prog_des,
                             rec.est_id, rec.prog_verhe

              CONSTRUCT BY NAME where_clause ON prog_id, 
                                                            prog_nom, 
                                                            est_id,
                                                            prog_tip, 
                                                            prog_verhe,
                                                            prog_dirpro,
                                                            prog_padre,
                                                            prog_des

              AFTER CONSTRUCT
                  IF int_flag = FALSE THEN
                      CALL get_fldbuf(prog_tip, prog_verhe) RETURNING  rec.prog_tip, rec.prog_verhe
                      CASE 
                      WHEN rec.prog_tip = '1' 
                          LET rec.prog_tip = "P"
                          EXIT CASE
                      WHEN rec.prog_tip = '2' 
                          LET rec.prog_tip = "R"
                          EXIT CASE
                      WHEN rec.prog_tip = '0' 
                          LET rec.prog_tip = "M"
                          EXIT CASE
                      END CASE

                      CASE 
                      WHEN rec.prog_verhe = 'BDL' 
                          LET rec.prog_verhe = 0
                      WHEN rec.prog_tip = 'GENERO' 
                          LET rec.prog_tip = 1
                      WHEN rec.prog_tip = 'BDL 3.54' 
                          LET rec.prog_tip = 2 
                      WHEN rec.prog_tip = 'GENERO 1.30' 
                          LET rec.prog_tip = 3 
                      END CASE

                      DISPLAY BY NAME rec.prog_tip, rec.prog_verhe
                  ELSE
                      EXIT CONSTRUCT 
                  END IF
              END CONSTRUCT

              IF int_flag = FALSE THEN
                  LET qry = 
                  "SELECT * FROM mprog WHERE est_id = 13 and ", where_clause CLIPPED, 
                  "ORDER BY 1"

                  PREPARE prpCM1 FROM qry
                  DECLARE curCM1 SCROLL CURSOR WITH HOLD FOR prpCM1

                  OPEN curCM1
                  FETCH FIRST curCM1 INTO rec.*

                  IF STATUS = NOTFOUND THEN
                      CALL msg("No existen datos en la tabla")
                      HIDE OPTION ALL
                      SHOW OPTION "buscar","agregar"
                  END IF

                  CASE 
                      WHEN rec.prog_tip = 'R' 
                            LET rec.prog_tip = "2"
                      WHEN rec.prog_tip = 'P' 
                            LET rec.prog_tip = "1"
                      WHEN rec.prog_tip = 'M' 
                            LET rec.prog_tip = "0"
                  END CASE

                  DISPLAY BY NAME rec.prog_id, rec.prog_nom, rec.prog_tip, 
                                 rec.prog_dirpro, rec.prog_padre, rec.prog_des,
                                 rec.est_id, rec.prog_verhe
              ELSE
                  CALL msg("B�squeda Cancelada")
                  LET int_flag = FALSE
                  LET rec. * = recbac.*
                  DISPLAY BY NAME rec.prog_id, rec.prog_nom, rec.prog_tip, 
                                 rec.prog_dirpro, rec.prog_padre, rec.prog_des,
                                 rec.est_id
              END IF


          ON ACTION agregar
              INITIALIZE rec.* TO NULL
              LET int_flag = FALSE

              LET cmb = "SELECT prog_id, prog_nom FROM mprog a WHERE a.prog_tip IN('M')"
              LET cmb1 = "SELECT est_id, est_desc FROM mest a WHERE a.est_id IN(13)"
              CALL combo_din2("prog_padre", cmb)
              CALL combo_din2("est_id", cmb1)

              LET rec.est_id = 13
                                    
              DISPLAY BY NAME rec.prog_id,
                                    rec.est_id,
                                    rec.prog_nom, 
                                    rec.prog_tip, 
                                    rec.prog_dirpro, 
                                    rec.prog_padre, 
                                    rec.prog_des, 
                                    rec.prog_verhe 

              INPUT BY NAME rec.prog_nom, rec.prog_tip, rec.prog_verhe, rec.prog_dirpro,
                                 rec.prog_padre, rec.prog_des WITHOUT DEFAULTS

                  AFTER INPUT
                      IF (NOT int_flag) THEN
                          LET rec.prog_id = 0
                          LET rec.prog_fechor = CURRENT
                          CASE
                          WHEN rec.prog_tip = 1  
                                 LET rec.prog_tip = "P"
                                 EXIT CASE
                          WHEN rec.prog_tip = 2
                                 LET rec.prog_tip = "R"

                      SELECT MAX(mprog.prog_idx)
                                 INTO rec.prog_idx	
                      FROM mprog mprog
                      WHERE mprog.prog_tip = 'R'
                                 IF rec.prog_idx IS NULL THEN
                                     LET rec.prog_idx = 0
                                 END IF
                                 LET rec.prog_idx = rec.prog_idx + 1 

                                 EXIT CASE
                          WHEN rec.prog_tip = 0  
                                 LET rec.prog_tip = "M"
                                 EXIT CASE
                          END CASE
                          BEGIN WORK
                          INSERT INTO mprog VALUES (rec.*)
                          IF rec.prog_tip = "R" THEN
                             LET reg_dprogopc.linkprop = 0
                              LET reg_dprogopc.prog_id = SQLCA.SQLERRD[2]
                              LET reg_dprogopc.prog_ord = 1
                              LET reg_dprogopc.des_cod = 30
                            LET reg_dprogopc.prop_desc = rec.prog_des
                            LET reg_dprogopc.prog_mail = NULL
                              INSERT INTO dprogopc VALUES 
                                    (reg_dprogopc.*)
                          END IF
                          IF STATUS = 0 THEN
                              COMMIT WORK
                              CALL msg(cAddOK)
                              DISPLAY BY NAME rec.prog_id, rec.prog_nom, 
                                                    rec.prog_tip, rec.prog_dirpro,
                                                    rec.prog_padre, rec.prog_des,
                                                    rec.est_id, rec.prog_verhe
                          ELSE
                              ROLLBACK WORK
                              CALL msg(cErr)
                          END IF
                      ELSE
                          LET int_flag = FALSE
                      END IF
              END INPUT
              CLEAR FORM


     ON ACTION actualizar
          LET recbac.* = rec.*
          IF rec.est_id <> 20 THEN
              LET cmb1 = "SELECT est_id, est_desc FROM mest WHERE mest.est_id IN(13,14,20)"

              CALL combo_din2("est_id",cmb1)
              CALL combo_din2("prog_padre",cmb)

              IF rec.prog_id <> cRaiz THEN
                    LET int_flag = FALSE

                    INPUT BY NAME rec.prog_nom, rec.est_id,rec.prog_tip, 
                                     rec.prog_dirpro, rec.prog_padre,
                                     rec.prog_des WITHOUT DEFAULTS

                    AFTER INPUT
                        IF (NOT int_flag) THEN
                            CASE
                          WHEN rec.prog_tip = 2  
                              LET rec.prog_tip = "R"
                          WHEN rec.prog_tip = 1  
                              LET rec.prog_tip = "P"
                          WHEN rec.prog_tip = 0  
                              LET rec.prog_tip = "M"
                          END CASE

                            BEGIN WORK
                            LET qry = "UPDATE mprog SET prog_nom = ?, prog_tip = ?, prog_dirpro = ?, prog_padre = ?, prog_des = ?, est_id = ? WHERE prog_id = ?"
                            PREPARE prpCMU FROM qry
                            EXECUTE prpCMU USING rec.prog_nom, rec.prog_tip, rec.prog_dirpro, rec.prog_padre, rec.prog_des, rec.est_id, rec.prog_id
                    IF STATUS = 0 THEN
                    COMMIT WORK
                    CALL msg(cUpdOK)
                            DISPLAY BY NAME rec.prog_id, 
                                                     rec.prog_nom, 
                                             rec.prog_tip, 
                                                     rec.prog_dirpro,
                                             rec.prog_padre, 
                                                     rec.prog_des
                    ELSE
                    ROLLBACK WORK
                    CALL msg(cErr)
                    END IF
                ELSE
                    LET int_flag = False
                END IF
            END INPUT
                    #CLEAR FORM
                    --HIDE OPTION ALL
                    --SHOW OPTION "buscar","agregar"
            END IF
            ELSE
            CALL msg("NO PUEDE MODIFICAR EL REGISTRO")
                HIDE OPTION ALL
                SHOW OPTION "buscar","agregar"
            END IF

ON ACTION eliminar
IF rec.est_id <> 20 THEN
IF rec.prog_id <> cRaiz THEN
   LET resp = FALSE
   LET resp = confirma("Esta seguro que desea eliminar el registro?")
   IF resp THEN

      LET rec.est_id = 20

      BEGIN WORK
        LET qry = "UPDATE mprog SET est_id = ? WHERE prog_id = ?" 
      PREPARE prpCMD FROM qry
      EXECUTE prpCMD USING rec.est_id, rec.prog_id
      IF STATUS = 0 THEN
         COMMIT WORK
         CALL msg(cDelOk)
         CLOSE curCM1
         OPEN curCM1
         FETCH FIRST curCM1 INTO rec.*
            DISPLAY BY NAME rec.prog_id, rec.prog_nom, 
                rec.prog_tip, rec.prog_dirpro,
                rec.prog_padre, rec.prog_des
      ELSE
         CALL msg(cErr)
         ROLLBACK WORK
      END IF
   END IF
        CLEAR FORM
        HIDE OPTION ALL
        SHOW OPTION "buscar","agregar"
END IF
ELSE
CALL msg("NO PUEDE MODIFICAR EL REGISTRO")
    HIDE OPTION ALL
    SHOW OPTION "buscar","agregar"
END IF

ON ACTION primero
FETCH FIRST curCM1 INTO rec.*
IF rec.prog_tip = "P" THEN
   LET rec.prog_tip = "PROGRAMA"
END IF
CASE 
WHEN rec.prog_tip = 'R' 
     LET rec.prog_tip = "2"
          EXIT CASE
WHEN rec.prog_tip = 'P' 
     LET rec.prog_tip = "1"
          EXIT CASE
WHEN rec.prog_tip = 'M' 
     LET rec.prog_tip = "0"
          EXIT CASE
END CASE

DISPLAY BY NAME rec.prog_id, rec.prog_nom, 
             rec.prog_tip, rec.prog_dirpro,
             rec.prog_padre, rec.prog_des,rec.est_id

ON ACTION siguiente
FETCH NEXT curCM1 INTO rec.*
IF sqlca.sqlcode  = 100 THEN
   LET message = "Esta posicionado en el ultimo registro"
   CALL msg(message) 
ELSE
    CASE 
    WHEN rec.prog_tip = 'R' 
     LET rec.prog_tip = "2"
          EXIT CASE
    WHEN rec.prog_tip = 'P' 
     LET rec.prog_tip = "1"
          EXIT CASE
    WHEN rec.prog_tip = 'M' 
     LET rec.prog_tip = "0"
          EXIT CASE
    END CASE
    DISPLAY BY NAME rec.prog_id, rec.prog_nom, 
             rec.prog_tip, rec.prog_dirpro,
             rec.prog_padre, rec.prog_des,rec.est_id
END IF


ON ACTION anterior
FETCH PREVIOUS curCM1 INTO rec.*
IF sqlca.sqlcode  = 100 THEN
   LET message = "Esta posicionado en el primer registro"
   CALL msg(message) 
ELSE
    CASE 
    WHEN rec.prog_tip = 'R' 
     LET rec.prog_tip = "2"
          EXIT CASE
    WHEN rec.prog_tip = 'P' 
     LET rec.prog_tip = "1"
          EXIT CASE
    WHEN rec.prog_tip = 'M' 
     LET rec.prog_tip = "0"
          EXIT CASE
    END CASE

    DISPLAY BY NAME rec.prog_id, rec.prog_nom, 
             rec.prog_tip, rec.prog_dirpro,
             rec.prog_padre, rec.prog_des,rec.est_id
END IF

ON ACTION ultimo
FETCH LAST curCM1 INTO rec.*
CASE 
WHEN rec.prog_tip = 'R' 
     LET rec.prog_tip = "2"
WHEN rec.prog_tip = 'P' 
    LET rec.prog_tip = "1"
WHEN rec.prog_tip = 'M' 
    LET rec.prog_tip = "0"
END CASE
DISPLAY BY NAME rec.prog_id, rec.prog_nom, 
            rec.prog_tip, rec.prog_dirpro,
            rec.prog_padre, rec.prog_des,rec.est_id

COMMAND KEY(INTERRUPT)
EXIT MENU
END MENU
CLOSE WINDOW menCatMenu
END FUNCTION