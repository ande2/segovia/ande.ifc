{
invqbe011.4gl 
Mynor Ramirez
Mantenimiento de medidas
}

{ Definicion de variables globales }

GLOBALS "invglo011.4gl" 
DEFINE totlin INT

-- Subrutina para busqueda de datos en el mantenimiento 

FUNCTION invqbe011_medidpro(operacion)
 DEFINE arrcols             DYNAMIC ARRAY OF VARCHAR(255),
        qrytext,qrypart     CHAR(1000),
        loop,existe,opc,res SMALLINT,
        operacion,scr,arr   SMALLINT,
        titmenu             STRING,
        qry                 STRING,
        msg                 STRING,
        rotulo              CHAR(12),
        w                   ui.Window,
        f                   ui.Form

  -- Obteniendo datos de la window actual
  LET w = ui.Window.getCurrent()
  LET f = w.getForm()

  -- Verificando operacion
  LET rotulo = NULL
  IF (operacion=3) THEN 
     LET rotulo  = "Borrar" 
  END IF   

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Inicion del loop
  LET loop = TRUE
  WHILE loop
   -- Inicializando las variables
   INITIALIZE qrytext,qrypart TO NULL
   CALL invmae011_inival(1)
   LET int_flag = 0

   -- Construyendo busqueda
   CONSTRUCT BY NAME qrytext ON a.codmed,a.codabr,a.desmed,a.unimed,a.epqfra,a.xlargo,
                                a.yancho,a.medtot,a.unimto,a.userid,a.fecsis,a.horsis 
    ATTRIBUTE(CANCEL=FALSE)

    ON ACTION cancel 
     -- Salida
     CALL invmae011_inival(1)
     LET loop = FALSE
     EXIT CONSTRUCT

   END CONSTRUCT
   IF NOT loop THEN
      EXIT WHILE
   END IF

   -- Preparando la busqueda
   ERROR "Busqueda en progreso ... por favor esperar ..." ATTRIBUTE(CYAN)

   -- Creando la busqueda
   LET qrypart = " SELECT UNIQUE a.codmed,a.codabr "||
                 " FROM inv_medidpro a "||
                 " WHERE "||qrytext CLIPPED||
                 " ORDER BY 2 "

   -- Declarando el cursor
   PREPARE cprod FROM qrypart
   DECLARE c_medidpro SCROLL CURSOR WITH HOLD FOR cprod

   -- Inicializando vector de seleccion
   CALL v_medidpro.clear() 
   
   --Inicializando contador
   LET totlin = 1

   -- Llenando vector de seleccion 
   FOREACH c_medidpro INTO v_medidpro[totlin].*         
    -- Incrementando contador
    LET totlin = (totlin+1) 
   END FOREACH 
   CLOSE c_medidpro
   FREE  c_medidpro
   LET totlin = (totlin-1) 
   ERROR "" 

   -- Verificando si hubieron datos   
   IF (totlin>0) THEN 
    -- Desplegando vector de seleccion 
    DISPLAY ARRAY v_medidpro TO s_medidpro.*
     ATTRIBUTE(COUNT=totlin,ACCEPT=FALSE,CANCEL=FALSE)

     ON ACTION cancel 
      -- Salida
      CALL invmae011_inival(1)
      LET loop = FALSE
      EXIT DISPLAY 

     ON ACTION buscar
      -- Ruscar 
      CALL invmae011_inival(1)
      EXIT DISPLAY 

     ON ACTION modificar
      -- Modificando 
      IF invmae011_medidpro(2) THEN
         EXIT DISPLAY 
      ELSE 
         CALL invqbe011_datos(v_medidpro[ARR_CURR()].tcodmed)
      END IF 

     ON KEY (CONTROL-M) 
      -- Modificando 
      IF (operacion=2) THEN 
         IF invmae011_medidpro(2) THEN
            EXIT DISPLAY 
         ELSE 
            CALL invqbe011_datos(v_medidpro[ARR_CURR()].tcodmed)
         END IF 
      END IF 

     ON ACTION borrar
      -- Borrando
      -- Verificando integridad 
      IF invqbe011_integridad() THEN
         CALL fgl_winmessage(
         " Atencion",
         " Esta medida existe en algun producto. \n Medida no puede borrarse.",
         "stop")
         CONTINUE DISPLAY 
      END IF 

      -- Comfirmacion de la accion a ejecutar 
      LET msg = " Esta SEGURO de "||rotulo CLIPPED||" esta medida ? "
      LET opc = librut001_menuopcs("Confirmacion",
                                    msg,
                                    "Si",
                                    "No",
                                    NULL,
                                    NULL)

      -- Verificando operacion
      CASE (opc) 
       WHEN 1
         IF (operacion=3) THEN
             --  Eliminando
             CALL invmae011_grabar(3)
             EXIT DISPLAY
         END IF 
       WHEN 2
         CONTINUE DISPLAY
      END CASE 

     ON ACTION reporte
      -- Reporte de datos seleccionados a excel
      -- Asignando nombre de columnas
      LET arrcols[1]  = "Medida"
      LET arrcols[2]  = "Codigo Abreviado"
      LET arrcols[3]  = "Nombre" 
      LET arrcols[4]  = "Unidad de Medida" 
      LET arrcols[5]  = "Empaque Fraccionable" 
      LET arrcols[6]  = "Alto" 
      LET arrcols[7]  = "Ancho" 
      LET arrcols[8]  = "Medida Referencia" 
      LET arrcols[9]  = "Unidad de Medida Referencia" 
      LET arrcols[10] = "Usuario Registro"
      LET arrcols[11] = "Fecha Registro"
      LET arrcols[12] = "Hora Registro"

      -- Se aplica el mismo query de la seleccion para enviar al reporte
      LET qry        = "SELECT a.codmed,a.codabr,a.desmed,d.nommed,a.epqfra,a.xlargo,",
                              "a.yancho,a.medtot,b.nommed,a.userid,a.fecsis,a.horsis ",
                       " FROM inv_medidpro a,inv_unimedid d,outer (inv_unimedid b) ",
                       " WHERE d.unimed = a.unimed ",
                         " AND b.unimed = a.unimto ",
                         " AND ", qrytext CLIPPED,
                       " ORDER BY 1 "

      -- Ejecutando el reporte
      LET res        = librut002_excelreport("Unidades de Medida",qry,12,1,arrcols)

     BEFORE ROW 
      -- Asignando contadores del vector
      LET arr = ARR_CURR()
      LET scr = SCR_LINE()

      -- Verificando control del total de lineas 
      IF (ARR_CURR()>totlin) THEN
         CALL FGL_SET_ARR_CURR(1)
         CALL invqbe011_datos(v_medidpro[1].tcodmed)
      ELSE
         CALL invqbe011_datos(v_medidpro[ARR_CURR()].tcodmed)
      END IF 

     BEFORE DISPLAY
      -- Desabilitando y habilitando opciones segun operacion
      CASE (operacion) 
       WHEN 1 -- Consultar 
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
       WHEN 2 -- Modificar
	 CALL DIALOG.setActionActive("modificar",TRUE)
	 CALL DIALOG.setActionActive("borrar",FALSE)
         CALL DIALOG.setActionActive("reporte",FALSE)
       WHEN 3 -- Eliminar        
	 CALL DIALOG.setActionActive("modificar",FALSE)
	 CALL DIALOG.setActionActive("borrar",TRUE)
         CALL DIALOG.setActionActive("reporte",FALSE)
      END CASE
    END DISPLAY 
   ELSE 
    CALL fgl_winmessage(
    " Atencion",
    " No existen medidas con el criterio seleccionado.",
    "stop")
   END IF 
  END WHILE
END FUNCTION

{ Subrutina para desplegar la datos del mantenimiento }

FUNCTION invqbe011_datos(wcodmed)
 DEFINE wcodmed LIKE inv_medidpro.codmed,
        existe    SMALLINT,
        qryres    STRING 

 -- Creando seleccion  
 LET qryres ="SELECT a.* "||
              "FROM  inv_medidpro a "||
              "WHERE a.codmed = "||wcodmed||
              " ORDER BY 2 "

 -- Declarando el cursor
 PREPARE cprodt FROM qryres
 DECLARE c_medidprot SCROLL CURSOR WITH HOLD FOR cprodt

 -- Llenando vector de seleccion
 FOREACH c_medidprot INTO w_mae_pro.*
  -- Desplegando datos
  DISPLAY BY NAME w_mae_pro.codabr,w_mae_pro.desmed THRU w_mae_pro.unimto 
  DISPLAY BY NAME w_mae_pro.codmed,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
 END FOREACH
 CLOSE c_medidprot
 FREE  c_medidprot

 -- Desplegando datos 
 DISPLAY BY NAME w_mae_pro.codabr,w_mae_pro.desmed THRU w_mae_pro.unimto 
 DISPLAY BY NAME w_mae_pro.codmed,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION 

{ Subrutina para verificar si la medida existe en algun prodcuto }

FUNCTION invqbe011_integridad()
 DEFINE conteo SMALLINT

 -- Verificando 
 SELECT COUNT(*)
  INTO  conteo
  FROM  inv_products a
  WHERE (a.codmed = w_mae_pro.codmed) 
  IF (conteo>0) THEN
     RETURN TRUE
  ELSE
     RETURN FALSE
  END IF

 RETURN FALSE 
END FUNCTION 
