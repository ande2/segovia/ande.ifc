{ 
Fecha    : Enero 2011
Programo : invrep005.4gl 
Objetivo : Reporte comparativo de inventario fisico
}

DATABASE segovia 

{ Definicion de variables globales }

DEFINE w_mae_bod RECORD LIKE inv_mbodegas.*,
       w_mae_emp RECORD LIKE glb_empresas.*,
       w_mae_suc RECORD LIKE glb_sucsxemp.*,
       w_datos   RECORD
        codbod   LIKE inv_mtransac.codbod,
        codemp   LIKE inv_mtransac.codemp,
        codsuc   LIKE inv_mtransac.codbod,
        codcat   LIKE inv_categpro.codcat,
        subcat   LIKE inv_subcateg.subcat,
        fecinv   DATE
       END RECORD,
       fnt       RECORD
        cmp      CHAR(12),
        nrm      CHAR(12),
        tbl      CHAR(12),
        fbl,t88  CHAR(12),
        t66,p12  CHAR(12),
        p10,srp  CHAR(12),
        twd      CHAR(12),
        fwd      CHAR(12),
        tda,fda  CHAR(12),
        ini      CHAR(12)
       END RECORD,
       existe    SMALLINT,
       filename  STRING,
       fcodcat   STRING,
       fsubcat   STRING,
       ffecinv   STRING

-- Subrutina principal 

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar7")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invrep005.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Llamando al reporte
 CALL invrep005_invfisico()
END MAIN

-- Subrutina para ingresar los parametros del reporte

FUNCTION invrep005_invfisico()
 DEFINE w_pro_bod         RECORD LIKE inv_proenbod.*,
        w_mae_pro         RECORD LIKE inv_products.*,
        imp1              RECORD
         codemp           LIKE inv_proenbod.codemp, 
         codsuc           LIKE inv_proenbod.codsuc, 
         codbod           LIKE inv_proenbod.codbod,
         cditem           LIKE inv_proenbod.cditem, 
         codabr           CHAR(20),
         dsitem           CHAR(35),
         nommed           CHAR(15),
         exican           LIKE inv_proenbod.exican,
         exifis           LIKE inv_proenbod.exican, 
         codepq           LIKE inv_epqsxpro.codepq,
         canepq           LIKE inv_epqsxpro.cantid,
         nomepq           CHAR(15) 
        END RECORD,
        wpais             VARCHAR(255),
        pipeline,qrytxt   STRING,
        strcodcat         STRING,
        strsubcat         STRING,
        loop              SMALLINT

 -- Abriendo la ventana para el reporte
 OPEN WINDOW wrep005a AT 5,2
  WITH FORM "invrep005a" ATTRIBUTE(BORDER)

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Desplegando datos del encabezado
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invrep005",wpais,1)

  -- Definiendo archivo de impresion
  LET filename = FGL_GETENV("SPOOLDIR") CLIPPED,"/invrep005.spl"

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Lllenando  combobox de bodegas x usuario
  CALL librut003_cbxbodegasxusuario(FGL_GETENV("LOGNAME"))
  -- Llenando combox de categorias
  CALL librut003_cbxcategorias()

  -- Inicio del loop
  LET loop = TRUE 
  WHILE loop 
   -- Inicializando datos
   INITIALIZE w_datos.*,pipeline TO NULL
   CLEAR FORM

   -- Construyendo busqueda
   INPUT BY NAME w_datos.codbod,
                 w_datos.codcat,
                 w_datos.subcat,
                 w_datos.fecinv
                 WITHOUT DEFAULTS ATTRIBUTES(UNBUFFERED,CANCEL=FALSE,ACCEPT=FALSE)

    ON ACTION salir
     -- Salida
     LET loop = FALSE
     EXIT INPUT

    ON ACTION visualizar
     -- Asignando dispositivo 
     LET pipeline = "screen" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)

     -- Verificando datos 
     IF w_datos.codbod IS NULL OR 
        w_datos.fecinv IS NULL THEN
        ERROR "Error: deben de completarse los filtros de seleccion ..." 
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

    ON ACTION imprimir 
     -- Asignando dispositivo 
     LET pipeline = "local" 

     -- Obteniendo filtros
     LET fcodcat = GET_FLDBUF(w_datos.codcat)
     LET fsubcat = GET_FLDBUF(w_datos.subcat)

     -- Verificando datos 
     IF w_datos.codbod IS NULL OR 
        w_datos.fecinv IS NULL THEN
        ERROR "Error: deben de completarse los filtros de seleccion ..." 
        NEXT FIELD codbod
     END IF 
     EXIT INPUT 

    ON CHANGE codbod
     -- Obteniendo datos de la bodega
     CALL librut003_bbodega(w_datos.codbod)
     RETURNING w_mae_bod.*,existe

     -- Asignando datos de empresa y sucursal de la bodega
     LET w_datos.codemp = w_mae_bod.codemp
     LET w_datos.codsuc = w_mae_bod.codsuc

     -- Obteniendo datos de la empresa
     CALL librut003_bempresa(w_mae_bod.codemp)
     RETURNING w_mae_emp.*,existe
     -- Obteniendo datos de la sucursal
     CALL librut003_bsucursal(w_mae_bod.codsuc)
     RETURNING w_mae_suc.*,existe

     -- Desplegando datos de la empresa y sucursal
     DISPLAY BY NAME w_datos.codemp,w_datos.codsuc,w_mae_emp.nomemp,w_mae_suc.nomsuc

     -- Limpiando combos de categorias y subcategorias
     LET w_datos.codcat = NULL
     LET w_datos.subcat = NULL
     CLEAR codcat,subcat

    ON CHANGE codcat
     -- Limpiando combos
     LET w_datos.subcat = NULL
     CLEAR subcat

     -- Llenando combox de subcategorias
     IF w_datos.codcat IS NOT NULL THEN 
        CALL librut003_cbxsubcategorias(w_datos.codcat)
     END IF 

    AFTER FIELD codbod
     -- Verificando bodega 
     IF w_datos.codbod IS NULL THEN
        ERROR "Error: debe de seleccionarse la bodega a listar."
        NEXT FIELD codbod
     END IF

    BEFORE FIELD fecinv
     -- Cargando combo de fecha de inventario fisico
     CALL librut003_cbxfecfisico(w_datos.codemp,w_datos.codsuc,w_datos.codbod)

    AFTER FIELD fecinv 
     -- Verificando fecha de inventario
     IF w_datos.fecinv IS NULL THEN
        NEXT FIELD fecinv
     END IF

    AFTER INPUT 
     -- Verificando datos
     IF w_datos.codbod IS NULL OR 
        w_datos.fecinv IS NULL OR
        pipeline IS NULL THEN
        NEXT FIELD codbod
     END IF
   END INPUT
   IF NOT loop THEN
      EXIT WHILE
   END IF 

   -- Verificando seleccion de categoria
   LET strcodcat = NULL
   IF w_datos.codcat IS NOT NULL THEN
      LET strcodcat = "AND d.codcat = ",w_datos.codcat
   END IF

   -- Verificando condicion de subcategoria
   LET strsubcat = NULL
   IF w_datos.subcat IS NOT NULL THEN
      LET strsubcat = "AND e.subcat = ",w_datos.subcat
   END IF

   -- Construyendo seleccion 
   LET qrytxt = "SELECT x.codemp,x.codsuc,x.codbod,x.cditem,b.codabr,b.dsitem,c.nommed,", 
                       "y.canexi,y.canuni ",
                  "FROM  inv_proenbod x,inv_tofisico y,inv_products b,inv_unimedid c,inv_categpro d,inv_subcateg e ",
                  "WHERE x.codemp = ",w_datos.codemp,
                   " AND x.codsuc = ",w_datos.codsuc,
                   " AND x.codbod = ",w_datos.codbod,
                   " AND y.codemp = x.codemp ", 
                   " AND y.codsuc = x.codsuc ", 
                   " AND y.codbod = x.codbod ", 
                   " AND y.aniotr = ",YEAR(w_datos.fecinv), 
                   " AND y.mestra = ",MONTH(w_datos.fecinv), 
                   " AND y.fecinv = '",w_datos.fecinv,"' ",
                   " AND y.cditem = x.cditem ", 
                   " AND b.cditem = x.cditem ",
                   " AND c.unimed = b.unimed ",
                   " AND d.codcat = b.codcat ",
                   " AND e.codcat = b.codcat ",
                   " AND e.subcat = b.subcat ",
                   strcodcat CLIPPED," ",
                   strsubcat CLIPPED," ",
                   " ORDER BY 1,2,3,4"

   -- Preparando seleccion
   ERROR "Atencion: seleccionando datos ... por favor espere ..."
   PREPARE c_rep005 FROM qrytxt 
   DECLARE c_crep005 CURSOR FOR c_rep005
   LET existe = FALSE
   FOREACH c_crep005 INTO imp1.* 
    -- Iniciando reporte
    IF NOT existe THEN
       -- Seleccionando fonts para impresora epson
       CALL librut001_fontsprn(pipeline,"epson")
       RETURNING fnt.*

       LET existe = TRUE
       LET existe = TRUE
       START REPORT invrep005_impinvfis TO filename
    END IF 

    -- Obteniendo primer empaque
    INITIALIZE imp1.codepq,imp1.canepq,imp1.nomepq TO NULL
    SELECT a.codepq,a.cantid,a.nomepq
     INTO  imp1.codepq,imp1.canepq,imp1.nomepq 
     FROM  inv_epqsxpro a
     WHERE a.lnkepq = (SELECT MIN(b.lnkepq)
                        FROM  inv_epqsxpro b
                        WHERE b.cditem = imp1.cditem
                          AND b.codepq >0)
      AND  a.cditem = imp1.cditem

    -- Llenando el reporte
    OUTPUT TO REPORT invrep005_impinvfis(imp1.*)
   END FOREACH
   CLOSE c_crep005 
   FREE  c_crep005 

   IF existe THEN
      -- Finalizando el reporte
      FINISH REPORT invrep005_impinvfis 

      -- Enviando reporte al destino seleccionado
      CALL librut001_enviareporte(filename,pipeline,"Comparativo de Inventario Fisico")
      ERROR "" 
      CALL fgl_winmessage(" Atencion","Reporte Emitido ...","information") 
   ELSE
      ERROR "" 
      CALL fgl_winmessage(" Atencion","No existen datos con el filtro seleccionado.","stop") 
   END IF 
  END WHILE
 CLOSE WINDOW wrep005a   
END FUNCTION 

-- Subrutina para generar el reporte 

REPORT invrep005_impinvfis(imp1)
 DEFINE imp1      RECORD
         codemp   LIKE inv_proenbod.codemp, 
         codsuc   LIKE inv_proenbod.codsuc, 
         codbod   LIKE inv_proenbod.codbod,
         cditem   LIKE inv_proenbod.cditem, 
         codabr   CHAR(20),
         dsitem   CHAR(35),
         nommed   CHAR(15),
         exican   LIKE inv_proenbod.exican, 
         exifis   LIKE inv_proenbod.exican,
         codepq   LIKE inv_epqsxpro.codepq,
         canepq   LIKE inv_epqsxpro.cantid,
         nomepq   CHAR(15) 
        END RECORD,
        convercon DEC(14,6),
        converexi DEC(14,6),
        converdif DEC(14,6),
        linea     CHAR(158),
        exis      SMALLINT

  OUTPUT LEFT MARGIN 2
         PAGE LENGTH 66
         TOP MARGIN 3 
         BOTTOM MARGIN 3 

  FORMAT 
   PAGE HEADER
    LET linea = "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________",
                "__________________________________________________",
                "______________________________________"

    -- Configurando tipos de letra
    PRINT ASCII 27 -- color negro

    -- Imprimiendo Encabezado
    PRINT COLUMN   1,"Inventarios",
	  COLUMN 134,PAGENO USING "Pagina: <<<<"
    PRINT COLUMN   1,"Invrep005",
          COLUMN  59,"COMPARARATIVO DE INVENTARIO FISICO",
          COLUMN 134,"Fecha : ",TODAY USING "dd/mmm/yyyy" 
    PRINT COLUMN   1,"(",FGL_GETENV("LOGNAME") CLIPPED,")",
          COLUMN  59,"Fecha del Inventario (",w_datos.fecinv USING "dd/mmm/yyyy",")", 
          COLUMN 134,"Hora  : ",TIME 
    PRINT linea 
    PRINT "Codigo                Descripcion del Producto             Unida",
          "d          --- Existencias Unidad de Medida ----- Existencias Empaque (1) ----- EMPAQUE"
    PRINT "Producto                                                   Medid",
          "a             Fisica   Inventario   Diferencia    Fisica  Inventario  Diferencia"
    PRINT linea

   BEFORE GROUP OF imp1.codbod 
    -- Imprimiendo datos de la bodega
    PRINT "EMPRESA  (",imp1.codemp USING "<<<",") ",w_mae_emp.nomemp CLIPPED
    PRINT "SUCURSAL (",imp1.codsuc USING "<<<",") ",w_mae_suc.nomsuc CLIPPED
    PRINT "BODEGA   (",imp1.codbod USING "<<<",") ",w_mae_bod.nombod CLIPPED
    SKIP 1 LINES
 
   ON EVERY ROW
    -- Calculando empaque 1 si aplica
    LET convercon = 0
    LET converexi = 0
    LET converdif = 0
    IF imp1.codepq IS NOT NULL THEN
       LET convercon = (imp1.exifis/imp1.canepq)
       LET converexi = (imp1.exican/imp1.canepq)
       LET converdif = ((imp1.exifis-imp1.exican)/imp1.canepq)
    END IF 

    -- Imprimiendo productos
    PRINT COLUMN   1,imp1.codabr                                 ,2 SPACES,
                     imp1.dsitem                                 ,2 SPACES,
                     imp1.nommed                                 ,1 SPACES,
                     imp1.exifis           USING "---,--&.&&"    ,2 SPACES,
                     imp1.exican           USING "---,--&.&&"    ,2 SPACES,
                    (imp1.exifis-
                     imp1.exican)          USING "---,--&.&&"    ,2 SPACES,
                     convercon             USING "---,--&.&&"    ,1 SPACES,
                     converexi             USING "---,--&.&&"    ,2 SPACES,
                     converdif             USING "--,--&.&&"     ,1 SPACES,
                     imp1.nomepq  
 
   AFTER GROUP OF imp1.codbod
    -- Totalizando por bodega
    SKIP 1 LINES
    PRINT COLUMN   1,"Total ",GROUP COUNT(*)    USING "<<<,<<&",
                     " Producto(s) en Bodega"
    SKIP 1 LINES 

   ON LAST ROW
    -- Imprimiendo filtros
    PRINT fnt.twd CLIPPED,"FILTROS",fnt.fwd CLIPPED
    IF (LENGTH(fcodcat)>0) THEN
       PRINT "Categoria    : ",fcodcat
    END IF
    IF (LENGTH(fsubcat)>0) THEN
       PRINT "Subcategoria : ",fsubcat
    END IF
END REPORT 
