{
invmae008.4gl 
Mynor Ramirez
Mantenimiento de sucursales 
}

-- Definicion de variables globales 

GLOBALS "invglo008.4gl"
DEFINE existe SMALLINT

-- Subrutina principal

MAIN
 -- Atrapando interrupts
 DEFER INTERRUPT

 -- Cargando estilos y acciones default
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")
 CALL ui.Interface.loadToolbar("../../std/toolbar")

 -- Definiendo teclas de control
 OPTIONS HELP KEY CONTROL-W,
         HELP FILE "inventario.hlp",
         MESSAGE LINE LAST

 -- Definiendo archivo de errores
 CALL startlog("invmae008.log")

 -- Cerrando pantalla
 CLOSE WINDOW SCREEN

 -- Menu de principal 
 CALL invmae008_mainmenu()
END MAIN

-- Subrutina para el menu principal del mantenimiento

FUNCTION invmae008_mainmenu()
 DEFINE titulo   STRING,
        wpais    VARCHAR(255),
        savedata SMALLINT

 -- Abriendo la ventana de mantenimiento 
 OPEN WINDOW wing001a AT 5,2
  WITH FORM "invmae008a" ATTRIBUTE(BORDER)

  -- Desplegando datos del encabezado 
  CALL librut001_parametros(1,0)
  RETURNING existe,wpais
  CALL librut001_header("invmae008",wpais,1) 

  -- Definiendo nivel de aislamiento
  SET ISOLATION TO DIRTY READ

  -- Menu de opciones
  MENU " Sucursals"
   BEFORE MENU
    -- Verificando accesos
    -- Consultar 
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),4) THEN 
       HIDE OPTION "Buscar"
    END IF
     --Ingresar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),1) THEN 
       HIDE OPTION "Nuevo"
    END IF
    -- Modificar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),2) THEN 
       HIDE OPTION "Modificar"
    END IF
    -- Deshabilitar
    IF NOT seclib001_accesos(FGL_GETENV("LOGNAME"),3) THEN 
       HIDE OPTION "Borrar"
    END IF
   COMMAND "Buscar"
    " Busqueda de sucursales."
    CALL invqbe008_sucursales(1) 
   COMMAND "Nuevo"
    " Ingreso de una nueva sucursal."
    LET savedata = invmae008_sucursales(1) 
   COMMAND "Modificar"
    " Modificacion de una sucursal existente."
    CALL invqbe008_sucursales(2) 
   COMMAND "Borrar"
    " Eliminacion de una sucursal existente."
    CALL invqbe008_sucursales(3) 
   COMMAND "Salir"
    " Salir del menu."
    EXIT MENU
   COMMAND KEY(F4,CONTROL-E)
    EXIT MENU
  END MENU
 CLOSE WINDOW wing001a
END FUNCTION

-- Subrutina para el ingreso o modificacion de datos del mantenimiento 

FUNCTION invmae008_sucursales(operacion)
 DEFINE loop,existe,opc   SMALLINT,
        operacion         SMALLINT,
        retroceso         SMALLINT,
        savedata          SMALLINT,
        msg               CHAR(80),
        qrytext           STRING 

 -- Verificando si opcion es nuevo ingreso
 IF (operacion=1) THEN
    LET retroceso = FALSE
 ELSE
    LET retroceso = TRUE
 END IF

 -- Inicio del loop
 LET loop = TRUE
 WHILE loop
  -- Verificando que no sea regreso
  IF NOT retroceso THEN
     -- Inicializando datos
     IF (operacion=1) THEN 
        CALL invmae008_inival(1)
     END IF 
  END IF

  -- Ingresando datos
  INPUT BY NAME w_mae_pro.codemp,
                w_mae_pro.nomsuc,
                w_mae_pro.nomabr,
                w_mae_pro.numnit,
                w_mae_pro.numtel,
                w_mae_pro.numfax,
                w_mae_pro.dirsuc
                WITHOUT DEFAULTS 
                ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE) 

   ON ACTION cancel    
    -- Salida
    LET loop = FALSE
    EXIT INPUT

   BEFORE INPUT
    -- Verificando integridad
    -- Si sucursal ya tiene movimientos no se puede modificar la empresa, la sucursal y el nombre
    IF (operacion=2) THEN -- Si es modificacion
     IF invqbe008_integridad() THEN
        CALL Dialog.SetFieldActive("codemp",FALSE)
        CALL Dialog.SetFieldActive("nomsuc",FALSE)
     ELSE
        CALL Dialog.SetFieldActive("codemp",TRUE)
        CALL Dialog.SetFieldActive("nomsuc",TRUE)
     END IF
    END IF

   BEFORE FIELD codemp
    -- Cargando combobox de empresas
    CALL librut003_cbxempresas() 

   AFTER FIELD codemp 
    --Verificando empresa
    IF w_mae_pro.codemp IS NULL THEN
       NEXT FIELD codemp
    END IF

   AFTER FIELD nomsuc  
    --Verificando nombre del sucursal
    IF (LENGTH(w_mae_pro.nomsuc)=0) THEN
       ERROR "Error: nombre de la sucursal invalida, VERIFICA"
       LET w_mae_pro.nomsuc = NULL
       NEXT FIELD nomsuc  
    END IF

    -- Verificando que no exista otra sucursal con el mismo nombre
    SELECT UNIQUE (a.codsuc)
     FROM  glb_sucsxemp a
     WHERE (a.codsuc != w_mae_pro.codsuc) 
       AND (a.nomsuc  = w_mae_pro.nomsuc) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otra sucursal con el mismo nombre, VERIFICA ...",
        "information")
        NEXT FIELD nomsuc
     END IF 
   AFTER FIELD numnit  
    --Verificando numero de nit 
    IF (LENGTH(w_mae_pro.numnit)=0) THEN
       ERROR "Error: numero de NIT invalido, VERIFICA"
       LET w_mae_pro.numnit = NULL
       NEXT FIELD numnit  
    END IF

    {-- Verificando que no exista otro NIT                      
    SELECT UNIQUE (a.numnit)
     FROM  glb_sucsxemp a
     WHERE (a.codsuc != w_mae_pro.codsuc) 
       AND (a.numnit  = w_mae_pro.numnit) 
     IF (status!=NOTFOUND) THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Existe otra sucursal con el mismo numero de NIT, VERIFICA ...",
        "information")
        NEXT FIELD numnit
     END IF } 
   AFTER FIELD dirsuc
    --Verificando direccion
    IF (LENGTH(w_mae_pro.dirsuc)=0) THEN
       ERROR "Error: direccion invalida, VERIFICA"
       LET w_mae_pro.dirsuc = NULL
       NEXT FIELD dirsuc  
    END IF
   AFTER INPUT   
    --Verificando ingreso de datos
    --Verificando empresa
    IF w_mae_pro.codemp IS NULL THEN
       NEXT FIELD codemp 
    END IF
    IF w_mae_pro.nomsuc IS NULL THEN 
       NEXT FIELD nomsuc
    END IF
    IF w_mae_pro.nomabr IS NULL THEN 
       NEXT FIELD nomabr
    END IF
    IF w_mae_pro.numnit IS NULL THEN 
       NEXT FIELD numnit
    END IF
    IF w_mae_pro.dirsuc IS NULL THEN 
       NEXT FIELD dirsuc
    END IF
  END INPUT
  IF NOT loop THEN
     EXIT WHILE
  END IF

  -- Menu de opciones
  LET savedata = FALSE 
  lET opc = librut001_menugraba("Confirmacion",
                                "Que desea hacer?",
                                "Guardar",
                                "Modificar",
                                "Cancelar",
                                "")

  CASE (opc)
   WHEN 0 -- Cancelando
    IF (operacion=1) THEN 
        CALL invmae008_inival(1)
    END IF 
    LET loop = FALSE
   WHEN 1 -- Grabando
    LET loop = FALSE

    -- Grabando sucursal
    CALL invmae008_grabar(operacion)
    LET loop     = FALSE
    LET savedata = TRUE 
   WHEN 2 -- Modificando
    LET retroceso = TRUE
    CONTINUE WHILE
  END CASE 
 END WHILE

 -- Si operacion es ingreso 
 IF (operacion=1) THEN
    CALL invmae008_inival(1) 
 END IF 

 -- Verificando grabacion 
 RETURN savedata 
END FUNCTION

-- Subrutina para grabar/modificar una sucursal

FUNCTION invmae008_grabar(operacion)
 DEFINE operacion SMALLINT,
        xcditem   INTEGER,
        msg       CHAR(80)

 -- Grabando transaccion
 ERROR " Guardando sucursal ..." ATTRIBUTE(CYAN)

 -- Iniciando la transaccion
 BEGIN WORK

 -- Grabando/Modificando
 -- Verificando operacon
 CASE (operacion)
  WHEN 1 -- Grabando 
   -- Asignando datos
   SELECT NVL(MAX(a.codsuc),0)
    INTO  w_mae_pro.codsuc 
    FROM  glb_sucsxemp a
    IF (w_mae_pro.codsuc IS NULL) THEN
       LET w_mae_pro.codsuc = 1
    ELSE
       LET w_mae_pro.codsuc = w_mae_pro.codsuc+1
    END IF

   -- Grabando 
   SET LOCK MODE TO WAIT
   INSERT INTO glb_sucsxemp   
   VALUES (w_mae_pro.*)
   DISPLAY BY NAME w_mae_pro.codsuc 

   --Asignando el mensaje 
   LET msg = "Sucursal (",w_mae_pro.codsuc USING "<<<<<<",") registrada."
  WHEN 2 -- Modificando
   -- Actualizando
   SET LOCK MODE TO WAIT

   --Actualizando 
   UPDATE glb_sucsxemp
   SET    glb_sucsxemp.*      = w_mae_pro.*
   WHERE  glb_sucsxemp.codsuc = w_mae_pro.codsuc 

   --Asignando el mensaje 
   LET msg = "Sucursal (",w_mae_pro.codsuc USING "<<<<<<",") actualizada."
  WHEN 3 -- Borrando
   -- Borrando         
   SET LOCK MODE TO WAIT

   --Borrando sucursales
   DELETE FROM glb_sucsxemp 
   WHERE  glb_sucsxemp.codsuc = w_mae_pro.codsuc 

   --Asignando el mensaje 
   LET msg = "Sucursal (",w_mae_pro.codsuc USING "<<<<<<",") borrado."
 END CASE

 -- Finalizando la transaccion
 COMMIT WORK
 ERROR "" 

 -- Desplegando mensaje
 CALL fgl_winmessage(" Atencion",msg,"information")

 -- Inicializando datos
 IF (operacion=1) THEN 
    CALL invmae008_inival(1)
 END IF 
END FUNCTION

-- Subrutina para inicializar las variables de trabajo 

FUNCTION invmae008_inival(i)
 DEFINE i SMALLINT

 -- Verificando tipo de inicializacion
 CASE (i)
  WHEN 1
   INITIALIZE w_mae_pro.* TO NULL
   LET w_mae_pro.codsuc = 0
   LET w_mae_pro.userid = FGL_GETENV("LOGNAME")
   LET w_mae_pro.fecsis = CURRENT
   LET w_mae_pro.horsis = CURRENT HOUR TO SECOND
   CLEAR FORM
 END CASE

 -- Desplegando datos
 DISPLAY BY NAME w_mae_pro.codemp,w_mae_pro.codsuc,w_mae_pro.nomsuc THRU w_mae_pro.dirsuc 
 DISPLAY BY NAME w_mae_pro.codsuc,w_mae_pro.userid THRU w_mae_pro.horsis ATTRIBUTE(REVERSE)
END FUNCTION
