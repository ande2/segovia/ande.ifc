{
Programo: Mynor Ramirez
Fecha   : Diciembre 2011
Objetivo: Menu Principal SISTEMA DE INVENTARIOS Y FACTURACION 
} 

DATABASE segovia 

MAIN
 DEFINE aui        om.DomNode 
 DEFINE smp        om.DomNode -- Menu Principal 
 DEFINE sm0        om.DomNode -- Inventarios 
 DEFINE sm0a       om.DomNode -- Catalogos
 DEFINE sm0b       om.DomNode -- Procesos       
 DEFINE sm0c       om.DomNode -- Reportes 

 DEFINE sm1        om.DomNode -- Ventas 
 DEFINE sm1a       om.DomNode -- Catalogos
 DEFINE sm1b       om.DomNode -- Procesos       
 DEFINE sm1c       om.DomNode -- Reportes 

 DEFINE opt        om.DomNode 
 DEFINE wpais      VARCHAR(255) 
 DEFINE cmdstr     STRING 
 DEFINE titlestr   STRING 
 DEFINE g_useriddb STRING 
 DEFINE g_useridos STRING 
 DEFINE existe     SMALLINT 

 OPTIONS ON CLOSE APPLICATION STOP

 -- Abriendo forma del menu
 OPEN FORM wmenu FROM "mainmenu" 
 DISPLAY FORM wmenu 

 -- Cargando estilos del menu
 CALL ui.Interface.loadActionDefaults("../../std/actiondefaults")
 CALL ui.Interface.loadStyles("../../std/styles")

 -- Desplegando datos del encabezado
 CALL librut001_parametros(1,0)
 RETURNING existe,wpais 
 CALL librut001_header("mainmenu",wpais,1)

 -- Definiendo arbol del menu 
 LET aui = ui.Interface.getRootNode()
 LET smp = aui.createChild("StartMenu") 
 LET titlestr = "Menu Principal"
 CALL smp.setAttribute("text",titlestr)

 -- Inventario de Productos
 LET sm0 = createStartMenuGroup(smp,"Inventarios","")

   -- Catalogos
      LET sm0a = createStartMenuGroup(sm0,"Catalogos","mars_catalogos")

      LET cmdstr = "fglrun empresas.42r segovia"
      LET opt    = createStartMenuCommand(sm0a,"Empresas",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun sucursales.42r segovia"
      LET opt    = createStartMenuCommand(sm0a,"Sucursales",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun categorias.42r segovia"
      LET opt    = createStartMenuCommand(sm0a,"Categorias",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun subcategorias.42r segovia"
      LET opt    = createStartMenuCommand(sm0a,"Sub-Categorias",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun colores.42r segovia"
      LET opt    = createStartMenuCommand(sm0a,"Colores",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun paises.42r segovia"
      LET opt    = createStartMenuCommand(sm0a,"Paises",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun proveedores.42r segovia"
      LET opt    = createStartMenuCommand(sm0a,"Proveedores",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun unidadesmedida.42r segovia"
      LET opt    = createStartMenuCommand(sm0a,"Unidades de Medida ",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun medidas.42r segovia"
      LET opt    = createStartMenuCommand(sm0a,"Medidas de Producto",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun empaques.42r segovia"
      LET opt    = createStartMenuCommand(sm0a,"Empaques",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun productos.42r segovia"
      LET opt    = createStartMenuCommand(sm0a,"Productos",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun bodegas.42r segovia"
      LET opt    = createStartMenuCommand(sm0a,"Bodegas",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun tiposmovimiento.42r segovia"
      LET opt    = createStartMenuCommand(sm0a,"Tipos de Movimiento",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun clientes.42r segovia"
      LET opt    = createStartMenuCommand(sm0a,"Clientes",cmdstr,"mars_p1") 

   -- Procesos    
      LET sm0b = createStartMenuGroup(sm0,"Procesos","mars_procesos")

      LET cmdstr = "fglrun movimientos.42r segovia"
      LET opt    = createStartMenuCommand(sm0b,"Movimientos de Inventario",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun ingresofisico.42r segovia"
      LET opt    = createStartMenuCommand(sm0b,"Inventario Fisico",cmdstr,"mars_p1") 

   -- Reportes    
      LET sm0c = createStartMenuGroup(sm0,"Reportes","mars_reportes")

      LET cmdstr = "fglrun rtomafisico.42r segovia"
      LET opt    = createStartMenuCommand(sm0c,"Toma de Inventario Fisico",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun rinvfisico.42r segovia"
      LET opt    = createStartMenuCommand(sm0c,"Comparativo de Inventario Fisico",cmdstr,"mars_p1") 
  
      LET cmdstr = "fglrun rexisgeneral.42r segovia"
      LET opt    = createStartMenuCommand(sm0c,"Inventario General",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun rexistencias.42r segovia"
      LET opt    = createStartMenuCommand(sm0c,"Existencias Inventario",cmdstr,"mars_p1")

      LET cmdstr = "fglrun rvalorizado.42r segovia"
      LET opt    = createStartMenuCommand(sm0c,"Inventario Valorizado",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun rvalcorte.42r segovia"
      LET opt    = createStartMenuCommand(sm0c,"Inventario Valorizado a Fecha de Corte (Excel)",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun rexistenciasconf.42r segovia"
      LET opt    = createStartMenuCommand(sm0c,"Existencias Confeccion",cmdstr,"mars_p1")

      LET cmdstr = "fglrun rcomparativo.42r segovia"
      LET opt    = createStartMenuCommand(sm0c,"Comparativo de Inventario",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun rkardex.42r segovia"
      LET opt    = createStartMenuCommand(sm0c,"Kardex de Producto",cmdstr,"mars_p1") 

 -- Ventas 
 LET sm1 = createStartMenuGroup(smp,"Ventas","")

   -- Catalogos
      LET sm1a = createStartMenuGroup(sm1,"Catalogos","mars_catalogos")

      LET cmdstr = "fglrun puntoventa.42r segovia"
      LET opt    = createStartMenuCommand(sm1a,"Puntos de Venta",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun tipodocs.42r segovia"
      LET opt    = createStartMenuCommand(sm1a,"Tipos de Documento",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun tdocsxpos.42r segovia"
      LET opt    = createStartMenuCommand(sm1a,"Tipos de Documento x Punto de Venta",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun resoluciones.42r segovia"
      LET opt    = createStartMenuCommand(sm1a,"Resoluciones",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun emitacre.42r segovia"
      LET opt    = createStartMenuCommand(sm1a,"Emisores Tarjeta de Credito",cmdstr,"mars_p1") 

   -- Procesos    
      LET sm1b = createStartMenuGroup(sm1,"Procesos","mars_procesos")

      LET cmdstr = "fglrun facturacion.42r segovia"
      LET opt    = createStartMenuCommand(sm1b,"Facturacion de Productos",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun cortecaja.42r segovia"
      LET opt    = createStartMenuCommand(sm1b,"Corte de Caja",cmdstr,"mars_p1") 

   -- Reportes    
      LET sm1c = createStartMenuGroup(sm1,"Reportes","mars_reportes")

      LET cmdstr = "fglrun rcorteventas.42r segovia"
      LET opt    = createStartMenuCommand(sm1c,"Corte de Ventas",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun rordenesint.42r segovia"
      LET opt    = createStartMenuCommand(sm1c,"Ordenes de Trabajo Internas",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun rordenesext.42r segovia"
      LET opt    = createStartMenuCommand(sm1c,"Ordenes de Trabajo Externas",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun rfacturas.42r segovia"
      LET opt    = createStartMenuCommand(sm1c,"Reporte de Facturacion",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun rvtapordoc.42r segovia"
      LET opt    = createStartMenuCommand(sm1c,"Reporte de Ventas por Cliente",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun rvtaporpro.42r segovia"
      LET opt    = createStartMenuCommand(sm1c,"Reporte de Ventas por Producto",cmdstr,"mars_p1") 

      LET cmdstr = "fglrun facrep100.42r segovia"
      LET opt    = createStartMenuCommand(sm1c,"Reporte de Ventas por Vendedor",cmdstr,"mars_p1") 
      
 -- Salida 
 MENU ""
  COMMAND "Salir" "Salir del sistema." 
   EXIT PROGRAM
  COMMAND "Perfiles" "Catalogo de perfiles de usuarios."
   LET cmdstr = "fglrun perfiles.42r"
   RUN cmdstr
  COMMAND "Usuarios" "Catalogo de usuarios del sistema." 
   LET cmdstr = "fglrun usuarios.42r"
   RUN cmdstr 
  COMMAND "Parametros" "Mantenimiento de parametros del sistema." 
   LET cmdstr = "fglrun parametros.42r"
   RUN cmdstr 
  COMMAND "Ayuda"
  COMMAND KEY(INTERRUPT)
   EXIT MENU
 END MENU 

 -- Cerrando forma 
 CLOSE FORM wmenu
END MAIN

FUNCTION createStartMenuGroup(p,t,i)
 DEFINE p om.DomNode
 DEFINE t,i STRING
 DEFINE s om.DomNode
 
 LET s = p.createChild("StartMenuGroup")
 CALL s.setAttribute("text",t) 
 CALL s.setAttribute("image",i) 
 RETURN s
END FUNCTION 

FUNCTION createStartMenuCommand(p,t,c,i)
 DEFINE p om.DomNode
 DEFINE t,c,i STRING
 DEFINE s om.DomNode
 
 LET s = p.createChild("StartMenuCommand")
 CALL s.setAttribute("text",t) 
 CALL s.setAttribute("exec",c) 
 CALL s.setAttribute("image",i) 
 RETURN s
END FUNCTION 
