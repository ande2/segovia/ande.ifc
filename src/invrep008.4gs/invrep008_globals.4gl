DATABASE segovia
GLOBALS
DEFINE w_mae_emp    RECORD LIKE glb_empresas.*,
       w_mae_suc    RECORD LIKE glb_sucsxemp.*,
       w_mae_bod    RECORD LIKE inv_mbodegas.*,
       w_datos      RECORD 
        codbod      LIKE inv_mtransac.codbod,
        fecinv      DATE
       END RECORD, 
       pInventario DYNAMIC ARRAY OF RECORD
        cditem      LIKE inv_products.cditem,  
        codabr      LIKE inv_products.codabr,  
        dsitem      LIKE inv_products.dsitem,
        invfis      DEC(16,2),
        entuni      DEC(16,2),
        saluni      DEC(16,2),
        slduni      DEC(16,2),
        salsis      DEC(16,2),
        saldif      DEC(16,2)
       END RECORD,
       existe       SMALLINT,
       cntprod      INTEGER,
		 lfechafisico	DATE
END GLOBALS